# INTRODUCTION


## Laplacien de graphe

- Principe
- Propriétés
- Valeurs propres, vecteurs propres

## Sequences de maillage
- Cas d'utilisation
- Exemples
- Format off

## Laplacien 3D+t

- Principe

# DECOMPOSITION SPECTRALE

## Comparaison des methodes existantes
- Presentation des methodes (numpy, scipy, lapack)

- Calcul de n eigenpaires

- Calcul des premieres eigenpaires


## BDC, BTF
- Principe
- Fortran <-> python (f2py)
- Problemes
  - BTF bon pour bandwidth petit par rapport a n, mais dans notre cas b ~= n / nframes et nframes pas super grand, donc btf pas forcement intéressant
  - BDC / BTF : utilisent pas le fait que les blocs sont creux
  - BDC : pas scalable du tout (workspace = O(n^2) en memoire)
  - BDC : peut pas specifier le nombre d'eigvecs a calculer
      - par design : le principe du divide and conquer fait que les resultats sont obtenus a la fin, 
- Resultats

## Spectral coarsening
- Principe
- Resultats

## Visualisation
- valeurs propres
- maillages colorés par les vecteurs propres
- eigengap
- variation de alpha


## Script de batch
- Motivation
- Structure

## Acceleration du calcul de laplacien
- identification du bottleneck avec vprof
- acceleration par precompilation avec numba


# SEGMENTATION

## Principe 
(cf tutorial on spectral clustering)

- pourquoi ca marche:
  - relation avec algos de cut
    - laplacien normalisé vs laplacien 
    - pas besoin de faire la distinction si lambda < min deg
    - toutes les sequences respectent ca sauf celles avec peu de verts ou les simplifiées
  - relation vecteurs propres / clusters
- visualisation vecteurs indicateurs vs vecteurs "normaux"
- comment ca marche:
  - clustering sur dimension reduite
  - reduction de dimension / espace des vecteurs propres (+visualisation)

## Visualisations
- LucyViewer
- variation de alpha
- maillage statique vs maillage dynamique
- clustering de different types de mouvement

## Problemes
- bcp de frames : bcp de vecteurs indicateurs pour alpha grand
- bcp de frames : laplacien trop gros -> simplification du maillage -> sequence non coherente


## Agglomerative clustering
- Non connexité
  - Influence du nombre de vecteurs propres
  - Visualisation en dimension reduite de kmeans
- Choix de la methode de clustering
  - Comparaison des methodes de sklearn
  - Resultats

# CONCLUSION