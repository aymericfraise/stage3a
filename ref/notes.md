**Questions**

Resultats bizarres sur march1/statique + histoires de signes suivant alpha (quand alpha petit)
Squirrel/23 cluster4 a0.01 // squat1 plusieurs clusters : clusters non connexes
MemoryError sur march1/full1of4 (57 frames, 20K faces par frame)

statique10 a10 : premiers vecs = indicateurs, ensuite 100% temporel
comportement p/r alpha :
    statique10 clusters alpha petit (1e-8) : ressemblent a du clustering statique
    statique10 clusters alpha grand (1e1) : ultra bizarre, pas forcement connexes ? (cf cluster bleu sur cluster10)
clustering naif ? horsegallop20 cluster2 : juste une pate en cluster 
DOUBLES CLUSTERS
    essayer a10 sur d'autres maillages que statique10, voir si le comportement de double cluster apparait aussi
    [[[[quadruple cluster sur statique2 c8p2]]]]
    normalement si y'a plus de clusters demandés que de clusters trouvés, kmeans met un warning
    TODO: pourquoi double clusters et pas erreur ?
        -> petite difference qui permet de segmenter qd meme en fait

je cluster en utilisant l'eigvec constant, p-e que le retirer ca resolverais la non connexité (puisque tous les vertex sont (presque) au meme point de l'axe du vec0)
    change rien
problemes de connexités viennent de l'espace des vecs propres ?
[[[[[qu'est ce qui garantit la connexité des clusters ?]]]]]


l'effet de alpha depend de l'échelle du mesh de base
faut calculer enormement de valeurs propres si on a bcp de frames et un gros alpha (pour aller au dela des vecteurs indicateurs)

clustering statique:
    segmentation des faces ou des verts ? 
    n_clusters = n_eigenvecs ?
    normalisation de la mat des vecs -> sur bunny apres ca il separe plus les oreilles, et il fait des clusters non connexes avec des points isolés (c12)
    clustering pas directement sur la mat des vecs ?
    N != L (c'est pas D-W)

simplification des maillages : on garde surement pas la coherence temporelle

dbscan : construit 1 seul cluster (vu que tt est connexe)
affinityprop, spectral : trop lent, fini pas
agglomerative : pareil que kmeans ?
birch : avec bon threshold, peut p-e resoudre probleme de non connexité
meanshift : n_clusters auto, mais a l'air tres lent

d'apres tutorial on spectral clustering, pas besoin de normaliser si lambda < min deg
    toutes les sequences respectent ca sauf celles avec peu de verts ou les simplifiées

BTF bon pour bandwidth petit par rapport a n, mais dans notre cas b ~= n / nframes et nframes pas super grand, donc btf pas forcement intéressant
BDC / BTF : utilisent pas le fait que les blocs sont creux !!!
BDC pas scalable du tout !!! workspace = O(n^2) en memoire
BDC : peut pas specifier le nombre d'eigvecs a calculer
    par design : le principe du divide and conquer fait que les resultats sont obtenus a la fin, 
BDC : iwork pas assez grand, crash
    la doc dit que len(iwork) >= 5*N + 4*NBLKS - 4, mais ca marche que a partir de len(iwork) = 5*N + 4*NBLKS - 1
    crash ligne 401: IWORK( ILSUM  + (NBLKS-1) - MERGED ) = LSUM 

Gansterer :
    umlaut dans finaldsrtdf.f ligne 453, dans btev_final lignes 243 et 340
    faut retirer les ALLOCATABLE pour pouvoir generer les shared libraries avec les clients
    btev_final : compile pas si integer f est défini (l. 11), faut le retirer
    Plasma install
        python2 ./setup.py --notesting --cc=gcc --fc=gfortran --blaslib="-lblas" --cblaslib="-lcblas" --lapacklib="-llapack" --lapclib="-llapacke" --fflags="-fPIC"

Spectral coarsening
    Conserve les eigenvecs de basse frequence (ie les plus gros ?)
        On veut les plus petits, p-e un probleme

A faire :
    Clustering :
        Agglomerative clustering avec connectivity matrix
        si agglomerative clust fonctionne pas:
            Mean shift et autres
            Algo djikstra Frank (voir avec Katia)
    Eigen:
        Etude de l'ecartement des valeurs propres des sequences
        P/DSEBVD
        voir si btf peut etre adaptés aux matrices creuses
        spectral coarsening
    Rapport:
        Plan detaillé

**Biblio**

[A 3D+t Laplace operator for temporal mesh sequences](http://www.sciencedirect.com/science/article/pii/S0097849316300619) *(hal-01318763)*

[Differential Representations for Mesh Processing](http://www.cis.upenn.edu/~cis610/Sorkine-cgf2006.pdf)

[OFF Meshes](https://seafile.unistra.fr/d/b4defa8139c0416db10d/)

[Segmentation of tempor al mesh sequences into rigidly
moving components](https://hal.inria.fr/hal-00749302/document)

#

**Misc**

* **positive definite** 
  * $z^TMz > 0$ for every non zero vector $z$
  * means the dot product of the input with the output is positive (in:$z$, out:$Mz$)   
* **partial vs complete pivoting**
  * partial pivoting : in the column currently considered, find the element with the largest absolute value and choose its row as the pivot
  * complete pivoting : consider all rows and columns to find the pivot
* [**Schurr's complement**](https://www.quora.com/Linear-Algebra-What-is-the-motivation-behind-defining-the-Schur-complement-of-a-matrix)
  * = gaussian elimination for 2x2 block matrices
* [**Subnormal numbers**](https://stackoverflow.com/a/53203428/11378690)
* [Guide LAPACK](http://wwwens.aero.jussieu.fr/lefrere/master/mni/mncs/guide-lapack.pdf)
* Hessenberg form = almost triangular : either zero below the subdiagonal, or zero above the superdiagonal

#

* **LU decomposition**
  * $M = LU$
  * $L$ lower triangular, $U$ upper triangular
  * LUP factorization : $M = PLU$, with $P$ a permutation matrix
  * permutations to avoid dividing by zero and for numerical stability (reduce precision errors by avoiding the use of small numbers)
  * Forward factorization : $M = PLU$, solved starting from the top left
  * Backward factorization : $M = PUL$, solved starting from the bottom right

* [**Cholesky decomposition**](http://mathforcollege.com/nm/mws/gen/04sle/mws_gen_sle_txt_cholesky.pdf)
  * $M = LL^*$
  * $M$ Hermitian definite positive matrix, $L$ lower triangular, $L^*$ conjugate transpose of $L$
  * 2x faster than LU
  * algo to solve $Ax = B$ ($A$ real valued):
    * Factorisation : find $U$ such that $A = U^TU$
    * Forward phase : find $y$ such that $U^Ty = B$
    * Backward phase : find $x$ such that $Ux = y$
    * Done (we got $x$)
      * Factorisation takes 95% of the solve time
  
* [**LDL\* decomposition**](http://mathforcollege.com/nm/mws/gen/04sle/mws_gen_sle_txt_cholesky.pdf) *(= Cholesky for non Hermitian definite positive matrices)*
  * $M = LDL^*$
  * $L$ lower unit triangular, $D$ diagonal
  * more general than cholesky

# 

**Mesh Laplacians**  
https://www.researchgate.net/publication/220507645_Spectral_Mesh_Processing (Section 6)

* **Types**
  * combinatorial mesh Laplacians (graph theory)
    * eigenvectors are influenced by the mesh connectivity
    * eigenvectors usually correspond to the geometric reality because the mesh connectivity usually encodes geometric information
    * works on general meshes  
  * geometric mesh Laplacians (discretizations the of Laplace Beltrami operator)
    * robust to changes to the mesh connectivity
    * works on manifold triangle mesh

* **Combinatorial mesh Laplacians:**  
  * Graph laplacian (or Kirchoff operator)
    * $K = D - W$ 
    * $W$ = adjacency matrix, $D$ = number of neighbours of each vertex (degree of the vertex)
  * Tutte Laplacian
    * $T = D^{-1}K$
    * $T = 1$ if $i=j$, $1/degree(i)$ if there is an edge between $i$ and $j$, $0$ otherwise
    * better reconstruction of spectrally truncated meshes than the graph laplacian
  * Normalized graph Laplacian

#

**Eigensolvers** (cf wikipedia )

* cf 
  * [Wikipedia/ Eigenvalue Algorithms](https://en.wikipedia.org/wiki/Eigenvalue_algorithm#Algorithms)
  * [Wikipedia/ Eigenvalue Algorithms (alt)](https://en.wikipedia.org/wiki/List_of_numerical_analysis_topics#Eigenvalue_algorithms)
  * [NETLIB/ Templates for the Solution of Algebraic Eigenvalue Problems](http://www.netlib.org/utk/people/JackDongarra/etemplates/book.html)
  * [Dense and sparse parallel linear algebra algorithms on graphics processing units](https://riunet.upv.es/bitstream/handle/10251/112425/Lamas%20-%20Dense%20and%20sparse%20parallel%20linear%20algebra%20algorithms%20on%20graphics%20processing%20units.pdf?sequence=1&isAllowed=y) 
* **Power iteration**
  * *[in]* $b_0$ : random vector or guess at the dominant eigenvector
  * *[out]* the eigenvector with eigenvalue of largest magnitude (the eigenvalue can be retrieved using the Rayleigh quotient)
  * *[alg]*
    * at each iteration, apply $A$ to $b_k$ and normalize the result (to prevent overflow)
    * $b_{k+1} = \frac{Ab_k}{\|Ab_k\|}$ 

* [**Inverse power iteration**](https://pdfs.semanticscholar.org/6403/e34904dbc1a4c4e30a2804086d54d8c312f4.pdf) *(or shifted inverse interation)*
  * Power iteration using $(A-\mu I)^{-1}$, with $\mu$ an approximation of the eigenvalue of the wanted eigenvector
  * $(A-\mu I)^{-1}$ has $((\lambda_i-\mu)^{-1})_i$ as its eigenvalues (the $\lambda_i$ are the eigenvalues of $A$), so the algorithm converges with the eigenvector whose eigenvalue is closest to $\mu$

* **Arnoldi iteration**
  * Power iteration but instead of using only the last result to get the dominant eigenvector, keep all the intermediate results $(b,Ab,A^2b,...,A^nb)$ and extract an orthogonal basis $(u_0,...,u_n)$ out of them (for example using Gram Schmidt orthogonalization)
  * $(u_0,...,u_n)$ approximates the eigenvectors of A

* QR Algorithm / Francis step / bulge chasing ([[1]](https://en.wikipedia.org/wiki/QR_algorithm),[[2]](http://people.inf.ethz.ch/arbenz/ewp/Lnotes/chapter4.pdf), [[3]](https://pdfs.semanticscholar.org/c1f6/de833e36dd4c447620f6d2538f4dc1977549.pdf))

* [**MRRR algorithm**](http://www.cs.utexas.edu/~inderjit/public_papers/DesignMRRR_toms06.pdf)
  * symmetric tridiagonal matrices
  * computes orthogonal eigenvectors without Gram-Schmidt

* **LAPACK symmetric band matrix routines** ([[1]](https://gams.nist.gov/cgi-bin/serve.cgi/PackageModules/LAPACK),[Comparison of eigensolvers for symmetric band matrices[2]](https://www.sciencedirect.com/science/article/pii/S0167642314000112#br0090))
  * [**DSBEV**](http://www.netlib.org/lapack/explore-html/dc/dd2/group__double_o_t_h_e_reigen_gad6db4d2faf998bf423820341e477e98f.html#gad6db4d2faf998bf423820341e477e98f)
    * Tridiagonalizes the given real symmetric band matrix, then applies the Pal-Walker-Kahan variant of the QL or QR algorithm to get all the eigenvalues and optionnaly the implicit QL or QR method to get all the eigenvectors. Transforms back the eigenvalues and eigenvectors. 
    * DSBTRD, DSTERF [+SSTEQR]
  * [**DSBEVD**](http://www.netlib.org/lapack/explore-html/dc/dd2/group__double_o_t_h_e_reigen_gaf8e2a8f5ea7f515fa0b5bd5ae8427322.html#gaf8e2a8f5ea7f515fa0b5bd5ae8427322)
    * Same as DSBEV but using divide and conquer for the eigenvectors. 
    * DSBTRD, DSTERF [+SSTEDC]
  * [**DSBEVX**](http://www.netlib.org/lapack/explore-html/dc/dd2/group__double_o_t_h_e_reigen_ga4b0b81bd79a12be53d13a172c8950995.html#ga4b0b81bd79a12be53d13a172c8950995)
    * Computes selected eigenvalues and eigenvectors of a real symmetric band matrix. (Expert driver.).
  * **DSBEVR**
    * Method from the "Comparison of eigensolvers for symmetric band matrices" article. 
    * DSBTRD (symmetric band to symmetric tridiagonal) + DSTEVR (eigenpairs of symmetric tridiagonal by MRRR)
  * [Input format for banded matrices](https://www.netlib.org/lapack/lug/node124.html)

* [**Computing eigenvectors of block tridiagonal matrices based on twisted block factorizations**](https://www.sciencedirect.com/science/article/pii/S0377042711003967)  
  * [**Comparison of eigensolvers for symmetric band matrices**](https://www.sciencedirect.com/science/article/pii/S0167642314000112)
    * BD&C *better than* TBF (except for some matrix types) *better than* LAPACK
  * **Motivation** : eigensolvers for block tridiagonal matrices usually work by first making the matrix tridiagonal then applying eigensolvers for tridiagonal matrices. But the tridiagonalization is the most costly step, so the goal is to find a way to find eigenpairs without having to tridiagonalize the matrix before hand.
    * Block tridiagonal divide and conquer exists for this, but is not necessarily sufficient
  * **3 steps**:
    *  LUP decomposition as described in [On twisted factorizations of block tridiagonal matrices](https://www.sciencedirect.com/science/article/pii/S1877050910000323)
     * Identification of a good starting vector
     * Efficient inverse iteration  
  * **Related** 
    * Twisted factorization
      * Tridiagonal matrices
        * [Twisted factorization of tridiagonal matrices](https://www.researchgate.net/publication/228728979_TWISTED_FACTORIZATION_OF_TRIDIAGONAL_MATRICES)  
        * A = PQ
          * P: 1 on diagonal, non zero on the lower diagonal up to row $m+1$, and on the upper diagonal from row $m+1$, factors 
          * Q: non zero on the diagonal, on the upper diagonal up to row $m+1$, and on the lower diagonal from row $m+1$
          * It's the result of combining some forward factorizaiton steps and some backwards fatorization steps 
      * Block tridiagonal matrices 
        * [On twisted factorizations of block tridiagonal matrices](https://www.sciencedirect.com/science/article/pii/S1877050910000323)
        * $W = PLU$, with $L$ lower bidiagonal until column $k$ then upper bidiagonal and $U$ upper bidiagonal until column $k$ then lower bidiagonal, and with $k$ the number describing the twisted block factorization
    * Divide and Conquer
      * [Block tridiagonal Divide and Conquer (BD&C)](https://www.researchgate.net/publication/220493316_An_extension_of_the_divide-and-conquer_method_for_a_class_of_symmetric_block-tridiagonal_eigenproblems)
      * [Tridiagonal Divide and Conquer (Cuppen 1980)](https://link.springer.com/article/10.1007/BF01396757)
      * [A generalization of the divide and conquer algorithm for the symmetric tridiagonal eigenproblem (2015)](https://arxiv.org/pdf/1506.08517.pdf)
    * [Fast Multipole Method (FMM)](https://github.com/davidson16807/fast-multipole-method) *(n-body problem)*

* [**Tridiagonalisation of symmetric matrices**](https://astro.temple.edu/~dhill001/course/NUMANAL_SP_2017/Lectures/Section%204_4%20Reduction%20to%20Symmetric%20Tridiag%20Form%20Spring%202017%20.pdf)
  * Apply successive similarity transformations (using orthogonal matrices for conditionning and ease of inverse)
  Each method differs in the kind of orthogonal matrix used
    * $A' = Q_n^T*...*Q_1^T*A*Q_1*...*Q_n$
  * **Household matrix**
    * $Q_i = I - 2w_iw_i^T$
    * $Q_i$ = symmetry w/r to plane defined by its normal $w_i$
    * $w_i$ determined to place the zeros on the (  n-i)th row / column

* **Spectral clustering**
  * **Graph cut**
    * Minimizing mincut
      * Ratiocut : weighted by number of elts in subsets
      * Ncut : weighted by volume of subsets (sum of weights of edges)

  * Polarization theorem:
    * As you project $N=D^{−1/2}WD^{−1/2}$ into successively lower rank, the cosine between the vectors of the points of its embedding in the lower rank space either go closer or further to each other towards -1 or +1.
    * High affinity points will go closer to each other and low affinity points will go further, which means clustering is going to be easier in lower dimensional space