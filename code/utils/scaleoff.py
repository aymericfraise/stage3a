import utils
from pathlib import Path

load = '../meshes/dynamic-simple/cylinder-bending'
save = '../meshes/dynamic-simple/cylinder-bending-scaled'
Path(save).mkdir(exist_ok=True)
for path in Path(load).glob('*.off'):
    print(path)
    vert_count, face_count, vertices, faces = utils.load_off_mesh(path)
    for i,v in enumerate(vertices):
        vertices[i] = [vj/5 for vj in v]
    utils.save_off_mesh(Path(save)/path.name, vert_count, face_count, vertices, faces)
