import subprocess
from pathlib import Path
import argparse
import sys
import os

def simplify(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('indir', type=Path, help='path from which to load meshes')
    parser.add_argument('outdir', type=Path, help='path into which to save meshes')
    parser.add_argument('ratio', type=float, help='reduce the number of vertices to ratio times the original number of vertices')
    parser.add_argument('-v','--verbose', action='store_true', help='Show subprocesses\' terminal output')
    args = parser.parse_args(args)

    args.outdir.mkdir(exist_ok=True, parents=True)
    
    files = [x for x in args.indir.iterdir() if x.is_file() and len(x.suffix) > 0]
    for i,f in enumerate(files,1):
        print('{} / {}'.format(i,len(files)))
        stdout = None if args.verbose else subprocess.DEVNULL
        stderr = stdout
        # convert to obj
        subprocess.call(['meshlabserver','-i',str(f),'-o','./tmp.obj'], stdout=stdout, stderr=stderr)
        # simplify obj mesh
        subprocess.call(['./simplify','./tmp.obj','./tmpreduce.obj',str(args.ratio)], stdout=stdout, stderr=stderr)
        # convert back
        outfile = args.outdir / f.name
        subprocess.call(['meshlabserver','-i','./tmpreduce.obj','-o',str(outfile)], stdout=stdout, stderr=stderr)
    os.remove('./tmp.obj')
    os.remove('./tmpreduce.obj')

if __name__=='__main__':
    simplify(sys.argv[1:])

