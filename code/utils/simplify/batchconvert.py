import subprocess
from pathlib import Path
import argparse
import sys
import os

def batchconvert(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('indir', type=Path, help='path from which to load meshes')
    parser.add_argument('outdir', type=Path, help='path into which to save meshes')
    parser.add_argument('format', type=str)
    parser.add_argument('-v','--verbose', action='store_true', help='Show subprocess\' terminal output')
    args = parser.parse_args(args)

    args.outdir.mkdir(exist_ok=True, parents=True)
    
    files = [x for x in args.indir.iterdir() if x.is_file() and len(x.suffix) > 0]
    for i,f in enumerate(files,1):
        print('{} / {}'.format(i,len(files)))
        stdout = None if args.verbose else subprocess.DEVNULL
        stderr = stdout
        # convert
        outfile = (args.outdir / f.name).with_suffix(args.format)
        subprocess.call(['meshlabserver','-i',str(f),'-o',str(outfile)], stdout=stdout, stderr=stderr)

if __name__=='__main__':
    batchconvert(sys.argv[1:])

