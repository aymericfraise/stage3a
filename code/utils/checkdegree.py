from pathlib import Path
import utils

def mean(data):
    n = 0
    mean = 0.0
    for x in data:
        n += 1
        mean += (x - mean)/n
    if n < 1:
        return float('nan')
    else:
        return mean

for Lpath in Path('../meshes').glob('**/L'):
    Epath = Lpath.parent / 'eig'
    if not Epath.exists():
        continue
    L = utils.load_laplacian(Lpath)
    vals, vecs = utils.load_object_from_file(Epath)
    min_deg = min(L.diagonal())
    # print(min_deg, all([v < min_deg for v in vals]))
    print(mean(min_deg-v for v in vals), Lpath)