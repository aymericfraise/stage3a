import queue
import argparse
from pathlib import Path
from multiprocessing import Pool
import logging
import itertools

import utils
import numpy as np
import scipy.sparse
import tqdm

class Checker:
    def __init__(self, laplacian_path, clusters_path):
        # Ls: list of sparse matrices, all square of the same dimension
        # Ds: list of 1D np arrays
        self.laplacian_path = laplacian_path
        self.clusters_path = clusters_path
        Ls, self.Ds = utils.load_object_from_file(laplacian_path)
        self.Ls = [L.tocsr() for L in Ls] 
        # labels: list of ints
        self.labels = utils.load_object_from_file(clusters_path)
        self.unique_labels = set(self.labels)
        self.f = len(Ls)  # frame count
        self.b = Ls[0].shape[0]  # number of vertices per mesh (ie blocksize)
        self.n = self.b * self.f  # number of vertices in the whole mesh sequence
        self.connected_clusters = None

    def clusters_are_connected(self):
        if self.connected_clusters is None:
            self.connected_clusters = self.get_connected_labels()
        return len(self.connected_clusters) == len(self.unique_labels)

    def get_connected_labels(self):
        # cluster_sizes = [self.cluster_size(l) for l in sorted(list(set(self.labels)))] # from smallest to biggest cluster
        # ordered_labels = np.argsort(cluster_sizes)
        connected = []
        for l in set(self.labels):
            if self.cluster_is_connected(l):
                connected.append(l)
        return sorted(connected)

    def cluster_is_connected(self, l):
        full_cluster_size = self.cluster_size(l)
        connected = self.get_connected_cluster(l)
        return len(connected) == full_cluster_size

    def get_connected_cluster(self, l, i0=None):
        if i0 is None:
            i0 = list(self.labels).index(l) 
        visited = np.zeros(self.n, dtype=bool)
        visited[i0] = True
        Q = queue.Queue()
        Q.put(i0)
        while not Q.empty():
            idx = Q.get()
            for n in self.get_neighbours(idx):
                if visited[n] or self.labels[n] != l:
                    continue
                visited[n] = True
                Q.put(n)
        cluster = np.flatnonzero(visited)
        return cluster

    def get_neighbours(self, idx):
        mesh_neighbours = self.get_mesh_neighbours(idx)

        # add the neighbouring vertex from previous and / or last frame
        frame_neighbours = []
        next_frame_idx = idx+self.b
        if next_frame_idx < self.n:
            frame_neighbours.append(next_frame_idx)
        previous_frame_idx = idx-self.b
        if previous_frame_idx >= 0:
            frame_neighbours.append(previous_frame_idx)

        if frame_neighbours:
            return np.append(mesh_neighbours, frame_neighbours)
        else:
            return mesh_neighbours

    def get_mesh_neighbours(self, idx):
        f_idx = idx // self.b
        L = self.Ls[f_idx] # current frame's mesh laplacian
        n_verts_before_frame = self.b*f_idx
        relative_idx = idx - n_verts_before_frame # idx of the vertex relative to the frame it's in
        start, stop = L.indptr[relative_idx],L.indptr[relative_idx+1]
        neighbours_relative_idx = L.indices[start:stop]
        neighbours_idx = neighbours_relative_idx + n_verts_before_frame
        return neighbours_idx

    def cluster_size(self, l):
        return np.count_nonzero(self.labels == l)


def check_laplacian(paths):
    laplacian_path, cluster_path = paths
    # print('checking {}, cluster {}'.format(laplacian_path, cluster_path))
    checker = Checker(laplacian_path, cluster_path)
    connected_labels = checker.get_connected_labels()
    res = (str(laplacian_path.parent), cluster_path.name, checker.clusters_are_connected(), len(checker.unique_labels)-len(connected_labels))
    # print(checker.clusters_are_connected())
    logging.log(logging.INFO, res)
    return res

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--check-all', action='store_true')
    parser.add_argument('--log-file', type=Path)
    parser.add_argument('--laplacian-folder', type=Path, help='Check connectedness of specific laplacian')
    parser.add_argument('--cluster-path', type=Path, help='Check connectedness of specific clustering')
    args = parser.parse_args()

    if args.log_file:
        logging.basicConfig(filename=str(args.log_file), level=logging.INFO, format="%(message)s")
    else:
        logging.disable = True

    if args.cluster_path:
        laplacian_path = args.cluster_path.parent.parent / 'L'
        checker = Checker(laplacian_path, args.cluster_path)
        print('frame', checker.b)
        for l in checker.unique_labels:
            cl = checker.get_connected_cluster(l)
            clfull = np.flatnonzero(checker.labels == l)
            diff = [x for x in clfull if x not in cl]
            print('diff ({}): {} (full={},cur={}), currange = {}-{}'.format(l,len(diff),len(clfull),len(cl), min(cl), max(cl)))
            if len(diff) == 0:
                continue
            for i in diff[::len(diff)//10]:
                ci = checker.get_connected_cluster(l, i0=i)
                mesh_neigh = checker.get_mesh_neighbours(i)
                frame_neigh = [x for x in checker.get_neighbours(i) if x not in mesh_neigh]
                print('ci',i,len(ci),mesh_neigh,frame_neigh)
                if len(ci) < 20:
                    print(ci)

    if args.laplacian_folder:
        laplacian_path = args.laplacian_folder / 'L' 
        cluster_folder = args.laplacian_folder /  'clusters'
        if cluster_folder.exists():
            tasks = ((laplacian_path, cluster_path) for cluster_path in cluster_folder.iterdir())
            p = Pool(4)
            res = p.map(check_laplacian, tasks)
        else:
            print('no clusters have been computed ({})'.format(args.laplacian_folder))
        
    if args.check_all:
        folders = (L.parent for L in Path('../meshes').glob('**/L'))
        tasks_list = []
        for folder in folders:
            laplacian_path = folder / 'L'
            cluster_folder = folder / 'clusters'
            if not cluster_folder.exists():
                continue
            folder_tasks = [(laplacian_path, cluster_path) for cluster_path in cluster_folder.iterdir() if cluster_path.is_file()]
            tasks_list.extend(folder_tasks)
        p = Pool(4)
        list(tqdm.tqdm(p.imap(check_laplacian, tasks_list), total = len(tasks_list)))