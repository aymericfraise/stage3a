import re
def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

lines = []
with open('../connectedlog.txt', 'r') as f:
    lines = f.readlines()
with open('../connectedlogsorted.txt', 'w+') as f:
    sorted_lines = natural_sort(lines)
    f.writelines(sorted_lines)