import matplotlib
import matplotlib.cm as cm
from numba import jit
import numpy as np
from pathlib import Path
import pickle
import re
from scipy import sparse
import shutil

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def stack_colormaps(colormaps):
    # sample about 256 colors in total from the given color maps
    colors = []
    samples_per_cmap = int(256/len(colormaps))
    for name in colormaps:
        cmap = matplotlib.cm.get_cmap(name)
        colors.append(cmap(np.linspace(0,1,samples_per_cmap)))
    # combine them and build a new colormap
    stacked_colors = np.vstack(colors)
    stacked_map = matplotlib.colors.LinearSegmentedColormap.from_list('custom_cmap', stacked_colors)
    return stacked_map

def vector_to_rgba(vector, colormaps=['viridis']):
    """ Converts a numpy 1D array of numbers into an rgba array, by mapping the numbers to the given colormap """
    norm = matplotlib.colors.Normalize(vector.min(),vector.max())
    cmap = stack_colormaps(colormaps)
    scalmap = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
    rgba = scalmap.to_rgba(vector)
    return rgba

def sparse_from_LD(Ls,Ds):
    f = len(Ls)
    if len(Ds) != f-1:
        raise ValueError()
    
    blocks = [[None for _ in range(f)] for _ in range(f)]
    for i in range(f):
        blocks[i][i] = Ls[i]
        if i < f-1:
            D = sparse.diags(Ds[i])
            blocks[i][i+1] = D
            blocks[i+1][i] = D
    L = sparse.bmat(blocks)
    return L

def load_laplacian(filepath):
    filepath = Path(filepath)
    Ls, Ds = load_object_from_file(filepath)
    L = sparse_from_LD(Ls,Ds).tocsc()
    return L

def load_object_from_file(filepath):
    filepath = Path(filepath)
    with filepath.open(mode='rb') as f:
        return pickle.load(f)

def save_object_to_file(obj, filepath):
    filepath = Path(filepath)
    with filepath.open(mode='wb+') as f:
        pickle.dump(obj,f)

def load_off_mesh(filepath):
    with Path(filepath).open(mode='r') as inF:
        header = inF.readline() # 'OFF' line
        if header.rstrip() == 'OFF':
            header = inF.readline()
        vert_count, face_count = [int(x) for x in header.split()[:2]]
        vertices = (inF.readline() for _ in range(vert_count))
        vertices = [[float(x) for x in v.rstrip().split()] for v in vertices]
        faces = [inF.readline() for _ in range(face_count)]
        for i,face in enumerate(faces):
            f = face.rstrip().split()
            f0 = int(f[0]) # number of vertices in the face
            # convert the indices of the face vertices to int, and the color channels to float
            faces[i] = [int(x) for x in f[:f0+1]] + [float(x) for x in f[f0+1:f0+4]]
    return vert_count, face_count, vertices, faces

def save_off_mesh(filepath, vert_count, face_count, vertices, faces):
    with Path(filepath).open('w') as f:
        f.write('OFF\n{} {} 0\n'.format(vert_count, face_count))
        lines = [' '.join([str(x) for x in elt]+['\n']) for elt in vertices+faces]
        f.writelines(lines)

def make_empy_dir(path):
    path = Path(path)
    if path.exists() and path.is_dir():
        shutil.rmtree(str(path.absolute()))
    path.mkdir(exist_ok=True)

@jit(nopython=True)
def face_rgba_mean(verts_rgba):
    """ computes the mean rgba color of 3 rgba colors
    expects a 3x4 immutable array with each row being an rgba value """
    return [(verts_rgba[0][i]+verts_rgba[1][i]+verts_rgba[2][i])/3 for i in range(4)]