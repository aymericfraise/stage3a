import main

from pathlib import Path
from tqdm import tqdm
from multiprocessing import Pool
import time
import argparse


def process_sequence(seq_path, alphas, cmds, global_cmds=None, quiet=True):
    # set the temporal window to all of the frames in the directory
    w0,w1 = 0,len(list(seq_path.glob('*.off')))-1
    if w1 == -1:
        raise ValueError('path {} doesn\'t contain any off files'.format(seq_path))
    for a in alphas:
        for cmd in cmds:
            print(seq_path, a, cmd, time.ctime())
            
            args = [Path('../meshes') / Path(seq_path), '-w', w0, w1, '-a', a] + cmd.split()
            if global_cmds:
                args.extend(global_cmds.split())
            if quiet:
                args.append('--quiet')
            main.main([str(x) for x in args])

def batch():
    parser = argparse.ArgumentParser()

    parser.add_argument('-s','--sequences', type=Path, nargs='+', required=True, help='path to the sequences on which to do computations')
    parser.add_argument('-a','--alphas', type=float, nargs='+', required=True)
    parser.add_argument('-c','--cmds', type=str, nargs='+', required=True)
    parser.add_argument('-g','--global-cmd', type=str, help='argument to be passed along with every command')
    parser.add_argument('-q','--quiet', action='store_true')

    args = parser.parse_args()

    for sequence_path in tqdm(args.sequences, desc='processing sequences', leave=False, disable=args.quiet):
        if not sequence_path.exists:
            print('invalid path : {}'.format(path))
            continue
        process_sequence(sequence_path, args.alphas, args.cmds, args.global_cmd, args.quiet)
        
    # pool = Pool(processes=4)
    # pool.imap_unordered(process_mesh_folder, mesh_folders.items())
    # pool.close()
    # pool.join()

    print('done')

if __name__ == '__main__':
    batch()