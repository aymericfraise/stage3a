from pathlib import Path
import numpy as np
from tqdm import tqdm # progress bars

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker
import matplotlib.image
from PIL import Image

import utils


def color_mesh(faces, vert_colors):
    """ Colors the mesh given by infile according to the given vector, and saves it to outfile"""
    colored_faces = []
    for face in faces:
        face_vert_count = face[0]
        face_vert_colors = tuple(vert_colors[int(i)] for i in face[1:face_vert_count+1])
        face_color = utils.face_rgba_mean(face_vert_colors)
        
        newface = face[:face_vert_count+1]+face_color
        colored_faces.append(newface)
    return colored_faces

def color_meshes_with_vectors(mesh_folder, outfolder, vecs, window, colormaps=['viridis'], prefixes=None, quiet=False):
    """ Using each vector in vecs, colors the meshes from mesh_folder that are inside the provided window

    prefixes: List[str] : If specified, use the provided strings as prefixes when saving the meshes (the first string will be used for all meshes colored with the first vector, etc)
    """
    if prefixes and len(prefixes) != len(vecs):
        raise ValueError('there should be as many prefixes as vectors')
    mesh_folder = Path(mesh_folder)
    utils.make_empy_dir(outfolder)
    vecs = [utils.vector_to_rgba(v, colormaps=colormaps) for v in vecs]
    
    all_meshes = utils.natural_sort([str(fp) for fp in mesh_folder.iterdir() if fp.suffix == '.off'])
    meshes_paths = all_meshes[window[0]:window[1]+1]
    
    for f,mesh_path in enumerate(tqdm(meshes_paths,desc="coloring meshes", leave=False, disable=quiet)):
        vert_count, face_count, vertices, faces = utils.load_off_mesh(mesh_path)
        for i, vec in enumerate(tqdm(vecs,desc="applying vectors", leave=False, disable=quiet)):
            v = int(len(vec)/(len(meshes_paths)))
            frame_vec = vec[v*f:v*(f+1)]
            colored_faces = color_mesh(faces, frame_vec)
            prefix = prefixes[i] if prefixes else i+1
            outfile = outfolder / '{}-{}.off'.format(prefix,f+1)
            utils.save_off_mesh(outfile, vert_count, face_count, vertices, colored_faces)


def plot_eigenvalues(vals, outfolder, title=None):
    plt.scatter([i for i in range(len(vals))],vals)
    plt.xticks([i for i in range(len(vals))], fontsize=10)
    plt.grid()
    vals_file = outfolder / 'vals'
    plt.savefig(str(vals_file))
    plt.clf()

def plot_vector(vec, save_path, n_frames=None, title=None, colormaps=None):
    # if n_frames is given, apply add a grid so that it's easier to differentiate frames 
    colors = utils.vector_to_rgba(vec, colormaps) if colormaps else None
    plt.scatter(range(len(vec)),vec,s=.5, c=colors)
    if n_frames:
        if type(n_frames) is not int:
            raise ValueError('n_frames has to be an int')
        n_vert = len(vec)
        vert_per_frame = int(n_vert/n_frames)
        # add grid
        ax = plt.gca()
        ax.set_axisbelow(True)
        ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(vert_per_frame))
        ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(1))
        plt.grid(which='minor', alpha=.2)
        plt.grid(axis='y', alpha=.2)
    plt.title(title)
    plt.savefig(str(save_path), dpi=400)
    plt.clf()

def plot_vectors(vecs, outfolder, title=None, quiet=False):
    for i,vec in enumerate(tqdm(vecs, desc="making vectors plots", leave=False, disable=quiet)):
        plot_vector(vec, Path(outfolder) / ('vec'+str(i+1)), title=title)


def merge_snaps(save_folder, snapshot_folder, row_len):
    """ Stitch all images from folder into one image saved into savepath, with row_len "sub-images" per row """ 
    sorted_paths = utils.natural_sort([str(x) for x in folder.iterdir()])
    sorted_paths = list(map(Path,sorted_paths))
    images = list(map(Image.open, sorted_paths))

    # prepare new image
    width, height = images[0].size
    n = len(images)
    new_width = width*row_len
    row_count =(n+row_len-1)//row_len
    new_height = height*row_count
    new_im = Image.new('RGB', (new_width, new_height))

    # paste snapshots onto new image
    x_offset = 0
    y_offset = 0
    for i,im in enumerate(images):
        if i != 0 and i%row_len == 0:
            x_offset = 0
            y_offset += height
        new_im.paste(im, (x_offset,y_offset))
        x_offset += width
    
    # add axes to image
    fig, ax = plt.subplots()
    for direction in ['top','right','bottom','left']:
        ax.spines[direction].set_visible(False)
    ax.set_title('frames', fontsize=40)
    ax.set_ylabel('eigenvectors (1=smallest)', rotation='vertical', fontsize=40)
    ax.xaxis.tick_top()
    ax.imshow(new_im, extent=[.5,row_len+.5,row_count+.5,.5])
    plt.xticks([i for i in range(1,row_len+1)], fontsize=28)
    plt.yticks([i for i in range(1,row_count+1)], fontsize=28)
    fig.set_size_inches(row_len*2, row_count*2) # max 2*2 inches per sub-image
    plt.tight_layout()

    savepath = Path(save_folder) / Path(snapshot_folder).stem
    fig.savefig(str(savepath), pad_inches=0)


def stitch(save_folder, snapshots_folder, row_len):
    """ Make one image for each subfolder in snapshots_folder out of the images it contains, and save the stitched images into save_folder. The stitched images are named after the folder that contained the original images"""

    rendered_ims = []
    for folder in (fp for fp in snapshots_folder.iterdir() if fp.is_dir()):
        if(len(list(folder.iterdir())) == 0):
            continue
        merged_path = save_folder / (folder.stem+'.jpg')
        merge_snaps(folder, row_len, merged_path)
        rendered_ims.append(merged_path)

