from pathlib import Path
import argparse
import os
import time
import sys
from tqdm import tqdm

import visualization
import eigensolver
import clustering
import utils

def create_folders(*folders):
    def create_folder(folder):
        folder.mkdir(parents=True, exist_ok=True)
    for folder in folders:
        create_folder(folder)

def compute_laplacian(editorpy_path, mesh_folder, window, alpha, laplacian_path, quiet=True):
    cmd_terms = ['python3', editorpy_path, mesh_folder, '-w', window[0], window[1], '-a', alpha, '--save-laplacian', laplacian_path]
    if quiet:
        cmd_terms.append('--quiet')
    cmd = ' '.join([str(x) for x in cmd_terms])
    os.system(cmd)

def compute_eigenpairs(laplacian_path, eigenpairs_path, k):
    t0 = time.time()
    eigensolver.eigsh(laplacian_path, eigenpairs_path, k=k)
    t1 = time.time()
    print('eigensolver took {} seconds'.format(t1-t0))

def color_eigenvectors(color_meshes_folder, eigenpairs_path, mesh_folder, window, indices=None, quiet=True):
    save_folder = color_meshes_folder / 'eigenvectors'
    save_folder.mkdir(parents=True, exist_ok=True)
    vals, vecs = utils.load_object_from_file(eigenpairs_path)
    V = vecs.T if indices == None else vecs.T[indices]
    visualization.color_meshes_with_vectors(mesh_folder, save_folder, V, [x for x in window], prefixes=[str(x) for x in indices], quiet=quiet)

def compute_clusters(cluster_list, clusters_folder, eigenpairs_path, quiet=True):
    for n_clusters in tqdm(cluster_list, desc='computing clusters', leave=False, disable=quiet):
        vals, vecs = utils.load_object_from_file(eigenpairs_path)
        savepath = clusters_folder / '{}'.format(n_clusters)
        clustering.make_clustering_vector(vecs, int(n_clusters), savepath)

def compute_progressive_clusters(cluster_list, clusters_folder, eigenpairs_path, quiet=True):
    for n_clusters in tqdm(cluster_list, desc='computing clusters', leave=False, disable=quiet):
        vals, vecs = utils.load_object_from_file(eigenpairs_path)
        n_vecs = vecs.shape[1]
        progressive_clusters_folder = clusters_folder / '{}p'.format(n_clusters)
        progressive_clusters_folder.mkdir(exist_ok=True)
        for i in tqdm(range(1,n_vecs+1), desc='computing prog clusters', leave=False, disable=quiet):
            savepath = progressive_clusters_folder / str(i)
            clustering.make_clustering_vector(vecs, int(n_clusters), savepath)

def plot_cluster(cluster_filepath, plots_folder, n_frames, alpha):
    if not cluster_filepath.exists():
        print('couldn\'t find file at {} for clustering, make sure to use --compute-clusters first'.format(cluster_filepath))
        return
    cluster_vec = utils.load_object_from_file(cluster_filepath)
    visualization.plot_vector(cluster_vec, plots_folder / cluster_filepath.stem, n_frames=n_frames, title=alpha, colormaps=['viridis'])

def plot_clusters(cluster_list, clusters_folder, plots_folder, window, alpha, quiet=True):
    for n in tqdm(cluster_list, desc='plotting clusters', leave=False, disable=quiet):
        n_frames = (window[1]-window[0])+1
        cluster_filepath = clusters_folder / str(n)
        plot_cluster(cluster_filepath, plots_folder, n_frames, alpha)

def plot_progressive_clusters(cluster_list, clusters_folder, plots_folder, window, alpha, quiet=True):
    for n in tqdm(cluster_list, desc='plotting clusters', leave=False, disable=quiet):
        n_frames = (window[1]-window[0])+1
        progressive_clusters_folder = clusters_folder / '{}p'.format(n)  
        if not progressive_clusters_folder.exists():
            raise ValueError('progressive clusters for cluster {} haven\'t been computed'.format(n))
        progressive_clusters = list(progressive_clusters_folder.glob('*'))
        progressive_plots_folder = plots_folder / '{}p'.format(n)
        progressive_plots_folder.mkdir(exist_ok=True)
        for cluster_filepath in tqdm(progressive_clusters, desc='plotting progressive clusters', leave=False, disable=quiet):
            plot_cluster(cluster_filepath, progressive_plots_folder, n_frames, alpha)
    
def color_clusters(cluster_list, clusters_folder, color_meshes_folder, mesh_folder, window, quiet=True):
    for n_clusters in tqdm(cluster_list, desc='coloring clusters', leave=False, disable=quiet):
        cluster_path = clusters_folder / str(n_clusters)
        if not cluster_path.exists():
            print('couldn\'t find file at {} for clustering, make sure to use --compute-clusters first'.format(cluster_path))
            continue
        cluster_vec = utils.load_object_from_file(cluster_path)
        
        save_folder = color_meshes_folder / 'c{}'.format(n_clusters)
        save_folder.mkdir(parents=True, exist_ok=True)
        visualization.color_meshes_with_vectors(mesh_folder, save_folder, [cluster_vec], colormaps=['viridis'], window=[x for x in window], quiet=quiet)


def main(args):

    parser = argparse.ArgumentParser()

    parser.add_argument('mesh_folder', type=Path, 
        help='Path to the folder containing the meshes to use')
    # parser.add_argument('laplacian_folder', type=Path, 
    #     help='Folder in which to save the laplacian object')
    
    parser.add_argument('--compute-laplacian', action='store_true')
    parser.add_argument('-w', '--window', type=int, nargs=2, 
        help='Temporal window (frame indices, 0-based)')
    parser.add_argument('-a', '--alpha', type=float)
        
    parser.add_argument('--compute-eigenpairs', nargs='?', type=int, const=20,
        help='Number of lowest eigenpairs to compute (defaults to 20)')
    parser.add_argument('--plot-eigenpairs', action='store_true')
    parser.add_argument('--color-eigenvectors', nargs='*', type=int,
        help='Colors all eigenvectors, or only those with the given indices if nargs>0')

    parser.add_argument('--clustering-method', type=str,
        help='Name of the method to use to perform the clustering')

    parser.add_argument('--clusters', type=str, nargs='+', 
        help='Specifies the numbers of clusters to use for the clustering commands')
    parser.add_argument('--compute-clusters', action='store_true')
    parser.add_argument('--plot-clusters', action='store_true')
    parser.add_argument('--color-clusters', action='store_true')
    
    parser.add_argument('--progressive-clusters', action='store_true',
        help='Perform clustering operations on eigenspaces spanned by more and more eigenvectors')

    parser.add_argument('--stitch-snapshots', type=Path, 
        help='Make one image out of the images contained in each subfolder of the given folder. The folder path is relative to the laplacian_folder')

    parser.add_argument('--quiet', action='store_true',
        help='If this is specified, progress bars won\'t be shown')

    args = parser.parse_args(args)

    # Arguments check
    if args.window is None or args.alpha is None:
        parser.error('--window and --alpha need to be specified')
    if args.color_eigenvectors == []: # --color-eigenvectors was specified but with no values
        args.color_eigenvectors = True
    
    # Enforce directory structure 
    editorpy_path = '/home/fraise/Documents/stage3a/code/3DtLaplace/editor.py'
    laplacian_folder = args.mesh_folder / 'laplacians/f{}-{}_a{}'.format(args.window[0], args.window[1], args.alpha)
    laplacian_path = laplacian_folder / 'L'
    eigenpairs_path = laplacian_folder / 'eig'
    clusters_folder = laplacian_folder / 'clusters'
    color_meshes_folder = laplacian_folder / 'color_meshes'
    plots_folder = laplacian_folder / 'plots'
    cluster_plots_folder = laplacian_folder / 'cluster_plots'

    # Arguments check
    if (not args.compute_eigenpairs) and (not eigenpairs_path.exists()) and \
        (args.plot_eigenpairs or args.color_eigenvectors or args.compute_clusters):
        parser.error('{} : you need to compute the eigenpairs before using --plot-eigenpairs, --color-eigenvectors, --compute-clusters'.format(str(args.laplacian_folder)))
    if not args.clusters and (args.compute_clusters or args.plot_clusters or args.color_clusters):
        parser.error('{} : you need to specify the clusters with --clusters before using --compute-clusters, --plot-clusters, --color-clusters'.format(str(args.mesh_folder)+str(args.alpha)))

    # Create the required folders
    create_folders(laplacian_folder, plots_folder, cluster_plots_folder, color_meshes_folder, clusters_folder)

    # Do the computations
    if args.compute_laplacian:
        compute_laplacian(editorpy_path, args.mesh_folder, args.window, args.alpha, laplacian_path, args.quiet)

    if args.compute_eigenpairs:
        compute_eigenpairs(laplacian_path, eigenpairs_path, k=args.compute_eigenpairs)

    if args.plot_eigenpairs:
        vals, vecs = utils.load_object_from_file(eigenpairs_path)
        visualization.plot_eigenvalues(vals, plots_folder)
        visualization.plot_vectors(vecs.T, plots_folder, title=args.alpha, quiet=args.quiet)
    
    if args.color_eigenvectors:
        indices = None if args.color_eigenvectors is True else args.color_eigenvectors
        color_eigenvectors(color_meshes_folder, eigenpairs_path, args.mesh_folder, args.window, indices, quiet=args.quiet)

    if args.compute_clusters:
        if not args.progressive_clusters:
            compute_clusters(args.clusters, clusters_folder, eigenpairs_path, quiet=args.quiet)
        else:
            compute_progressive_clusters(args.clusters, clusters_folder, eigenpairs_path, quiet=args.quiet)

    if args.plot_clusters:
        if not args.progressive_clusters:
            plot_clusters(args.clusters, clusters_folder, cluster_plots_folder, args.window, args.alpha, args.quiet)
        else:
            plot_progressive_clusters(args.clusters, clusters_folder, cluster_plots_folder, args.window, args.alpha, args.quiet)
        
    if args.color_clusters:
        color_clusters(args.clusters, clusters_folder, color_meshes_folder, args.mesh_folder, args.window, args.quiet)

    if args.stitch_snapshots:
        render_folder = args.laplacian_folder / args.stitch_snapshots
        snapshot_folder = args.laplacian_folder / args.stitch_snapshots / 'snapshots'
        visualization.stitch(render_folder, snapshot_folder, row_len=args.window[1]+1-args.window[0])


if __name__ == '__main__':
    main(sys.argv[1:])