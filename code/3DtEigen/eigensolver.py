import utils
from scipy.sparse import linalg as linalgs

def eigsh(laplacian_path, savepath, k=20):
    """Computes the first k eigenpairs of L and saves them into save_path
        :param L: Sparse matrix 
        :param savepath: 
        :param k=20: 
    """
    L = utils.load_laplacian(laplacian_path)
    vals, vecs = linalgs.eigsh(L, k=k, sigma=0, which='LA')
    utils.save_object_to_file([vals,vecs], savepath)