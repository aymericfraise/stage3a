import numpy as np
from scipy import linalg as linalg
from scipy import sparse as sparse
from scipy.sparse import linalg as linalgs

import pickle
import timeit
import logging
import functools

class Laplacian:
    def __init__(self):
        self.L_sparse = None
        self.L_dense = None
        # shape : size, vertex count per mesh, frame count
        self.N = None
        self.V = None
        self.F = None

    def init_from_file(self,filepath):
        with open(filepath,"rb") as f:
            Ls,Ds = pickle.load(f)
            self.V = Ls[0].shape[0]
            self.F = len(Ls)
            self.N = self.V*self.F
            self.L_sparse = Laplacian.sparse_from_LD(Ls,Ds)
            print(self.L_sparse.shape)

    def init_random(self, V, F):
        self.V = V
        self.F = F
        self.N = V*F
        def genSym(n):
            # density is n/6 as there should be about 6 elements per column because of a mesh's usual connectivity
            X = sparse.random(n, n, density=min(6/n, 1))
            upper_X = sparse.triu(X) 
            result = upper_X + upper_X.T - sparse.diags(X.diagonal())
            return result
        def genDiag(n):
            return [np.random.random() for _ in range(n)]
        Ls = [genSym(V) for _ in range(F)]
        Ds = [genDiag(V) for _ in range(F-1)]
        self.L_sparse = Laplacian.sparse_from_LD(Ls,Ds)
        self.L_dense = self.L_sparse.toarray()
        print(self.L_sparse.shape)

    @staticmethod
    def sparse_from_LD(Ls,Ds):
        f = len(Ls)
        if len(Ds) != f-1:
            raise ValueError()
        
        blocks = [[None for _ in range(f)] for _ in range(f)]
        for i in range(f):
            blocks[i][i] = Ls[i]
            if i < f-1:
                D = sparse.diags(Ds[i])
                blocks[i][i+1] = D
                blocks[i+1][i] = D
        L = sparse.bmat(blocks)
        return L

    def solve_eigh(self, k=None):
        # returns eigenvalues sorted algebraically, not by absolute value, so be careful with that
        if self.L_dense is None:
            print("can't run eigh because the dense laplacian hasn't been computed")
            return None
        if k is None:
            k = self.N-1
        if k > self.N-1:
            print("k={0} > N-1={1} : too many eigenpairs asked for, asking for N-1={1} eigenpairs".format(k,self.N-1))
            k = self.N-1
        return linalg.eigh(self.L_dense, eigvals=(self.N-k,self.N-1))

    def solve_eigsh(self, k=None):
        # can't return all eigenpairs, so be careful with that
        if k is None:
            k = self.N-1
        if k > self.N-1:
            print("k={0} > N-1={1} : too many eigenpairs asked for, asking for N-1={1} eigenpairs".format(k,self.N-1))
            k = self.N-1
        return linalgs.eigsh(self.L_sparse, k=k, which="LA")

def bench(out_file, funcs):
    logging.basicConfig(filename=out_file, level=logging.INFO, format="%(message)s")
    
    for (func_name, (func, mat_type)) in solvers.items():
        start = timeit.default_timer()
        func(M[mat_type])
        end = timeit.default_timer()
        time = end-start
        log = "{0} {1} {2} {3}".format(func_name, n, f, time)
        debug = "{0:12}{1:6}{2:6}{3:10.3f}".format(func_name, n, f, time)
        print(debug)
        logging.info(log)

def compare_results(e):
    # e : List[eigenvalues,eigenvectors]
    # sort eigenvalues and eigenvectors according to eigenvalues magnitude
    sorted_e = []
    for val,vec in e:
        p = np.argsort(np.abs(val))
        val = val[p]
        vec = vec[:, p]
        sorted_e.append((val,vec))
    
    print("\n".join([str(ei[0]) for ei in sorted_e]))
        
    # compare results
    val0, vec0 = sorted_e[0]
    for val, vec in sorted_e:
        if val0.shape != val.shape:
            print("incompatible shapes, skipping comparison")
            continue
        print(np.allclose(val0, val, rtol=1e-1), "\t",
              np.allclose(np.abs(vec0), np.abs(vec), rtol=1e-1), "\t")

if __name__ == '__main__':
    filepath = "../laplacians/bunny0-15"
    L = Laplacian()
    #L.init_from_file(filepath)
    L.init_random(3,3)
    e = [L.solve_eigh(), L.solve_eigsh()]
    print("\n".join([str(ei[0]) for ei in e]))
    compare_results(e)

    # funcs = [("eigsh", functools.partial(L.solve_eigsh,k=20))]
    # bench("res/benchbunny.log", funcs)
    