import numpy as np
from scipy import linalg as linalg
from scipy import sparse as sparse
from scipy.sparse import linalg as linalgs

import timeit
import logging

import pandas
import matplotlib.pyplot as plt
import matplotlib.cm as cm

class Laplacian:
    def __init__(self):
        self.L = None
        # shape : size, vertex count per mesh, frame count
        self.N = None
        self.V = None
        self.F = None

    def init_from_file(self,filepath):
        import pickle
        with open(filepath,"rb") as f:
            Ls,Ds = pickle.load(f)
            self.V = Ls[0].shape[0]
            self.F = len(Ls)
            self.N = self.V*self.F
            self.L_sparse = Laplacian.sparse_from_LD(Ls,Ds)
            print(self.L_sparse.shape)

    def init_random(self, V, F):
        self.V = V
        self.F = F
        self.N = V*F
        def random_sparse(n,m,density):
            # only accurate for big matrices with low densities (n*m > 1000000, density < .01)
            # adjust the density to make it account for the bias in the method
            density = density-(density**2)/2 
            np.random.seed()
            n_alt = int(n*n*density)
            row_ids = np.random.randint(n, size=n_alt)
            col_ids = np.random.randint(n, size=n_alt)
            data = np.random.rand(n_alt)
            S = sparse.coo_matrix((data, (row_ids, col_ids)), shape=(n,n))
            return S
        def random_sparse_symmetric(n):
            # density is n/6 as there should be about 6 elements per column because of a mesh's usual connectivity
            X = random_sparse(n,n,density=min(6/n,1))
            upper_X = sparse.triu(X) 
            result = upper_X + upper_X.T - sparse.diags(X.diagonal())
            return result
        def random_diagonal(n):
            return [np.random.random() for _ in range(n)]
        Ls = [random_sparse_symmetric(V) for _ in range(F)]
        Ds = [random_diagonal(V) for _ in range(F-1)]
        self.L = Laplacian.sparse_from_LD(Ls,Ds)

    @staticmethod
    def sparse_from_LD(Ls,Ds):
        f = len(Ls)
        if len(Ds) != f-1:
            raise ValueError()
        
        blocks = [[None for _ in range(f)] for _ in range(f)]
        for i in range(f):
            blocks[i][i] = Ls[i]
            if i < f-1:
                D = sparse.diags(Ds[i])
                blocks[i][i+1] = D
                blocks[i+1][i] = D
        L = sparse.bmat(blocks)
        return L

def bench(out_file, vfk_arr):
    logging.basicConfig(filename=out_file, level=logging.INFO, format="%(message)s")
    
    L = Laplacian()
    for v,f,k in vfk_arr:
        t0 = timeit.default_timer()
        L.init_random(v,f)
        t1 = timeit.default_timer()
        M = L.L.todok()
        t2 = timeit.default_timer()
        linalgs.eigsh(M, k=k, sigma=0, which="LM")
        t3 = timeit.default_timer()
        log = "{0} {1} {2} {3}".format(v, f, k, t3-t2)
        debug = "{0:6}{1:6}{2:6}{3:10.3f}(+{4:10.3f})".format(v, f, k, t3-t2, t2-t0)
        print(debug)
        logging.info(log)

def plot(infile,outfile):
    df = pandas.read_csv(infile, names=['N','v','f','t','type'], sep=' ').sort_values(by='N')
    fig, ax = plt.subplots()
    colors = {'csc':'r','csr':'g','dok':'b'}
    for label, grp in df.groupby('type'):
        grp.plot.scatter(x ='N',y='t',ax = ax, label=label, c=colors[label])
    fig.savefig(outfile)
    
def compare_results(e):
    # e : List[eigenvalues,eigenvectors]
    # sort eigenvalues and eigenvectors according to eigenvalues magnitude
    sorted_e = []
    for val,vec in e:
        p = np.argsort(np.abs(val))
        val = val[p]
        vec = vec[:, p]
        sorted_e.append((val,vec))
    
    print("\n".join([str(ei[0]) for ei in sorted_e]))
        
    # compare results
    val0, vec0 = sorted_e[0]
    for val, vec in sorted_e:
        if val0.shape != val.shape:
            print("incompatible shapes, skipping comparison")
            continue
        print(np.allclose(val0, val, rtol=1e-1), "\t",
              np.allclose(np.abs(vec0), np.abs(vec), rtol=1e-1), "\t")

def runbench(name, V=range(100,1001,100), F=range(15,16,5), K=range(20,21,5), b=True, p=True):
    # v: block size, f: block count, k: number of eigvecs to compute 
    vfk_arr = [(v,f,k) for v in V for f in F for k in K]
    logfile = "res/log/"+name+".log"
    plotfile = "res/plot/"+name+".png"
    if b:
        bench(logfile, vfk_arr)
    if p:
        plot(logfile,plotfile)

def runcompare():
    L = Laplacian()
    L.init_random(10,10)
    e = []
    # append results to e here
    compare_results(e)

if __name__ == '__main__':
    runbench('dsbtdcf5k20', V=range(10,101,10), F=range(5,6,5), K=range(20,21,5), p=False)
    