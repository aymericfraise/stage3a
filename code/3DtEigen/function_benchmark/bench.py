import numpy as np
from scipy import linalg as linalg
from scipy import sparse as sparse
from scipy.sparse import linalg as linalgs
import functools

# benchmark
import timeit
import logging

# plot
import matplotlib
matplotlib.use('agg')
from matplotlib import cm as cm
from matplotlib import pyplot as plt


def genDiag(n):
    return sparse.diags([np.random.random() for _ in range(n)])

def genSym(n):
    # density is n/6 because there should be about 6 elements per column because of a mesh's usual connectivity
    X = sparse.random(n, n, density=min(6/n, 1))
    upper_X = sparse.triu(X) 
    result = upper_X + upper_X.T - sparse.diags(X.diagonal())
    return result

def genLap(n, f):
    blocks = [[None for _ in range(f)] for _ in range(f)]
    for i in range(f):
        blocks[i][i] = genSym(n)
        if i+1 < f:
            D = genDiag(n)
            blocks[i][i+1] = D
            blocks[i+1][i] = D
    M_sparse = sparse.bmat(blocks)
    M_square = sparse_to_square(M_sparse)
    M_banded = square_to_banded(M_square, n)
    matrices = {"sparse": M_sparse, "banded": M_banded, "square": M_square}
    return matrices

def square_to_banded(M, kd):
    # kd = number of bands above the main diagonal
    # see https://www.netlib.org/lapack/lug/node124.html
    n, m = M.shape
    if n != m:
        raise ValueError("input matrix is not square")
    M_banded = np.zeros((kd+1, n))
    for (i, j) in ((i, j) for j in range(n) for i in range(max(0, j-kd), j+1)):
        M_banded[kd+i-j, j] = M[i, j]
    return M_banded

def banded_to_square(M_banded):
    kd = M_banded.shape[0] - 1
    n = M_banded.shape[1]
    M = np.zeros((n, n))
    for (i, j) in ((i, j) for j in range(n) for i in range(max(0, j-kd), j+1)):
        M[i, j] = M_banded[kd+i-j, j]
    M = M+np.transpose(np.triu(M, 1))
    return M

def sparse_to_square(M_sparse):
    return M_sparse.toarray()

def compare_results(e, k=None):
    # e : List[eigenvalues,eigenvectors]
    # sort eigenvalues and eigenvectors according to eigenvalues magnitude
    sorted_e = []
    for val,vec in e:
        p = np.argsort(np.abs(val))
        val = val[p]
        vec = vec[:, p]
        sorted_e.append((val,vec))
    
    


def compare_solvers(solvers, n, f, k=None):
    # compare results of the different methods
    np.set_printoptions(precision=2)
    M = genLap(n, f)

    # solve
    e = []
    for (func_name, (func, mat_type)) in solvers.items():
        # print(func_name)
        if func_name in ['sparse_eigs','sparse_eigsh']:
            ei = func(M[mat_type], k=M[mat_type].shape[0]-1)
        else:
            ei = func(M[mat_type])
        val, vec = ei[0], ei[1]
        # sort eigenvalues and eigenvectors according to eigenvalues magnitude
        p = np.argsort(np.abs(val))
        val = val[p]
        vec = vec[:, p]
        e.append((func_name, val, vec))
        # print(func_name, "\nval\n", val, "\nvec\n", vec, "\n")

    # truncate results to first k elts
    if k:
        sorted_e = [(name, val[-k:], vec[:,-k:]) for (name,val,vec) in e]
    
    print("\n".join([str(ei[0]) for ei in sorted_e]))

    # compare results
    _, val0, vec0 = sorted_e[0]
    for name, val, vec in sorted_e:
        if val0.shape != val.shape:
            print("{}: incompatible shapes ({}, {}), skipping comparison".format(name, val0.shape, val.shape))
            continue
        print(np.allclose(val0, val, rtol=1e-1), "\t",
              np.allclose(np.abs(vec0), np.abs(vec), rtol=1e-1), "\t")

def bench(out_file, solvers, nf_arr):
    logging.basicConfig(filename=out_file, level=logging.INFO, format="%(message)s")
    
    for i, (n, f) in enumerate(nf_arr):
        print("{}/{}".format(i, len(nf_arr)))
        M = genLap(n,f) 
        for (func_name, (func, mat_type)) in solvers.items():
            start = timeit.default_timer()
            if func_name in ['sparse_eigs','sparse_eigsh']:
                func(M[mat_type], k=20)
            else:
                func(M[mat_type])
            end = timeit.default_timer()
            time = end-start
            log = "{0} {1} {2} {3}".format(func_name, n, f, time)
            debug = "{0:12}{1:6}{2:6}{3:10.3f}".format(func_name, n, f, time)
            print(debug)
            logging.info(log)

def plot(in_file, out_file, to_plot):
    data = {}
    with open(in_file) as F:
        for line in F:
            name, n, f, time = line.split()
            n, f, time = int(n), int(f), float(time)
            if f not in data.keys():
                data[f] = {}
            if name not in data[f]:
                data[f][name] = []
            data[f][name].append((n*f, time))

    n,m = 1, len(data.keys())
    fig, axes = plt.subplots(n,m,squeeze=False)
    axes = axes.reshape((-1))
    for (f,data_f),ax in zip(data.items(),axes):
        # populate the subplot corresponding to the block size n
        # print(data_f)
        last_means = []
        for i,(name, times) in enumerate(data_f.items()):
            if name not in to_plot:
                continue

            # add all the data points to the plot
            X = [time[0] for time in times]
            Y = [time[1] for time in times]
            #ax.scatter(X, Y, s=.5, label=name)

            # add the mean of each distinct x value to the plot. sorting is needed for the line to be plotted correctly afterwards
            means_X, means_Y = [], []
            for x in sorted(list(set(X))):
                means_X.append(x)
                means_Y.append(np.mean([y for x_, y in times if x_ == x]))
            # ax.scatter(X,Y,label=name, alpha=.5)
            ax.plot(means_X, means_Y, linestyle='-', marker='o',linewidth=1.5, label=name, alpha=.8)
            last_means.append(means_Y[-1])
        
        ax.set_title('n_frames={}'.format(f))
        ax.set_xlabel('N')
        ax.set_ylabel('t (sec)')
        ax.legend(loc="upper left")
        ax.set_yscale('log')
        # set square aspect ratio
        xleft, xright = ax.get_xlim()
        ybottom, ytop = ax.get_ylim()
        ax.set_aspect(abs((xright-xleft)/(ybottom-ytop)))
        # set the legends' color
        handles, labels = ax.get_legend_handles_labels()
        print(labels)
        colors = cm.rainbow(np.linspace(0, 1, len(handles)))
        for h,c in zip(handles,colors):
            h.set_color(c)
        # order legend according to last means
        order = np.argsort(last_means)[::-1]
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles=list(np.array(handles)[order]),labels=list(np.array(labels)[order]))

    # fig.set_size_inches(15,6)
    fig.tight_layout
    fig.set_dpi(200)
    fig.savefig(out_file)

if __name__ == "__main__":
    solvers = {
        "sparse_eigs": [linalgs.eigs, "sparse"],
        "sparse_eigsh": [linalgs.eigsh, "sparse"],
        "numpy_eig": [np.linalg.eig, "square"],
        "scipy_eig": [linalg.eig, "square"],
        "eigh": [functools.partial(linalg.eigh, eigvals=(0,20)), "square"],
        "eigh_opt": [functools.partial(linalg.eigh, turbo=True, check_finite=False),"square"],
        "eig_banded": [linalg.eig_banded, "banded"],
        "dsbev": [linalg.lapack.dsbev, "banded"],
        "dsbevd": [linalg.lapack.dsbevd, "banded"],
        "dsbevx": [functools.partial(linalg.lapack.dsbevx, vl=0, vu=0, il=1, iu=1), "banded"],
    }
    all_names = ["numpy_eig", "scipy_eig", "eigh",
                 "eigh_opt", "eig_banded", "dsbev", "dsbevd", "dsbevx", "sparse_eigs", "sparse_eigsh"]
    to_bench = ['eigh', 'sparse_eigsh','sparse_eigs']
    to_plot = all_names
    to_compare = all_names


    nf_arr = [(n, f) for n in range(100, 501, 100)
              for f in range(10, 11, 5) for c in range(10)]
    # compare_solvers(
    #     solvers={name: solvers[name] for name in to_compare if name in solvers.keys()}, 
    #     n=10, f=10, k=20)
    name = 'bench19-07_k20'
    logfile = "res/log/{}.log".format(name)
    plotfile = "res/plot/{}.png".format(name)
    # bench(
    #     out_file=logfile, 
    #     solvers={name: solvers[name] for name in to_bench if name in solvers.keys()}, 
    #     nf_arr=nf_arr)
    plot(
        in_file=logfile, 
        out_file=plotfile, 
        to_plot=to_plot)
