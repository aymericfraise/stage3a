import sklearn.cluster
import utils
import math
import numpy as np

def reorder_label(label):
    # reorders the labels so that they appear in increasing order
    correspondences = [None for _ in range(max(label)+1)]
    ordered = np.empty(len(label))
    next_max = 0
    for i,l in enumerate(label):
        if correspondences[l] is None:
            correspondences[l] = next_max
            next_max += 1
        ordered[i] = correspondences[l]
    return ordered

def kmeans(V, n):
    cluster_centers, label, _ = sklearn.cluster.k_means(V, n)
    # debug_non_conn(V, label, cluster_centers)
    return label

def dbscan(V, n):
    clust = sklearn.cluster.DBSCAN()
    clust.fit(V)
    return clust.labels_

def affinityprop(V,n):
    clust = sklearn.cluster.AffinityPropagation(max_iter=20, convergence_iter=5)
    clust.fit(V)
    return clust.labels_

def spectral(V,n):
    clust = sklearn.cluster.SpectralClustering(n_clusters=n)
    clust.fit(V)
    return clust.labels_

def agglomerative(V,n):
    clust = sklearn.cluster.AgglomerativeClustering(n_clusters=n)
    clust.fit(V)
    return clust.labels_

def birch(V,n):
    clust = sklearn.cluster.Birch(threshold=1e-3, n_clusters=n)
    clust.fit(V)
    return clust.labels_

def meanshift(V,n):
    clust = sklearn.cluster.MeanShift()
    clust.fit(V)
    return clust.labels_

def make_clustering_vector(V, n, savepath):
    """ V is the matrix containing the eigenvectors v_1...v_k as columns.
    For i = 1...n, let y_i be the vector corresponding to the i-th row of vecs.
    Clusters the points (y_i)i=1...n with the k-means algorithm into clusters C_1...C_k."""

    METHODS = {
        'kmeans':kmeans,
        'dbscan':dbscan,
        'affinityprop':affinityprop, 
        'spectral':spectral,
        'agglomerative':agglomerative, 
        'birch':birch, 
        'meanshift':meanshift
        }
    EIG_NB = {'all':V.shape[1],
        'n':n,
        'n/2':(n+1)//2,
        'log':math.ceil(math.log2(n)),
        'sqrt':int(round(math.sqrt(n)))
        }

    
    # V = (V - V.min(axis=0)) / V.max(axis=0) #normalize V
    clustering_func = METHODS['kmeans']
    max_eig = EIG_NB['n']
    V_trunc = V[:,0:min(max_eig,V.shape[1])]

    label = clustering_func(V_trunc, n)
    label = reorder_label(label)
    utils.save_object_to_file(label, savepath)

def debug_non_conn(V, label, cluster_centers, nframes=23):
    import matplotlib.pyplot as plt
    frame = np.repeat(np.arange(0,nframes),len(label)//nframes,axis=0)
    # sample the points to speed up the plotting
    frame = frame[::10]
    V = V[::10,:]
    label = label[::10]
    x = np.reshape(V[:,0:1], (1,-1))[0]
    y = np.reshape(V[:,1:2], (1,-1))[0]
    z = np.reshape(V[:,2:3], (1,-1))[0]
    print(x)
    from mpl_toolkits.mplot3d import Axes3D
    f1 = plt.figure(1)
    ax1 = Axes3D(f1)
    ax1.scatter(x,y,z, c=label, s=.4)
    # print(cluster_centers.shape)
    # ax1.scatter(cluster_centers[:,0],cluster_centers[:,1],cluster_centers[:,2], c=[0,1,2], s=10, marker='s')
    # f2 = plt.figure(2)
    # ax2 = Axes3D(f2)
    # ax2.scatter(x,y,z, c=frame, s=.4)
    # f3 = plt.figure(3)
    # ax3 = Axes3D(f3)
    # ax3.scatter(x,y,z, c=list(range(len(label))), s=.4)
    for ax in [ax1]:
        ax.set_xlabel('vec 0')
        ax.set_ylabel('vec 1')
        ax.set_zlabel('vec 2')
    plt.show()

def debug_double_clusters(V, label, clusters_centers):
    import matplotlib.pyplot as plt
    n = len(set(label))
    X = [float(x) for x in V]
    diff = [x1 - x2 if abs(x1-x2) < 5e-12 else 0 for x1,x2 in zip(X[1:], X[:-1])]
    # plt.plot(diff)
    # plt.savefig('/home/fraise/Documents/stage3a/ref/log/cluster simple vec/{}'.format('diff'))
    plt.plot(label)
    plt.savefig('/home/fraise/Documents/stage3a/ref/log/kmeans_too_many_clusters/{}'.format('v1c{}'.format(n)))
    exit()

    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.set_ylabel('label', color=color)  # we already handled the x-label with ax1
    ax1.plot(label, color=color)
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:red'
    ax2.set_xlabel('vertices')
    ax2.set_ylabel('relative difference', color=color)
    ax2.plot(diff, color=color)
    ax2.tick_params(axis='y', labelcolor=color)

    # fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig('/home/fraise/Documents/stage3a/ref/log/kmeans_too_many_clusters/{}'.format('diff0'))
    exit()