import numpy as np

class Vertex:
    def __init__(self, point, index=-1):
        """
        Create a new Vertex located at the specified 3D point.

        Arguments
        ---------

        point           : List/numpy array with 3D coordinates
        index           : Index of this vertex inside a certain Mesh.
        """
        
        self.point = np.array(point)
        self.index = index
        self.out_edges = []             # Edges that "leave" this vertex
        self.normal = np.array([0,0,0])

    def has_spatial_coordinates(self, x, y, z):
        return self.point[0] == x and self.point[1] == y and self.point[2] == z

    # Operator overloading
    def __eq__(self, other):
        if (len(self.point) != len(other.point)):
            return False

        return self.point[0] == other.point[0] and self.point[1] == other.point[1] and self.point[2] == other.point[2]

    def __str__(self):
        res = "("

        for i in range(len(self.point)):
            if i > 0:
                res += ", "

            res += str(self.point[i])
             
        res += ")"
        return res