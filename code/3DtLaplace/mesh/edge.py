import math
import numpy as np

class Edge:

    def __init__(self, vertex_from, vertex_to, index=-1):
        """
        Create new edge with the specified vertices, oriented vertex_from to vertex_to.
        """

        self.vertex_from = vertex_from
        self.vertex_to = vertex_to
        self.index = index

        # To be completed by the mesh
        self.face = None                # incident face to this edge
        self.opposite = None            # edge with opposite orientation

        # Add this edge to the "outgoing edges" list in vertex_from
        self.vertex_from.out_edges.append(self)

        # Find the opposite edge
        for i in range(0, len(self.vertex_to.out_edges)):
            e = self.vertex_to.out_edges[i]

            # Check if the current edge is the same as this, except for the orientation
            if e.vertex_to == self.vertex_from:
                # Found it: update information on both edges
                e.opposite = self
                self.opposite = e
                break

    def get_length(self):
        return np.linalg.norm(self.vertex_from.point - self.vertex_to.point)
        
    def __str__(self):
        return str(self.vertex_from.point) + " -> " + str(self.vertex_to.point)