import numpy as np
from bisect import bisect_left
from . import Mesh, Vertex, Edge, Face

class Loader:
    
    def load_mesh(self, filepath, roi=None):
        raise NotImplementedError()

    def read_vertex_coordinates(self, filepath, roi=None):
        raise NotImplementedError()

# -----------------------------------------------------------------------------------------------

class OFFLoader(Loader):

    def load_mesh(self, off_file_path, roi=None):
        """
        Load Mesh object from OFF file.

        If ROI is not None, then only the specified ROI vertices will be loaded.
        In this case, a mapping between roi indices and real indices
        will be returned in the mesh structure.

        NOTE: ROI indices must be sorted.
        """
        
        with open(off_file_path) as f:
            content = f.read().splitlines()

        if content[0] != 'OFF' and content[0] != 'COFF' and content[0] != 'NOFF':   # check if first line is correct
            raise MeshLoadException("Invalid .off file")

        # Second line contains the number of vertices and faces
        counters = content[1].split(" ")

        if len(counters) != 3:
            counters = [it for it in counters if it != '']     # remove possible extra spaces

        fullmesh_numv = int(counters[0])
        fullmesh_numf = int(counters[1])

        # Initialize mesh object
        mesh = Mesh()

        # -------------
        # Read vertices
        # -------------

        totallines = len(content)

        if roi is not None:
            # We have a ROI: just read vertices that belong to it
            roi_prev_val = None

            # Init mapping between roi vertex indices and full mesh's indices
            mesh.real2roi_idx = dict()

            for v_real_i in roi:
                # Check that ROI is indeed sorted
                if roi_prev_val is not None and roi_prev_val > v_real_i:
                    raise Exception("Cannot load file: ROI must be sorted")

                roi_prev_val = v_real_i

                # Go to line (this is why is better if roi is sorted)
                # We're expecting for the first vertex to begin in the second line...
                line = content[v_real_i+2].strip()

                # and we're always expecting a vertex here...
                if (not line) or line[0] == "#":        # if it's empty or a comment...
                    raise Exception("ROI index is not pointing to a valid vertex")

                point = self._get_coordinates_from_string(line)
                v = mesh.add_vertex(point)
                mesh.real2roi_idx[v_real_i] = v.index
        else:
            # No ROI - just iterate lines
            for linenum in range(2, totallines):

                line = content[linenum].strip()

                if (not line) or line[0] == "#":        # if it's empty or a comment...
                    continue

                point = self._get_coordinates_from_string(line)
                v = mesh.add_vertex(point)

                if v.index == fullmesh_numv -1:
                    # collected all vertices: now read faces
                    break

        # ----------
        # Read faces
        # ----------

        for linenum in range(fullmesh_numv + 2, totallines):

            line = content[linenum].strip()

            if (not line) or line[0] == "#":        # if it's empty or a comment...
                continue

            face_indices = line.split(" ")

            if len(face_indices) != 4 and len(face_indices) != 8:         # 8: case when color information is there
                # remove extra blank spaces
                face_indices = [it for it in face_indices if it != '']

            # Check that we have 3 vertices for the face
            # Else we will not load the file
            if int(face_indices[0]) != 3:
                msg = "Invalid .off file: Each face should be defined by three vertices, but they are being defined by " + content[lineNum][0] + "."
                raise LoaderException(msg)

            # Load vertex indices
            v1 = int(face_indices[1])
            v2 = int(face_indices[2])
            v3 = int(face_indices[3])

            # Check that all three vertex indices belong to the ROI
            if roi is not None:
                # we can't use sets because we need to index the roi... (I think)
                v1_pos = bisect_left(roi, v1)
                v2_pos = bisect_left(roi, v2)
                v3_pos = bisect_left(roi, v3)
                
                v1_in_roi = (v1_pos != len(roi) and (roi[v1_pos] == v1))        # "v1 in roi" using bisect
                v2_in_roi = (v2_pos != len(roi) and (roi[v2_pos] == v2))
                v3_in_roi = (v3_pos != len(roi) and (roi[v3_pos] == v3))

                if not v1_in_roi or not v2_in_roi or not v3_in_roi:
                    continue

                # Indices are based on the full mesh: get roi index before saving the face
                v1 = mesh.real2roi_idx[v1]
                v2 = mesh.real2roi_idx[v2]
                v3 = mesh.real2roi_idx[v3]

            # Add new face
            mesh.add_face(v1, v2, v3)

        mesh.num_vertices = len(mesh.vertices)       # TODO: add methods for this!!
        mesh.num_faces = len(mesh.faces)

        return mesh

    def read_vertex_coordinates(self, off_file_path, roi=None):
        """
        Returns a numpy array of size n*3, with n the number of vertices,
        containing the 3D coordinates of the mesh.

        Arguments
        ---------

        file_path           : Path to the mesh file
        roi                 : List of indices for the vertices inside the region of interest.
                              Only these vertices will be loaded.

        """

        # TODO: this is almost copy-paste from load_mesh!!!

        with open(off_file_path) as f:
            content = f.read().splitlines()

        content[0] = content[0].strip()
        if content[0] != 'OFF' and content[0] != 'COFF' and content[0] != 'NOFF':   # check if first line is correct
            raise MeshLoadException("Invalid .off file")

        # Second line contains the number of vertices and faces
        counters = content[1].split(" ")

        if len(counters) != 3:
            counters = [it for it in counters if it != '']     # remove possible extra spaces

        totallines = len(content)
        fullmesh_numv = int(counters[0])
        fullmesh_numf = int(counters[1])

        n = len(roi) if roi is not None else fullmesh_numv
        npvertices = np.zeros((n, 3), np.double)

        # Get coordinates for each vertex
        if roi is not None:
            # We have a ROI: just read lines for vertices that belong to it
            roi_ind = 0

            for v_real_i in roi:
                # Go to line (this is why is better if roi is sorted)
                # We're expecting for the first vertex to begin in the second line...
                line = content[v_real_i+2].strip()

                # and we're always expecting a vertex here...
                if (not line) or line[0] == "#":        # if it's empty or a comment...
                    raise Exception("ROI index is not pointing to a valid vertex")

                point = self._get_coordinates_from_string(line)
                npvertices[roi_ind, :] = point
                roi_ind += 1
        else:
            # No ROI - just iterate lines
            i = 0
            for lineNum in range(2, totallines):

                line = content[lineNum].strip()

                if (not line) or line[0] == "#":        # if it's empty or a comment...
                    continue

                point = self._get_coordinates_from_string(line)
                npvertices[i, :] = point
                i += 1

                if i == fullmesh_numv:
                    break

        return npvertices

    def _get_coordinates_from_string(self, str_coords):
        coord_parts = str_coords.split(" ") # start looking from 3rd line

        if len(coord_parts) != 3 and len(coord_parts) != 7:             # 7: case when color information is there
            coord_parts = [it for it in coord_parts if it != '']
        
        # TODO: time coordinate is zero because of the new sequence structure (we don't need it here...)
        # Change mesh structure also and do it properly!!
        return [float(coord_parts[0]), float(coord_parts[1]), float(coord_parts[2])]


# ----------------------------------------------------------------------------------------

class OBJLoader(Loader):

    def load_mesh(self, filepath, roi=None):
        """
        Load Mesh object from OBJ file.

        If ROI is not None, then only the specified ROI vertices will be loaded.
        In this case, a mapping between roi indices and real indices
        will be returned in the mesh structure.

        NOTE: ROI indices must be sorted.
        """

        with open(filepath) as f:
            content = f.read().splitlines()

        mesh = Mesh()
        
        if roi is not None:
            mesh.real2roi_idx = dict()

        totallines = len(content)
        
        # -------------
        # Read vertices
        # -------------

        v_ind = 0
        ri = 0
        curr_roi_val = roi[ri] if roi is not None else None
        prev_roi_val = None         # this will be for checking that roi indices are sorted

        if roi is not None:
            # We have a ROI: iterate through all vertices
            # and only keep those which are in the ROI list
            for linenum in range(totallines):

                line = content[linenum].strip()

                if len(line) == 0:
                    continue

                if line[0] == 'v' and line[1] == ' ':
                    # Check if this is belongs to the ROI
                    if v_ind != curr_roi_val:
                        v_ind += 1
                        continue

                    # It does...
                    point = self._get_coordinates_from_string(line)
                    mesh.add_vertex(point)
                    
                    # Map real to roi index
                    mesh.real2roi_idx[v_ind] = ri

                    # Move to next roi index, and check that list was indeed sorted
                    ri += 1

                    if ri == len(roi):
                        break

                    prev_roi_val = curr_roi_val
                    curr_roi_val = roi[ri]

                    if (prev_roi_val > curr_roi_val):
                        raise Exception("Cannot load sequence: ROI indices must be sorted")

                    v_ind += 1
        else:
            # We don't have a ROI: just iterate lines
            for linenum in range(totallines):

                line = content[linenum].strip()
                
                if len(line) == 0:
                    continue

                if line[0] == 'v' and line[1] == ' ':
                    point = self._get_coordinates_from_string(line)
                    mesh.add_vertex(point)
                    v_ind += 1

        # ----------
        # Read faces
        # ----------

        e_ind = 0
        for linenum in range(totallines):

            line = content[linenum].strip()
            if len(line) == 0:
                continue

            if line[0] == 'f':
                str_inds = line.split(" ")

                if len(str_inds) > 4:
                    # Non-triangular faces, for now we're just ignoring it...
                    continue

                # Get indices (only first if we have indices of typ a/b or a/b/c)
                vtx_inds = []
                for i in range(1, 4):
                    ind_parts = str_inds[i].split("/")
                    vtx_inds.append(int(ind_parts[0]))

                # NOTE: indices are 1-based
                v1 = int(vtx_inds[0] - 1)
                v2 = int(vtx_inds[1] - 1)
                v3 = int(vtx_inds[2] - 1)

                # Check that all three vertices are part of the roi
                if roi is not None:
                    # we can't use sets because we need to index the roi... (I think)
                    v1_pos = bisect_left(roi, v1)
                    v2_pos = bisect_left(roi, v2)
                    v3_pos = bisect_left(roi, v3)
                    
                    v1_in_roi = (v1_pos != len(roi) and (roi[v1_pos] == v1))        # "v1 in roi" using bisect
                    v2_in_roi = (v2_pos != len(roi) and (roi[v2_pos] == v2))
                    v3_in_roi = (v3_pos != len(roi) and (roi[v3_pos] == v3))

                    #if v1 not in roi or v2 not in roi or v3 not in roi:
                    if not v1_in_roi or not v2_in_roi or not v3_in_roi:
                        continue

                    # Get index of vertices according to roi
                    v1 = mesh.real2roi_idx[v1]
                    v2 = mesh.real2roi_idx[v2]
                    v3 = mesh.real2roi_idx[v3]

                # Add face
                mesh.add_face(v1, v2, v3)

            # We don't support anything else for now...
        
        mesh.num_vertices = len(mesh.vertices)
        mesh.num_faces = len(mesh.faces)

        return mesh

    def read_vertex_coordinates(self, filepath, roi=None):
        """
        Returns a numpy array of size n*3, with n the number of vertices,
        with the 3D coordinates of the mesh.

        Arguments
        ---------

        file_path           : Path to an .obj file
        roi                 : List of indices for the vertices inside the region of interest.
                              Only these vertices will be loaded.

        """

        with open(filepath) as f:
            content = f.read().splitlines()

        # If we're not being given a ROI, first count the number of vertices
        if roi is None:
            n = 0
            for line in content:
                if len(line) == 0:
                    continue

                if line[0] == 'v' and line[1] == ' ':
                    n += 1
        else:
            n = len(roi)

        # Initialize array and get coordinates
        npvertices = np.zeros((n, 3))
        totallines = len(content)

        v_ind = 0
        ri = 0
        curr_roi_val = roi[ri] if roi is not None else None
        prev_roi_val = None         # this will be for checking that roi indices are sorted

        if roi is not None:
            # We have a ROI: iterate through all vertices
            # and only keep those which are in the ROI list
            for linenum in range(totallines):

                line = content[linenum].strip()

                if len(line) == 0:
                    continue

                if line[0] == 'v' and line[1] == ' ':
                    # Check if this is belongs to the ROI
                    if v_ind != curr_roi_val:
                        v_ind += 1
                        continue

                    # It does...
                    point = self._get_coordinates_from_string(line)
                    npvertices[ri, :] = point
                    
                    # Move to next roi index, and check that list was indeed sorted
                    ri += 1

                    if ri == len(roi):
                        break

                    prev_roi_val = curr_roi_val
                    curr_roi_val = roi[ri]

                    if (prev_roi_val > curr_roi_val):
                        raise Exception("Cannot load sequence: ROI indices must be sorted")

                    v_ind += 1
        else:
            # We don't have a ROI: just iterate lines
            for linenum in range(totallines):

                line = content[linenum].strip()
                
                if len(line) == 0:
                    continue

                if line[0] == 'v' and line[1] == ' ':
                    point = self._get_coordinates_from_string(line)
                    npvertices[v_ind, :] = point
                    v_ind += 1

        return npvertices

    def _get_coordinates_from_string(self, line):
        str_coords = line.split(" ")
        num_coords = 0
        point = np.zeros(3)

        for coord in str_coords:
            if len(coord) == 0:
                continue

            try:
                point[num_coords] = float(coord)
                num_coords += 1
            except Exception:
                continue

            if num_coords == 3:
                break

        if num_coords != 3:
            raise Exception("Vertex coordinates could not be loaded.")

        return point
