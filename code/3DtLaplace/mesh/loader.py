import os
from . import MeshSequence
from .fileloaders import OFFLoader, OBJLoader

# -----------------------------------------------------------------------------------------------
# Mesh Reader / Writer
def load_mesh_from_file(filepath, roi=None):
    """
    Create new mesh with the contents of the .off file.

    Arguments
    ---------

    filepath            : Path to the mesh file (supports OFF and OBJ formats)
    time_coord          : Time coordinate for this mesh
    roi                 : Region of Interest (list of vertex indices to load).
                          If not None, then only these vertices will be loaded, as well as
                          the faces which only include the roi's vertices

    """
    filename, file_extension = os.path.splitext(filepath)

    if file_extension == '.off':
        floader = OFFLoader()
    elif file_extension == '.obj':
        floader = OBJLoader()

    mesh = floader.load_mesh(filepath, roi)
    return mesh

def read_vertex_coordinates(filepath, roi=None):
    """
    Returns a numpy array of size n*3, with n the number of vertices,
    containing the 3D coordinates of the mesh.

    Arguments
    ---------

    file_path           : Path to a mesh file (off and obj supported for now)
    roi                 : Region of Interest (list of vertex indices to load).
                          If not None, then only these vertices will be loaded, as well as
                          the faces which only include the roi's vertices

    """
    filename, file_extension = os.path.splitext(filepath)

    if file_extension.upper() == '.OFF':
        floader = OFFLoader()
    elif file_extension.upper() == '.OBJ':
        floader = OBJLoader()

    mesh = floader.read_vertex_coordinates(filepath, roi)
    return mesh

def save_mesh_to_file(mesh, file_path, coords=None):
    """
    Save mesh object in new .off file.

    Arguments
    ---------

    mesh                : Mesh to save.
    file_path           : Path to the .off file where the mesh will be saved.

    """
    
    f = open(file_path, 'w')
    f.write("OFF\n")

    vertexnum = len(mesh.vertices)
    facenum = len(mesh.faces)

    f.write(str(vertexnum)+ " " + str(facenum) + " 0" + '\n' )

    for i in range(vertexnum):
        if coords is None:
            p = mesh.vertices[i].point
        else:
            p = coords[i, :]

        strp1 = str.format("{0:.12f}", p[0])
        strp2 = str.format("{0:.12f}", p[1])
        strp3 = str.format("{0:.12f}", p[2])

        f.write(strp1 + " " + strp2 + " " + strp3 + '\n' )

    # Prepare a list of the face's vertices
    for i in range(facenum):
        p1 = mesh.faces[i].edges[0].vertex_from.index
        p2 = mesh.faces[i].edges[1].vertex_from.index
        p3 = mesh.faces[i].edges[2].vertex_from.index

        f.write("3 " + str(p1) + " " + str(p2) + " " + str(p3) + '\n' )

    f.close()

def save_with_colors(mesh, savefile_path, vertices_to_color, color):
    """
    Save mesh into .off file, adding color information to the faces associated with the specified vertices.

    Arguments
    ---------

    mesh                : Mesh to save
    savefile_path       : Path to the .off file where the mesh will be saved.
    vertices_to_color   : List of indices of vertices.
    color               : 3-tuple of floats, each in range (0- 255)

    """

    f = open(savefile_path, 'w')
    f.write("COFF\n")

    num_vertices = len(mesh.vertices)
    num_faces = len(mesh.faces)

    f.write(str(num_vertices)+ " " + str(num_faces) + " 0" + '\n' )

    for i in range(num_vertices):
        p = mesh.vertices[i].point

        strp1 = str.format("{0:.12f}", p[0])
        strp2 = str.format("{0:.12f}", p[1])
        strp3 = str.format("{0:.12f}", p[2])

        f.write(strp1 + " " + strp2 + " " + strp3)
        
        if mesh.vertices[i].index in vertices_to_color:
            f.write(" " + str(color[0]) + " " + str(color[1]) + " " + str(color[2]) + " 255")
        else:
            f.write(" 185 185 185 255")

        f.write("\n")

    # Prepare a list of the face's vertices
    for i in range(num_faces):
        p1 = mesh.faces[i].edges[0].vertex_from.index
        p2 = mesh.faces[i].edges[1].vertex_from.index
        p3 = mesh.faces[i].edges[2].vertex_from.index

        f.write("3 " + str(p1) + " " + str(p2) + " " + str(p3) + "\n")

    f.close()

# ---------------
# Mesh Sequences
# ---------------

def load_sequence(folder_path, t0, dt, window_start=None, window_end=None, roi=None):
    """
    Load mesh sequence by reading all .off files from a folder.
    The files will be loaded in lexicographical order.

    Arguments
    ---------

    folder_path     : Path to the folder where the .off files are.
    t0              : Time coordinate for the first sequence.
    dt              : Time difference between succesive meshes.
    window_start    : Index of frame that marks the beginning of the temporal window.
                      If None, the window begins at the beginning of the sequence.
    window_end      : Index of frame that marks the end of the temporal window (included).
                      If None, the window ends at the end of the sequence.
    roi             : Region of Interest (list of vertex indices to load).
                      If not None, then only these vertices will be loaded, as well as
                      the faces which only include the roi's vertices

    """

    # Get file names and sort
    filenames = []
    extension = ".off"

    for f in os.listdir(folder_path):
        if f.endswith(extension):
            filenames.append(f)

    if len(filenames) == 0:
        extension = ".obj"
        for f in os.listdir(folder_path):
            if f.endswith(extension):
                filenames.append(f)

    filenames = sorted(filenames)

    # Load meshes
    seq = MeshSequence(t0, dt)
    first = True
    num_frame = 0

    for f in filenames:
        if f.endswith(extension):
            if (window_start is None or num_frame >= window_start) and (window_end is None or num_frame <= window_end):
                if first:
                    first = False
                    m_file = folder_path + "/" + f
                    m = load_mesh_from_file(m_file, roi)
                    
                    seq.set_topology(m)
                    seq.topology_file = m_file

                coords = read_vertex_coordinates(folder_path + "/" + f, roi)
                seq.add_frame_coordinates(coords)
            num_frame += 1

    return seq

def load_static_sequence(filepath, numframes, t0, dt, roi=None):
    """
    Load static mesh animation from one single off file.

    Arguments
    ---------

    filepath        : Path fot the off file.
    numframes       : Number of frames the sequence will have.
    t0              : Time coordinate for the first sequence.
    dt              : Time difference between succesive meshes.
    roi             : Region of Interest (list of vertex indices to load).
                      If not None, then only these vertices will be loaded, as well as
                      the faces which only include the roi's vertices

    """

    seq = MeshSequence(t0, dt)
    
    m = load_mesh_from_file(filepath, roi)
    seq.set_topology(m)
    seq.topology_file = filepath

    for i in range(numframes):
        seq.add_new_frame_mesh(m)

    return seq

def save_sequence_points(faces, coordinates, num_frames, num_vertices, folder_path):
    """
    TODO.
    """

    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)

    # TODO: there is no real need to construct a full mesh for this...
    for k in range(num_frames):
        # Create mesh file
        filename = '{0:05d}'.format(k+1) + ".off"
        full_path = folder_path + "/" + filename

        f = open(full_path, 'w')

        # TODO: should have offwriter class...
        f.write("OFF\n")
        f.write(str(num_vertices)+ " " + str(len(faces)) + " 0" + '\n' )

        # Save vertex coordinates
        for i in range(num_vertices):
            p = coordinates[:, k*num_vertices + i]

            strp1 = str.format("{0:.12f}", p[0])
            strp2 = str.format("{0:.12f}", p[1])
            strp3 = str.format("{0:.12f}", p[2])

            f.write(strp1 + " " + strp2 + " " + strp3 + '\n' )

        # Save face indices
        for face in faces:
            f.write("3 " + str(face[0]) + " " + str(face[1]) + " " + str(face[2]) + '\n' )

        f.close()
    
def save_sequence(mesh_sequence, folder_path):
    """
    Save sequence of meshes as off files, in the specified path.

    Arguments
    ---------

    mesh_sequence       : List of Mesh object.
    folder_path         : Path to the folder where the off files  will be saved.
                          The folder must already exist. 
    """

    if not os.path.isdir(folder_path):
        os.makedirs(folder_path)

    # TODO: there is no real need to construct a full mesh for this...
    m = mesh_sequence.topology

    for f in range(mesh_sequence.num_frames):
        
        # Update 3D coordinates
        for i in range(mesh_sequence.num_vertices):
            
            for c in range(3):
                m.vertices[i].point[c] = mesh_sequence.coordinates[f][i, c]
        
        filename = '{0:05d}'.format(f+1) + ".off"                 # number of mesh with leading zeros (5 posititons)
        full_path = folder_path + "/" + filename
        save_mesh_to_file(m, full_path)


# -------------------------------------------------------------------------------------------------------------
# Exceptions
# -------------------------------------------------------------------------------------------------------------
class MeshLoadException(Exception):
    pass