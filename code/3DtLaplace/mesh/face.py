import numpy as np
import math

class Face:
    def __init__(self, edge1, edge2, edge3):
        self.edges = [None] * 3
        self.edges[0] = edge1
        self.edges[1] = edge2
        self.edges[2] = edge3

        # Associate this face to the added edges
        self.edges[0].face = self
        self.edges[1].face = self
        self.edges[2].face = self

    def get_vertex_indices(self):
        """
        Returns three indices, one for each vertex in the face.
        """

        return self.edges[0].vertex_from.index, self.edges[1].vertex_from.index, self.edges[2].vertex_from.index

    def get_area(self):
        v1 = self.edges[0].vertex_from.point[:3]
        v2 = self.edges[1].vertex_from.point[:3]
        v3 = self.edges[2].vertex_from.point[:3]

        e1 = v2 - v1
        e2 = v3 - v1

        return 0.5 * math.sqrt(\
           (e1[1]*e2[2] - e1[2]*e2[1]) ** 2 + \
           (e1[0]*e2[2] - e1[2]*e2[0]) ** 2 + \
           (e1[0]*e2[1] - e1[1]*e2[0]) ** 2)

    def get_opposite_angle(self, edge):
        """
        TODO
        """

        e1 = None
        e2 = None
        extra = [0, 1, 2]

        for i in range(3):
            if self.edges[i] == edge:
                extra.remove(i)
                e1 = self.edges[extra[0]]
                e2 = self.edges[extra[1]]

        # considering orientation...
        e1_point = e1.vertex_from.point - e1.vertex_to.point
        e2_point = e2.vertex_to.point - e2.vertex_from.point

        a = np.dot(e1_point, e2_point) / (np.linalg.norm(e1_point) * np.linalg.norm(e2_point))
        return np.arccos(a)

    def __str__(self):
        return str(self.edges[0].vertex_from.point) + " -> " + str(self.edges[0].vertex_from.point) + " -> " + str(self.edges[0].vertex_from.point)