"""
Represents a temporally coherent sequence of meshes.
"""

from .mesh import Mesh
import numpy as np
import math

class MeshSequence:

    def __init__(self, t0, dt):
        """
        Initialize mesh sequence with initial time t0, 
        and frame interval dt

        Arguments
        ---------

        t0                  : Time value for the first frame.
        dt                  : Interval value between subsequent frames.
        
        """
        self.t0 = t0
        self.dt = dt
        
        # Initialize rest of components
        self.num_vertices = 0
        self.num_frames = 0
        self.coordinates = []              # List of numpy arrays, with coordinates for each frame
        self.topology = None               # First mesh. The entire structure will be saved for the first mesh.
        
        self.min_x = None
        self.max_x = None
        self.min_y = None
        self.max_y = None
        self.min_z = None
        self.max_z = None

    def set_topology(self, mesh):
        """
        Add mesh structure that will be used to query on topology information.
        Coordinates information will not be taken into account.
        """

        self.topology = mesh
        self.num_vertices = len(self.topology.vertices)

    def add_frame_coordinates(self, mesh_coords):
        """
        Add new frame to sequence with the specified list of coordinates.

        Arguments
        ---------

        mesh_coords             : Numpy array of size n*3, with n the number of vertices.
                                  Temporal coordinate must not be included.
                          
        """

        # Copy coordinates... (else doesn't work with static sequences)
        n = self.num_vertices
        points = np.zeros((n, 3), dtype=np.double)
        pind = 0

        for i in range(n):
            for c in range(3):
                points[pind, c] = mesh_coords[i, c]

            pind += 1

        self.coordinates.append(points)
        self.num_frames += 1

    def add_new_frame_mesh(self, mesh):
        """
        Append mesh as a new frame in the sequence.
        TODO: not checking if it's temporally coherent..
        """

        # Copy coordinates... (else doesn't work with static sequences)
        points = np.zeros((self.num_vertices, 3), dtype=np.double)
        pind = 0

        for i in range(self.num_vertices):
            for c in range(3):
                points[pind, c] = mesh.vertices[i].point[c]

            pind += 1

        self.coordinates.append(points)
        self.num_frames += 1

    # ---------------------------------------------------------------------------------
    def get_coordinates(self, frame_ind, vertex_ind, dimension, copy=False):
        """
        Get the spatial coordinates for a vertex in the specified frame.

        Arguments
        ---------

        frame_ind                   : Number of frame.
        vertex_ind                  : Index of vertex.
        dimension                   : Get coordinates in either three or four dimensions.
        copy                        : If true, a copy of the point will be return. 
                                      Else the modification of this point will modify the value
                                      in the sequence. 
                                      Only works when asking for 3D coordinates.

        """

        if dimension == 3 and not copy:
            return self.coordinates[frame_ind][vertex_ind, :]
        else:
            point = np.zeros(dimension, dtype=np.double)

            for i in range(3):
                point[i] = self.coordinates[frame_ind][vertex_ind, i]

            if dimension == 4:
                point[3] = self.t0 + frame_ind * self.dt

            return point

    def get_temporal_coordinate(self, frame):
        """
        Get temporal coordinate for the specified frame.

        Arguments
        ---------

        frame               : Index of frame whose temporal coordinate we want to know
                              (zero-based)

        """
        return self.t0 + (frame * self.dt)