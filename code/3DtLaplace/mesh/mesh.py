import sys
import math
import numpy as np
from scipy import sparse
from . import Vertex, Edge, Face

class Mesh:
    
    def __init__(self):
        """
        Create a new empty Mesh
        """
        self.vertices = []
        self.edges = []  

        self.num_vertices = 0
        self._adj_matrix = None

        # TODO: mixing old and new version for testing, merge!
        self.faces_inds = []
        self.faces = []
        self.num_faces = 0

        # TODO: comment on this and add methods to access / load it!!
        # Maps between index of vertices in ROI and in entire mesh
        self.real2roi_idx = None

    def num_vertices(self):
        return len(self.vertices)

    def make_adjacency_matrix(self):
        """
        Create the adjacency matrix for this mesh.
        Position (i,j) will contain the index of the Edge that connects the i-th vertex with the j-th vertex, 
        plus one (+ 1 because an entry of 0 should mean that there is no edge between vertex i and vertex j)
        """

        # The result will be a sparse matrix in lil format, since the most frequent use will be
        # to iterate through the non-zero rows (neighbours of vertex i).
        n = len(self.vertices)
        self._adj_matrix = sparse.lil_matrix((n, n), dtype=np.int32)

        # Loop through all of the mesh's edges
        for e in range(len(self.edges)):
            curr_edge = self.edges[e]
            v_i = curr_edge.vertex_from.index
            v_j = curr_edge.vertex_to.index

            # It's a symmetric matrix...
            self._adj_matrix[v_i, v_j] = curr_edge.index + 1      
            self._adj_matrix[v_j, v_i] = curr_edge.index + 1

    def get_incident_edges(self, vertex_index):
        """
        Returns sequence of indices for the edges that connect 
        the specified vertex with its neighbours.

        Arguments
        ---------

        vertex_index            : Index of desired vertex in the mesh's list of Vertices
        """

        # getrowview(i) returns a view (not copy) of the i-th row (AdjacencyMatrix must be of lil format)
        # data returns all non-zero values on that row as a 1*m matrix, which is why we take data[0]
        if self._adj_matrix is None:
            self.make_adjacency_matrix()

        return self._adj_matrix.getrowview(vertex_index).data[0]

    def get_neighbouring_vertices(self, vertex_index):
        """
        Returns sequence of indices for the vertices in the 1-ring neighbourhood 
        of the specified vertex.

        Arguments
        ---------

        vertex_index            : Index of vertex whose neighbours we want.
        """

        # returns indices of non-zero columns in adjacency matrix
        # (attribute rows contains column indices for data as a matrix, which is why we need to do rows[0])
        if self._adj_matrix is None:
            self.make_adjacency_matrix()
            
        return self._adj_matrix.getrowview(vertex_index).rows[0]

    def update_coordinates(self, new_coords):
        """
        Modify all vertex coordinates.

        Arguments
        ---------

        new_coords          : numpy matrix of size n * 3, with n the number
                              of vertices of the mesh.
        """

        for i in range(len(self.vertices)):
            for c in range(3):
                self.vertices[i].point[c] = new_coords[i, c]

    def add_vertex(self, point):
        """
        Add vertex to this mesh. 

        Arguments
        ---------

        point               : numpy array of size 3.
        """

        v_ind = len(self.vertices)
        v = Vertex(point, v_ind)
        self.vertices.append(v)
        return v

    def add_edge(self, v1_index, v2_index):
        v1 = self.vertices[v1_index]
        v2 = self.vertices[v2_index]
        e_ind = len(self.edges)

        e = Edge(v1, v2, e_ind)
        self.edges.append(e)
        return e

    def add_face(self, v1_index, v2_index, v3_index):
        # TODO: mixing old and new code!
        self.faces_inds.append([v1_index, v2_index, v3_index])

        e1 = self.add_edge(v1_index, v2_index)
        e2 = self.add_edge(v2_index, v3_index)
        e3 = self.add_edge(v3_index, v1_index)

        f = Face(e1, e2, e3)
        self.faces.append(f)
        self.num_faces += 1

        return f