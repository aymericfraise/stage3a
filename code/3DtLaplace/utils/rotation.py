import numpy as np
import math

class Rotation:
    def __init__(self, angle, axis):
        self.angle_deg = angle
        self.angle_rad = math.radians(angle)
        self.axis = axis

        if not axis == "x" and not axis == "y" and not axis == "z":
            raise Exception("Invalid rotation axis")

        self._rot  = None

    def apply(self, point):
        """
        Apply rotation to the specified 3D point.

        Arguments
        ---------

        point           : Three-dimensional numpy array.
        """

        if self._rot is None:
            if self.axis == "x":
                axis = 0
            elif self.axis == "y":
                axis = 1
            elif self.axis == "z":
                axis = 2
        
            # Build rotation matrix
            cosval = math.cos(self.angle_rad)
            sinval = math.sin(self.angle_rad)
            self._rot = np.zeros((3, 3))
            
            for i in range(3):
                self._rot[i, i] = cosval if i != axis else 1

            # TODO: there are better ways...
            if axis == 0:
                self._rot[1, 2] = -sinval
                self._rot[2, 1] = sinval

            elif axis == 1:
                self._rot[0, 2] = sinval
                self._rot[2, 0] = -sinval            

            else:
                self._rot[0, 1] = -sinval
                self._rot[1, 0] = sinval

        return np.dot(self._rot, point)