#!/bin/sh
# cython: for scikits.sparse
DEP_LIST = "python-numpy python-scipy libsuitesparse-dev"
apt-get update
apt-get install -y $DEP_LIST
pip install scikits.sparse