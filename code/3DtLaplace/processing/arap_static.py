"""
Static editing with the ARAP framework.
"""
import sys
import os
import numpy as np

sys.path.insert(0, os.path.abspath('..'))
from mesh import loader
from .laplacian_cot import LaplacianCot

class ARAPStaticEditor:

    def __init__(self):
        # Initialize Laplacian matrix
        self.l = LaplacianCot()

        # Default values
        self._stop_max_iterations   = 1000
        self._stop_max_error        = 0.001
        self._sr_arap_alpha          = 0.001
        self._mesh_area  = None
        self._rotations  = None

    def add_constraint(self, vertex_id, target_point, weight):
        self.l.add_soft_constraint(vertex_id, weight, target_point)

    def set_max_iterations(self, max_iterations):
        self._stop_max_iterations = max_iterations

    def set_max_error(self, max_error):
        self._stop_max_error = max_error

    def edit(self, mesh, use_sr_arap=False, its_folder=None):
        """
        Edit mesh in a as-rigid-as-possible way.

        Arguments
        ---------

        mesh                : Mesh to edit. This object will be modified.
        use_sr_arap         : If True, use SR-ARAP formulation.
        its_folder          : If not None, save intermediate results in this folder.
        """

        # Build Laplacian matrix
        self.l.build(mesh)
        self.num_vertices = len(mesh.vertices)
        self._rotations = [None] * self.num_vertices

        if use_sr_arap:
            self._mesh_area = 0.0

            for f in range(len(mesh.faces)):
                self._mesh_area += mesh.faces[f].get_area()

        # Store original point values
        self._new_points = np.zeros((3, self.num_vertices), dtype=np.double)

        for i in range(self.num_vertices):
            self._new_points[:, i] = mesh.vertices[i].point

        # Calculate matrix of vertex differences for all vertices (P_i in paper).
        # self.P will be a vector of length n, with n the number of vertices.
        # Each entry P[i] will contain a 3 * N(i) matrix with the coordinates of the 
        # difference (p_i - p_j), for each *spatial* neighbour p_j.
        self.P = self._create_diff_matrix()

        Rs = None
        it = 0
        changing = True
        b = np.zeros((self.num_vertices, 3))

        while it < self._stop_max_iterations and changing:
            
            # Compute right hand side of the system
            # (initial guess: no rotations)
            use_rotation = (it > 0)
            
            if use_rotation:
                Rs = self._estimate_rotations(use_sr_arap)

            for i in range(self.num_vertices):                                  # v: vertex index according to mesh   

                weights, ind = self.l.get_spatial_neighbours(i, include_diagonal=True)
                nneigh = 0
                b[i, :] = 0.0

                for j in range(len(weights)):                          # j : number of neighbour | ind[j] : index of this neighbour in the roi
                    if ind[j] == i:     # diagonal
                        continue

                    w       = -weights[j]
                    pi_pj   = self.P[i][:, nneigh]

                    if use_rotation:
                        R = Rs[i] + Rs[ ind[j] ]                      # R_i + R_j
                        b[i, :] += 0.5 * w * np.dot(R, pi_pj)
                    else:
                        b[i, :] += w * pi_pj

                    nneigh += 1

            # Add constraints to b
            new_points = self.l.solve_lstsq(b)
            
            # Update sequence
            max_diff = 0.0
            p_ind = 0

            for j in range(self.num_vertices):
                # Check convergence
                old_point = mesh.vertices[j].point
                new_point = new_points[j, :]
                curr_diff = self._get_iteration_diff(old_point, new_point)

                if curr_diff > max_diff:
                    max_diff = curr_diff

                # Update values
                mesh.vertices[j].point = new_point
                self._new_points[:, j] = new_point
                p_ind += 1

            it += 1

            # ------------------------------------------
            if its_folder is not None:
                meshpath = its_folder + "/iteration-" + str(it-1) + ".off"
                loader.save_mesh_to_file(mesh, meshpath)
            # ------------------------------------------

            # Check if converged
            changing = (max_diff > self._stop_max_error)

    # ------------------------------------------------------------------------------------------------
    # PRIVATE

    def _create_diff_matrix(self, transposed=False):
        # Create matrix with coordinates of (p_i - p_j), for each vertex i and neighbour j of i.
        # Each element on the list will have a numpy matrix of size |3 x N(v_i)|, 
        # with the coordinates of the difference in each column.

        result = [None] * self.num_vertices
            
        for i in range(self.num_vertices):

            p_i = self._new_points[:, i]

            # Get vertex's spatial neighbours
            weights, neigh_ind = self.l.get_spatial_neighbours(i, include_diagonal=True)
            total_neighs = len(neigh_ind) - 1           # -1 for diagonal element

            if not transposed:
                result[i] = np.zeros((3, total_neighs), dtype=np.double)
            else:
                result[i] = np.zeros((total_neighs, 3), dtype=np.double)

            nneigh = 0
            for j in range(len(neigh_ind)):
                vj = neigh_ind[j]
                
                if vj == i:     # diagonal
                    continue

                p_j = self._new_points[:, vj]
                diff = p_i - p_j

                if not transposed:
                    result[i][:, nneigh] = diff
                else:
                    result[i][nneigh, :] = diff

                nneigh += 1

        return result

    def _estimate_rotations(self, sr_arap):
        """
        Rotation calculation according to SR-ARAP energy.
        """

        if self._rotations is None:
            # As initial values for rotations we'll estimate them the normal way...
            self._rotations = [None] * self.num_vertices

        # Edges for result of previous iteration
        Pp = self._create_diff_matrix(transposed=True)

        for ii in range(self.num_vertices):
            i = self.num_vertices - ii - 1

            # --- ARAP rotation calculation --
            # Get array with weights for all neighbours of the vertex (D)
            weights, inds = self.l.get_spatial_neighbours(i, include_diagonal=False)
            weights       *= (-1)         # using positive weights. Not changing original matrix because we received a copy...

            # Create covariance matrix S (S = PDPt)
            # We will treat the weights array as a diagonal matrix (broadcasting - we need numpy arrays)    
            left_mul    = np.asarray(self.P[i]) * np.asarray(weights)     # asarray() for correct broadcasting

            if not sr_arap:
                PDPt = np.dot(left_mul, Pp[i])
            else:
                PDPt = 2 * np.dot(left_mul, Pp[i])

                # Add SR-ARAP term: + 4 \alpha A \sum w_{kl} R_l^T
                sr_rot      = np.zeros_like(PDPt)

                # ind contains indices for i's neighbours
                for j in range(len(inds)):
                    Rl = self._rotations[inds[j]]

                    if Rl is None:
                        Rl = np.identity(3)

                    wl = weights[j]
                    sr_rot += (wl * Rl.transpose())

                PDPt += (4 * self._sr_arap_alpha * self._mesh_area) * sr_rot

            # Find the SVD of S = UDV'
            U, s, Vt    = np.linalg.svd(PDPt, full_matrices = True)
            Ut          = np.transpose(U)
            V           = np.transpose(Vt)

            # Create rotation matrix R = VU'
            self._rotations[i] = np.dot(V, Ut)
            
            if np.linalg.det(self._rotations[i]) < 0:
                Ut[2,:] *= -1               # change sign of last column in U
                self._rotations[i] = np.dot(V, Ut)

        return self._rotations

    def _get_iteration_diff(self, old_point, new_point):
        old = np.linalg.norm(old_point, ord=2)
        new = np.linalg.norm(new_point, ord=2)
        absdiff = np.linalg.norm(old_point - new_point, ord=2)
        
        if old > 0 or new > 0:
            curr_diff = absdiff / (max(old, new))
        else:
            curr_diff = 0.0

        return curr_diff