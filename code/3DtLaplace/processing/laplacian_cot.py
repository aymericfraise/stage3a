"""
Cotangent weights Laplacian.
TODO: inherit from base class common to both laplacians... (check repeated code)
"""

import math 
import scipy
import numpy as np
from . import volumes
from .laplacian_base import Laplacian

TOLERANCE = 0.0000001

class LaplacianCot(Laplacian):

    def __init__(self):
        Laplacian.__init__(self)
        self.divide_by_dual_volume = True

    def build(self, mesh):
        self.mesh = mesh

        n = len(mesh.vertices)
        self.size = n
        self.L = scipy.sparse.dok_matrix((n,n), dtype=np.double)
            
        for i in range(n):
            vtx = mesh.vertices[i].point[:3]
            neighs = mesh.get_incident_edges(i)

            for eIndex in neighs:
                edge = mesh.edges[eIndex - 1]
                vtx_j = None

                # p_j could be in any of the edge's vertices
                if  edge.vertex_from.point[0] == vtx[0] and \
                    edge.vertex_from.point[1] == vtx[1] and \
                    edge.vertex_from.point[2] == vtx[2]:
                    vtx_j = edge.vertex_to
                else:
                    vtx_j = edge.vertex_from
                
                p_j = vtx_j.point
                alpha = edge.face.get_opposite_angle(edge)
                
                if abs(alpha) < TOLERANCE or abs(abs(alpha) - math.pi/2) < 0.0001:
                    cotAlpha = 0.0
                else:
                    cotAlpha = 1.0 / (math.tan(alpha) + 0.0000001)

                if edge.opposite is None:
                    cotBeta = 0.0
                else:
                    beta = edge.opposite.face.get_opposite_angle(edge.opposite)
                    
                    if abs(beta) < TOLERANCE or abs(abs(beta) - math.pi/2) < 0.0001:
                        cotBeta = 0.0
                    else:
                        cotBeta = 1.0 / (math.tan(beta) + 0.0000001)

                weight = -(cotAlpha + cotBeta)

                # ugly fix for negative weights...
                if weight > 0:
                    weight = 0.0

                if not self.divide_by_dual_volume:
                    weight *= 0.5
                
                # Save weight
                j = vtx_j.index
                self._add_off_diagonal_entry(i, j, weight)

        if self.divide_by_dual_volume:
            # Divide by the volume of the 3D dual of each vertex
            # (using barycentric coordinates because it's simpler...)
            vols = np.zeros(n, dtype=np.double)

            for face in mesh.faces_inds:
                v1 = mesh.vertices[face[0]].point
                v2 = mesh.vertices[face[1]].point
                v3 = mesh.vertices[face[2]].point

                face_points = (v1, v2, v3)
                full_vol = volumes.get_area_triangle(face_points)

                for vi in range(3):
                    vols[ face[vi] ] += full_vol / 3.0

            # Iterate non-zero elements and divide them by square root of volumes.
            for (row, col), val in self.L.items():
                
                if row == col:
                    continue

                newval = 2.0 * val
                newval /= (math.sqrt(vols[row] * vols[col]))
                self.L._update( {(row, col) : newval} )

        # Set diagonal elements
        self._add_diagonal_values()

        # Finally, convert to csc format
        self.L = self.L.tocsc()

    def get_spatial_neighbours(self, vertex_id, include_diagonal = False):
        """
        Returns a set of indices for the neighbours of vertex_id, along
        with a set of the same size with the weights associated with each
        neighbour.
        """

        # Get all values in column vtx_idx of Ls(k)
        # NOTE: we are counting on the fact that L is in CSC format
        indptr = self.L.indptr
        data_1 = self.L.data[ indptr[vertex_id] : indptr[vertex_id + 1] ]
        indices_1 = self.L.indices[ indptr[vertex_id] : indptr[vertex_id + 1] ]

        # Remove diagonal element
        if include_diagonal:
            data = data_1
            indices = indices_1
        else:
            res_size = len(data_1) - 1
            data = np.zeros(res_size, dtype=np.double)
            indices = np.zeros(res_size, dtype=np.int32)

            pos = 0
            for i in range(len(data_1)):
                if indices_1[i] == vertex_id:
                    continue

                data[pos] = data_1[i]
                indices[pos] = indices_1[i]   
                pos += 1
        
        if len(indices) == 0:
            raise Exception("No neighbours found for vertex" + str(vertex_id))

        return data, indices

    def solve_build_full_matrix(self):

        num_constraints = len(list(self._constraints.keys()))
        anchor_rows = scipy.sparse.dok_matrix((self.size, num_constraints), dtype=np.double)
        
        j = 0

        for vertex_id, (weight, target) in list(self._constraints.items()):
            anchor_rows[vertex_id, j] = weight
            j += 1

        # TODO: very inefficient!
        self._full_Lt = scipy.sparse.hstack([self.L, anchor_rows], format="csc")

    # ------------------------------------------------------------------------------------
    # AUX
    def _add_off_diagonal_entry(self, i, j, value):
        """
        Add entry to the laplacian matrix (DOK format)
        """
        if (i, j) not in self.L:
            self.L._update( {(i, j) : value} )
            self.L._update( {(j, i) : value} )

    def _add_diagonal_values(self):
        """
        Put negated sum of off-diagonal values into the diagonal
        entry of the row.

        The matrix must be in DOK format.
        """
        # initialize values
        for i in range(self.size):
            self.L._update( {(i, i) : 0.0} )

        # accumulate sum by iterating non-zero elements
        for (row, col), val in self.L.items():
            
            if row == col: 
                continue
            
            self._sparse_add_value(row, row, -val)

    def _sparse_add_value(self, row, col, value_to_add):
        """
        Do the equivalent of L[i, j] += value_to_add.
        (assuming L matrix in DOK format)
        """
        if (row, col) in self.L:
            oldval = self.L.get((row, col), 0)
            newval = oldval + value_to_add
        else:
            newval = value_to_add
            
        self.L._update( {(row, col) : newval} )