"""
3D+t laplacian operator with uniform weights
"""

from .laplacian_3dt import Laplacian3Dt

class Laplacian3Dt_Comb(Laplacian3Dt):

    def __init__(self, alpha, num_frames, num_vertices):
        Laplacian3Dt.__init__(self, alpha, num_frames, num_vertices)

    def build(self, mesh_sequence):
        nfaces = mesh_sequence.topology.num_faces
        self._init_sparse_blocks()

        # D(k) blocks
        for k in range(self._f-1):
            for i in range(self._n):
                self._Ds[k][i] = -1

        # Loop faces
        for f_ind in range(nfaces):

            face = mesh_sequence.topology.faces_inds[f_ind]

            for k in range(self._f):

                for i in range(3):
                    v1 = face[i]
                    v2 = face[(i+1) % 3]

                    # Off-diagonal elements                    
                    if (v1, v2) not in self._Ls[k]:
                        self._Ls[k]._update( {(v1, v2) : -1.0} )
                        self._Ls[k]._update( {(v2, v1) : -1.0} )

        # L(k) diagonal elements
        for k in range(self._f):
            # initialize values
            for i in range(self._n):
                self._Ls[k]._update( {(i, i) : 0.0} )

            # accumulate sum by iterating non-zero elements
            for (row, col), val in self._Ls[k].items():
                
                if row == col: 
                    continue
                
                self._init_Lk_add_value(k, row, row, val)

            # finally, add D(k) values and negate sums
            for i in range(self._n):
                if k > 0 and k < self._f -1:
                    self._init_Lk_add_value(k, i, i, -2)
                else:
                    self._init_Lk_add_value(k, i, i, -1)

                oldval = self._Ls[k].get((i, i))
                self._Ls[k]._update( {(i, i) : -oldval} )

        # Convert to format for efficient matrix operations
        self._convert_sparse_blocks()