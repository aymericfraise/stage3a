"""
3D+t laplacian operator for temporal mesh sequences
"""

import sys
import os
import math

from scipy import sparse
import numpy as np

from .laplacian_base import Laplacian
from . import volumes

# saving
import pickle
import pathlib

# printing
from tqdm import tqdm

class Laplacian3Dt(Laplacian):

    # ---------------------------------------------------------------------------------
    # Initialization Methods

    def __init__(self, alpha, num_frames, num_vertices):
        """ 
        Create an empty Laplacian matrix, with the associated value for alpha.
        """

        Laplacian.__init__(self)

        self.alpha              = alpha
        self._f                 = num_frames
        self._n                 = num_vertices
        
        self._Ls                = None  # array of 2D sparse dok matrices
        self._Ds                = None  # array of 1D np arrays

    def build(self, mesh_sequence, quiet=False):
        """
        Build Laplacian matrix for the specified mesh sequence.
        """

        if self._n != mesh_sequence.num_vertices:
            raise Exception("Invalid number of vertices.")

        if self._f != mesh_sequence.num_frames:
            raise Exception("The sequence has an invalid number of frames.")


        # TODO: build directly in csc format
        self._init_sparse_blocks()

        # -----------------------------------
        # (1) Calculate all necessary volumes
        # -----------------------------------

        # These volumes will be stored directly in the L(k) and D(k)
        # matrices, and the next pass will be in charge of putting the 
        # correct coefficients in each place.
        t_prev, t_current, t_next = None, None, None
        nfaces = mesh_sequence.topology.num_faces

        # print("Computing volumes")
        for k in tqdm(range(self._f), desc='computing volumes', leave=False, disable=quiet):

            # Temporal coordinates for current, previous and next frame
            # (previous: always set at the end of the loop)
            if t_current is None:
                t_current = mesh_sequence.get_temporal_coordinate(k)
            
            if k < (self._f - 1):
                t_next = mesh_sequence.get_temporal_coordinate(k+1)

            if t_prev is not None:
                t_prev_mid = (t_prev + t_current) / 2.0

            if t_next is not None:
                t_next_mid = (t_current + t_next) / 2.0

            # Loop faces on this frame
            for f_ind in tqdm(range(nfaces), desc='faces', leave=False, disable=quiet):
                face = mesh_sequence.topology.faces_inds[f_ind]

                v1_ind = face[0]
                v2_ind = face[1]
                v3_ind = face[2]

                # Get coordinates for the points on this face, in the current frame
                v1 = mesh_sequence.get_coordinates(k, v1_ind, 3, copy=False)
                v2 = mesh_sequence.get_coordinates(k, v2_ind, 3, copy=False)
                v3 = mesh_sequence.get_coordinates(k, v3_ind, 3, copy=False)

                face_current_points = (v1, v2, v3)

                # Get coordinates for the points in the next and previous face,
                # and calculate the midpoints from here to them
                face_next_points = None
                face_prev_points = None

                if t_prev is not None:
                    # original points
                    v1_prev = mesh_sequence.get_coordinates(k-1, v1_ind, 3, copy=False)
                    v2_prev = mesh_sequence.get_coordinates(k-1, v2_ind, 3, copy=False)
                    v3_prev = mesh_sequence.get_coordinates(k-1, v3_ind, 3, copy=False)

                    # midpoints
                    v1_prev_mid = (v1_prev + v1) / 2.0
                    v2_prev_mid = (v2_prev + v2) / 2.0
                    v3_prev_mid = (v3_prev + v3) / 2.0

                    face_prev_points = (v1_prev, v2_prev, v3_prev)
                    face_prev_midpoints = (v1_prev_mid, v2_prev_mid, v3_prev_mid)

                if t_next is not None:
                    # original points
                    v1_next = mesh_sequence.get_coordinates(k+1, v1_ind, 3, copy=False)
                    v2_next = mesh_sequence.get_coordinates(k+1, v2_ind, 3, copy=False)
                    v3_next = mesh_sequence.get_coordinates(k+1, v3_ind, 3, copy=False)

                    # midpoints
                    v1_next_mid = (v1 + v1_next) / 2.0
                    v2_next_mid = (v2 + v2_next) / 2.0
                    v3_next_mid = (v3 + v3_next) / 2.0

                    face_next_midpoints = (v1_next_mid, v2_next_mid, v3_next_mid)
                    face_next_points = (v1_next, v2_next, v3_next)

                # ------------------------------
                # 1. Volumes for dual of vertices
                # ------------------------------

                # Get volume of 3-cell that goes from face_{k-1} to face_k
                # The volume of this big 3-cell contributes a "slice" to the 
                # volume of the dual of each vertex.
                if t_prev is not None:
                    vol_back = volumes.get_volume_temporal_3cell(t_prev_mid, t_current, face_prev_midpoints, face_current_points, self.alpha)
                    vol_slice = vol_back / 6.0

                    # for now we store the volume of the dual of each vertex 
                    # in the diagonal of the L(k) matrix
                    pk_pk1 = np.zeros(4)
                    pk_pk1[3] = t_current - t_prev_mid

                    for c in range(3):
                        v_ind = face[c]

                        pk_pk1[:3] = face_current_points[c] - face_prev_midpoints[c]
                        norm_pkpk1 = volumes.get_norm(pk_pk1, self.alpha)
                        self._init_Lk_add_value(k, v_ind, v_ind, vol_slice * norm_pkpk1)

                # Do the same for the 3-cell that goes from the current to the next face
                if t_next is not None:

                    vol_next = volumes.get_volume_temporal_3cell(t_current, t_next_mid, face_current_points, face_next_midpoints, self.alpha)
                    vol_slice = vol_next / 6.0

                    pk_pk1 = np.zeros(4)
                    pk_pk1[3] = t_current - t_next_mid

                    for c in range(3):
                        v_ind = face[c]

                        pk_pk1[:3] = face_current_points[c] - face_next_midpoints[c]
                        norm_pkpk1 = volumes.get_norm(pk_pk1, self.alpha)
                        self._init_Lk_add_value(k, v_ind, v_ind, vol_slice * norm_pkpk1)

                # -------------------------------------
                # 2. Volumes for dual of temporal edges
                # -------------------------------------

                # Volumes for temporal edges are saved in the D(k) matrices,
                # divided by the length of the full temporal edge.
                if t_next is not None:
                    # volume of temporal edge is the sum of the areas of triangles located in midpoint
                    vol = volumes.get_area_triangle(face_next_midpoints)
                    vtx_vol = vol / 3.0

                    # get length of temporal edges
                    temp_edge_1 = np.zeros(4, dtype=np.double)
                    temp_edge_2 = np.zeros(4, dtype=np.double)
                    temp_edge_3 = np.zeros(4, dtype=np.double)

                    temp_edge_1[:3] = v1 - v1_next
                    temp_edge_2[:3] = v2 - v2_next
                    temp_edge_3[:3] = v3 - v3_next

                    temp_edge_1[3] = t_current - t_next
                    temp_edge_2[3] = t_current - t_next
                    temp_edge_3[3] = t_current - t_next

                    v1_length = volumes.get_norm(temp_edge_1, self.alpha)
                    v2_length = volumes.get_norm(temp_edge_2, self.alpha)
                    v3_length = volumes.get_norm(temp_edge_3, self.alpha)

                    self._Ds[k][v1_ind] += (vtx_vol / v1_length)
                    self._Ds[k][v2_ind] += (vtx_vol / v2_length)
                    self._Ds[k][v3_ind] += (vtx_vol / v3_length)

                # -----------------------------------
                # 3. Volumes for dual of spatial edge
                # -----------------------------------

                # Get volume for the dual of each edge in this triangle, 
                # and store it in L(k)[i, j] and L(k)[j, i] (for edge between i and j)
                # Each spatial edge needs information from one or two faces, so we'll accumulate the sum
                
                # The dual of a spatial edge is a temporal quadrilateral made up
                # of these four points:
                #       (1) face's barycenter in time k
                #       (2) midpoint of edge in time k
                #       (3) midpoint between face's barycenter in time k and k + 1
                #       (4) midpoint of edge that goes from mipoint in time k and k + 1
                # Plus the same for time k and (k - 1) 
                
                face_barycenter_current = None
                face_barycenter_next = None
                face_barycenter_prev = None

                for fi in range(3):

                    # The two indices of the face that make the edge we're currently analyzing
                    ind1 = fi
                    ind2 = (fi + 1) % 3

                    # The indices of the vertices of this edge
                    v1_ind = face[ind1]
                    v2_ind = face[ind2]

                    v1 = face_current_points[ind1]
                    v2 = face_current_points[ind2]

                    vol_dual_edge = 0.0

                    # Each quadrilateral has two edges: one in the current frame, and the
                    # other half-way between the current frame and the following or previous one.
                    # The edge in the current frame is common for both quadrilaterals, so we calculate it first.
                    quad_edge_current = np.empty([2, 4])            # each row will be one point 
                    quad_edge_other   = np.empty([2, 4])

                    if face_barycenter_current is None:
                        face_barycenter_current = (face_current_points[0] + face_current_points[1] + face_current_points[2]) / 3.0

                    quad_edge_current[0, :3] = face_barycenter_current
                    quad_edge_current[1, :3] = (v1 + v2) / 2.0
                    quad_edge_current[:, 3]  = t_current 

                    # Get area for the quadrilateral between this and the previous frame (midpoint, actually)
                    if t_prev is not None:
                        if face_barycenter_prev is None:
                            face_barycenter_prev = (face_prev_midpoints[0] + face_prev_midpoints[1] + face_prev_midpoints[2]) / 3.0
                        
                        quad_edge_other[0, :3] = face_barycenter_prev
                        quad_edge_other[1, :3] = (face_prev_midpoints[ind1] + face_prev_midpoints[ind2]) / 2.0
                        quad_edge_other[:, 3]  = t_prev_mid
    
                        vol_dual_edge += volumes.get_area_temporal_quadrilateral(quad_edge_other, quad_edge_current, self.alpha)

                    # Get area for the quadrilateral between this and the next frame
                    if t_next is not None:
                        if face_barycenter_next is None:
                            face_barycenter_next = (face_next_midpoints[0] + face_next_midpoints[1] + face_next_midpoints[2]) / 3.0
                        
                        quad_edge_other[0, :3] = face_barycenter_next
                        quad_edge_other[1, :3] = (face_next_midpoints[ind1] + face_next_midpoints[ind2]) / 2.0
                        quad_edge_other[:, 3]  = t_next_mid

                        vol_dual_edge += volumes.get_area_temporal_quadrilateral(quad_edge_current, quad_edge_other, self.alpha)

                    # Add the volume, divided by the length of the edge
                    # Since they are both in the same time frame, we don't need to call the special norm

                    edge_length = np.linalg.norm(v1 - v2)
                    vol_dual_edge /= edge_length

                    self._init_Lk_add_value(k, v1_ind, v2_ind, vol_dual_edge)
                    self._init_Lk_add_value(k, v2_ind, v1_ind, vol_dual_edge)       # symmetric...

            # Change temporal coordinates for next loop
            t_prev = t_current
            t_current = t_next
            t_next = None

        # --------------------------------------------
        # (B) Using the previously calculated volumes, 
        # get the correct coefficients
        # --------------------------------------------

        for k in tqdm(range(self._f), desc='computing coeffs', leave=False, disable=quiet):
            # --------------------------------------
            # 1. Update L(k)'s off-diagonal elements
            # --------------------------------------

            # We'll iterate over the non-zero elements of the DOK matrix (which is basically a dictionary)
            # For each row i and column j:
            #       - if i == j we're in the diagonal, we don't care about it for now
            #       - if i != j, we'll only update when i < j, since the opposite case
            #         is handled by the symmetry.

            for (i, j), val in self._Ls[k].items():
                if i < j:
                    # for off-diagonal values we need:
                    #   (a) volume of dual of vertex i
                    #   (b) volume of dual of vertex j
                    # the rest is already there...

                    vol_i = self._Ls[k].get((i, i))
                    vol_j = self._Ls[k].get((j, j))
                    coeff = val * (-1.0 / (math.sqrt(vol_i * vol_j)))

                    self._Ls[k]._update( {(i, j) : coeff} )
                    self._Ls[k]._update( {(j, i) : coeff} )

            # ---------------------------
            # 2. Update D(k) coefficients
            # ---------------------------

            if k != self._f - 1:
                for i in range(self._n):
                    vol_k = self._Ls[k].get((i, i))
                    vol_k1 = self._Ls[k+1].get((i, i))

                    self._Ds[k][i] *= (-1.0 / (math.sqrt(vol_k * vol_k1)))

        # Now that we have everything in place, we only need to update the diagonal elements,
        # which are just the sum of the rows (negated).
        for k in tqdm(range(self._f), desc='updating diags', leave=False, disable=quiet):
            # first set diagonal elements to zero
            for i in range(self._n):
                self._Ls[k]._update( {(i, i) : 0.0} )

            # now accumulate sum by iterating non-zero elements
            for (row, col), val in self._Ls[k].items():
                if row == col: 
                    continue

                self._init_Lk_add_value(k, row, row, val)

            # finally, add D(k) values and negate sums
            for i in range(self._n):
                if k > 0:
                    self._init_Lk_add_value(k, i, i, self._Ds[k-1][i])

                if k < self._f - 1:
                    self._init_Lk_add_value(k, i, i, self._Ds[k][i])

                oldval = self._Ls[k].get((i, i))
                self._Ls[k]._update( {(i, i) : -oldval} )

        # ---------------------------------------------------
        # (D) Convert to csr matrix for efficient arithmetics
        # ---------------------------------------------------

        self._convert_sparse_blocks()

    # ---------------------------------------------------------------------------------
    # Retrieve Information
    def get_spatial_neighbours(self, frame_id, vertex_id, include_vertex = False):
        """
        Get neighbours of the specified vertex and their weights (i.e. the coefficients 
        in the L(frame_id) submatrix, for the row belonging to vertex_id).

        If include_vertex is False the weight for that vertex will not be returned
        (that is, the diagonal element of the L(k) submatrix will be excluded). 
        This is usually the required behaviour, but it works faster when include_vertex is True.

        Returns two arrays, the first one containing the weights and 
        the second one containing the indices of columns to which those weights belong
        (i.e. the neighbour indices).

        Arguments
        ---------

        frame_id                : Number of frame from which the weights will be retrieved.
        vertex_id               : Number of row in the L(k) submatrix from which to take 
                                  the spatial weights.
        include_vertex          : If True, the element located in the diagonal will be included
                                  in the result.
        """

        # Get all values in column vtx_idx of Ls(k)
        # NOTE: we are counting on the fact that the Ls are in CSC format
        indptr = self._Ls[frame_id].indptr
        data_1 = self._Ls[frame_id].data[ indptr[vertex_id] : indptr[vertex_id + 1] ]
        indices_1 = self._Ls[frame_id].indices[ indptr[vertex_id] : indptr[vertex_id + 1] ]

        # Remove diagonal element
        if include_vertex:
            data = data_1
            indices = indices_1
        else:
            res_size = len(data_1) - 1
            data = np.zeros(res_size, dtype=np.double)
            indices = np.zeros(res_size, dtype=np.int32)

            pos = 0
            for i in range(len(data_1)):
                if indices_1[i] == vertex_id:
                    continue

                data[pos] = data_1[i]
                indices[pos] = indices_1[i]   
                pos += 1
        
        return data, indices

    def get_temporal_neighbours(self, frame_id, vertex_id):
        """
        Get temporal neighbours for the specified vertex, along with their coefficients.

        Returns two arrays, the first one containing the coefficients and 
        the second one containing the indices of the neigbour frames.

        Arguments
        ---------

        frame_id                : Index of the frame where the desired vertex is.
        vertex_id               : Index for the vertex whose temporal weights we want.

        """

        weights = []
        inds = []

        if frame_id > 0:
            weights.append(self._Ds[frame_id-1][vertex_id])
            inds.append(frame_id-1)

        if frame_id < self._f-1:
            weights.append(self._Ds[frame_id][vertex_id])
            inds.append(frame_id+1)

        return weights, inds

    def solve_build_full_matrix(self):
        """
        Builds the Laplacian matrix as a nf * nf sparse matrix, with f the number
        of frames and n the number of vertices, with the previously specified 
        constraints included as extra rows.
        """

        full_l_blocks = []

        for k in range(self._f):
            row_block = []

            # Add zero block entries
            for i in range(k-1):
                row_block.append(None)

            # Place D(k-1) L(k) D(k)
            if k > 0:
                row_block.append(sparse.dia_matrix((self._Ds[k-1], [0]), shape=(self._n, self._n)))

            row_block.append(self._Ls[k])

            if k < self._f-1:
                row_block.append(sparse.dia_matrix((self._Ds[k], [0]), shape=(self._n, self._n)))

            # Add remaining zero block entries
            for i in range(k+2, self._f):
                row_block.append(None)

            full_l_blocks.append(row_block)

        # Sort constraints per frame and vertex
        sorted_keys = sorted(self._constraints)

        # Add constraints
        # We'll divide anchors in blocks, one for each frame,
        # to use bmat afterwards
        anchors_per_frame = [None] * self._f          # we need None's to complete full matrix with bmat
        num_constraints = len(sorted_keys)

         # TODO: doing this here because of the order of constraints... change?
        self._b_constraints = None

        for c in range(len(sorted_keys)):
            ki = sorted_keys[c]
            (weight, target) = self._constraints[ki]

            k = int(ki // self._n)
            i = ki % self._n

            # Add constraint to matrix
            if anchors_per_frame[k] is None:
                anchors_per_frame[k] =  sparse.dok_matrix((num_constraints, self._n))

            anchors_per_frame[k][c, i] = weight

            # Add constraints to use in right-hand side
            bdim = len(target)
            
            if self._b_constraints is None:
                self._b_constraints = np.zeros((num_constraints, bdim), dtype=np.double)

            self._b_constraints[c, :] = weight * target[:bdim]

        full_l_blocks.append(anchors_per_frame)

        # Convert laplacian to sparse matrix
        self._full_Lt = sparse.bmat(full_l_blocks, format='csc').transpose()        # TODO: build already tranposed version...

    # ---------------------------------------------------------------------------------
    # AUX

    def _init_sparse_blocks(self):
        # Initialize sparse structures
        self._Ls = [None] * self._f
        self._Ds = [None] * (self._f - 1)

        for k in range(self._f):
            # L(k)'s are dictionary of keys because we'll access the values several times
            # before getting the final matrix
            self._Ls[k] = sparse.dok_matrix((self._n, self._n), dtype=np.double)         

            if k != self._f - 1:
                # The Ds matrix need not be in sparse format, we can just use them as arrays
                self._Ds[k] = np.zeros(self._n, dtype=np.double)

    def _init_Lk_add_value(self, k, row, col, value_to_add):
        # Add value to current value in L(k)[i, j]. 
        # That is, do the equivalent to:
        #           L[k][i, j] += value_to_add
        if (row, col) in self._Ls[k]:
            oldval = dict.get(self._Ls[k],(row, col),0)
            newval = oldval + value_to_add
        else:
            newval = value_to_add
            
        self._Ls[k]._update( {(row, col) : newval} )

    def _init_Lk_put_sum_in_diagonal(self):
        for k in range(self._f):
            # initialize values
            for i in range(self._n):
                self._Ls[k]._update( {(i, i) : 0.0} )

            # accumulate sum by iterating non-zero elements
            for (row, col), val in self._Ls[k].items():
                
                if row == col: 
                    continue
                
                self._init_Lk_add_value(k, row, row, val)

            # finally, add D(k) values and negate sums
            for i in range(self._n):
                if k > 0:
                    self._init_Lk_add_value(k, i, i, self._Ds[k-1][i])

                if k < self._f-1:
                    self._init_Lk_add_value(k, i, i, self._Ds[k][i])                    
                
                oldval = self._Ls[k].get((i, i))
                self._Ls[k]._update( {(i, i) : -oldval} )

    def _convert_sparse_blocks(self):
        for k in range(self._f):
                self._Ls[k] = self._Ls[k].tocsc()

    def _save_to_file(self, file_path):
        fp = pathlib.Path(file_path)
        fp.parent.mkdir(parents=True, exist_ok=True)
        with fp.open(mode='wb+') as f:
            pickle.dump([self._Ls, self._Ds], f)