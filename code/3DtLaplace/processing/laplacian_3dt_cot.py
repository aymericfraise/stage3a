"""
3D+t laplacian operator with static cotangent weights in space, 
uniform in temporal weights.
"""

from scipy import sparse
from .laplacian_3dt import Laplacian3Dt
from .laplacian_cot import LaplacianCot

class Laplacian3Dt_Cot(Laplacian3Dt):

    def __init__(self, alpha, num_frames, num_vertices):
        Laplacian3Dt.__init__(self, alpha, num_frames, num_vertices)

    def build(self, mesh_sequence):
        self._init_sparse_blocks()
        m = mesh_sequence.topology
        n = self._n

        # L(K)s: cotangent laplacian
        for k in range(self._f):
            # Put coordinates in mesh
            m.update_coordinates(mesh_sequence.coordinates[k])
            lcot = LaplacianCot()
            lcot.build(m)
            self._Ls[k] = sparse.dok_matrix(lcot.L)           # dok format because we'll update diagonal later

            # Ds(k): just ones
            if k < self._f-1:
                for i in range(self._n):
                    self._Ds[k][i] = -1.0
                    #self._Ds[k][i] = -self.alpha

        # Update L(k) diagonal elements and convert to sparse matricesR
        self._init_Lk_put_sum_in_diagonal()
        self._convert_sparse_blocks()