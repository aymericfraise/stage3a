import numpy as np
import scipy.integrate as spi
import math
from numba import jit
import utils.linalg_3 as linalg3 # fast linear algebra for 3D vecs

TOLERANCE = 0.0000001


@jit(nopython=True)
def lerp_3cell(beta, A, B, C):
    """ Compute norm((beta ** 2) * A + beta * B + C) using fast compiled code """
    bbA, bB, res = np.zeros(3), np.zeros(3), np.zeros(3)
    linalg3.mul3(beta**2, A, bbA)
    linalg3.mul3(beta, B, bB)
    linalg3.sum3(bbA, bB, res)
    linalg3.sum2(C, res)
    return linalg3.norm(res)

def get_volume_temporal_3cell(t0, t1, face_points_t0, face_points_t1, alpha):
    """
    Calculate volume of a temporal 3-cell made up of two triangles on its extreme, 
    the first one in time t0 and the second one in time t1.

    Arguments
    ---------

    TODO.
    Points are in 3D, that's why we include temporal coordinate in a separate argument

    """

    # Triangles of the 3-cell are:
    #   t0: (p_k, q_k, r_k)
    #   t1: (p_k1, q_k1, r_k1)
    p_k = face_points_t0[0]
    q_k = face_points_t0[1]
    r_k = face_points_t0[2]

    p_k1 = face_points_t1[0]
    q_k1 = face_points_t1[1]
    r_k1 = face_points_t1[2]

    # ---------------------------------------------------------------------------
    # Formula:
    #   |F_{i, j, l}^k| = 0.5 * ||pkpk1|| int(0, 1) { ||\beta^2A + \beta B + C||_s } d\beta
    #   with
    #       A = (pq_{k1} - pq_k) x (pr_{k1} - pr_k)
    #       B = ((pq_{k1} - pq_k) x pr_k)  +  (pq_k x (pr_{k1} - pr_k))
    #       C = pq_k x pr_k
    # ---------------------------------------------------------------------------

    pq_k = p_k - q_k
    pr_k = p_k - r_k
    pq_k1 = p_k1 - q_k1
    pr_k1 = p_k1 - r_k1

    pq_k1_k = pq_k1 - pq_k
    pr_k1_k = pr_k1 - pr_k

    A = linalg3.cross(pq_k1_k, pr_k1_k)
    B = linalg3.cross(pq_k1_k, pr_k) + linalg3.cross(pq_k, pr_k1_k)
    C = linalg3.cross(pq_k, pr_k)

    integ_val, integ_error = spi.quad(lambda beta : lerp_3cell(beta, A, B, C), 0, 1)
    return integ_val

def get_area_triangle(triangle_points):
    """
    Get area for a triangle located in a some moment in time,
    according to the metric defined by alpha.

    Arguments
    ---------

    TODO. Points are in 3D.
    """

    center = triangle_points[0]
    vec1 = triangle_points[1] - center
    vec2 = triangle_points[2] - center
    
    area = math.sqrt(\
            (vec1[1]*vec2[2]-vec1[2]*vec2[1]) * (vec1[1]*vec2[2]-vec1[2]*vec2[1]) + \
            (vec1[0]*vec2[2]-vec1[2]*vec2[0]) * (vec1[0]*vec2[2]-vec1[2]*vec2[0]) + \
            (vec1[0]*vec2[1]-vec1[1]*vec2[0]) * (vec1[0]*vec2[1]-vec1[1]*vec2[0]))

    return 0.5 * area

def get_norm(vec, alpha):
    """
    Calculate norm in the 4-dimensional Riemannian manifold
    using the defined metric tensor.

    Arguments
    ---------

    vec                 : four-dimensional vector, with t as its last coordinate.
    """

    res = math.sqrt(vec[0] ** 2 + vec[1] ** 2 + vec[2] ** 2 + alpha * (vec[3] ** 2))
    return res

def build_temporal_quadrilateral(t_current, t_other_midpoint, face_current, face_other, edge_from_index, edge_to_index):
    # TODO: not sure this should go here...
    # Build temporal quadrilateral which forms the dual of a spatial edge

    # Quadrilateral has these four points:
    #       (1) face's barycenter in t0
    #       (2) midpoint of edge in t0
    #       (3) midpoint between face's barycenter in t0 and t1
    #       (4) midpoint of edge that goes from mipoint in t0 and t1

    quad = np.empty([4, 4])                     # each point is 4-dimensional

    face_current_barycenter = (face_current[0] + face_current[1] + face_current[2]) / 3.0
    face_other_barycenter = (face_other[0] + face_other[1] + face_other[2]) / 3.0
    face_midpoint_barycenter = (face_current_barycenter + face_other_barycenter) / 2.0

    edge_current_midpoint = (face_current[edge_from_index] + face_current[edge_to_index]) / 2.0
    edge_other_midpoint = (face_other[edge_from_index] + face_other[edge_to_index]) / 2.0
    temporal_midpoint = (edge_current_midpoint + edge_other_midpoint) / 2.0
    
    quad[0, :3] = face_current_barycenter
    quad[1, :3] = edge_current_midpoint

    quad[2, :3] = face_midpoint_barycenter
    quad[3, :3] = temporal_midpoint

    quad[0:1, 4] = t_current
    quad[2:3, 4] = t_other_midpoint

    return quad

def get_area_temporal_quadrilateral(quad_edge_t0, quad_edge_t1, alpha):
    """
    Calculate the area of a temporal quadrilateral, defined by two edges
    in two different time frames.

    Arguments
    ---------

    TODO. Quadrilaterals are defined by their two edges, the first one 
    in time t0 and the second one in time t1.
    Each edge is a 2x4 matrix, with each row a 4D point.

    """

    # Edge of quadrilateral at time k
    p_k = quad_edge_t0[0]
    q_k = quad_edge_t0[1]
    pq_k = p_k - q_k

    # Edge of quadrilateral at time k+1
    p_k1 = quad_edge_t1[0]
    q_k1 = quad_edge_t1[1]
    pq_k1 = p_k1 - q_k1

    # Temporal edge from p at time k to p at time k+1
    p_temporal = p_k1 - p_k

    # If the spatial coordinates from time k and time k+1 are equal
    # then the volume is || pq_k || * || pq_{k+1} ||
    pq_diff = pq_k1 - pq_k
    if (pq_diff[0] + pq_diff[1] + pq_diff[2] < TOLERANCE):
        return get_norm(pq_k, alpha) * get_norm(p_temporal, alpha)

    # Else, the volume of the 2-cell is the following
    k1 = ( pq_k[0] * pq_diff[0] + pq_k[1] * pq_diff[1] + pq_k[2] * pq_diff[2] )
    k2 = (pq_diff[0] * pq_diff[0] + (pq_diff[1] * pq_diff[1]) + pq_diff[2] * pq_diff[2])
    K = k1 / k2

    A = get_norm(p_temporal, alpha) * get_norm(pq_diff, alpha)
    pq_cross = linalg3.cross(pq_k[:-1], pq_k1[:-1])
    a = linalg3.norm(pq_cross)                            # norm in the 3D euclidean space
    a_sq = a ** 2

    if (a_sq) < TOLERANCE:
        return A * 0.5 * (K ** 2 + (K+1) ** 2)

    # Limits of the integral
    u1 = K
    u2 = K + 1

    u1_sq = u1 ** 2
    u2_sq = u2 ** 2

    if u1_sq < TOLERANCE:
        u1 = 0
        u1_sq = 0
        u2 = 1
        u2_sq = 1

    A = A * 0.5
    b = (math.sqrt(u2_sq + a_sq) + u2) /         \
        (math.sqrt(u1_sq + a_sq) + u1)

    res = \
        A * (u2 * math.sqrt((u2_sq + a_sq)) - u1 * math.sqrt(u1_sq + a_sq)) + \
        A * a_sq * math.log(b)

    return res