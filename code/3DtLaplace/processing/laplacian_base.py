"""
Base class for Laplacian matrix.
"""

import numpy as np
from scikits.sparse.cholmod import cholesky

class Laplacian:

    def __init__(self):
        self.L = None
        
        self._constraints = dict()
        self._LtL_decomposition = None
        self._b_constraints = None

    # ---------------------------------------------------------------------------------
    # Abstract

    def build(self, obj):
        """
        Build Laplacian matrix
        """
        raise NotImplementedError

    def solve_build_full_matrix(self):
        """
        Build Laplacian matrix with constraints.
        """
        raise NotImplementedError

    # ---------------------------------------------------------------------------------
    # Linear system solving

    def remove_constraints(self):
        self._constraints = dict()
        self._full_Lt = None
        self._LtL_decomposition = None
        self._b_constraints = None

    def add_soft_constraint(self, vertex_id, weight, target_value):
        """
        Add soft constraint to solve Lx = b in the least squares sense.

        Arguments
        ---------

        vertex_id           : Index of vertex to constraint
        weight              : Weight to put on the constraint
        target_value        : Expected coordinates for this vertex. Numpy array of 1, 3 or 4 values (x, y, z, t)

        """
        
        if vertex_id not in self._constraints:
            self._constraints[vertex_id] = (weight, target_value)

        self._LtL_decomposition = None
        self._b_constraints = None

    def solve_lstsq(self, b, method=None):
        """
        Solve linear systems of type Lx = b in the least squares sense.
        At least one hard or soft constraint must have been specified.

        b can have more than one column, to simultaneously solve for more than
        one coordinate.

        Constraints must not be included in b, as they will be automatically added
        according to the values specified when calling add_hard_constraints or
        add_soft_constraints.

        The result will have the same shape as b.

        Arguments
        ---------

        b                       : Right-hand side.
        method                  : Method to solve least squares. Must be "normal-eq"
                                  for normal equations or "qr" (not implemented so ignoring
                                  this for now...)

        """

        if self._constraints is None or len(self._constraints) == 0:
            raise Exception("No constraints were added")

        num_constraints = len(list(self._constraints.keys()))

        # Build LtL and get decomposition
        if self._LtL_decomposition is None:
            # TODO: very inefficient?
            self.solve_build_full_matrix()
            L = self._full_Lt.transpose()
            LtL = (self._full_Lt * L).tocsc()
            self._LtL_decomposition = cholesky(LtL)
        
        # Add constraints to b
        if self._b_constraints is None:
            bdim = b.shape[1]
            self._b_constraints = np.zeros((num_constraints, bdim), dtype=np.double)
            ind = 0
            
            for i, (weight, target) in self._constraints.items():
                self._b_constraints[ind, :] = weight * target[:bdim]
                ind += 1

        # TODO: this is expensive!!
        full_b = np.append(b, self._b_constraints, axis=0)

        # Solve
        # If we solve all coordinates at the same time the result 
        # is an (immutable?) sparse matrix...
        res = np.zeros_like(b)

        for d in range(b.shape[1]):
            Ltb = self._full_Lt * full_b[:, d]
            res[:, d] = self._LtL_decomposition(Ltb)
            
        return res