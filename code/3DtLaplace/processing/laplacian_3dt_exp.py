"""
3D+t laplacian operator with exponential weights.
"""

import math
from .laplacian_3dt import Laplacian3Dt

TOLERANCE = 0.0000001
EXP_BETA = 0.1

class Laplacian3Dt_Exp(Laplacian3Dt):

    def __init__(self, alpha, num_frames, num_vertices):
        Laplacian3Dt.__init__(self, alpha, num_frames, num_vertices)

    def build(self, mesh_sequence):
        nfaces = mesh_sequence.topology.num_faces

        print("Building Exponential Laplacian...")
        print("\t# Vertices:", self._n)
        print("\t# Frames:", self._f)

        self._init_sparse_blocks()

        # -----------
        # D(k) blocks
        # -----------

        # alpha: is used to define the importance of "time" edges
        # with respect to spatial edges, the bigger the more important.
        # => we will use 1/alpha for fair comparison (since in the DEC case, 
        # the smaller the more important)
        # Note that the matrix is not symmetric in this case
        exp_alpha = 1.0 / self.alpha

        # 1. Get maximum euclidean distance between temporal edges
        d_max = 0.0
        for k in range(self._f-1):
            for i in range(self._n):
                vi_k = mesh_sequence.get_coordinates(k, i, 3, copy=False)
                vi_k1 = mesh_sequence.get_coordinates(k+1, i, 3, copy=False)
                temp_edge = (vi_k - vi_k1)
                temp_d_sq = temp_edge[0] ** 2 + temp_edge[1] ** 2 + temp_edge[2] ** 2

                if temp_d_sq > d_max:
                    d_max = temp_d_sq

        # 2. Set D elements: 1 + \alpha * exp(-\beta * (d_ij^2 / d_max^2))
        for k in range(self._f-1):
            for i in range(self._n):
                # Get squared euclidean distance between temporal edges
                vi_k = mesh_sequence.get_coordinates(k, i, 3, copy=False)
                vi_k1 = mesh_sequence.get_coordinates(k+1, i, 3, copy=False)
                temp_edge = (vi_k - vi_k1)
                temp_d_sq = temp_edge[0] ** 2 + temp_edge[1] ** 2 + temp_edge[2] ** 2

                # If max is zero it will just be the combinatorial laplacian...
                if d_max > TOLERANCE:
                    self._Ds[k][i] = -(1 + exp_alpha * math.exp(-EXP_BETA * (temp_d_sq / d_max)))
                else:
                    self._Ds[k][i] = -1

        # --------------------------
        # L(k) off-diagonal elements
        # --------------------------

        for f_ind in range(nfaces):

            face = mesh_sequence.topology.faces_inds[f_ind]

            for k in range(self._f):

                for i in range(3):
                    v1 = face[i]
                    v2 = face[(i+1) % 3]

                    if (v1, v2) not in self._Ls[k]:
                        self._Ls[k]._update( {(v1, v2) : -1.0} )
                        self._Ls[k]._update( {(v2, v1) : -1.0} )

        # ----------------------
        # L(k) diagonal elements
        # ----------------------
        
        self._init_Lk_put_sum_in_diagonal()

        # Convert to csc format for efficient matrix operations
        self._convert_sparse_blocks()