import numpy as np
from scipy import interpolate
import sys
import os

# Path hack to get sibling modules...
sys.path.insert(0, os.path.abspath('..'))

from .laplacian_3dt import Laplacian3Dt
from mesh import loader, MeshSequence

class ARAPEditor:

    def __init__(self, laplacian, source_sequence, dim=4):
        """
        Initialize ARAP spacetime editor.

        Arguments
        ---------

        laplacian               : 3D+t Laplace operator to use
        source_sequence         : Mesh sequence to edit (see class MeshSequence).
        dim                     : Dimension of the data

        """

        self.l              = laplacian
        self._alpha         = laplacian.alpha
        self._source_seq    = source_sequence
        self._num_frames    = self._source_seq.num_frames
        self._num_vertices  = self._source_seq.num_vertices
        self._dim           = dim

        self._constraint_added      = False

        self._new_points            = None
        self._stop_max_iterations   = None
        self._stop_max_error        = None

    # -------------------------------------------------------------------------------------------------------------------
    # Configuration
    def set_stop_max_iterations(self, max_iterations):
        self._stop_max_iterations = max_iterations

    def set_stop_max_error(self, max_error):
        self._stop_max_error = max_error

    def set_initial_points(self, initial_sequence):
        """
        Set initial sequence to begin the iterative process
        TODO. points have to be copied...
        """

        self._new_points = np.zeros((self._dim, self._num_vertices * self._num_frames))

        for k in range(self._num_frames):
            frame_start = k * self._num_vertices

            for i in range(self._num_vertices):
                self._new_points[:, frame_start + i] = initial_sequence.get_coordinates(k, i, self._dim, copy=True)

                # If we're working in 4D we need to properly scale the time coordinate
                if self._dim == 4:
                    self._new_points[3, frame_start + i] *= self._alpha

    # -------------------------------------------------------------------------------------------------------------------
    # Constraining
    def add_constraint(self, frame_id, vertex_id, target_point, weight):
        """
        Add constraint over a vertex in a certain frame.
        """

        if self._dim == 4:
            target_point[3] *= self._alpha

        row = frame_id * self._num_vertices + vertex_id
        self.l.add_soft_constraint(row, weight, target_point)
        self._constraint_added = True

    def constrain_source_vertices(self, frame_id, vertices, weight):
        """
        Constraint the specified list of vertices in frame_id to remain
        in their original position.
        """

        for i in vertices:
            target = self._source_seq.get_coordinates(frame_id, i, self._dim, copy=False)

            if self._dim == 4:
                target[3] *= self._alpha

            row = frame_id * self._num_vertices + i
            self.l.add_soft_constraint(row, weight, target)

        self._constraint_added = True

    def constrain_source_frame(self, frame_id, weight):
        """
        Constraint an entire frame to remain in its original position.
        """
        
        for i in range(self._num_vertices):
            target = self._source_seq.get_coordinates(frame_id, i, self._dim, copy=False)

            if self._dim == 4:
                target[3] *= self._alpha

            row = frame_id * self._num_vertices + i
            self.l.add_soft_constraint(row, weight, target)

        self._constraint_added = True

    def constrain_temporal_boundary(self, weight):
        """
        Constraint entire first and last mesh in the original sequence.
        """

        for k in [0, self._num_frames-1]:
            for i in range(self._num_vertices):
                target = self._source_seq.get_coordinates(k, i, self._dim, copy=True)

                if self._dim == 4:
                    target[3] *= self._alpha

                row = k * self._num_vertices + i
                self.l.add_soft_constraint(row, weight, target)

        self._constraint_added = True


    # -------------------------------------------------------------------------------------------------------------------
    # ARAP Methods

    def edit(self, save_iterations_in=None, create_new_sequence=False):
        """
        Edit mesh sequence.

        Arguments
        ---------

        save_iterations_in              : Path where intermediate iterations must be saved.
                                          Can be None.

        create_new_sequence             : If true, the method returns a new mesh sequence
                                          object with the results of the edit. If False,
                                          the result is saved in the source sequence.
        """

        if self._source_seq is None:
            raise Exception("No source sequence specified.")

        if not self._constraint_added:
            raise Exception("At least one constraint must be specified before editing")

        # Initialize the iterative process with the original sequence
        if self._new_points is None:
            self.set_initial_points(self._source_seq)

        # Iterative process
        it = 0
        changing = True

        while it < self._stop_max_iterations and changing:

            # Compute right-hand side of system
            # (initial guess: without rotations)
            b = self.compute_b(use_rotation=(it > 0))

            # Solve for new positions
            new_points = self.l.solve_lstsq(b)
                
            max_diff = 0.0
            p_ind = 0

            for k in range(self._num_frames):

                for j in range(self._num_vertices):
                    # Get difference with previous iteration to see if it converged
                    curr_diff = self._get_iteration_diff(self._new_points[:3, p_ind], new_points[p_ind, :3], norm=2)
                    
                    if curr_diff > max_diff:
                        max_diff = curr_diff

                    # Update values
                    self._new_points[:, p_ind] = new_points[p_ind, :]
                    p_ind += 1

            # Check convergence
            changing = (max_diff > self._stop_max_error)

            if save_iterations_in is not None:
                seqpath = save_iterations_in + "/iteration-" + str(it) 
                loader.save_sequence_points(self._source_seq.topology.faces_inds, self._new_points, self._num_frames, self._num_vertices, seqpath)

            it += 1

        # Update result
        if create_new_sequence:
            res_seq = MeshSequence(self._source_seq.t0, self._source_seq.dt)
            res_seq.set_topology(self._source_seq.topology)

            for k in range(self._source_seq.num_frames):
                res_seq.add_frame_coordinates(self._source_seq.coordinates[k])
        else:
            res_seq = self._source_seq

        p_ind = 0
        for k in range(self._num_frames):
            for i in range(self._num_vertices):
                res_seq.coordinates[k][i, :] = self._new_points[:3, p_ind]
                p_ind += 1
                
        return res_seq, it

    def compute_b(self, use_rotation=True):
        """
        Compute right-hand side of the ARAP equation.
        """

        b = np.zeros((self._num_frames * self._num_vertices, self._dim))
        b_ind = 0

        Rs, Rs_prev, Rs_next = None, None, None

        for k in range(self._num_frames):

            # Estimate rotation matrices for this frame (and neighbouring frames,
            # if necesary)
            if use_rotation:

                if Rs is None:
                    Rs = [None] * self._num_vertices
                    for i in range(self._num_vertices):
                        Rs[i] = self.get_optimal_cell_rotation(k, i)

                if self._dim == 4 and k < self._num_frames-1:
                    # Rs_prev will always be already calculated from previous iteration,
                    # and we will always need to calculate Rs_next at the beginning
                    if Rs_next is None:
                        Rs_next = [None] * self._num_vertices

                    for i in range(self._num_vertices):
                        Rs_next[i] = self.get_optimal_cell_rotation(k+1, i)

            # Build right-hand side for each vertex on this frame
            for i in range(self._num_vertices):

                pi = self._source_seq.get_coordinates(k, i, self._dim, copy=False)

                if self._dim == 4:
                    pi[3] *= self._alpha

                # Spatial neighbours
                weights, ind = self.l.get_spatial_neighbours(k, i, include_vertex=True)

                for j in range(len(weights)):                          # j : number of neighbour | ind[j] : index of this neighbour in the roi
                    if ind[j] == i:
                        continue

                    pj = self._source_seq.get_coordinates(k, ind[j], self._dim, copy=False)

                    if self._dim == 4:
                        pj[3] *= self._alpha
                        #pi_pj[3] = 0.0

                    pi_pj = pi - pj
                    w = -weights[j]

                    if use_rotation:
                        R = Rs[i] + Rs[ ind[j] ]                      # R_i + R_j
                        b[b_ind, :] += (0.5 * w * np.dot(R, pi_pj))
                    else:
                        b[b_ind, :] += w * pi_pj

                # Add temporal edges to the sum
                # Temporal neighbours
                weights, ind = self.l.get_temporal_neighbours(k, i)

                for j in range(len(weights)):

                    k_other = ind[j]
                    w = -weights[j]

                    pj = self._source_seq.get_coordinates(k_other, i, self._dim, copy=False)
                    
                    if self._dim == 4:
                        pj[3] *= self._alpha

                    pi_pj = pi - pj

                    if use_rotation and self._dim == 4:
                        if k_other == k - 1:
                            R = Rs[i] + Rs_prev[i]
                        elif k_other == k + 1:
                            R = Rs[i] + Rs_next[i]
                        else:
                            raise Exception("Something went wrong...")

                        b[b_ind, :] += (0.5 * w * np.dot(R, pi_pj))
                    else:
                        b[b_ind, :] += w * pi_pj

                b_ind += 1

            # Re-use rotations
            if use_rotation:
                if self._dim == 4:
                    Rs_prev = Rs
                    Rs  = Rs_next
                    Rs_next = None
                else:
                    Rs = None

        return b

    def get_optimal_cell_rotation(self, frame_id, vertex_id):
        """
        Get optimal rotation for the cell defined over the specified vertex and
        frame.
        """

        # Get array with weights and indices of vertex's neighbours
        weights, ind = self.l.get_spatial_neighbours(frame_id, vertex_id, include_vertex=True)
        num_neighs   = len(weights) - 1

        if self._dim == 4:
            weights_t, ind_t = self.l.get_temporal_neighbours(frame_id, vertex_id)
            num_neighs += len(weights_t)

        # Create matrix with cell edges (for old and new points)
        P_i = np.zeros((self._dim, num_neighs), dtype=np.double)
        Pp_i = np.zeros((num_neighs, self._dim), dtype=np.double)

        pi_source = self._source_seq.get_coordinates(frame_id, vertex_id, self._dim, copy=False)
        pi_target = self._new_points[:, frame_id * self._num_vertices + vertex_id]

        # We need to scale the time coordinate of the source sequence by alpha 
        # (working in sequence embedding space...)
        # We don't need to do it in _new_points because they are already scaled
        if self._dim == 4:
            pi_source[3] *= self._alpha

        # Spatial neighbours
        nneigh = 0

        for j in range(len(ind)):
            if ind[j] == vertex_id:                                             # because we included diagonal value...
                continue

            pj_source = self._source_seq.get_coordinates(frame_id, ind[j], self._dim, copy=False)
            pj_target = self._new_points[:, frame_id * self._num_vertices + ind[j]]
            
            diff_source = -weights[j] * (pi_source - pj_source)

            if self._dim == 4:
                diff_source[3] = 0.0
                
            diff_target = pi_target - pj_target

            P_i[:, nneigh] = diff_source
            Pp_i[nneigh, :] = diff_target
            nneigh += 1

        # Temporal neighbours
        if self._dim == 4:
            for j in range(len(ind_t)):
                k_other = ind_t[j]
                w_other = -weights_t[j]

                pi_other_source = self._source_seq.get_coordinates(k_other, vertex_id, self._dim, copy=False)
                pi_other_source[3] *= self._alpha
                pi_other_target = self._new_points[:, k_other * self._num_vertices + vertex_id]
                
                diff_source = w_other * (pi_source - pi_other_source)
                diff_target = pi_target - pi_other_target

                P_i[:, nneigh] = diff_source
                Pp_i[nneigh, :] = diff_target
                nneigh += 1

        # Create covariance matrix S
        PDPt        = np.dot(P_i, Pp_i)
        
        # Find SVD of covariance matrix S = UDV'
        U, s, Vt    = np.linalg.svd(PDPt, full_matrices = True)
        Ut          = np.transpose(U)
        V           = np.transpose(Vt)

        # Create rotation matrix R = VU'
        R = np.dot(V, Ut)

        if np.linalg.det(R) < 0:
            Ut[self._dim-1, :] *= -1               # change sign of last column in U
            R = np.dot(V, Ut)

        return R

    # -------------------------------------------------------------------------------------------------------------------
    # AUX
    def _get_iteration_diff(self, old_point, new_point, norm=2):
        old = np.linalg.norm(old_point, ord=norm)
        new = np.linalg.norm(new_point, ord=norm)
        absdiff = np.linalg.norm(old_point - new_point, ord=norm)
        
        if old > 0 or new > 0:
            curr_diff = absdiff / (max(old, new))
        else:
            curr_diff = 0.0

        return curr_diff