"""
ARAP space-time editing of mesh animations, using the 3D+t Laplace operator.
"""

import os
import math
import argparse
from datetime import datetime
import numpy as np

from mesh import loader
from processing import ARAPEditor, ARAPStaticEditor
from processing import Laplacian3Dt, LaplacianCot, Laplacian3Dt_Cot, Laplacian3Dt_Comb, Laplacian3Dt_Exp
from utils import Rotation

# ---------------------------------------------------------------------------------------------------------------
# DEFAULT VALUES

DEFAULT_ALPHA               = 100
DEFAULT_MAX_ITERS           = 1000
DEFAULT_MAX_ERROR           = 0.001
DEFAULT_MAX_ITERS_STATIC    = 1000
DEFAULT_MAX_ERROR_STATIC    = 0.0001
DEFAULT_ANCHORS_RING        = 0
CONSTRAIN_TEMPORAL_WINDOW   = True
DEFAULT_WEIGHT_HANDLES      = 100000000
DEFAULT_WEIGHT_ANCHORS      = 100000000
DEFAULT_WEIGHT_FRAMES       =  100000000

PARAMETERS_FILE             = "params"

# ---------------------------------------------------------------------------------------------------------------
class SpacetimeEditor:

    def __init__(self):
        """
        Initialize the Spacetime Editor with default parameters.
        """

        self._alpha                 = DEFAULT_ALPHA
        self._arap_max_iterations   = DEFAULT_MAX_ITERS
        self._arap_max_error        = DEFAULT_MAX_ERROR
        self._keyframe_max_iterations = DEFAULT_MAX_ITERS_STATIC
        self._keyframe_max_error    = DEFAULT_MAX_ERROR_STATIC

        self._lstsq_weight_handles  = DEFAULT_WEIGHT_HANDLES
        self._lstsq_weight_anchors  = DEFAULT_WEIGHT_ANCHORS
        self._lstsq_weight_frames   = DEFAULT_WEIGHT_FRAMES
        self._anchors_ring          = DEFAULT_ANCHORS_RING

        self._constrain_temporal_window     = CONSTRAIN_TEMPORAL_WINDOW
        self._keyframe_constraints_folder   = None
        self._constrained_frames            = set()
        self._l_type                        = "dec"

        # Options
        self._edit_target_frame     = False                     # If true, first edit target frame and constraint result
        self._use_sr_arap           = False                     # Use SR-ARAP for key-frame edit (sets _edit_target_frame automatically to True)

        self._translation           = None
        self._rotation              = None

        self._seq                   = None
        self._seq_folder            = None
        self._roi_file              = None
        self._handle_file           = None
        self._temp_window_start     = None
        self._temp_window_end       = None

        self._roi                   = None
        self._handles               = None
        self._anchors               = None

    # ----------------------------------------------------------------------------------------------
    # SETTERS

    def SetAlpha(self, new_alpha):
        self._alpha = new_alpha

    def SetTranslation(self, new_translation):
        if len(new_translation) != 3:
            raise Exception("Invalid value for translation. Must be of type (x, y, z)")
        self._translation = new_translation

    def SetRotation(self, new_angle, new_axis):
        if new_axis != "x" and new_axis != "y" and new_axis != "z":
            raise Exception("Invalid axis for rotation. Must be 'x', 'y' or 'z'")

        self._rotation = Rotation(new_angle, new_axis)

    def SetMaxIterations(self, new_max_iters):
        self._arap_max_iterations = new_max_iters

    def SetMaxError(self, new_max_error):
        self._arap_max_error = new_max_error

    def SetKeyframeMaxIterations(self, new_max_iters):
        self._keyframe_max_iterations = new_max_iters

    def SetKeyframeMaxError(self, new_max_error):
        self._keyframe_max_error = new_max_error

    def SetLstsqWeightHandles(self, new_weight):
        self._lstsq_weight_handles = new_weight

    def SetLstsqWeightAnchors(self, new_weight):
        self._lstsq_weight_anchors = new_weight

    def SetLstsqWeightFrames(self, new_weight):
        self._lstsq_weight_frames = new_weight

    def SetAnchorsRing(self, new_ring):
        self._anchors_ring = new_ring

    def SetEditTargetFrame(self, val=True):
        """
        Call this method to statically edit the target frame 
        before editing the whole sequence.
        """
        self._edit_target_frame = val

        if not self._edit_target_frame:
            self._use_sr_arap = False

    def SetType(self, ltype):
        """
        Set weighting scheme for 3D+t Laplacian.

        Options: 
            'dec',
            'comb' (combinatorial), 
            'exp' (exponential weights), 
            'cot' (cotangent weights in spatial edges, uniform weights for temporal edges)

        """
        self._l_type = ltype

    def UseSRARAP(self, use_srarap=True):
        """
        Statically edit target frame using SR-ARAP.
        """
        self._use_sr_arap = use_srarap
        self._edit_target_frame = True

    def SetConstraintsOnTemporalWindow(self, val):
        """
        Specifies whether to add constraints over the entire border
        in the temporal boundary of the sequence.
        """
        self._constrain_temporal_window = val

    def SetKeyframesPath(self, keyframes_path):
        """
        Instead of specifying translation/rotation of the handle, directly specify 
        target points over one or more keyframes.

        The folder must contain full OFF files which contain the target deformation
        for the specified frames (positions for all vertices in the specified ROI, with same
        indices). Files must be named as constraint-[f], with f the number of frame that 
        must satisfy that deformation (e.g. "constraint-01" to constrain the second frame). 

        TODO: allow to define positions for a subset of the vertices.
        
        """

        self._keyframe_constraints_folder = keyframes_path
        

    # ----------------------------------------------------------------------------------------------
    # LOAD METHODS
    def LoadSequence(self, sequence_folder, roi_file, handle_file, t0, dt, window_start, window_end):
        """
        Load sequence for editing.

        Arguments
        ---------

        sequence_folder         : Path to the folder where the sequence is located
        roi_file                : File with specification for indices of ROI vertices
        handle_file             : File with specification for indices of handle vertices
        t0                      : Time coordinate for the first frame
        dt                      : Difference between consecutive frames
        window_start            : Index of first frame in temporal window (0-based). 
                                  If None, temporal window starts from the beginning of the sequence.
        window_end              : Index of last frame in temporal window (0-based, included)
                                  If None, temporal window ends on the last frame of the sequence.
        """

         # Check that the window is correct
        if window_start is not None and not isinstance(window_start, int):
            raise Exception("Invalid data type for temporal window. Must be an integer.")

        if window_end is not None and not isinstance(window_end, int):
            raise Exception("Invalid data type for temporal window. Must be an integer.")

        if window_start > window_end:
            raise Exception("Invalid temporal window. Start of window must be less or equal to the end")

        self._anchors = None

        # Remember parameters for afterwards
        self._roi = None
        self._anchors = None

        self._seq_folder = sequence_folder
        self._roi_file = roi_file
        self._handle_file = handle_file
        self._temp_window_start = window_start
        self._temp_window_end = window_end

        # Load anchors, handles and roi
        if roi_file is not None or handle_file is not None:
            self._load_edit_parts(roi_file, handle_file)
        
        # Load sequence
        self._seq = loader.load_sequence(sequence_folder, t0, dt, window_start, window_end, self._roi)
        self._isstatic = False

        # Load full sequence
        # (we'll need one mesh for anchor calculation and the whole of it for saving results...)
        # TODO: think of a better way! Quick fix.
        self._full_seq = loader.load_sequence(sequence_folder, t0, dt, window_start, window_end, None)

    def LoadStaticSequence(self, sequence_folder, mesh_file, roi_file, handle_file, num_frames, t0, dt):
        """
        Create a sequence with no motion by repeating a mesh a number of times.

        Arguments
        ---------

        sequence_folder         : Path to the folder where the file is located
        mesh_file               : Name of the mesh file that will be repeated.
        roi_file                : File with specification for indices of ROI vertices. TODO: can be None
        handle_file             : File with specification for indices of handle vertices. TODO: can be None
        num_frames              : Number of times to repeat the mesh
        t0                      : Time coordinate for the first frame
        dt                      : Difference between consecutive frames
        """

        # Remember parameters for afterwards
        self._seq_folder = sequence_folder
        self._static_file = mesh_file
        self._static_num_frames = num_frames
        self._roi_file = roi_file
        self._handle_file = handle_file

        # Load anchors, handles and roi
        if handle_file is not None or roi_file is not None:
            self._load_edit_parts(roi_file, handle_file)

        # Create static sequence
        fullpath = sequence_folder + "/" + mesh_file
        self._seq = loader.load_static_sequence(fullpath, num_frames, t0, dt, self._roi)
        self._isstatic = True

        # Load full sequence
        # (see comment in LoadSequence)
        self._full_seq = loader.load_static_sequence(fullpath, num_frames, t0, dt, None)
        
    # ----------------------------------------------------------------------------------------------
    # EDIT METHODS

    def EditSequence(self, results_folder, target_frame = None, save_iterations = True, laplacian_path = None):
        """
        Edit target frame and propagate the changes over the mesh sequence.

        Arguments
        ---------
        results_folder              : Folder where results will be saved
        save_iterations             : If true, save results of each iteration
        target_frame                : Index of key-frame (0-based). If None, it will be the
                                      middle of the sequence.
        """

        # Check that we've loaded a sequence and all necessary data
        if self._seq is None:
            raise Exception("No sequence loaded.")

        # Calculate anchor vertices according to roi, if they weren't specified
        if self._roi_file is not None:
            if self._anchors is None:
                self._set_anchors(self._full_seq.topology)

            # Correct indices for handles and anchors (they're based on the 
            # full mesh, but the sequence to edit is based on the length of the ROI)
            handles_roi = []
            anchors_roi = []

            if self._handles is not None:
                for h in self._handles:
                    h_roi = self._seq.topology.real2roi_idx[h]
                    handles_roi.append(h_roi)

            for a in self._anchors:
                a_roi = self._seq.topology.real2roi_idx[a]
                anchors_roi.append(a_roi)
        else:
            handles_roi = None
            anchors_roi = None

        # Calculate target frame if not specified
        # (will remain None if there are user-specified constraints)
        self._target_frame = target_frame

        if self._target_frame is None and self._keyframe_constraints_folder is None:
            self._target_frame = int(self._seq.num_frames / 2)

        # Create results folder if it doesn't exist, and iterations folder if required
        if results_folder is not None and not os.path.isdir(results_folder):
            os.makedirs(results_folder)

        its_folder = None
        if results_folder is not None and save_iterations:
            its_folder = results_folder + "/iterations/"

            if not os.path.isdir(its_folder):
                os.makedirs(its_folder)

        self._anchors_roi = anchors_roi
        self._handles_roi = handles_roi

        # Save arguments
        if results_folder is not None:
            # Save colored off files showing the specified roi and handles
            mesh_complete = self._full_seq.topology

            if self._handle_file is not None:
                loader.save_with_colors(mesh_complete, results_folder + "/handle.off", self._handles, (0, 255, 0))

            if self._roi is not None:
                loader.save_with_colors(mesh_complete, results_folder + "/roi.off", self._roi, (255, 0, 0))

            if self._anchors is not None:
                loader.save_with_colors(mesh_complete, results_folder + "/anchors.off", self._anchors, (0, 0, 255))

            self._save_arguments(results_folder)

        # If working with combinatorial laplacian, scale temporal coordinate
        # so that norms work like in DEC
        #   DEC: dt=1 => || X ||^2 = x^2 + y^2 + z^2 + \alpha * 1^2
        #   combinatorial: use dt=\sqrt{ \alpha }
        if self._l_type == "comb":
            self._seq.dt = math.sqrt(self._alpha)

        # Initialize operator according to options
        # (will build it after adding constraints)
        l = self._init_laplacian()

        # Set editor arguments
        arap = ARAPEditor(l, self._seq, dim=4)
        arap.set_stop_max_iterations(self._arap_max_iterations)
        arap.set_stop_max_error(self._arap_max_error)

        # Edit keyframe, if necessary
        if self._edit_target_frame:
            # This changes the points specified in the topology structure, 
            # so coordinates in the mesh sequence remain the same...
            # TODO: quick fix.
            self._edit_keyframe(its_folder)

        # Add user-specified constraints
        if self._keyframe_constraints_folder is not None:
            
            for f in os.listdir(self._keyframe_constraints_folder):

                if f.startswith("constraint-"):
                    # Get keyframe id
                    try:
                        keyframe_num = int(f[len("constraint-"):-4])
                    except:
                        raise Exception("Invalid file name for keyframe constraints.")

                    if keyframe_num >= self._seq.num_frames:
                        continue
                
                    # Load vertex positions
                    keyframe_path = self._keyframe_constraints_folder + "/" + f
                    coords = loader.read_vertex_coordinates(keyframe_path, roi=None)

                    # Add them as constraints
                    n = coords.shape[0]
                    target_t = self._seq.get_temporal_coordinate(keyframe_num)

                    for i in range(n):
                        target_point = np.zeros(4, dtype=np.double)
                        target_point[:3] = coords[i,:]
                        target_point[3] = target_t
                        arap.add_constraint(keyframe_num, i, target_point, self._lstsq_weight_handles)

                    self._constrained_frames.add(keyframe_num)
                
            if len(self._constrained_frames) == 0:
                raise Exception("No constraints found in the specified folder.")

        # Add rest of constraints
        self._add_constraints(arap)

        # Build Laplacian and edit
        l.build(self._seq, quiet=args.quiet)

        # If specified, save laplacian and quit
        if laplacian_path:
            l._save_to_file(laplacian_path)
            return

        if self._edit_target_frame:
            arap.set_initial_points(self._seq)

        res, num_its = arap.edit(save_iterations_in=its_folder, create_new_sequence=False)

        # Save number of iterations
        writepath = results_folder + "/" + PARAMETERS_FILE
        with open(writepath, 'a') as f:
            f.write("\nIterations: " + str(num_its))
        
        # Save new sequence
        if results_folder is not None:

            # We have to combine the results of editing with the original mesh file,
            # which is why we loaded the full mesh sequence and replace it there
            # TODO!! Quick fix. No need to have it all in memory.
            if self._roi is not None:
                for k in range(self._full_seq.num_frames):
                    for i in range(self._seq.num_vertices):
                        real_ind = self._roi[i]
                        self._full_seq.coordinates[k][real_ind, :] = res.coordinates[k][i, :]
            else:
                self._full_seq = res

            loader.save_sequence(self._full_seq, results_folder)

        return num_its, self._full_seq
        
    # ----------------------------------------------------------------------------------------------
    # AUX

    def _edit_keyframe(self, its_folder=None):
    
        init_its_folder = None
        if its_folder is not None:
            init_its_folder = its_folder + "/keyframe"
        
            if not os.path.isdir(init_its_folder):
                os.makedirs(init_its_folder)

        # TODO: quick fix. Put coordinates of target frame
        # in mesh object associated with topology
        for i in range(self._seq.num_vertices):
            for c in range(3):
                self._seq.topology.vertices[i].point[c] = self._seq.coordinates[self._target_frame][i, c]

        arap_static = ARAPStaticEditor()
        arap_static.set_max_iterations(self._keyframe_max_iterations)
        arap_static.set_max_error(self._keyframe_max_error)

        # Add constraints for this editing
        for h in self._handles_roi:
            target_point = self._apply_transformation(h)
            arap_static.add_constraint(h, target_point, self._lstsq_weight_handles)

        for a in self._anchors_roi:
            target_point = self._seq.get_coordinates(self._target_frame, a, 3, copy=True)
            arap_static.add_constraint(a, target_point, self._lstsq_weight_handles)

        arap_static.edit(self._seq.topology, self._use_sr_arap, init_its_folder)

    def _add_constraints(self, arapeditor):
        """
        Add vertices that need to be constrained in the system
        """

        # 1. Entire first frame
        anchor_first_frame = (self._constrain_temporal_window \
            and (self._target_frame is None or self._target_frame != 0) \
            and 0 not in self._constrained_frames)

        last_f = self._seq.num_frames-1
        anchor_last_frame = (self._constrain_temporal_window \
            and (self._target_frame is None or self._target_frame != last_f) \
            and last_f not in self._constrained_frames)

        if anchor_first_frame:
            arapeditor.constrain_source_frame(0, self._lstsq_weight_frames)

        # 2. Middle frames: constraints in stationary anchors, except for the middle
        # frame which will be fully constrained if specified by the user.
        if self._anchors is not None:
            # stationary anchors in all frames 
            # (except the border ones, if they are being fully constrained)
            anchors_frame_start = 1 if anchor_first_frame else 0
            anchors_frame_end = self._seq.num_frames-1 if anchor_last_frame else self._seq.num_frames
            k_values = range(anchors_frame_start, anchors_frame_end)
            
            for k in k_values:
                if k in self._constrained_frames:
                    continue

                if self._target_frame is not None and k == self._target_frame:
                    if self._edit_target_frame:
                        # Target frame: constrain entirely. The result of editing 
                        # the target frame is in the topology structure (TODO: fix this)
                        # Constraint result in target frame and set as initial guess
                        target_t = self._seq.get_temporal_coordinate(self._target_frame)

                        for i in range(self._seq.num_vertices):
                            target_point = np.zeros(4, dtype=np.double)
                            target_point[:3] = self._seq.topology.vertices[i].point[:3]
                            target_point[3] = target_t

                            arapeditor.add_constraint(self._target_frame, i, target_point, self._lstsq_weight_handles)

                    else:
                        # Target frame: constrain specified deformation.
                        for i in self._handles_roi:
                            target_point = self._apply_transformation(i)
                            arapeditor.add_constraint(self._target_frame, i, target_point, self._lstsq_weight_handles)
                            
                # All frames: constraints in anchor vertices
                arapeditor.constrain_source_vertices(k, self._anchors_roi, self._lstsq_weight_anchors)

        # 3. Entire last frame
        if anchor_last_frame:
            arapeditor.constrain_source_frame(self._seq.num_frames-1, self._lstsq_weight_frames)

    def _apply_transformation(self, vertex):
        """
        Apply transformation to the specified point in the source sequence.
        Changes will be done over a copy of the point; the original sequence will not
        be modified.
        """
        
        # 1. Rotate
        res = np.zeros(4, dtype=np.double)

        if self._rotation is not None:
            res[:3] = self._rotation.apply(self._seq.get_coordinates(self._target_frame, vertex, 3, copy=True))
            res[3] = self._seq.get_temporal_coordinate(self._target_frame)
        else:
            # Just copy point
            res = self._seq.get_coordinates(self._target_frame, vertex, 4, copy=True)
            
        # 2. Translate
        if self._translation is not None:
            res[:3] += self._translation

        return res

    def _load_indices(self, filepath):
        inds = []
    
        with open(filepath) as f:
            for line in f:
                for word in line.split():
                    try:
                        inds.append(int(word))
                    except:
                        print("something strange happened while loading indices, continuing anyway...")
                        pass
        return inds

    def _load_edit_parts(self, roi_file, handle_file):
        # Get information for handles and ROI
        if roi_file is not None:
            if not os.path.isfile(roi_file):
                print("ROI file:", roi_file)
                raise Exception("No roi file found.")

            self._roi = self._load_indices(roi_file)
            self._roi = sorted(self._roi)

        if handle_file is not None:
            if not os.path.isfile(handle_file):
                raise Exception("No handles file found.")
            self._handles = self._load_indices(handle_file)

    def _set_anchors(self, full_mesh):
        # Stationary anchors: those at the boundary of the ROI
        self._anchors = set()
        
        # Iterate through every vertex in the ROI 
        # and check which belong to the boundary
        
        for v_i in self._roi:
            neighs = full_mesh.get_neighbouring_vertices(v_i)

            for v_j in neighs:
                if v_j not in self._roi:
                    self._anchors.add(v_i)
                    break

        # Anchor also [ring] layers of neighbours which are
        # outside the ROI.
        # NOTE: inside for now, it's easier...
        for i in range(self._anchors_ring):
            extra = set()
            
            for c in self._anchors:
                neighs = full_mesh.get_neighbouring_vertices(c)

                # Add neighbours which are outside (inside...) the ROI
                for n in neighs:
                    if n in self._roi:
                        extra.add(n)

            self._anchors = self._anchors.union(extra)

    def _init_laplacian(self):
        
        if self._l_type == "dec":
            lapl = Laplacian3Dt
        elif self._l_type == "comb":
            lapl = Laplacian3Dt_Comb
        elif self._l_type == "cot":
            lapl = Laplacian3Dt_Cot
        elif self._l_type == "exp":
            lapl = Laplacian3Dt_Exp
        else:
            raise Exception("Invalid Laplacian type")

        l = lapl(self._alpha, self._seq.num_frames, self._seq.num_vertices)
        return l

    def _save_arguments(self, results_folder):
        writepath = results_folder + "/" + PARAMETERS_FILE

        with open(writepath, 'w') as f:
            
            if self._isstatic:
                f.write("Static sequence, created from " + self._static_file + " using " + str(self._static_num_frames) + " frames")
            else:
                f.write("Temporal Window: ")
                if self._temp_window_start is not None:
                    f.write("From frame " + str(self._temp_window_start))

                    if self._temp_window_end is not None:
                        f.write(" to frame " + str(self._temp_window_end))
                    else:
                        f.write(" to end")
                else:
                    if self._temp_window_end is not None:
                        f.write("From beginning to frame " + str(self._temp_window_end))
                    else:
                        f.write("Whole sequence")

            f.write("\nTarget Frame: " + str(self._target_frame))
            f.write("\nLaplacian weights: " + self._l_type)
            f.write("\nStatic key-frame editing: " + str(self._edit_target_frame))

            if self._edit_target_frame:
                f.write("\nSR-ARAP: " + str(self._use_sr_arap))
                f.write("\nKeyframe editing - max iterations: " + str(self._keyframe_max_iterations))
                f.write("\nKeyframe editing - max error: " + str(self._keyframe_max_error))
                
            if self._roi is not None:
                f.write("\nSize of ROI: " + str(len(self._roi)))
                f.write("\nFile for ROI: " + self._roi_file)
            else:
                f.write("\nROI: entire mesh")

            if self._handle_file is not None:
                f.write("\nFile for handles: " + self._handle_file)

            if len(self._constrained_frames) != 0:
                f.write("\nUser-specified constraints in frames ")

                for fr in self._constrained_frames:
                    f.write(str(fr) + " ")

            if self._translation is not None:
                f.write("\nTranslation: (" + str(self._translation[0]) + ", " + str(self._translation[1]) + ", " + str(self._translation[2]) + ")")

            if self._rotation is not None and self._rotation.angle_deg != 0:
                f.write("\nRotation: " + str(self._rotation.angle_deg))
                f.write(" in axis " + self._rotation.axis)

            f.write("\nAlpha: " + str(self._alpha))
            f.write("\nt0: " + str(self._seq.t0))
            f.write("\ndt: " + str(self._seq.dt))
            f.write("\nConstrain temporal boundary: " + str(self._constrain_temporal_window))
            f.write("\nMax. Iterations: " + str(self._arap_max_iterations))
            f.write("\nMax. Error: " + str(self._arap_max_error))
            f.write("\nLeast Squares - Weight for Handles: " + str(self._lstsq_weight_handles))
            f.write("\nLeast Squares - Weight for Spatial Anchors: " + str(self._lstsq_weight_anchors))
            f.write("\nLeast Squares - Weight for Temporal Anchors: " + str(self._lstsq_weight_frames)) 
            f.write("\nAnchored boundary ring: " + str(self._anchors_ring))

# ---------------------------------------------------------------------------------------------------------------
# COMMAND LINE INTERFACE

if __name__ == '__main__':
    
    # TODO: explain arguments.   
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Mesh Animations Editor')
    
    # Non-optional arguments
    parser.add_argument("sequence_folder", action="store", help="Path to folder where the mesh sequence is saved. Supported formats: OBJ, OFF")
    
    # Optional arguments - Editing
    parser.add_argument("--roi", action="store", dest="roi_file")
    parser.add_argument("--handle", action="store", dest="handle_file")
    parser.add_argument("--keyframes", action="store", dest="keyframes_folder")
    parser.add_argument("--target", action="store", dest="target_frame", type=int)

    parser.add_argument("-t", "--translation", action="store", type=float, nargs=3, default=[0, 0, 0])
    parser.add_argument("-r", "--rotation", action="store", type=float, default=0)
    parser.add_argument("-x", "--axis", action="store", choices=set(["x", "y", "z"]))
    parser.add_argument("-w", "--window", action="store", type=int, nargs=2, help="Borders for temporal window (frame indices, 0-based)")

    # Other optional arguments
    parser.add_argument("--name", action="store")
    parser.add_argument("-l", "--laplacian-type", action="store", choices=set(["dec", "comb", "exp", "cot"]), dest="l_type", default="dec")
    parser.add_argument("-a", "--alpha", action="store", dest="alpha", type=float)
    parser.add_argument("--t0", action="store", dest="t0", type=float, default=0)
    parser.add_argument("--dt", action="store", dest="dt", type=float, default=1)
    
    parser.add_argument("-i", "--max-iterations", action="store", dest="max_iterations", type=int, default=None)
    parser.add_argument("-e", "--max-error", action="store", dest="max_error", type=float)
    parser.add_argument("--weight-handle", action="store", type=int, dest="weight_handle")
    parser.add_argument("--weight-sp-anchors", action="store", type=int, dest="weight_spatial_anchors")
    parser.add_argument("--weight-tp-anchors", action="store", type=int, dest="weight_temporal_anchors")
    parser.add_argument("--anchors-ring", action="store", type=int, dest="anchors_ring")
    parser.add_argument("--no-tmp-constraints", action="store_false", dest="constrain_temp", default=CONSTRAIN_TEMPORAL_WINDOW, help="Wether to add constraints in the temporal boundary of the sequence.")

    # Static sequences
    parser.add_argument("-s", "--static", action="store_true", default=False, dest="isstatic")
    parser.add_argument("-f", "--file", action="store", dest="mesh_file")
    parser.add_argument("-n", "--numframes", action="store", type=int, dest="numframes")

    # Flags
    parser.add_argument("--edit-target", action="store_true", dest="edit_target_frame", default=False)
    parser.add_argument("--sr-arap", action="store_true", dest="use_sr_arap", default=False)

    # Arguments not from the original code
    parser.add_argument("--save-laplacian", action="store", dest="laplacian_path")
    parser.add_argument('--quiet', action='store_true',
        help='If this is specified, progress bars won\'t be shown')

    args = parser.parse_args()

    # Initialize editor and set parameters 
    stEditor = SpacetimeEditor()

    if args.translation:
        tr = (args.translation[0], args.translation[1], args.translation[2])
        stEditor.SetTranslation(tr)

    if args.rotation:
        if not args.axis:
            print("Error: no axis defined for rotation.")
            exit(1)

        rot_angle = args.rotation
        rot_axis = args.axis
        stEditor.SetRotation(rot_angle, rot_axis)

    if args.alpha:
        stEditor.SetAlpha(args.alpha)

    if args.max_iterations is not None:
        stEditor.SetMaxIterations(args.max_iterations)

    if args.max_error:
        stEditor.SetMaxError(args.max_error)

    if args.weight_handle:
        stEditor.SetLstsqWeightHandles(args.weight_handle)

    if args.weight_spatial_anchors:
        stEditor.SetLstsqWeightAnchors(args.weight_spatial_anchors)

    if args.weight_temporal_anchors:
        stEditor.SetLstsqWeightFrames(args.weight_temporal_anchors)

    if args.anchors_ring:
        stEditor.SetAnchorsRing(args.anchors_ring)

    if args.constrain_temp is not None:
        stEditor.SetConstraintsOnTemporalWindow(args.constrain_temp)

    if args.use_sr_arap:
        stEditor.UseSRARAP()

    elif args.edit_target_frame:
        stEditor.SetEditTargetFrame()

    stEditor.SetType(args.l_type)

    if not args.t0:
        args.t0 = 0

    if not args.dt:
        args.dt = 1

    # Complete ROI and handle paths
    roi_full_path, handle_full_path, keyframes_fullpath = None, None, None

    if args.roi_file:
        roi_full_path = args.sequence_folder + "/" + args.roi_file

    if args.handle_file:
        handle_full_path = args.sequence_folder + "/" + args.handle_file

    # Load sequence
    if args.isstatic:
        if not args.numframes:
            print("Error: For a static sequence you must specify the number of frames.")
            exit(1)

        stEditor.LoadStaticSequence(args.sequence_folder, args.mesh_file, roi_full_path, handle_full_path, args.numframes, args.t0, args.dt)
    else:
        window_start = None
        window_end = None
        if args.window:
            window_start = args.window[0]
            window_end = args.window[1]

        stEditor.LoadSequence(args.sequence_folder, roi_full_path, handle_full_path, args.t0, args.dt, window_start, window_end)

    # Set constraints on user-specified keyframes
    if args.keyframes_folder:
        fullpath = args.sequence_folder + "/" + args.keyframes_folder
        stEditor.SetKeyframesPath(fullpath)
        
    # Make folder for saving results
    tnow = datetime.now()
    strnow = str(tnow.year) + str(tnow.month).zfill(2) + str(tnow.day).zfill(2) + str(tnow.hour).zfill(2) + str(tnow.minute).zfill(2) + str(tnow.second).zfill(2)

    if args.name is not None:
        save_path_folder = "results/" + args.name + "-" + strnow
    else:
        save_path_folder = "results/" + strnow

    if not os.path.isdir(save_path_folder):
        os.makedirs(save_path_folder)

    # Edit
    stEditor.EditSequence(save_path_folder, target_frame=args.target_frame, save_iterations = True, laplacian_path=args.laplacian_path)