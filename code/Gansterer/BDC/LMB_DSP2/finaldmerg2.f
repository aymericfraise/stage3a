      SUBROUTINE FINALDMERG2( JOBZ2, RKCT, N, EV, Q, LDQ, INDXQ, RHO, U,
     $                   SBRKP1, V, SBRK, CUTPNT, WORK, LWORK, IWORK,
     $                   TOL, INFO,MODWRK,LDM, LEN,ND  )
*
*  -- Routine written in LAPACK Version 3.0 style --
****************************************************
*     started: March 3, 2000
*     last modification: August 28, 2003
****************************************************
*
*     .. Scalar Arguments ..
      CHARACTER          JOBZ2
      INTEGER            CUTPNT, INFO, RKCT, LDQ, LWORK, N,
     $                   SBRK, SBRKP1, LEN, LDM,ND
      DOUBLE PRECISION   RHO, TOL
*     ..
*     .. Array Arguments ..
      INTEGER            INDXQ( * ), IWORK( * )
      DOUBLE PRECISION   EV( * ), Q( LDQ, * ), U( * ), V( * ),
     $                   WORK( * ), MODWRK(*)
*     ..
*
*  Purpose
*  =======
*
*  DMERG2 computes the updated eigensystem of a diagonal matrix after
*  modification by a rank-one symmetric matrix.  The diagonal matrix
*  consists of two diagonal submatrices, and the vectors defining the
*  rank-1 matrix similarly have two underlying subvectors each.
*  The dimension of the first subproblem is CUTPNT, the dimension of
*  the second subproblem is N-CUTPNT.
*
*  T = Q(in) ( EV(in) + RHO * Z*Z' ) Q'(in) = Q(out) * EV(out) * Q'(out)
*
*     where Z = Q'[V U']', where V is a row vector and U is a column
*     vector with dimensions corresponding to the two underlying
*     subproblems.
*
*     The eigenvectors of the original matrix are stored in Q, and the
*     eigenvalues in EV.  The algorithm consists of three stages:
*
*        The first stage consists of deflating the size of the problem
*        when there are multiple eigenvalues or if there is a zero in
*        the Z vector.  For each such occurrence the dimension of the
*        secular equation problem is reduced by one.  This stage is
*        performed by the routine DSRTDF.
*
*        The second stage consists of calculating the updated
*        eigenvalues. This is done by finding the roots of the secular
*        equation via the routine DLAED4 (as called by DLAED3M).
*        This routine also calculates the eigenvectors of the current
*        problem.
*
*        If( JOBZ.EQ.'D' ) then the final stage consists
*        of computing the updated eigenvectors directly using the updated
*        eigenvalues. The eigenvectors for the current problem are multiplied
*        with the eigenvectors from the overall problem.
*
*  Arguments
*  =========
*
*  JOBZ       (input) CHARACTER*1
*       = 'N': Compute eigenvalues only;
*       = 'D': Compute eigenvalues and eigenvectors.
*                 Eigenvectors are accumulated in the divide-and-conquer
*                 process.
*
*  RKCT   (input) INTEGER
*         The number of the rank modification which is accounted for
*         (RKCT >= 1). Required parameter, because the update operation of the
*         modification vector can be performed much more efficiently
*         if RKCT.EQ.1. In that case, the eigenvector matrix is still
*         block-diagonal. For RKCT.GE.2 the eigenvector matrix for the update
*         operation has filled up and is a full matrix.
*
*  N      (input) INTEGER
*         The dimension of the symmetric block tridiagonal matrix.
*         N >= 0.
*
*  EV     (input/output) DOUBLE PRECISION array, dimension (N)
*         On entry, the eigenvalues (=diagonal values) of the
*         rank-1-perturbed matrix.
*         On exit, the eigenvalues of the repaired matrix.
*
*  Q      (input/output) DOUBLE PRECISION array, dimension (LDQ,N)
*         On entry, the eigenvectors of the rank-1-perturbed matrix.
*         On exit, the eigenvectors of the repaired tridiagonal matrix.
*
*  LDQ    (input) INTEGER
*         The leading dimension of the array Q.  LDQ >= max(1,N).
*
*  INDXQ  (input/output) INTEGER array, dimension (N)
*         On entry, the permutation which separately sorts the two
*         subproblems in EV into ascending order.
*         On exit, the permutation which will reintegrate the
*         subproblems back into sorted order,
*         i.e. EV( INDXQ( I = 1, N ) ) will be in ascending order.
*
*  RHO    (input/output) DOUBLE PRECISION
*         The scalar in the rank-1 perturbation. Modified (multiplied
*         by 2) in DSRTDF.
*
*  U      (input) DOUBLE PRECISION array; dimension (SBRKP1), where SBRKP1
*         is the size of the first (original) block after CUTPNT.
*         The column vector of the rank-1 subdiagonal connecting the
*         two diagonal subproblems.
*         Theoretically, zero entries might have to be appended after U
*         in order to make it have dimension (N-CUTPNT). However, this
*         is not required because it can be accounted for in the
*         matrix-vector product using the argument SBRKP1.
*
*  SBRKP1 (input) INTEGER
*         Dimension of the relevant (non-zero) part of the vector U.
*         Equal to the size of the first original block after the
*         breakpoint.
*
*  V      (input) DOUBLE PRECISION array; dimension (SBRK), where SBRK
*         is the size of the last (original) block before CUTPNT.
*         The row vector of the rank-1 subdiagonal connecting the two
*         diagonal subproblems.
*         Theoretically, zero entries might have to be inserted in front
*         of V in order to make it have dimension (CUTPNT). However, this
*         is not required because it can be accounted for in the
*         matrix-vector product using the argument SBRK.
*
*  SBRK   (input) INTEGER
*         Dimension of the relevant (non-zero) part of the vector V.
*         Equal to the size of the last original block before the
*         breakpoint.
*
*  CUTPNT (input) INTEGER
*         The location of the last eigenvalue of the leading diagonal
*         block.  min(1,N) <= CUTPNT <= max(1,N).
*
*  WORK   (workspace) DOUBLE PRECISION array, dimension (LWORK)
*
*  LWORK  (input) INTEGER
*         The dimension of the array WORK.
*         In order to guarantee correct results in all cases,
*         LWORK must be at least ( 2*N**2 + 3*N  ). In many cases,
*         less workspace is required. The absolute minimum required is
*         ( N**2 + 3*N ).
*         If the workspace provided is not sufficient, the routine will
*         return a corresponding error code and report how much workspace
*         was missing (see INFO).
*         NOTE: This parameter is needed for determining whether enough
*               workspace is provided, and, if not, for computing how
*               much workspace is needed.
*
*  IWORK  (workspace) INTEGER array, dimension ( 4*N )
*
*  TOL    (input) DOUBLE PRECISION
*      User specified deflation tolerance for the routine DSRTDF.
*
*  INFO   (output) INTEGER
*          = 0:  successful exit.
*          < -200: not enough workspace
*                ABS(INFO + 200) numbers have to be stored in addition
*                to the workspace provided, otherwise some eigenvectors
*                will be incorrect.
*          < 0, > -99:  if INFO.EQ.-i, the i-th argument had an
*                       illegal value.
*          > 0:  if INFO.EQ.1, an eigenvalue did not converge
*                if INFO.EQ.2, the deflation counters DZ and DE do not sum
*                              up to the total number N-K of components
*                              deflated
*
*  Further Details
*  ===============
*
*  Written by 
*     Wilfried Gansterer and Bob Ward,
*     Department of Computer Science, University of Tennessee
*
*  Based on the design of the LAPACK code Dlaed1.f written by Jeff
*  Rutter, Computer Science Division, University of California at
*  Berkeley, and modified by Francoise Tisseur, University of Tennessee.
*
*  =====================================================================
*
*     .. Parameters ..
      INTEGER            DEBUG
      PARAMETER          ( DEBUG = 0 )
      DOUBLE PRECISION   ZERO, ONE
      PARAMETER          ( ZERO = 0.0D0, ONE = 1.0D0 )
*     .. Local Scalars ..
      CHARACTER          DEFL, JOBZ
      INTEGER            COLTYP, CPP1, I, IDLMDA, INDX, INDXC, INDXP,
     $                   IQ2, IS, IW, IZ, K, N1, N2, NMC, SPNEED, LWMIN,
     $                   TMPCUT, DZ, DE, L, ERROR
      DOUBLE PRECISION time
      INTEGER t_count, t_count_rate, t_count_max, t_tmp
*     ..
*     .. External Subroutines ..
      EXTERNAL           DGEMV, DSRTDF, DLAED3M, DLAMRG, XERBLA
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          MAX, MIN
*     ..
*     .. Executable Statements ..
*
*     ...Check the input parameters..................................
*
      INFO  = 0
      LWMIN = N**2 + 3*N
*
      IF( N.LT.0 )THEN
         INFO = -3
      ELSE IF( LDQ.LT.MAX( 1,N ) )THEN
         INFO = -6
      ELSE IF( ( CUTPNT.LT.MIN( 1,N ) ) .OR.
     $         ( CUTPNT.GT.MAX( 1,N ) ) )THEN
         INFO = -13
      ELSE IF( LWORK.LT.LWMIN )THEN
         INFO = -15
      END IF
      IF( INFO.NE.0 )THEN
         CALL XERBLA( 'DMERG2', -INFO )
         RETURN
      END IF
*
*****************************************************************************
*
*     Quick return if possible
*
      IF( N.EQ.0 ) RETURN
*
*****************************************************************************
*
*     The following values are integer pointers which indicate
*     the portion of the workspace used by a particular array in DSRTDF
*     and DLAED3M.
*


      if(JOBZ2.eq.'N')then
        if(ND.eq.0)then
            JOBZ = 'N'
        else
            JOBZ = 'D'
        end if
      else
        JOBZ = 'D'
      end if

      IZ     = 1
      IDLMDA = IZ + N
      IW     = IDLMDA + N
      IQ2    = IW + N
      IS     = IQ2 + N*N
*
*     After the pointer IS the matrix S is stored and read in WORK
*     in the routine DLAED3M.
*
      INDX   = 1
      INDXC  = INDX + N
      COLTYP = INDXC + N
      INDXP  = COLTYP + N

****    print *, 'BLOCKS: ', CUTPNT, N-CUTPNT

*
      IF( JOBZ.EQ.'N' )THEN
*
*        Assign the z-vector (starting at WORK( IZ )),
*        whose parts are already input.
*

         CALL DCOPY( CUTPNT, V, 1, WORK( IZ ), 1 )
         CALL DCOPY( N-CUTPNT, U, 1, WORK( IZ+CUTPNT ), 1 )
*
      ELSE
*
*        If eigenvectors are to be accumulated in the divide-and-conquer
*        process ( JOBZ.EQ.'D' ) form the z-vector which consists of
*        Q_1^T * V and Q_2^T * U.
*
         CPP1 = CUTPNT + 1
         IF( RKCT.EQ.1 )THEN
*
*           for the first rank modification the eigenvector matrix has
*           special block-diagonal structure and therefore Q_1^T * V and
*           Q_2^T * U can be formed separately.
*
            CALL DGEMV( 'T', SBRK, CUTPNT, ONE, Q( CUTPNT-SBRK+1,1 ),
     $                  LDQ, V, 1, ZERO, WORK( IZ ), 1 )
            NMC = N - CUTPNT
            CALL DGEMV( 'T', SBRKP1, NMC, ONE, Q( CPP1,CPP1 ), LDQ,
     $                  U, 1, ZERO, WORK( IZ+CUTPNT ), 1 )
*
         ELSE
*
*           for the higher rank modifications, the vectors V and U
*           have to be multiplied with the full eigenvector matrix
*
            CALL DGEMV( 'T', SBRK, N, ONE, Q( CUTPNT-SBRK+1,1 ),
     $                  LDQ, V, 1, ZERO, WORK( IZ ), 1 )
            CALL DGEMV( 'T', SBRKP1, N, ONE, Q( CPP1,1 ), LDQ, U,
     $                  1, ONE, WORK( IZ ), 1 )
*
         END IF
      END IF
*
*****************************************************************************
*
*     Deflate eigenvalues.
*
      IF( RKCT.EQ.1 )THEN
*
*        for the first rank modification we need the actual cutpoint
*
         N1     = CUTPNT
         TMPCUT = CUTPNT
      ELSE
*
*        for the later rank modifications there is no actual cutpoint any more
*
         N1     = N
*
*        The original value of CUTPNT has to be preserved for the next time
*        this subroutine is called (therefore, CUTPNT is an INPUT parameter
*        and not to be changed). Thus, assign N to TMPCUT and use the local
*        variable TMPCUT from now on for the cut point.
*
         TMPCUT = N
      END IF
*
*     initialize the flag DEFL (indicates whether deflation occurred -
*     this information is needed later in DLAED3M)
*
      DEFL = '0'
*
*     call DSRTDF for deflation
*

*    if (LEN.le.125 .and. LEN.ge.120)then
*            print *, JOBZ, LEN, WORK( IZ:IZ+N-1 )
*            print *, JOBZ, LEN, 'EV', EV(1:N)
*    end if

*    if (LEN.eq.121 .or. LEN.eq.121)then
*    DO 121 J = 1, LEN
*        print *, JOBZ, LEN,  J, MODWRK(1+(J-1)*LDM:(J-1)*LDM+N)
*  121 CONTINUE
*    end if



      CALL FINALDSRTDF( K, N, N1, EV, Q, LDQ, INDXQ, RHO, WORK( IZ ),
     $             WORK( IDLMDA ), WORK( IW ), WORK( IQ2 ),
     $             IWORK( INDX ), IWORK( INDXC ), IWORK( INDXP ),
     $             IWORK( COLTYP ), TOL, DZ, DE, INFO,MODWRK,LDM,
     $             LEN,JOBZ )

      if(JOBZ2.eq.'N' .and. JOBZ.eq.'D')then
        ND = K
      end if

******************************************************************************************************************
********************************************************switch here if k is large and len small enough for the ssp
******************************************************************************************************************


*    if (LEN.le.125 .and. LEN.ge.120)then
*            print *, JOBZ, LEN, WORK( IZ:IZ+N-1 )
*            print *, JOBZ, LEN, 'EV', EV(1:N)
*            print *, JOBZ, LEN, INDXQ(1:N)
*            print *, JOBZ, LEN, IWORK(INDX:INDX+N-1)
*    end if

*    if(IWORK(COLTYP+2).ne.0 .and. JOBZ.eq.'N')then
*        print *, 'INDXQ: ', INDXQ(1:N)
*        print *, 'INDX: ', IWORK(INDX:INDX+N-1)
*        print *, 'INDXC: ', IWORK(INDXC:INDXC+N-1)
*        print *, 'INDXP: ', IWORK(INDXP:INDXP+N-1)
*    end if

      if(JOBZ.eq.'G')then
      print *, 'Q:'
      DO 210 J = 1, N
        print *, Q(J,1:N)
  210 CONTINUE
      end if

** **    print *, 'DEFLATED:', K, N, N1

      IF( INFO.NE.0 )THEN
         WRITE( *,* ) '   Dmerg2: ERROR in DSRTDF, INFO =', INFO
         GO TO 800
      END IF

****DEBUGGING BEGIN***********************************************
      IF( DEBUG.NE.0 )THEN
         WRITE( *,* ) '  dmerg2: DLAMDA( 96 ) = ', WORK( IDLMDA+95 )
         WRITE( *,* ) '  dmerg2: DLAMDA( 97 ) = ', WORK( IDLMDA+96 )
         ERROR = 0
         DO 920 L = 1, K-1
            IF( WORK( IDLMDA+L-1 ).GE.WORK( IDLMDA+L ) )THEN
               WRITE( *,* ) ' K = ', K
               ERROR = 1
               WRITE( *,* ) '  dmerg2: PROBLEM for DLAED4 ',
     $                      'after DSRTDF !'
                WRITE( *,* ) 'DLAMDA(', L, ')=', WORK( IDLMDA+L-1 ),
     $                     ' >= ', 'DLAMDA(', L+1, ')=',
     $                      WORK( IDLMDA+L )
            END IF
  920    CONTINUE
*         IF( ERROR.EQ.1 )THEN
*            WRITE( *,* ) 'stopping after calling DSRTDF.'
*             STOP
*         END IF
      END IF
****DEBUGGING END*************************************************
*
****DEBUGGING BEGIN***********************************************
      IF( N-K.NE.DZ+DE )THEN
         WRITE( *,* ) '   Dmerg2: ERROR----Deflation counters',
     $                'DZ and DE do not sum up to N-K.'
         INFO = 2
         GO TO 800
      END IF
****DEBUGGING END*************************************************
*
      IF( K.LT.N )THEN
*
*        ....some deflation occurred in dsrtdf, set the flag DEFL
*            (needed in DLAED3M.f, since Givens rotations need to be
*            applied to the eigenvector matrix only if some deflation
*            happened)
*
         DEFL = '1'
*    print *,'DEFL:',DEFL

*    if(JOBZ.eq.'N')then
*        print *, 'INDXQ: ', INDXQ(1:N)
*        print *, 'INDX: ', IWORK(INDX:INDX+N-1)
*        print *, 'INDXC: ', IWORK(INDXC:INDXC+N-1)
*        print *, 'INDXP: ', IWORK(INDXP:INDXP+N-1)
*    end if

      END IF
*
****DEBUGGING BEGIN*********************************************
      IF( DEBUG.NE.0 )THEN
         WRITE( *,* ) '   Dmerg2: ', N-K, ' of N =', N,
     $                ' eigenvalues could be deflated.'
         WRITE( *,* ) '            ', DZ, ' due to zero components,',
     $                DE, ' due to close eigenvalues.'
      END IF
****DEBUGGING END***********************************************
*
*****************************************************************************
*
*     Solve the Secular Equation.
*
      IF( K.NE.0 .or. K.eq.0)THEN
*
*        ....not everything was deflated.
*
*        ....check whether enough workspace is available:
*
*        Note that the following (upper) bound SPNEED for the workspace
*        requirements should also hold in the extreme case TMPCUT=N,
*        which happens for every rank modification after the first one.
*
         SPNEED = IS + MAX( (IWORK( COLTYP ) + IWORK( COLTYP+1 ))*K,
     $            (IWORK( COLTYP+1 ) + IWORK( COLTYP+2 ))*K ) - 1
*
         IF( SPNEED.GT.LWORK )THEN
            INFO = -200 + LWORK - SPNEED 
            WRITE( *,* ) '   Dmerg2: ERROR - Workspace needed',
     $                   ' exceeds the workspace provided'
            WRITE( *,* ) '           by', SPNEED-LWORK, ' numbers.'
            WRITE( *,* ) '           LWORK = ', LWORK,
     $                   ', SPNEED = ', SPNEED
            WRITE( *,* ) '           4N^2/2+3N = ', 4*N**2/2+3*N
            WRITE( *,* ) '           IZ=', IZ, ', IDLAMDA=', IDLMDA,
     $                   ', IW=', IW, ', IQ2=', IQ2, ', IS=', IS
            WRITE( *,* ) '           SPNEED+1-IS=', SPNEED+1-IS
            WRITE( *,* ) '           K=', K, ', C1=', IWORK( COLTYP ),
     $                   ', C2=', IWORK( COLTYP+1 ),
     $                   ', C3=', IWORK( COLTYP+2 ),
     $                   ', TMPCUT=', TMPCUT
            GO TO 800
         END IF
*
****DEBUGGING BEGIN*********************************************
         IF( DEBUG.NE.0 )THEN
            WRITE( *,* )
            WRITE( *,* ) '   Dmerg2: COLTYP1 =', IWORK( COLTYP ),
     $                   ', COLTYP2 =', IWORK( COLTYP+1 ),
     $                   ', COLTYP3 =', IWORK( COLTYP+2 ),
     $                   ', IS =', IS
         END IF
****DEBUGGING END***********************************************
*
****DEBUGGING BEGIN***********************************************
         ERROR = 0
         DO 810 L = 1, K-1
            IF( WORK( IDLMDA+L-1 ).GE.WORK( IDLMDA+L ) )THEN
               WRITE( *,* ) ' K = ', K
               ERROR = 1
               WRITE( *,* ) '  dmerg2: PROBLEM for DLAED4 before ',
     $                      'calling DLAED3M !'
               WRITE( *,* ) '  DLAMDA(', L, ')=', WORK( IDLMDA+L-1 ),
     $                      ' >= ', 'DLAMDA(', L+1, ')=',
     $                      WORK( IDLMDA+L )
            END IF
  810    CONTINUE
*         IF( ERROR.EQ.1 )THEN
*            WRITE( *,* ) 'stopping before calling DLAED3M.'
*            STOP
*         END IF
****DEBUGGING END*************************************************
*
*        calling DLAED3M for solving the secular equation.
*

         CALL FINALDLAED3M( JOBZ, DEFL, K, N, TMPCUT, EV, Q, LDQ, RHO,
     $                 WORK( IDLMDA ), WORK( IQ2 ), IWORK( INDXC ),
     $                 IWORK( COLTYP ), WORK( IW ), WORK( IS ), INFO,
     $                 MODWRK,LDM,LEN,WORK(IS+N*K),IWORK( INDX ))

****    if (LEN.eq.125 .or. LEN.eq.125)then
*        DO 360 J = 1, LEN
*            print *, MODWRK(1+(J-1)*LDQ:(J-1)*LDQ+N)
*  360 CONTINUE
****    end if

         IF( INFO.NE.0 )THEN
            WRITE( *,* ) '   Dmerg2: ERROR in DLAED3M, INFO =', INFO
            GO TO 800 
         END IF
*
*        Prepare the INDXQ sorting permutation.
*
         N1 = K
         N2 = N - K
         CALL DLAMRG( N1, N2, EV, 1, -1, INDXQ )
      if(K.eq.0) then
        
         DO 15 I = 1, N
            INDXQ( I ) = I
   15    CONTINUE
      end if
*
      ELSE
*
*        ....everything was deflated (K.EQ.0)
*
         DO 10 I = 1, N
            INDXQ( I ) = I
   10    CONTINUE
*
      END IF
*
  800 CONTINUE
*
      RETURN
*
*     End of DMERG2
*
      END
