import timeit
import logging

import pandas
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import numpy as np
from scipy import linalg as linalg
from scipy import sparse as sparse
from scipy.sparse import linalg as linalgs

import bdc

class Laplacian:
    def __init__(self):
        self.L = None
        self.diag = None
        self.subdiag = None
        # shape : size, vertex count per mesh, frame count
        self.N = None
        self.V = None
        self.F = None

    def init_from_file(self,filepath):
        import pickle
        with open(filepath,"rb") as f:
            Ls,Ds = pickle.load(f)
            self.V = Ls[0].shape[0]
            self.F = len(Ls)
            self.N = self.V*self.F
            self.L_sparse = Laplacian.sparse_from_LD(Ls,Ds)
            print(self.L_sparse.shape)

    def init_random(self, V, F):
        self.V = V
        self.F = F
        self.N = V*F
        def random_sparse(n,m,density):
            # only accurate for big matrices with low densities (n*m > 1000000, density < .01)
            # adjust the density to make it account for the bias in the method
            density = density-(density**2)/2 
            np.random.seed()
            n_alt = int(n*n*density)
            row_ids = np.random.randint(n, size=n_alt)
            col_ids = np.random.randint(n, size=n_alt)
            data = np.random.rand(n_alt)
            S = sparse.coo_matrix((data, (row_ids, col_ids)), shape=(n,n))
            return S
        def random_sparse_symmetric(n):
            # density is n/6 as there should be about 6 elements per column because of a mesh's usual connectivity
            X = random_sparse(n,n,density=min(6/n,1))
            upper_X = sparse.triu(X) 
            result = upper_X + upper_X.T - sparse.diags(X.diagonal())
            return result
        def random_diagonal(n):
            return [np.random.random() for _ in range(n)]
        Ls = [random_sparse_symmetric(V) for _ in range(F)]
        Ds = [random_diagonal(V) for _ in range(F-1)]
        self.diag = Ls
        self.subdiag = Ds
        self.L = Laplacian.sparse_from_LD(Ls,Ds)

    @staticmethod
    def sparse_from_LD(Ls,Ds):
        f = len(Ls)
        if len(Ds) != f-1:
            raise ValueError()
        
        blocks = [[None for _ in range(f)] for _ in range(f)]
        for i in range(f):
            blocks[i][i] = Ls[i]
            if i < f-1:
                D = sparse.diags(Ds[i])
                blocks[i][i+1] = D
                blocks[i+1][i] = D
        L = sparse.bmat(blocks)
        return L

def bench(vfk_arr, logfile=None):
    print('logfile', logfile)
    if logfile:
        logging.basicConfig(filename=logfile, level=logging.INFO, format="%(message)s")
    
    L = Laplacian()
    for v,f,k in vfk_arr:
        t0 = timeit.default_timer()
        L.init_random(v,f)
        
        # t1 = timeit.default_timer()
        # M = L.L.todok()
        # t2 = timeit.default_timer()
        # linalgs.eigsh(M, k=M.shape[0]-1)
        # t3 = timeit.default_timer()

        # t1 = timeit.default_timer()        
        # d,e,block_size,nblks = bdc.to_fortran_indexing(L.diag, L.subdiag)
        # t2 = timeit.default_timer()
        # bdc.solve(d,e,block_size, nblks)
        # t3 = timeit.default_timer()

        t1 = timeit.default_timer()        
        M = L.L.todense()
        t2 = timeit.default_timer()
        linalg.eig(M)
        t3 = timeit.default_timer()

        
        log = "{0} {1} {2} {3} {4} {5}".format(v*f, v, f, k, t3-t2,'eig')
        debug = "{0:6}{1:6}{2:6}{3:10.3f}(+{4:10.3f})".format(v, f, k, t3-t2, t2-t0)
        print('debug', debug)
        if logfile:
            logging.info(log)

def plot(infile,outfile):
    df = pandas.read_csv(infile, names=['N','v','f','k','t','type'], sep=' ').sort_values(by='N')
    fig, ax = plt.subplots()
    colors = {0:'r',1:'g',2:'b'}
    for i,(label, grp) in enumerate(df.groupby('type')):
        grp.plot.scatter(x ='N',y='t',ax = ax, label=label, c=colors[i])
    fig.savefig(outfile)
    
def compare_results(e, k=None):
    # e : List[eigenvalues,eigenvectors]
    # sort eigenvalues and eigenvectors according to eigenvalues magnitude
    sorted_e = []
    for val,vec in e:
        p = np.argsort(np.abs(val))
        val = val[p]
        vec = vec[:, p]
        sorted_e.append((val,vec))
    
    
    # truncate results to first k elts
    if k:
        sorted_e = [(vals[-k:], vecs[:,-k:]) for (vals,vecs) in sorted_e]
    
    print("\n".join([str(ei[0]) for ei in sorted_e]))

    # compare results
    val0, vec0 = sorted_e[0]
    for val, vec in sorted_e:
        if val0.shape != val.shape:
            print("incompatible shapes, skipping comparison")
            continue
        print(np.allclose(val0, val, rtol=1e-1), "\t",
              np.allclose(np.abs(vec0), np.abs(vec), rtol=1e-1), "\t")

def runbench(V=range(100,1001,100), F=range(15,16,5), K=range(20,21,5), logfile=None):
    # v: block size, f: block count, k: number of eigvecs to compute 
    vfk_arr = [(v,f,k) for v in V for f in F for k in K]
    bench(vfk_arr, logfile=logfile)

def runcompare():
    e = []
    L = Laplacian()
    L.init_random(10,10)
    k = 10
    e1 = linalgs.eigsh(L.L, k=k)
    e2full = bdc.main(L.diag, L.subdiag)
    e2 = e2full[0][:k], e2full[1][:,:k]
    e.append(e1)
    e.append(e2full)
    print('e',[x.shape for x in e[0]], [x.shape for x in e[1]])
    v = [x[0] for x in e]
    # append results to e here
    compare_results(e,k=k)

if __name__ == '__main__':
    runbench(V=range(200,401,100), F=range(10,11,10), K=range(20,21,5), logfile='./res/log/bdcf10_19-07')
    # plot('./res/log/bdcf10_19-07','./res/plot/comp_19-07')
    # runcompare()