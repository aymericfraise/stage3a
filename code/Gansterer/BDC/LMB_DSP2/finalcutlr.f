      SUBROUTINE FINALCUTLR( START, N, BLKCT, BSIZES,
     $                  RANKS, CUT, LSUM, LBLKS, INFO )
*
*  -- Routine written in LAPACK Version 3.0 style --
****************************************************
*     started: June 19, 2000
*     last modification: June 10, 2003
****************************************************
*
*     .. Scalar Arguments ..
      INTEGER            START, N, BLKCT, MINRNK,
     $                   CUT, LSUM, LBLKS, INFO
*     ..
*     .. Array Arguments ..
      INTEGER            BSIZES( * ), RANKS( * )
*
*  Purpose
*  =======
*
*  CUTLR computes the optimal cut in a sequence of BLKCT neighboring
*  blocks whose sizes are given by the array BSIZES.
*  The sum of all block sizes in the sequence considered is given by N.
*  The cut is optimal in the sense that the difference of the sizes of
*  the resulting two halves is minimum over all cuts with minimum ranks
*  between blocks of the sequence considered.
*
*  Arguments
*  =========
*
*  START  (input) INTEGER
*         In the original array KSIZES of the calling routine DIBTDC, 
*         the position where the sequence considered in this routine starts.
*         START >= 1.
****NOTE: We could do without this parameter START.
****      For this purpose, we would return CUT as the relative position
****      inside the sequence. We would have to set 
****      CUT = CUT + I ???? (or CUT = I ????)
****      in this routine and
****      CUT = CUT + START
****      at the calling level (in DIBTDC) right after the call of CUTLR !
*
*  N      (input) INTEGER
*         The sum of all the block sizes of the sequence to be cut =
*         = sum_{i=1}^{BLKCT} BSIZES( I ).
*         N >= 3.
*
*  BLKCT  (input) INTEGER
*         The number of blocks in the sequence to be cut.
*         BLKCT >= 3. 
*
*  BSIZES (input) INTEGER array, dimension (BLKCT)
*         The dimensions of the (quadratic) blocks of the sequence to be
*         cut. sum_{i=1}^{BLKCT} BSIZES( I ) = N.
*
*  RANKS  (input) INTEGER array, dimension (BLKCT-1)
*         The ranks determining the approximations of the off-diagonal
*         blocks in the sequence considered.
*
*  CUT    (output) INTEGER
*         After the optimum cut has been determined, the position (in the
*         overall problem as worked on in DIBTDC !) of the last block in
*         the first half of the sequence to be cut.
*         START <= CUT <= START+BLKCT-2.
*
*  LSUM   (output) INTEGER
*         After the optimum cut has been determined, the sum of the
*         block sizes in the first half of the sequence to be cut.
*         LSUM < N.
*
*  LBLKS  (output) INTEGER
*         After the optimum cut has been determined, the number of the
*         blocks in the first half of the sequence to be cut.
*         1 <= LBLKS < BLKCT.
*
*  INFO   (output) INTEGER
*          = 0:  successful exit.
*          < 0:  illegal arguments.
*                if INFO = -i, the i-th (input) argument had an illegal
*                value.
*          > 0:  illegal results. 
*                if INFO = i, the i-th (output) argument had an illegal
*                value.
*
*  Further Details
*  ===============
*
*  Written by
*     Wilfried Gansterer,
*     Department of Computer Science, University of Tennessee
*
*  =====================================================================
*
*     .. Parameters ..
      INTEGER            DEBUG
      PARAMETER          ( DEBUG = 0 )
*     ..
*     .. Local Scalars ..
      INTEGER I, J, KSK, KSUM, KCHK, NHALF, TMPSUM, DEVIAT, MINDEV
*     ..
*     .. Executable Statements ..
*
*     ...Check the input parameters...
*
      INFO = 0
*
      IF( START.LT.1 ) THEN
         INFO = -1
      ELSE IF( N.LT.3 ) THEN
         INFO = -2
      ELSE IF( BLKCT.LT.3 ) THEN
         INFO = -3
      END IF
      IF( INFO.EQ.0 )THEN
         KSUM = 0
         KCHK = 0
         DO 1 I = 1, BLKCT
            KSK = BSIZES( I )
            KSUM = KSUM + KSK
            IF( KSK.LT.1 ) KCHK = 1
    1    CONTINUE
         IF( KSUM.NE.N .OR. KCHK.EQ.1 )THEN
            INFO = -4
         END IF
      END IF
      IF( INFO.NE.0 ) RETURN

***    print *, START, BLKCT
***    print *, RANKS(1:BLKCT-1)
***    print *, BSIZES(1:BLKCT)
*
*     determine smallest rank in the range considered
*
      if(0<1)then
        MINRNK = N
        DO 100 I = 1, BLKCT-1
           IF( BSIZES( I ).LT.MINRNK )THEN
              MINRNK = BSIZES( I )
           END IF
  100   CONTINUE
  
        NHALF  = N/2
        TMPSUM = 0
        MINDEV = N
        DO 110 I = 1, BLKCT
           TMPSUM = TMPSUM + BSIZES( I )
           IF( BSIZES( I ).EQ.MINRNK )THEN
*
*           determine deviation from "optimal" cut NHALF
*
            DEVIAT = ABS(TMPSUM - NHALF)
*
*           compare to best deviation so far
*
              IF( DEVIAT.LT.MINDEV )THEN
                 MINDEV = DEVIAT
                 CUT    = START+I-1
                 LBLKS  = I
                 LSUM   = TMPSUM
              END IF
           END IF
  110   CONTINUE
  
        else
          if(BLKCT.ge.3)then
              I=BLKCT/2
          else
              I=BLKCT/2
          end if
              MINRNK = RANKS(I)
  
        TMPSUM = 0
        DO 120 J = 1, I
           TMPSUM = TMPSUM + BSIZES( J )
  120   CONTINUE
  
                 CUT    = START+I-1
                 LBLKS  = I
                 LSUM   = TMPSUM
          
  
  
*        MINRNK = RANKS(BLKCT/2)
      end if

*
*     determine best cut among those with smallest rank
*
*
      IF( CUT.LT.START .OR. CUT.GE.START+BLKCT-1 )THEN
         INFO = 6
      ELSE IF( LSUM.LT.1 .OR. LSUM.GE.N )THEN
         INFO = 7
      ELSE IF( LBLKS.LT.1 .OR. LBLKS.GE.BLKCT )THEN
         INFO = 8
      END IF

      RETURN
*
*     End of CUTLR
*
      END
