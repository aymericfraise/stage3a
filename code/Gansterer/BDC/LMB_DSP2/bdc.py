from finaldsbtdc import dsbtdc

import numpy as np
import scipy.sparse

from pathlib import Path
import pickle

def load_object_from_file(filepath):
    filepath = Path(filepath)
    with filepath.open(mode='rb') as f:
        return pickle.load(f)

def save_object_to_file(obj, filepath):
    filepath = Path(filepath)
    with filepath.open(mode='wb+') as f:
        pickle.dump(obj,f)

def to_fortran_indexing(diag,subdiag):
    n_blocks = len(diag)
    block_size = diag[0].shape[0]
    D = np.empty((block_size,block_size,n_blocks), dtype=np.float)
    E = np.empty((2*block_size+1,2*block_size+1,n_blocks-1), dtype=np.float)
    for i,block in enumerate(diag):
        b = scipy.sparse.tril(block)
        D_ = D[:,:,i]
        D[:,:,i] = b.todense()
    for i,block in enumerate(subdiag):
        b = scipy.sparse.diags(block) # subdiagonal blocks are diagonal
        E[0:block_size,0:block_size,i] = b.todense()
    return D, E, block_size, n_blocks

def solve(d, e, block_size, nblks):
    # d: diag blocks , e: subdiag blocks
    jobz = 'D' # compute eigenvalues and eigenvectors
    jobacc = 'A' # choose tau1 and tau2 automatically according to tol
    # d, e, block_size, nblks = load_laplacian_fortran(laplacian_path)
    n = nblks*block_size # size of the symmetric block tridiag matrix
    ksizes = np.array([block_size for _ in range(nblks)]) # sizes of square diag blocks
    tol = 1e-7 # tolerance for the residuals of the eigpairs, 1e-7 ~= float32's epsilon
    tau1 = 1  # determined automatically according to tol (if jobacc=='A')
    tau2 = 1  # determined automatically according to tol (if jobacc=='A')
    ldz = n # leading dimension of the eigvecs array of dim (ldz,n) (=number of eigvecs to compute ?)
    # workspace sizes are set according to dsbtdc documentation
    lwork = 2*n**2+6*n if nblks == 1 else 2*n**2+3*n
    liwork = 5*n + 4*nblks - 1 # doc specifies 5*N + 4*NBLKS - 4, but 5*N + 4*NBLKS - 1 is the lowest value that doesn't cause a crash
    work = np.zeros((lwork),dtype=np.float)
    iwork = np.zeros((liwork),dtype=np.int)
    modwrk = np.zeros((n,n), dtype=np.float, order='c')
    # imodwrk isn't documented, but it is iterated on from 2*1 to 2*(nblks-1) in dibtdc and integer values are put inside so i infered the size and type
    imodwrk = np.zeros((2*(nblks-1),2*(nblks-1)),dtype=np.int)
    change = 0 # not documented so i guessed a value from its usage
    
    args = [jobz,jobacc,n,nblks,ksizes,d,e,tol,tau1,tau2,ldz,work,iwork,modwrk,imodwrk,change]
    
    # print(dsbtdc.__doc__)
    # ev=eigenvalues, z=eigenvectors, mingap=min(abs( ev[i+1]-ev[i] )), mingapi=index of mingap, info=see dsbtdc doc for the error codes
    ev, z, mingap, mingapi, info = dsbtdc(*args)
    # print(ev, z, mingap, mingapi, info)
    return ev, z

def main(diag, subdiag):
    d,e,block_size,nblks = to_fortran_indexing(diag,subdiag)
    return solve(d,e,block_size,nblks)

if __name__ == '__main__':
    laplacian_path = '/home/fraise/Documents/stage3a/code/meshes/Bunny/1of4trunc/laplacians/f0-23_a1e-06/L'
    # diag, subdiag = load_object_from_file(laplacian_path)
    diag, subdiag = load_object_from_file('/home/fraise/Documents/stage3a/code/Gansterer/BDC/LMB_DSP2/L')
    main(diag, subdiag)