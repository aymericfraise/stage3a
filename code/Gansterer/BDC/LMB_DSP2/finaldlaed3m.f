      SUBROUTINE FINALDLAED3M( JOBZ, DEFL, K, N, N1, D, Q, LDQ, RHO, 
     $                    DLAMDA, Q2, INDX, CTOT, W, S, INFO,
     $                    MODWRK,LDM,LEN,WORK,INDXQ  )
*
*  -- Routine written in LAPACK version 3.0 style --
****************************************************
*       started: July 22, 2000
*	last modification: June 12, 2008
****************************************************
*
*     .. Scalar Arguments ..
      CHARACTER          JOBZ, DEFL
      INTEGER            INFO, K, LDQ, N, N1, LEN,LDM
      DOUBLE PRECISION   RHO, TMPSUM
*     ..
*     .. Array Arguments ..
      INTEGER            CTOT( * ), INDX( * ),INDXQ( * )
      DOUBLE PRECISION   D( * ), DLAMDA( * ), Q( LDQ, * ), Q2( * ),
     $                   S( * ), W( * ), MODWRK(*), WORK(*)
      REAL     secnds,t0,t1
*     ..
*
*  Purpose
*  =======
*
*  DLAED3M finds the roots of the secular equation, as defined by the
*  values in D, W, and RHO, between 1 and K.  It makes the
*  appropriate calls to DLAED4 and then updates the eigenvectors by
*  multiplying the matrix of eigenvectors of the pair of eigensystems
*  being combined by the matrix of eigenvectors of the K-by-K system
*  which is solved here.
*
*  This code makes very mild assumptions about floating point
*  arithmetic. It will work on machines with a guard digit in
*  add/subtract, or on those binary machines without guard digits
*  which subtract like the Cray X-MP, Cray Y-MP, Cray C-90, or Cray-2.
*  It could conceivably fail on hexadecimal or decimal machines
*  without guard digits, but we know of none.
*
*  Arguments
*  =========
*
*
*  JOBZ    (input) CHARACTER*1
*          = 'N':  Do not accumulate eigenvectors;
*          = 'D':  Do accumulate eigenvectors in the divide-and-conquer
*                  process.
*
*  DEFL    (input) CHARACTER*1
*          = '0':  No deflation happened in DSRTDF
*          = '1':  Some deflation happened in DSRTDF (and therefore some
*                  Givens rotations need to be applied to the computed
*                  eigenvector matrix Q)
*
*  K       (input) INTEGER
*          The number of terms in the rational function to be solved by
*          DLAED4. 0 <= K <= N.
*
*  N       (input) INTEGER
*          The number of rows and columns in the Q matrix.
*          N >= K (deflation may result in N>K).
*
*  N1      (input) INTEGER
*          The location of the last eigenvalue in the leading submatrix.
*          min(1,N) <= N1 <= max(1,N-1).
*
*  D       (output) DOUBLE PRECISION array, dimension (N)
*          D(I) contains the updated eigenvalues for
*          1 <= I <= K.
*
*  Q       (output) DOUBLE PRECISION array, dimension (LDQ,N)
*          Initially the first K columns are used as workspace.
*          On output the columns 1 to K contain
*          the updated eigenvectors.
*
*  LDQ     (input) INTEGER
*          The leading dimension of the array Q.  LDQ >= max(1,N).
*
*  RHO     (input) DOUBLE PRECISION
*          The value of the parameter in the rank one update equation.
*          RHO >= 0 required.
*
*  DLAMDA  (input/output) DOUBLE PRECISION array, dimension (K)
*          The first K elements of this array contain the old roots
*          of the deflated updating problem.  These are the poles
*          of the secular equation. May be changed on output by
*          having lowest order bit set to zero on Cray X-MP, Cray Y-MP,
*          Cray-2, or Cray C-90, as described above.
*
*  Q2      (input) DOUBLE PRECISION array, dimension (LDQ2, N)
*          The first K columns of this matrix contain the non-deflated
*          eigenvectors for the split problem.
*
*  INDX    (input) INTEGER array, dimension (N)
*          The permutation used to arrange the columns of the deflated
*          Q matrix into three groups (see DLAED2).
*          The rows of the eigenvectors found by DLAED4 must be likewise
*          permuted before the matrix multiply can take place.
*
*  CTOT    (input) INTEGER array, dimension (4)
*          A count of the total number of the various types of columns
*          in Q, as described in INDX.  The fourth column type is any
*          column which has been deflated.
*
*  W       (input/output) DOUBLE PRECISION array, dimension (K)
*          The first K elements of this array contain the components
*          of the deflation-adjusted updating vector. Destroyed on
*          output.
*
*  S       (workspace) DOUBLE PRECISION array, dimension
*          ( MAX(CTOT(1)+CTOT(2),CTOT(2)+CTOT(3)) + 1 )*K
*          Will contain parts of the eigenvectors of the repaired matrix
*          which will be multiplied by the previously accumulated
*          eigenvectors to update the system. This array is a major
*          source of workspace requirements !
*
*  INFO    (output) INTEGER
*          = 0:  successful exit.
*          < 0:  if INFO = -i, the i-th argument had an illegal value.
*          > 0:  if INFO = i, eigenpair i was not computed successfully
*
*  Further Details
*  ===============
*
*  DLAED3M is LAPACK routine DLAED3 with small modifications made by
*  Wilfried Gansterer and Bob Ward,
*  Department of Computer Science, University of Tennessee.
*  Note that in contrast to the original DLAED3, this routine
*  DOES NOT require that N1 <= N/2 !
*
*  Based on contributions by
*     Jeff Rutter, Computer Science Division, University of California
*     at Berkeley, USA
*  Modified by Francoise Tisseur, University of Tennessee.
*
*  =====================================================================
*
*     .. Parameters ..
      INTEGER            DEBUG
      PARAMETER          ( DEBUG = 0 )
      DOUBLE PRECISION   ONE, ZERO
      PARAMETER          ( ONE = 1.0D0, ZERO = 0.0D0 )
*     ..
*     .. Local Scalars ..
      INTEGER            I, II, IQ2, J, N12, N2, N23, L, ERROR
      DOUBLE PRECISION   TEMP
*     ..
*     .. External Functions ..
      DOUBLE PRECISION   DLAMC3, DNRM2
      EXTERNAL           DLAMC3, DNRM2
*     ..
*     .. External Subroutines ..
      EXTERNAL           DCOPY, DGEMM, DLACPY, DLAED4, DLASET, XERBLA
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          MAX, SIGN, SQRT
*     ..
*     .. Executable Statements ..
*
*     Check the input parameters.
*
      INFO = 0
*
      IF( K.LT.0 ) THEN
         INFO = -3
      ELSE IF( N.LT.K ) THEN
         INFO = -4
      ELSE IF( ( N1.LT.MIN( 1,N ) ) .OR.
     $         ( N1.GT.MAX( 1,N ) ) ) THEN
         INFO = -5
      ELSE IF( LDQ.LT.MAX( 1, N ) ) THEN
         INFO = -8
      ELSE IF( RHO.LT.ZERO ) THEN
         INFO = -9
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'DLAED3M', -INFO )
         RETURN
      END IF

*
*     Quick return if possible
*
      IF( K.EQ.0 )then
      if( JOBZ.eq.'N' )then
*			print *, 'ALL DEFL:', K, DEFL
         N2  = N - N1
         N12 = CTOT( 1 ) + CTOT( 2 )
         N23 = CTOT( 2 ) + CTOT( 3 )

** **	print *, 'SEC2: ',JOBZ, secnds(0.0), N, K, LEN, N1, N2, N12, N23

                     CALL DLACPY( 'A', N, Len,
     $                            MODWRK, LDM, WORK( 1 ),
     $                            N )

            if(0<1)then
      I=1
  150 CONTINUE
        J = I
  160 CONTINUE
        J = J + 1
        if(INDXQ(J).eq.INDXQ(J-1)+1 .and. J.le.N)then
                  GO TO 160
        end if
        CALL DLACPY( 'A',J-I,LEN,WORK(INDXQ(I)),N,MODWRK(I),LDM)
        I = J
*			call DCOPY(LEN,WORK(INDXQ(I)),N1,MODWRK(I),LDM)
      if(I.le.N)then
        GO TO 150
      end if
                
            else 
                     CALL DGEMM( 'T', 'N', N, Len,
     $                           N,ONE, 
     $                           Q, LDQ, WORK( 1 ), N,
     $                           ZERO, MODWRK, LDM )
            end if
      end if
      RETURN
      endif
*
*     Modify values DLAMDA(i) to make sure all DLAMDA(i)-DLAMDA(j) can
*     be computed with high relative accuracy (barring over/underflow).
*     This is a problem on machines without a guard digit in
*     add/subtract (Cray XMP, Cray YMP, Cray C 90 and Cray 2).
*     The following code replaces DLAMDA(I) by 2*DLAMDA(I)-DLAMDA(I),
*     which on any of these machines zeros out the bottommost
*     bit of DLAMDA(I) if it is 1; this makes the subsequent
*     subtractions DLAMDA(I)-DLAMDA(J) unproblematic when cancellation
*     occurs. On binary machines with a guard digit (almost all
*     machines) it does not change DLAMDA(I) at all. On hexadecimal
*     and decimal machines with a guard digit, it slightly
*     changes the bottommost bits of DLAMDA(I). It does not account
*     for hexadecimal or decimal machines without guard digits
*     (we know of none). We use a subroutine call to compute
*     2*DLAMBDA(I) to prevent optimizing compilers from eliminating
*     this code.
*
****DEBUGGING BEGIN***********************************************
         ERROR = 0
         DO 810 L = 1, K-1
            IF( DLAMDA( L ).GE.DLAMDA( L+1 ) )THEN
               WRITE( *,* ) ' K = ', K
               ERROR = 1
               WRITE( *,* ) 'dlaed3m: PROBLEM for DLAED4 !'
               WRITE( *,* ) 'DLAMDA(', L, ')=', DLAMDA( L ),
     $                      ' >= ', 'DLAMDA(', L+1, ')=',
     $                      DLAMDA( L+1 )
            END IF
  810    CONTINUE
         IF( ERROR.EQ.1 )THEN
            WRITE( *,* ) 'stopping at 810.'
            STOP
         END IF
****DEBUGGING END*************************************************
*
      DO 10 I = 1, K
         DLAMDA( I ) = DLAMC3( DLAMDA( I ), DLAMDA( I ) ) - DLAMDA( I )
   10 CONTINUE
*
      DO 20 J = 1, K
*
*        ....calling DLAED4 for eigenpair J....
*
         CALL DLAED4( K, J, DLAMDA, W, Q( 1, J ), RHO, D( J ), INFO )
         IF( INFO.NE.0 )THEN
*
*           If the zero finder fails, the computation is terminated.
*
            WRITE( *,* ) '    DLAMDA(', J,') =', DLAMDA(J),
     $                   ', DLAMDA(', J+1,') =', DLAMDA(J+1)
            WRITE( *,* ) '    Dlaed3m: ERROR in Dlaed4, INFO =', INFO
            WRITE( *,* ) '             Zerofinder failed when',
     $                   ' computing eigenvalue D(', J, ') =',
     $                   D(J)
            INFO = J
            GO TO 120
         END IF
*
         IF( J.LT.K ) THEN
*
*           If the zero finder terminated properly, but the computed
*           eigenvalues are not ordered, issue an error statement
*           but continue computation.
*
            IF( DLAMDA(J).GE.DLAMDA(J+1) ) THEN
               WRITE( *,* ) ' ERROR ! DLAMDA(', J,') IS .GE. DLAMDA(',
     $                      J+1, ')!!!'
            END IF
            IF( D(J).LT.DLAMDA(J) .OR. D(J).GT.DLAMDA(J+1) ) THEN
               WRITE( *,* ) ' ERROR ! DLAMDA(', J,') =', DLAMDA(J),
     $                      ' D(', J, ') =', D( J )
               WRITE( *,* ) '  DLAMDA(', J+1,') =', DLAMDA(J+1)
            END IF
         END IF
   20 CONTINUE
*
      IF( K.EQ.1 ) GO TO 110
*
      IF( K.EQ.2 ) THEN
*
*        permute the components of Q(:,J) (the information returned by DLAED4
*        necessary to construct the eigenvectors) according to the permutation
*        stored in INDX, resulting from deflation
*
         DO 30 J = 1, K
            W( 1 ) = Q( 1, J )
            W( 2 ) = Q( 2, J )
            II = INDX( 1 )
            Q( 1, J ) = W( II )
            II = INDX( 2 )
            Q( 2, J ) = W( II )
   30    CONTINUE
         GO TO 110
      END IF
*
*     ....K.GE.3.... 
*     Compute updated W (used for computing the eigenvectors corresponding
*     to the previously computed eigenvalues).
*
      CALL DCOPY( K, W, 1, S, 1 )
*
*     Initialize W(I) = Q(I,I)
*
      CALL DCOPY( K, Q, LDQ+1, W, 1 )
      DO 60 J = 1, K
         DO 40 I = 1, J - 1
            W( I ) = W( I )*( Q( I, J ) / ( DLAMDA( I )-DLAMDA( J ) ) )
   40    CONTINUE
         DO 50 I = J + 1, K
            W( I ) = W( I )*( Q( I, J ) / ( DLAMDA( I )-DLAMDA( J ) ) )
   50    CONTINUE
   60 CONTINUE
      DO 70 I = 1, K
         W( I ) = SIGN( SQRT( -W( I ) ), S( I ) )
   70 CONTINUE
*
*     Compute eigenvectors of the modified rank-1 modification (using the
*     vector W).
*
      DO 100 J = 1, K
         DO 80 I = 1, K
            S( I ) = W( I ) / Q( I, J )
   80    CONTINUE
         TEMP = DNRM2( K, S, 1 )
         DO 90 I = 1, K
*
*           apply the permutation resulting from deflation as stored
*           in INDX
*
            II = INDX( I )
            Q( I, J ) = S( II ) / TEMP
   90    CONTINUE
  100 CONTINUE
*
***************************************************************************
*
* ....updating the eigenvectors....
*
*     NOTE: Here begins a portion of the code which can be improved further:
*
*       IF WE DON'T ACCUMULATE EIGENVECTORS (JOBZ.EQ.'N'), AND
*          IF NO DEFLATION happened, then we can get rid of the following
*          operations.
*
*       IF WE DON'T ACCUMULATE EIGENVECTORS (JOBZ.EQ.'N'), AND
*          IF some deflation happened, then we have to apply Givens
*          rotations to the eigenvector matrix. For now this is
*          done naively and inefficiently in the part that follows.
*          We have to improve this update of the eigenvectors later.
*
  110 CONTINUE
*
         N2  = N - N1
         N12 = CTOT( 1 ) + CTOT( 2 )
         N23 = CTOT( 2 ) + CTOT( 3 )

*	print *, 'SEC: ',JOBZ, secnds(0.0), N, K, LEN, N1, N2, N12, N23


      IF( JOBZ.EQ.'N')then

        
*	print *, 'DEFL:', DEFL, N, N12, N23, K, CTOT(1), LEN
*	print *, 'INDXQ:', INDXQ(1:N)

*	IF( N23.NE.0 ) THEN
*		print *, 'BEFORE PERMUTATION'
*		DO 360 J = 1, N
*			print *, MODWRK(1+(J-1)*LDQ:(J-1)*LDQ+N)
*  360 CONTINUE
*	end if
        

*	IF( N23.eq.0 ) THEN

          CALL DLACPY( 'A', N, LEN,
     $                 MODWRK( 1 ), LDM, WORK( 1 ),
     $                 N)
      I=1
  250 CONTINUE
        J = I
  260 CONTINUE
        J = J + 1
        if(INDXQ(J).eq.INDXQ(J-1)+1 .and. J.le.N)then
                  GO TO 260
        end if
        CALL DLACPY( 'A',J-I,LEN,WORK(INDXQ(I)),N,MODWRK(I),LDM)
        I = J
      if(I.le.N)then
        GO TO 250
      end if

*	end if

*	IF( N23.NE.0 ) THEN
*		print *, 'AFTER PERMUTATION'
*		DO 370 J = 1, N
*			print *, MODWRK(1+(J-1)*LDQ:(J-1)*LDQ+N)
*  370 CONTINUE
*	end if

**	print *, 'Q2-1:', N1
**	DO 211 J = 1, N1
**		print *, J, Q2(1+(J-1)*N1:J*N1)
**  211 CONTINUE

         IQ2 = N1*N12 + 1

**	print *, 'Q2-2:', N23
**	DO 212 J = 1, N23
**		print *, J, Q2(IQ2+(J-1)*N2:IQ2-1+J*N2)
**  212 CONTINUE

*	if(LEN.eq.1)then
*	print *, 'Start Q:'
*	DO 213 J = 1, N
*		print *, Q(J,1:N)
*  213 CONTINUE
*	print *, 'End Q:'
*	end if


    
       CALL DLACPY( 'A', K, Len,
     $              MODWRK, LDM, WORK( 1 ),
     $              N )
       CALL DGEMM( 'T', 'N', K, Len,
     $             K,ONE, 
     $             Q, LDQ, WORK( 1 ), N,
     $             ZERO, MODWRK, LDM )

      end if

      if(JOBZ.eq.'G')then
      print *, 'BEFORE Q:'
      DO 210 J = 1, N
        print *, Q(J,1:N)
  210 CONTINUE
      end if


      IF( JOBZ.EQ.'D')THEN

*        NOTE that we will have to treat the case
*        ( JOBZ.EQ.'N' .AND. DEFL.EQ.'1' )
*        SEPARATELY !!! (more efficient application of Givens rotations)
*
*        Compute the updated eigenvectors. (NOTE that every call of
*        DGEMM requires three DISTINCT arrays !!! That is why we have to
*        use S the way we use it...)
*

*
*        copy Q( CTOT(1)+1:K,1:K ) to S
*
         CALL DLACPY( 'A', N23, K, Q( CTOT( 1 )+1, 1 ), LDQ, S, N23 )
         IQ2 = N1*N12 + 1
*
         IF( N23.NE.0 ) THEN
*
*           multiply the second part of Q2 (the eigenvectors of the
*           lower block) with S and write the result into the lower part of
*           Q, i.e., Q( N1+1:N,1:K )
*
            CALL DGEMM( 'N', 'N', N2, K, N23, ONE, Q2( IQ2 ), N2, S,
     $                  N23, ZERO, Q( N1+1,1 ), LDQ )
         ELSE
            CALL DLASET( 'A', N2, K, ZERO, ZERO, Q( N1+1, 1 ), LDQ )
         END IF
*
*        copy Q( 1:CTOT(1)+CTOT(2),1:K ) to S
*
         CALL DLACPY( 'A', N12, K, Q, LDQ, S, N12 )
*
         IF( N12.NE.0 ) THEN
*
*           multiply the first part of Q2 (the eigenvectors of the
*           upper block) with S and write the result into the upper part of
*           Q, i.e., Q( 1:N1,1:K )
*
            CALL DGEMM( 'N', 'N', N1, K, N12, ONE, Q2, N1, S, N12, ZERO,
     $                  Q, LDQ )
         ELSE
            CALL DLASET( 'A', N1, K, ZERO, ZERO, Q( 1, 1 ), LDQ )
         END IF

      END IF

      if(JOBZ.eq.'G')then
      print *, 'After Q:'
      DO 220 J = 1, N
        print *, Q(J,1:N)
  220 CONTINUE
      end if

*
  120 CONTINUE
      RETURN
*
*     End of DLAED3M
*
      END
