      SUBROUTINE FINALDSRTDF( K, N, N1, D, Q, LDQ, INDXQ, RHO, Z,
     $                   DLAMDA, W, Q2, INDX, INDXC, INDXP, COLTYP,
     $                   RELTOL, DZ, DE, INFO,MODWRK,LDM,LEN, JOBZ )
*
*  -- Routine written in LAPACK Version 3.0 style --
****************************************************
*     started: March 15, 2000
*     last modification: August 11, 2008
****************************************************
*
*     .. Scalar Arguments ..
      CHARACTER          JOBZ
      INTEGER            INFO, K, LDQ, N, N1, DZ, DE, LEN, LDM
      DOUBLE PRECISION   RHO, RELTOL
*     ..
*     .. Array Arguments ..
      INTEGER            COLTYP( * ), INDX( * ), INDXC( * ), INDXP( * ),
     $                   INDXQ( * )
      DOUBLE PRECISION   D( * ), DLAMDA( * ), Q( LDQ, * ), Q2( * ),
     $                   W( * ), Z( * ), MODWRK(*)
*     ..
*
*  Purpose
*  =======
*
*  DSRTDF merges the two sets of eigenvalues of a rank-one modified
*  diagonal matrix D+ZZ^T together into a single sorted set. Then it tries
*  to deflate the size of the problem.
*  There are two ways in which deflation can occur:  when two or more
*  eigenvalues of D are close together or if there is a tiny entry in the
*  Z vector.  For each such occurrence the order of the related secular
*  equation problem is reduced by one.
*
*  Arguments
*  =========
*
*  K      (output) INTEGER
*         The number of non-deflated eigenvalues, and the order of the
*         related secular equation. 0 <= K <=N.
*
*  N      (input) INTEGER
*         The dimension of the diagonal matrix.  N >= 0.
*
*  N1     (input) INTEGER
*         The location of the last eigenvalue in the leading sub-matrix.
*         min(1,N) <= N1 <= max(1,N).
*
*  D      (input/output) DOUBLE PRECISION array, dimension (N)
*         On entry, D contains the eigenvalues of the two submatrices to
*         be combined.
*         On exit, D contains the trailing (N-K) updated eigenvalues
*         (those which were deflated) sorted into increasing order.
*
*  Q      (input/output) DOUBLE PRECISION array, dimension (LDQ, N)
*         On entry, Q contains the eigenvectors of two submatrices in
*         the two square blocks with corners at (1,1), (N1,N1)
*         and (N1+1, N1+1), (N,N).
*         On exit, Q contains the trailing (N-K) updated eigenvectors
*         (those which were deflated) in its last N-K columns.
*
*  LDQ    (input) INTEGER
*         The leading dimension of the array Q.  LDQ >= max(1,N).
*
*  INDXQ  (input/output) INTEGER array, dimension (N)
*         The permutation which separately sorts the two sub-problems
*         in D into ascending order.  Note that elements in the second
*         half of this permutation must first have N1 added to their
*         values. Destroyed on exit.
*
*  RHO    (input/output) DOUBLE PRECISION
*         On entry, the off-diagonal element associated with the rank-1
*         cut which originally split the two submatrices which are now
*         being recombined.
*         On exit, RHO has been modified to the value required by
*         DLAED3M (made positive and multiplied by two, such that
*         the modification vector has norm one).
*
*  Z      (input/output) DOUBLE PRECISION array, dimension (N)
*         On entry, Z contains the updating vector.
*         On exit, the contents of Z has been destroyed by the updating
*         process.
*
*  DLAMDA (output) DOUBLE PRECISION array, dimension (N)
*         A copy of the first K non-deflated eigenvalues which
*         will be used by DLAED3M to form the secular equation.
*
*  W      (output) DOUBLE PRECISION array, dimension (N)
*         The first K values of the final deflation-altered z-vector
*         which will be passed to DLAED3M.
*
*  Q2     (output) DOUBLE PRECISION array, dimension
*         ( N*N ) (if everything is deflated) or
*         ( N1*(COLTYP(1)+COLTYP(2)) + (N-N1)*(COLTYP(2)+COLTYP(3)) )
*         (if not everything is deflated)
*         If everything is deflated, then N*N intermediate workspace
*         is needed in Q2.
*         If not everything is deflated, Q2 contains on exit a copy of the
*         first K non-deflated eigenvectors which will be used by
*         DLAED3M in a matrix multiply (DGEMM) to accumulate the new
*         eigenvectors, followed by the N-K deflated eigenvectors.
*
*  INDX   (workspace) INTEGER array, dimension (N)
*         The permutation used to sort the contents of DLAMDA into
*         ascending order.
*
*  INDXC  (output) INTEGER array, dimension (N)
*         The permutation used to arrange the columns of the deflated
*         Q matrix into three groups:  columns in the first group contain
*         non-zero elements only at and above N1 (type 1), columns in the
*         second group are dense (type 2), and columns in the third group
*         contain non-zero elements only below N1 (type 3).
*
*  INDXP  (workspace) INTEGER array, dimension (N)
*         The permutation used to place deflated values of D at the end
*         of the array.  INDXP(1:K) points to the nondeflated D-values
*         and INDXP(K+1:N) points to the deflated eigenvalues.
*
*  COLTYP (workspace/output) INTEGER array, dimension (N)
*         During execution, a label which will indicate which of the
*         following types a column in the Q2 matrix is:
*         1 : non-zero in the upper half only;
*         2 : dense;
*         3 : non-zero in the lower half only;
*         4 : deflated.
*         On exit, COLTYP(i) is the number of columns of type i,
*         for i=1 to 4 only.
*
*  RELTOL (input) DOUBLE PRECISION
*         User specified deflation tolerance. If RELTOL.LT.20*EPS, then
*         the standard value used in the original LAPACK routines is used.
*
*  DZ     (output) INTEGER, DZ.GE.0
*         counts the deflation due to a small component in the modification
*         vector.
*
*  DE     (output) INTEGER, DE.GE.0
*         counts the deflation due to close eigenvalues.
*
*  INFO   (output) INTEGER
*          = 0:  successful exit.
*          < 0:  if INFO = -i, the i-th argument had an illegal value.
*
*  Further Details
*  ===============
*
*  DSRTDF is LAPACK routine DLAED2 with modifications made by
*  Wilfried Gansterer and Bob Ward,
*  Department of Computer Science, University of Tennessee
*  to allow a block divide and conquer scheme
*  
*  DLAED2 was written by Jeff Rutter, Computer Science Division, University 
*  of California at Berkeley, USA, and modified by Francoise Tisseur, 
*  University of Tennessee.
*
*  =====================================================================
*
*     .. Parameters ..
      INTEGER            DEBUG
      PARAMETER          ( DEBUG = 0 )
      DOUBLE PRECISION   MONE, ZERO, ONE, TWO, EIGHT
      PARAMETER          ( MONE = -1.0D0, ZERO = 0.0D0, ONE = 1.0D0,
     $                   TWO = 2.0D0, EIGHT = 8.0D0 )
*     ..
*     .. Local Arrays ..
      INTEGER            CTOT( 4 ), PSM( 4 )
*     ..
*     .. Local Scalars ..
      INTEGER            CT, I, IMAX, IQ1, IQ2, J, JMAX, JS, K2, N1P1,
     $                   N2, NJ, PJ, L, ERROR, FACTMP
      DOUBLE PRECISION   C, EPS, S, T, TAU, TOL
*     ..
*     .. External Functions ..
      INTEGER            IDAMAX
      DOUBLE PRECISION   DLAMCH, DLAPY2
      EXTERNAL           IDAMAX, DLAMCH, DLAPY2
*     ..
*     .. External Subroutines ..
      EXTERNAL           DCOPY, DLACPY, DLAMRG, DROT, DSCAL, XERBLA
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          DABS, MAX, MIN, SQRT
*     ..
*     .. Executable Statements ..
*
*     Check the input parameters.
*
      INFO = 0
*
      IF( N.LT.0 ) THEN
         INFO = -2
      ELSE IF( ( N1.LT.MIN( 1,N ) ) .OR.
     $         ( N1.GT.MAX( 1,N ) ) ) THEN
         INFO = -3
      ELSE IF( LDQ.LT.MAX( 1, N ) ) THEN
         INFO = -6
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'DSRTDF', -INFO )
         RETURN
      END IF
*
*     Initialize deflation counters
*
      DZ = 0
      DE = 0
*
*****************************************************************************
*
*     Quick return if possible
*
      IF( N.EQ.0 ) RETURN
*
*****************************************************************************
*
      N2   = N - N1
      N1P1 = N1 + 1
*
*     Modify Z so that RHO is positive.
*
      IF( RHO.LT.ZERO )THEN
         CALL DSCAL( N2, MONE, Z( N1P1 ), 1 )
      END IF
*
*     Normalize z so that norm2(z) = 1.  Since z is the concatenation of
*     two normalized vectors, norm2(z) = sqrt(2). (NOTE that this holds also
*     here in the approximate block-tridiagonal D&C because the two vectors are
*     singular vectors, but it would not necessarily hold in a D&C for a
*     general banded matrix !)
*
      T = ONE / SQRT( TWO )
      CALL DSCAL( N, T, Z, 1 )
*
*     NOTE: at this point the value of RHO is modified in order to
*           normalize Z:    RHO = ABS( norm2(z)**2 * RHO
*           in our case:    norm2(z) = sqrt(2), and therefore: 
*
      RHO = DABS( TWO*RHO )
*
*     Sort the eigenvalues into increasing order
*
      DO 10 I = N1P1, N
         INDXQ( I ) = INDXQ( I ) + N1
   10 CONTINUE
*
*     re-integrate the deflated parts from the last pass
*
      DO 20 I = 1, N
         DLAMDA( I ) = D( INDXQ( I ) )
   20 CONTINUE
      CALL DLAMRG( N1, N2, DLAMDA, 1, 1, INDXC )
      DO 30 I = 1, N
         INDX( I ) = INDXQ( INDXC( I ) )
   30 CONTINUE

      INDXQ(1:N) = 0
*
****DEBUGGING BEGIN***********************************************
      IF( DEBUG.NE.0 ) THEN
         ERROR = 0
         DO 810 L = 1, N-1
            IF( DLAMDA( L ).GE.DLAMDA( L+1 ) )THEN
               ERROR = 1
               WRITE( *,* ) '   dsrtdf: after initial sorting: DLAMDA(',
     $                      L, ')=', DLAMDA( L ),
     $                      ' >= ', 'DLAMDA(', L+1, ')=',
     $                      DLAMDA( L )
            END IF
  810    CONTINUE
*         IF( ERROR.EQ.1 )THEN
*            WRITE( *,* ) 'stopping before calling DLAED3M.'
*            STOP
*         END IF
      END IF
****DEBUGGING END*************************************************
*
*     Calculate the allowable deflation tolerance
*
      IMAX = IDAMAX( N, Z, 1 )
      JMAX = IDAMAX( N, D, 1 )
      EPS  = DLAMCH( 'Epsilon' )
      IF( RELTOL.LT.20*EPS )THEN
*
*        use the standard deflation tolerance from the original LAPACK code
*
         TOL = EIGHT*EPS*MAX( DABS( D( JMAX ) ), DABS( Z( IMAX ) ) )
      ELSE
*
*        otherwise set TOL to the input parameter RELTOL !
*
         TOL = RELTOL*MAX( DABS( D( JMAX ) ), DABS( Z( IMAX ) ) )
      END IF
*
****DEBUGGING BEGIN*********************************************
      IF( DEBUG.NE.0 ) THEN
         WRITE( *,* ) '    Dsrtdf: TOL =', TOL
      END IF
****DEBUGGING END***********************************************
*
*     If the rank-1 modifier is small enough, no more needs to be done
*     except to reorganize Q so that its columns correspond with the
*     elements in D.
*
      IF( RHO*DABS( Z( IMAX ) ).LE.TOL )THEN 
*
*        complete deflation because of small rank-one modifier
*        NOTE: in this case we need N*N space in the array Q2 !
*

         DZ  = N
         K   = 0
         IQ2 = 1
         DO 40 J = 1, N
            I = INDX( J )
        INDXQ(J) = I
            CALL DCOPY( N, Q( 1, I ), 1, Q2( IQ2 ), 1 )
*	if(JOBZ.eq.'N')then
*		print *, Q(1:N,I)
*	end if
            DLAMDA( J ) = D( I )
            IQ2         = IQ2 + N
   40    CONTINUE
         CALL DLACPY( 'A', N, N, Q2, N, Q, LDQ )
         CALL DCOPY( N, DLAMDA, 1, D, 1 )
*	if(JOBZ.eq.'N')then
*		print *, indxq(1:n)
*		print *, 'COMPLETE', N
*	end if
         GO TO 190
      END IF
*
*     If there are multiple eigenvalues then the problem deflates.  Here
*     the number of equal eigenvalues is found.  As each equal
*     eigenvalue is found, an elementary reflector is computed to rotate
*     the corresponding eigensubspace so that the corresponding
*     components of Z are zero in this new basis.
*
*     initialize the column types
*
      DO 50 I = 1, N1
*
*        first N1 columns are initially (before deflation) of type 1
*
         COLTYP( I ) = 1
   50 CONTINUE
      DO 60 I = N1P1, N
*
*        columns N1+1 to N are initially (before deflation) of type 3
*
         COLTYP( I ) = 3
   60 CONTINUE
*
      K  = 0
      K2 = N + 1
      DO 70 J = 1, N
         NJ = INDX( J )
         IF( RHO*DABS( Z( NJ ) ).LE.TOL ) THEN
*
*           Deflate due to small z component.
*
            DZ = DZ + 1
            K2 = K2 - 1
*
*           deflated eigenpair, therefore column type 4
*
            COLTYP( NJ ) = 4
            INDXP( K2 )  = NJ
        INDXQ(K2) = NJ
            IF( J.EQ.N )
     $         GO TO 100
         ELSE
*
*           not deflated
*
            PJ = NJ
            GO TO 80
         END IF
   70 CONTINUE
      FACTMP=1
   80 CONTINUE
      J  = J + 1
      NJ = INDX( J )
      IF( J.GT.N )
     $   GO TO 100
      IF( RHO*DABS( Z( NJ ) ).LE.TOL )THEN
*
*        Deflate due to small z component.
*

*	if(JOBZ.eq.'N')then
*		print *,'Deflation(small z):', NJ, K2
*	end if

         DZ = DZ + 1
         K2 = K2 - 1
         COLTYP( NJ ) = 4
         INDXP( K2 )  = NJ
        INDXQ(K2) = NJ
      ELSE
*
*        Check if eigenvalues are close enough to allow deflation.
*
         S = Z( PJ )
         C = Z( NJ )
*
*        Find sqrt(a**2+b**2) without overflow or
*        destructive underflow.
*
         TAU = DLAPY2( C,S )
         T   = D( NJ ) - D( PJ )
         C   = C / TAU
         S   = -S / TAU

         IF( DABS( T*C*S ).LE.TOL )THEN
*
*           Deflate due to close eigenvalues.
*

            DE      = DE + 1
            Z( NJ ) = TAU
            Z( PJ ) = ZERO
            IF( COLTYP( NJ ).NE.COLTYP( PJ ) )
     $         COLTYP( NJ ) = 2
*
*              if deflation happens across the two eigenvector blocks
*              (eigenvalues corresponding to different blocks), then
*              on column in the eigenvector matrix fills up completely
*              (changes from type 1 or 3 to type 2)
*
*           eigenpair PJ is deflated, therefore the column type changes
*           to 4
*


*	if(JOBZ.eq.'N')then
*	print *, 'Before Q:'
*		print *, Q(1:N,PJ)
*		print *, Q(1:N,NJ)
*	end if

            COLTYP( PJ ) = 4
        if(JOBZ.eq.'D') then
            CALL DROT( N, Q( 1, PJ ), 1, Q( 1, NJ ), 1, C, S )
        end if

*	if(JOBZ.eq.'N')then
*	print *, 'After Q:'
*		print *, Q(1:N,PJ)
*		print *, Q(1:N,NJ)
*	end if


***************************GIVENS*********************************** -S weil Transponiert!?!
*** IRGENDWAS STIMMT HIER NOCH NICHT, vergleiche Q mit veranderungen in MODWRK
*** SIND NICHT GLEICH, aber warum und was stimmt dann?
            if(JOBZ.eq.'N')then
*		print *, 'JOBZ ', JOBZ, LEN, PJ, NJ, C, S
*		print *, 'GIVENS', MODWRK(PJ), MODWRK(NJ)
*	DO 220 I = 1, LEN
*		print *, MODWRK((I-1)*LDQ+1:(I-1)*LDQ+N)
*  220       CONTINUE

      CALL DROT( LEN, MODWRK( PJ), LDM,MODWRK( NJ),LDM, C,S)

*	DO 221 I = 1, LEN
*		print *, MODWRK((I-1)*LDQ+1:(I-1)*LDQ+N)
*  221       CONTINUE
*		print *, 'GIVENS', MODWRK(PJ), MODWRK(NJ)


            end if
********************************************************************

            T       = D( PJ )*C**2 + D( NJ )*S**2
            D( NJ ) = D( PJ )*S**2 + D( NJ )*C**2
            D( PJ ) = T
            K2      = K2 - 1
            I       = 1
   90       CONTINUE
            IF( K2+I.LE.N )THEN
               IF( D( PJ ).LT.D( INDXP( K2+I ) ) ) THEN
                  INDXP( K2+I-1 ) = INDXP( K2+I )
            INDXQ(K2+I-1) = INDXQ( K2+I )
                  INDXP( K2+I )   = PJ
            INDXQ(K2+I-1) = PJ
                  I               = I + 1
                  GO TO 90
               ELSE
                  INDXP( K2+I-1 ) = PJ
            INDXQ(K2+I-1) = PJ
               END IF
            ELSE
               INDXP( K2+I-1 ) = PJ
            INDXQ(K2+I-1) = PJ
            END IF
            PJ = NJ
      FACTMP=-1
         ELSE
*
*           do not deflate
*
            K           = K + 1
            DLAMDA( K ) = D( PJ )
            W( K )      = Z( PJ )
            INDXP( K )  = PJ
        INDXQ(K) = PJ
        FACTMP=1
            PJ          = NJ
         END IF
      END IF
      GO TO 80
  100 CONTINUE
*
*     Record the last eigenvalue.
*
      K           = K + 1
      DLAMDA( K ) = D( PJ )
      W( K )      = Z( PJ )
      INDXP( K )  = PJ
        INDXQ(K) = PJ*FACTMP
*
*     Count up the total number of the various types of columns, then
*     form a permutation which positions the four column types into
*     four uniform groups (although one or more of these groups may be
*     empty).
*
      DO 110 J = 1, 4
         CTOT( J ) = 0
  110 CONTINUE
      DO 120 J = 1, N
         CT         = COLTYP( J )
         CTOT( CT ) = CTOT( CT ) + 1
  120 CONTINUE
*
*     PSM(*) = Position in SubMatrix (of types 1 through 4)
*
      PSM( 1 ) = 1
      PSM( 2 ) = 1 + CTOT( 1 )
      PSM( 3 ) = PSM( 2 ) + CTOT( 2 )
      PSM( 4 ) = PSM( 3 ) + CTOT( 3 )
      K        = N - CTOT( 4 )
*
*     Fill out the INDXC array so that the permutation which it induces
*     will place all type-1 columns first, all type-2 columns next,
*     then all type-3's, and finally all type-4's.
*
      DO 130 J = 1, N
         JS                 = INDXP( J )
         CT                 = COLTYP( JS )
         INDX( PSM( CT ) )  = JS
         INDXC( PSM( CT ) ) = J
         PSM( CT )          = PSM( CT ) + 1
  130 CONTINUE
*
*     Sort the eigenvalues and corresponding eigenvectors into DLAMDA
*     and Q2 respectively.  The eigenvalues/vectors which were not
*     deflated go into the first K slots of DLAMDA and Q2 respectively,
*     while those which were deflated go into the last N - K slots.
*
      I   = 1
      IQ1 = 1
      IQ2 = 1 + ( CTOT( 1 )+CTOT( 2 ) )*N1
      DO 140 J = 1, CTOT( 1 )
         JS = INDX( I )
         CALL DCOPY( N1, Q( 1, JS ), 1, Q2( IQ1 ), 1 )
         Z( I ) = D( JS )
         I      = I + 1
         IQ1    = IQ1 + N1
  140 CONTINUE
*
      DO 150 J = 1, CTOT( 2 )
         JS = INDX( I )
         CALL DCOPY( N1, Q( 1, JS ), 1, Q2( IQ1 ), 1 )
         CALL DCOPY( N2, Q( N1+1, JS ), 1, Q2( IQ2 ), 1 )
         Z( I ) = D( JS )
         I      = I + 1
         IQ1    = IQ1 + N1
         IQ2    = IQ2 + N2
  150 CONTINUE
*
      DO 160 J = 1, CTOT( 3 )
         JS = INDX( I )
         CALL DCOPY( N2, Q( N1+1, JS ), 1, Q2( IQ2 ), 1 )
         Z( I ) = D( JS )
         I      = I + 1
         IQ2    = IQ2 + N2
  160 CONTINUE
*
      IQ1 = IQ2
      DO 170 J = 1, CTOT( 4 )
         JS = INDX( I )

        if(JOBZ.eq.'D') then
         CALL DCOPY( N, Q( 1, JS ), 1, Q2( IQ2 ), 1 )
        end if
         IQ2    = IQ2 + N
         Z( I ) = D( JS )
         I      = I + 1
  170 CONTINUE
*
*     The deflated eigenvalues and their corresponding vectors go back
*     into the last N - K slots of D and Q respectively.
*

        if(JOBZ.eq.'D') then
      CALL DLACPY( 'A', N, CTOT( 4 ), Q2( IQ1 ), N, Q( 1, K+1 ), LDQ )
        end if
      CALL DCOPY( N-K, Z( K+1 ), 1, D( K+1 ), 1 )

*
****DEBUGGING BEGIN***********************************************
      IF( DEBUG.NE.0 ) THEN
         ERROR = 0
         DO 840 L = 1, N-1
            IF( DLAMDA( L ).GE.DLAMDA( L+1 ) )THEN
               ERROR = 1
               WRITE( *,* ) '   dsrtdf: after final sorting: DLAMDA(',
     $                      L, ')=', DLAMDA( L ),
     $                      ' >= ', 'DLAMDA(', L+1, ')=',
     $                      DLAMDA( L )
            END IF
  840    CONTINUE
*         IF( ERROR.EQ.1 )THEN
*            WRITE( *,* ) 'stopping before calling DLAED3M.'
*            STOP
*         END IF
      END IF
****DEBUGGING END*************************************************
*
*     Copy CTOT into COLTYP for referencing in DLAED3M.
*
      DO 180 J = 1, 4
         COLTYP( J ) = CTOT( J )
  180 CONTINUE
*
  190 CONTINUE
*
****DEBUGGING BEGIN***********************************************
      IF( DEBUG.NE.0 ) THEN
         WRITE( *,* ) 'dsrtdf: dimension of Q2: ',
     $        N1*(COLTYP(1)+COLTYP(2)) + (N-N1)*(COLTYP(2)+COLTYP(3))
      END IF
****DEBUGGING END*************************************************
*
      RETURN
*
*     End of DSRTDF
*
      END
