      SUBROUTINE FINALDIBTDC( JOBZ, N, NBLKS, KSIZES, D, L1D, L2D, E,
     $                   RANK, L1E, L2E, TOL, EV, Z, LDZ, WORK, LWORK,
     $                   IWORK, LIWORK, INFO, MODWRK, IMODWRK, CHANGE )
*
*  -- Routine written in LAPACK Version 3.0 style --
****************************************************
*     started:           February 20, 2000
*     April 20, 2003:    cleaned up research code and finalized version 1.0
*     last modification: July 19, 2008
****************************************************
*
*     .. Scalar Arguments ..
      CHARACTER          JOBZ
      INTEGER            N, NBLKS, L1D, L2D, L1E, L2E, INFO, LDZ,
     $                   LWORK, LIWORK, CHANGE
      DOUBLE PRECISION   TOL
*     ..
*     .. Array Arguments ..
      INTEGER            KSIZES(*), RANK(*), IWORK(*),IMODWRK(*)
      DOUBLE PRECISION   D( L1D, L2D, * ), E( L1E, L2E, * ),
     $                   Z( LDZ, * ), EV( * ), WORK( * ),
     $                   MODWRK( * )
*     ..
*  Purpose
*  =======
*
*  DIBTDC computes all eigenvalues and corresponding eigenvectors of a
*  symmetric irreducible block tridiagonal matrix with rank RANK matrices
*  as the subdiagonal blocks using a block divide and conquer method.
*
*  Arguments
*  =========
*
*  JOBZ    (input) CHARACTER*1
*          = 'N':  Compute eigenvalues only;
*          = 'D':  Compute eigenvalues and eigenvectors.
*                  Eigenvectors are accumulated in the
*                  divide-and-conquer process.
*
*  N      (input) INTEGER
*         The dimension of the symmetric irreducible block tridiagonal
*         matrix.  N >= 2.
*
*  NBLKS  (input) INTEGER, 2 <= NBLKS <= N
*         The number of diagonal blocks in the matrix.
*
*  KSIZES (input) INTEGER array, dimension (NBLKS)
*         The dimension of the square diagonal blocks from top left
*         to bottom right.  KSIZES(I) >= 1 for all I, and the sum of
*         KSIZES(I) for I = 1 to NBLKS has to be equal to N.
*
*  D      (input) DOUBLE PRECISION array, dimension (L1D,L2D,NBLKS)
*         The lower triangular elements of the symmetric diagonal
*         blocks of the block tridiagonal matrix.  Elements of the top
*         left diagonal block, which is of dimension KSIZES(1), are
*         contained in D(*,*,1); the elements of the next diagonal
*         block, which is of dimension KSIZES(2), are contained in
*         D(*,*,2); etc.
*
*  L1D    (input) INTEGER
*         The leading dimension of the array D.  L1D >= max(3,KMAX),
*         where KMAX is the dimension of the largest diagonal block.
*
*  L2D    (input) INTEGER
*         The second dimension of the array D.  L2D >= max(3,KMAX),
*         where KMAX is as stated in L1D above.
*
*  E      (input) DOUBLE PRECISION array, dimension (L1E,L2E,NBLKS-1)
*         Contains the elements of the scalars (singular values) and
*         vectors (singular vectors) defining the rank RANK subdiagonal
*         blocks of the matrix.
*         E(1:RANK(K),RANK(K)+1,K) holds the RANK(K) scalars,
*         E(:,1:RANK(K),K) holds the RANK(K) column vectors, and
*         E(:,RANK(K)+2:2*RANK(K)+1,K) holds the row vectors for the K-th
*         subdiagonal block.
*
*  RANK   (input) INTEGER array, dimension (NBLKS-1).
*         The ranks of all the subdiagonal blocks contained in the array E.
*         RANK( K ) <= MIN( KSIZES( K ), KSIZES( K+1 ) )
*
*  L1E    (input) INTEGER
*         The leading dimension of the array E.  L1E >= max(3,2*KMAX+1),
*         where KMAX is as stated in L1D above.
*
*  L2E    (input) INTEGER
*         The second dimension of the array E.  L2E >= max(3,2*KMAX+1),
*         where KMAX is as stated in L1D above.
*
*  TOL    (input) DOUBLE PRECISION, TOL <= 1.0D-1
*         User specified deflation tolerance for the routine DMERG2.
*         If ( 1.0D-1 >= TOL >= 20*EPS ) then TOL is used as
*         the deflation tolerance in DSRTDF.
*         If ( TOL < 20*EPS ) then the standard deflation tolerance from
*         LAPACK is used as the deflation tolerance in DSRTDF.
*
*  EV     (output) DOUBLE PRECISION array, dimension (N)
*         If INFO = 0, then EV contains the eigenvalues of the
*         symmetric block tridiagonal matrix in ascending order.
*
*  Z      (input/output) DOUBLE PRECISION array, dimension (LDZ, N)
*         On entry, Z will be the identity matrix.
*         On exit, Z contains the eigenvectors of the block tridiagonal
*         matrix.
*
*  LDZ    (input) INTEGER
*         The leading dimension of the array Z.  LDZ >= max(1,N).
*
*  WORK   (workspace) DOUBLE PRECISION array, dimension (LWORK)
*
*  LWORK   (input) INTEGER
*          The dimension of the array WORK.
*          In order to guarantee correct results in all cases,
*          LWORK must be at least ( 2*N**2 + 3*N ). In many cases,
*          less workspace is required. The absolute minimum required is
*          ( N**2 + 3*N ).
*          If the workspace provided is not sufficient, the routine will
*          return a corresponding error code and report how much workspace
*          was missing (see INFO).
*
*  IWORK  (workspace) INTEGER array, dimension (LIWORK)
*
*  LIWORK  (input) INTEGER
*          The dimension of the array IWORK.
*          LIWORK must be at least ( 5*N + 3 + 4*NBLKS - 4 ):
*                 5*KMAX+3 for DSYEVD, 5*N for ????,
*                 4*NBLKS-4 for the preprocessing (merging order)
*          Summarizing, the minimum integer workspace needed is
*          MAX( 5*N, 5*KMAX + 3 ) + 4*NBLKS - 4
*
*  INFO   (output) INTEGER
*          = 0:  successful exit.
*          < 0, > -99: illegal arguments.
*                if INFO = -i, the i-th argument had an illegal value.
*          = -99: error in the preprocessing (call of CUTLR).
*          < -200: not enough workspace. Space for ABS(INFO + 200)
*                numbers is required in addition to the workspace provided,
*                otherwise some eigenvectors will be incorrect.
*          > 0:  The algorithm failed to compute an eigenvalue while
*                working on the submatrix lying in rows and columns
*                INFO/(N+1) through mod(INFO,N+1).
*
*  MODWRK  (workspace/input) DOUBLE PRECISION array, dimension ( N*N )
*          If ( JOBZ.EQ.'N' ) workspace used for
*          storing and updating the modification vectors in case the
*          eigenvector matrices are NOT accumulated in the
*          divide-and-conquer process. Otherwise not referenced.
*FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
****NOTE: MODWRK IS A ONE-DIMENSIONAL DOUBLE PRECISION WORKSPACE ! IN A FUTURE
****      CODE VERSION IT WILL BE INTEGRATED INTO THE PARAMETER WORK !!!!
*FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
*
*
*  Further Details
*  ===============
*
*  Written by
*     Wilfried Gansterer and Bob Ward,
*     Department of Computer Science, University of Tennessee
*
*  This routine is comparable to Dlaed0.f from LAPACK.
*
*  =====================================================================
*
*     .. Parameters ..
      INTEGER            DEBUG
      PARAMETER          ( DEBUG = 0 )
      DOUBLE PRECISION   ZERO, ONE, MONE, TWO
      PARAMETER          ( ZERO = 0.0D0, ONE = 1.0D0, MONE = -1.0D0,
     $                     TWO = 2.0D0 )
*     ..
*     .. Local Scalars ..
      INTEGER   I, J, K, L NP, MATSIZ, K2,
     $          KSUM, KSK, KSKP1, KCHK, KMAX, LWMIN, LIWMIN,
     $          LIWORK2, NPP1, NBLKM1, KM1, RP1, RKK, VSTRT,
     $          ii,jj,kk, cutlen, cutstart, cutend, cutmod
      INTEGER   MAT1, KBRK, CUT, START, SIZE, BLKS, MERGED,
     $          LSUM, LBLKS, RBLKS, RKTOT, CURMOD,
     $          STARTP, ISTRTP, ICUT, ISIZE, ILSUM,
     $          ISTCK1, ISTCK2, ISTCK3
      DOUBLE PRECISION RHO
      CHARACTER          ORG_JOBZ, JOBZ2
      DOUBLE PRECISION time
      INTEGER t_count, t_count_rate, t_count_max, t_tmp
*     ..
*     .. External Subroutines ..
      EXTERNAL           DCOPY, DGEMM, DLACPY, DMERG2, DSTEQR,
     $                   XERBLA, DSYEV, CUTLR
*     ..
*     .. External Functions ..
      INTEGER            ILAENV
      EXTERNAL           ILAENV
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          ABS, MAX
*     ..
*     .. Executable Statements ..
*
*     ...Check the input parameters...........................
*
      INFO = 0
*
      IF( JOBZ.NE.'N' .AND. JOBZ.NE.'D' )THEN
         INFO = -1
      ELSE IF( N.LT.2 )THEN
         INFO = -2
      ELSE IF( NBLKS.LT.2 .OR. NBLKS.GT.N )THEN
         INFO = -3
      END IF
      IF( INFO.EQ.0 )THEN
         KSUM   = 0
         KMAX   = 0
         KCHK   = 0
         DO 1 K = 1, NBLKS
            KSK = KSIZES(K)
            KSUM = KSUM + KSK
            IF( KSK.GT.KMAX ) KMAX = KSK
            IF( KSK.LT.1 )    KCHK = 1
    1    CONTINUE
         LWMIN  = N**2 + 3*N
         LIWMIN = MAX( 5*N, 5*KMAX + 3 ) + 4*NBLKS - 4
         IF ( KSUM.NE.N .OR. KCHK.EQ.1 ) THEN
              INFO = -4
         ELSE IF( L1D.LT.MAX( 3,KMAX ) ) THEN
              INFO = -6
         ELSE IF( L2D.LT.MAX( 3,KMAX ) ) THEN
              INFO = -7
         ELSE IF( L1E.LT.MAX( 3,2*KMAX+1 ) ) THEN
              INFO = -10
         ELSE IF( L2E.LT.MAX( 3,2*KMAX+1 ) ) THEN
              INFO = -11
         ELSE IF( TOL.GT.1.0D-1 ) THEN
              INFO = -12
         ELSE IF( LDZ.LT.MAX( 1,N ) ) THEN
              INFO = -15
         ELSE IF( LWORK.LT.LWMIN ) THEN
              INFO = -17
         ELSE IF( LIWORK.LT.LIWMIN ) THEN
              INFO = -19
         END IF
      END IF
      IF( INFO.EQ.0 )THEN
         DO 904 K = 1, NBLKS-1
            IF( RANK( K ).GT.MIN( KSIZES( K ), KSIZES( K+1 ) )
     $          .OR. RANK( K ).LT.1 )THEN
               INFO = -9
            END IF
  904    CONTINUE
      END IF
*
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'DIBTDC', -INFO )
         RETURN
      END IF

*
*****************************************************************************
*
*     size of integer workspace needed for DSYEVD
*
      LIWORK2 = 5*N+3
*
* ...Preprocessing.....................................................
*    Determine the optimal order for merging the subblocks and how much
*    workspace will be needed for the merging (determined by the last
*    merge). Cutpoints for the merging operations are determined and stored
*    in reverse chronological order (starting with the final merging
*    operation).
*
*    integer workspace requirements for the preprocessing:
*         4*(NBLKS-1) for merging history
*         at most 3*(NBLKS-1) for stack
*
      START  = 1
      SIZE   = N
      BLKS   = NBLKS
      MERGED = 0
      K      = 0
*
*     integer workspace used for the stack is not needed any more after the
*     preprocessing and therefore can use part of the 5*N
*     integer workspace needed later on in the code
*
      ISTCK1 = 0
      ISTCK2 = ISTCK1 + NBLKS
      ISTCK3 = ISTCK2 + NBLKS
*
*     integer workspace used for storing the order of merges starts AFTER
*     the integer workspace 5*N+3 which is needed later on in the code
*     (5*KMAX+3 for DSYEVD, 4*N in FINALDMERG2)
*
      ISTRTP = 5*N + 4
      ICUT   = ISTRTP + NBLKS - 1
      ISIZE  = ICUT + NBLKS - 1
      ILSUM  = ISIZE + NBLKS - 1

**** new method
      if(JOBZ.eq.'N')then
  
      J=2
      CUT = 1
      K = NBLKS
      L = NBLKS/2

      DO 300 MERGED = 1, NBLKS-1

      STARTP = 0
      DO 310 I = 1, START-1
         STARTP = STARTP + KSIZES( I )
  310 CONTINUE

      SIZE = 0
      DO 311 I = START, START+J-1
        if(I.le.NBLKS)then
         SIZE = SIZE + KSIZES( I )
        end if
  311 CONTINUE

      LSUM = 0
      DO 312 I = START, CUT
         LSUM = LSUM + KSIZES( I )
  312 CONTINUE

***      IWORK( ISTRTP + MERGED-1 ) = STARTP + 1
***      IWORK( ICUT   + MERGED-1 ) = CUT
***      IWORK( ISIZE  + MERGED-1 ) = SIZE
***      IWORK( ILSUM  + MERGED-1 ) = LSUM

      IWORK( ISTRTP + L-1 ) = STARTP + 1
      IWORK( ICUT   + L-1 ) = CUT
      IWORK( ISIZE  + L-1 ) = SIZE
      IWORK( ILSUM  + L-1 ) = LSUM
      START = START + J
      CUT = CUT + J
      L = L - 1
  
      if(CUT.ge.NBLKS)then
          CUT = J
          J=J*2
          START = 1
          K = K/2 + MOD(K,2)
          L = K/2 + MERGED
      end if

  300 CONTINUE




      else
***    print *, "TEST"

*
  200 CONTINUE  
*
      IF( NBLKS.GE.3 ) THEN
*
*        Determine the cut point. Note that in the routine FINALCUTLR it is
*        chosen such that it yields the best balanced merging operation
*        among all the rank modifications with minimum rank.

*
         CALL FINALCUTLR( START, SIZE, BLKS, KSIZES( START ),
     $               RANK( START ), CUT, LSUM, LBLKS, INFO )

***    print *, CUT, LSUM, LBLKS
***    print *, ""

         IF( INFO.NE.0 ) THEN
            WRITE( *,* ) '  Dibtdc: Error in CUTLR, INFO =',
     $                   INFO
            GO TO 820
         END IF 
*
****DEBUGGING BEGIN***********************************************
         IF( DEBUG.NE.0 ) THEN
            WRITE( *,* ) '  Dibtdc: preprocessing (', MERGED,
     $                   ' merged ): CUT = ', CUT
            WRITE( *,* ) '  Dibtdc: preprocessing (', MERGED,
     $                   ' merged ): LSUM = ', LSUM
            WRITE( *,* ) '  Dibtdc: preprocessing (', MERGED,
     $                   ' merged ): LBLKS = ', LBLKS
         END IF
****DEBUGGING END*************************************************
*
      ELSE
         CUT   = 1
         LSUM  = KSIZES( 1 )
         LBLKS = 1
      END IF
*
      MERGED = MERGED + 1
      STARTP = 0
      DO 210 I = 1, START-1
         STARTP = STARTP + KSIZES( I )
  210 CONTINUE
      IWORK( ISTRTP + (NBLKS-1) - MERGED ) = STARTP + 1
      IWORK( ICUT   + (NBLKS-1) - MERGED ) = CUT
      IWORK( ISIZE  + (NBLKS-1) - MERGED ) = SIZE
      IWORK( ILSUM  + (NBLKS-1) - MERGED ) = LSUM

***    print *, STARTP + 1, CUT, SIZE, LSUM
*
      IF( LBLKS.EQ.2 ) THEN
*
*        one merge in left branch, left branch done
*
        MERGED = MERGED + 1
        IWORK( ISTRTP + (NBLKS-1) - MERGED ) = STARTP + 1
        IWORK( ICUT   + (NBLKS-1) - MERGED ) = START
        IWORK( ISIZE  + (NBLKS-1) - MERGED ) = LSUM
        IWORK( ILSUM  + (NBLKS-1) - MERGED ) = KSIZES( START )
      END IF
*
      IF( (LBLKS.EQ.1).OR.(LBLKS.EQ.2) ) THEN
*
*        left branch done, continue on the right side
*
         START = START + LBLKS
         SIZE  = SIZE - LSUM
         BLKS  = BLKS - LBLKS
*
         IF( BLKS.LE.0 ) THEN
            WRITE( *,* ) '   Dibtdc: ERROR in preprocessing - BLKS =',
     $                   BLKS
            STOP
         END IF
*
         IF( BLKS.EQ.2 ) THEN
*
*           one merge in right branch, right branch done
*
            MERGED = MERGED + 1
            STARTP = STARTP + LSUM
            IWORK( ISTRTP + (NBLKS-1) - MERGED ) = STARTP + 1
            IWORK( ICUT   + (NBLKS-1) - MERGED ) = START
            IWORK( ISIZE  + (NBLKS-1) - MERGED ) = SIZE
            IWORK( ILSUM  + (NBLKS-1) - MERGED ) = KSIZES( START )
         END IF
*
         IF( (BLKS.EQ.1).OR.(BLKS.EQ.2) ) THEN
*
*           get the next subproblem from the stack or finished
*
            IF( K.GE.1 ) THEN
*
*              something left on the stack
*
               START = IWORK( ISTCK1 + K )
               SIZE  = IWORK( ISTCK2 + K )
               BLKS  = IWORK( ISTCK3 + K )
               K = K - 1
               GO TO 200
            ELSE
                
*
*              nothing left on the stack
*
               IF( MERGED.NE.NBLKS-1 ) THEN
                  WRITE( *,* ) '   Dibtdc: ERROR in preprocessing -',
     $                         ' not enough merges performed.'
                  GO TO 820
               END IF
*
*              exit preprocessing
*
            END IF
         ELSE
*
*           BLKS.GE.3, and therefore analyze the right side
*
            GO TO 200
         END IF
       ELSE
*
*        LBLKS.GE.3, and therefore check the right side and
*        put it on the stack if required
*
         RBLKS = BLKS - LBLKS
         IF( RBLKS.GE.3 ) THEN
            K = K + 1
            IWORK( ISTCK1 + K ) = CUT + 1
            IWORK( ISTCK2 + K ) = SIZE - LSUM
            IWORK( ISTCK3 + K ) = RBLKS
         ELSE IF( RBLKS.EQ.2 ) THEN
*
*           one merge in right branch, right branch done
*           (note that nothing needs to be done if RBLKS.EQ.1 !)
*
            MERGED = MERGED + 1
            STARTP = STARTP + LSUM
            IWORK( ISTRTP + (NBLKS-1) - MERGED ) = STARTP + 1
            IWORK( ICUT   + (NBLKS-1) - MERGED ) = START + LBLKS
            IWORK( ISIZE  + (NBLKS-1) - MERGED ) = SIZE - LSUM
            IWORK( ILSUM  + (NBLKS-1) - MERGED ) =
     $                                     KSIZES( START+LBLKS )
         END IF
         IF( RBLKS.LE.0 ) THEN
            WRITE( *,* ) '   Dibtdc: ERROR in preprocessing -',
     $                   ' RBLKS=', RBLKS
         END IF
*
*        continue on the left side
*
         SIZE = LSUM
         BLKS = LBLKS
         GO TO 200
       END IF


**** new method
      end if


*
****DEBUGGING BEGIN***********************************************
       IF( DEBUG.NE.0 )THEN
          WRITE( *,* ) '  Dibtdc: given block structure:'
          DO 970 I = 1, NBLKS
             WRITE( *,* ) '          KSIZES(',I,') =', KSIZES( I )
  970     CONTINUE
          WRITE( *,* ) '  Dibtdc: after preprocessing:'
          WRITE( *,* ) '          K=', K, ', MERGED=', MERGED
          WRITE( *,* ) '          Merging history:'
          DO 971 I = 1, NBLKS - 1
             WRITE( *,* ) '           total size of ',
     $                    'modification problem (MATSIZ)=',
     $                    IWORK( ISIZE+I-1 )
             WRITE( *,* ) '              NP=', IWORK( ISTRTP+I-1 ),
     $                    ', KBRK=', IWORK( ICUT+I-1 ),
     $                    ', MAT1=', IWORK( ILSUM+I-1 )
  971     CONTINUE
       END IF
****DEBUGGING END*************************************************
*
*       SIZE = IWORK( ISIZE+NBLKS-2 )
*       MAT1 = IWORK( ILSUM+NBLKS-2 )
*
*      Note: after the dimensions SIZE and MAT1 of the last merging
*      operation have been determined, an upper bound for the workspace
*      requirements which is independent of how much deflation occurs in
*      the last merging operation could be determined as follows
*      (based on (3.15) and (3.19) from UT-CS-00-447):
*
*       IF( MAT1.LE.N/2 ) THEN
*          WSPREQ = 3*N + 3/2*( SIZE-MAT1 )**2 + N*N/2 + MAT1*MAT1
*       ELSE
*          WSPREQ = 3*N + 3/2*MAT1*MAT1 + N*N/2 + ( SIZE-MAT1 )**2
*       END IF
*
*       IF( LWORK-WSPREQ.LT.0 )THEN
**
**         not enough work space provided
**
*          INFO = -200 - ( WSPREQ-LWORK )
*          RETURN
*       END IF
*       However, this is not really useful, since the actual check whether
*       enough workspace is provided happens in DMERG2.f !
*
**************************************************************************
*
* ...Solve subproblems...................................
*
*     Divide the matrix into NBLKS submatrices using rank-r
*     modifications (cuts) and solve for their eigenvalues and
*     eigenvectors. Initialize index array to sort eigenvalues.
*
*     first block: ......................................
*
*        correction for block 1: D1 - V1 \Sigma1 V1^T
*
      KSK = KSIZES( 1 )
      RP1 = RANK( 1 ) + 1
*
*     initialize the proper part of Z with the diagonal block D1
*     (the correction will be made in Z and then the call of DSYEVD will
*      overwrite it with the eigenvectors)
*
      CALL DLACPY( 'L', KSK, KSK, D( 1,1,1 ), L1D, Z( 1,1 ), LDZ )

*
****DEBUGGING BEGIN***********************************************
      IF( DEBUG.EQ. 2 )THEN
      write(*,*) 'test'
           DO 20 I = 1, KSK
      write(*,*) 'test', I
              DO 21 J = 1, I
      write(*,*) 'test', J
               WRITE( *,* ) '  DLACPY: Z(', I, ',', J, ') =', Z( I, J )
   21       CONTINUE
   20    CONTINUE
      END IF
****DEBUGGING END*************************************************
*
*     copy D1 into WORK (in order to be able to restore it afterwards)
*
      CALL DLACPY( 'L', KSK, KSK, D( 1, 1, 1 ), L1D,
     $             WORK( 1 ), KSK )
*
*     copy V1 into the first RANK(1) columns of D1 and then
*     multiply with \Sigma1
*
      DO 22 I = 1, RANK( 1 )
         CALL DCOPY( KSK, E( 1, RP1+I, 1), 1, D( 1, I, 1), 1)
         CALL DSCAL( KSK, E( I, RP1, 1), D( 1, I, 1), 1)
   22 CONTINUE
*
*     multiply the first RANK( 1 ) columns of D1 with V1^T and
*     subtract the result from the proper part of Z (previously
*     initialized with D1)
*
      CALL DGEMM( 'N', 'T', KSK, KSK, RANK( 1 ), MONE, D( 1, 1, 1 ),
     $            L1D, E( 1, RANK( 1 )+2, 1 ), L1E, ONE, Z( 1, 1 ),
     $            LDZ )
*
*     restore the original D1 from WORK
*
      CALL DLACPY( 'L', KSK, KSK, WORK( 1 ), KSK, 
     $             D( 1, 1, 1 ), L1D )
*
*     eigenanalysis of block 1 (using DSYEVD)
*
*      WRITE( *,* ) '  dibtdc: calling DSYEV for block 1.'
      CALL DSYEV( 'V', 'L', KSK, Z, LDZ, EV, WORK, LWORK, INFO )
*      WRITE( *,* ) '  dibtdc: calling DSYEVD for block 1.'
*      CALL DSYEVD( 'V', 'L', KSK, Z, LDZ, EV, WORK, LWORK,
*     $             IWORK( 1 ), LIWORK2, INFO )
      IF( INFO.NE.0 ) THEN
         WRITE( *,* ) '  Dibtdc: Error in DSYEVD for block 1, ',
     $                'INFO =', INFO
         K = 1
         GO TO 800
      END IF
*
****DEBUGGING BEGIN***********************************************
      IF( DEBUG.NE.0 ) THEN
         DO 901 I = 1, KSK
           WRITE( *,* ) '  EV(', I, ') =', EV(I)
  901    CONTINUE
         DO 902 J = 1, KSK
            DO 903 I = 1, KSK
               WRITE( *,* ) '  Z(', I, ',', J, ') =', Z( I, J )
  903       CONTINUE
  902    CONTINUE
      END IF
****DEBUGGING END*************************************************
*
*     EV( 1: ) contains the eigenvalues in ascending order
*     (they are returned this way by DSYEVD)
*
      DO 25 I = 1, KSK
         IWORK( I ) = I
   25 CONTINUE
*
*     intermediate blocks: ..............................
*
      NP = KSK
*
*     starting index of the second block
*
      NPP1 = NP + 1
*
*     remaining number of blocks
*
      NBLKM1 = NBLKS - 1
*
      IF( NBLKM1.GT.1 )THEN
         DO 35 K = 2, NBLKM1
*
*           correction for block K:
*           Dk - U(k-1) \Sigma(k-1) U(k-1)^T - Vk \Sigmak Vk^T
*
            KM1 = K - 1
            KSK = KSIZES( K )
            RP1 = RANK( K ) + 1
*
*           initialize the proper part of Z with the diagonal block Dk
*           (the correction will be made in Z and then the call of DSYEVD will
*            overwrite it with the eigenvectors)
*
            CALL DLACPY( 'L', KSK, KSK, D( 1,1,K ), L1D,
     $                   Z( NP+1,NP+1 ), LDZ )
*
*           copy Dk into WORK (in order to be able to restore it afterwards)
*
            CALL DLACPY( 'L', KSK, KSK, D( 1, 1, K ), L1D,
     $                   WORK( 1 ), KSK )
*
*           copy U(K-1) into the first RANK(K-1) columns of Dk and then
*           multiply with \Sigma(K-1)
*
            DO 122 I = 1, RANK( K-1 )
               CALL DCOPY( KSK, E( 1, I, K-1 ), 1, D( 1, I, K ), 1)
               CALL DSCAL( KSK, E( I, RANK( K-1 )+1, K-1 ),
     $                     D( 1, I, K ), 1 )
  122       CONTINUE
*
*           multiply the first RANK(K-1) columns of Dk with U(k-1)^T and
*           subtract the result from the proper part of Z (previously
*           initialized with Dk)
*
            CALL DGEMM( 'N', 'T', KSK, KSK, RANK( K-1 ), MONE,
     $                  D( 1, 1, K ), L1D, E( 1, 1, K-1 ), L1E, ONE,
     $                  Z( NPP1, NPP1 ), LDZ )
*
*           copy Vk into the first RANK(K) columns of Dk and then
*           multiply with \Sigmak
*
            DO 123 I = 1, RANK( K )
               CALL DCOPY( KSK, E( 1, RP1+I, K ), 1,
     $                     D( 1, I, K ), 1 )
               CALL DSCAL( KSK, E( I, RP1, K ),
     $                     D( 1, I, K ), 1 )
  123       CONTINUE
*
*           multiply the first RANK(K) columns of Dk with Vk^T and
*           subtract the result from the proper part of Z (previously
*           updated with [- U(k-1) \Sigma(k-1) U(k-1)^T] )
*
            CALL DGEMM( 'N', 'T', KSK, KSK, RANK( K ), MONE,
     $                  D( 1, 1, K ), L1D, E( 1, RANK( K )+2, K ),
     $                  L1E, ONE, Z( NPP1, NPP1 ), LDZ )
*
*           restore the original Dk from WORK
*
            CALL DLACPY( 'L', KSK, KSK, WORK( 1 ), KSK, 
     $                   D( 1, 1, K ), L1D )
*
*           eigenanalysis of block K (using dsyevd)
*
*            WRITE( *,* ) '  dibtdc: calling DSYEV for block ', K 
            CALL DSYEV( 'V', 'L', KSK, Z( NPP1, NPP1 ), LDZ,
     $                   EV( NPP1 ), WORK, LWORK, INFO )
*            WRITE( *,* ) '  dibtdc: calling DSYEVD for block ', K 
*            CALL DSYEVD( 'V', 'L', KSK, Z( NPP1, NPP1 ), LDZ,
*     $                   EV( NPP1 ), WORK, LWORK,
*     $                   IWORK( 1 ), LIWORK2, INFO )
            IF( INFO.NE.0 )THEN
               WRITE( *,* ) '  Dibtdc: Error in DSYEVD for block ',
     $                      K, ' INFO =', INFO
               GO TO 800
            END IF
*
****DEBUGGING BEGIN***********************************************
            IF( DEBUG.NE.0 )THEN
               DO 910 I = NPP1, NP + KSK
                  WRITE( *,* ) '  EV(', I, ') =', EV(I)
  910          CONTINUE
               DO 912 J = NPP1, NP + KSK
                  DO 913 I = NPP1, NP + KSK
                     WRITE( *,* ) '  Z(', I, ',', J, ') =', Z( I, J )
  913             CONTINUE
  912          CONTINUE
            END IF
****DEBUGGING END*************************************************
*
*     EV( NPP1: ) contains the eigenvalues in ascending order
*     (they are returned this way by DSYEVD)
*
            DO 32 I = 1, KSK
               IWORK( NP+I ) = I
   32       CONTINUE
*
*           update NP
*
            NP   = NP + KSK
            NPP1 = NP + 1
   35    CONTINUE
      END IF
*
*     last block: .......................................
*
*        correction for block NBLKS:
*        D(nblks) - U(nblks-1) \Sigma(nblks-1) U(nblks-1)^T
*
      KSK = KSIZES( NBLKS )
*
*     initialize the proper part of Z with the diagonal block D(nblks)
*     (the correction will be made in Z and then the call of DSYEVD will
*      overwrite it with the eigenvectors)
*
      CALL DLACPY( 'L', KSK, KSK, D( 1,1,NBLKS ), L1D,
     $                   Z( NP+1,NP+1 ), LDZ )
*
*     copy D(nblks) into WORK (in order to be able to restore it afterwards)
*
      CALL DLACPY( 'L', KSK, KSK, D( 1, 1, NBLKS ), L1D,
     $             WORK( 1 ), KSK )
*
*     copy U(nblks-1) into the first RANK(nblks-1) columns of D(nblks) and then
*     multiply with \Sigma(nblks-1)
*
      DO 128 I = 1, RANK( NBLKS-1 )
         CALL DCOPY( KSK, E( 1, I, NBLKS-1 ), 1, D( 1, I, NBLKS ), 1 )
         CALL DSCAL( KSK, E( I, RANK( NBLKS-1 )+1, NBLKS-1 ),
     $               D( 1, I, NBLKS ), 1)
  128 CONTINUE
*
*     multiply the first RANK(nblks-1) columns of D(nblks) with U(nblks-1)^T
*     and subtract the result from the proper part of Z (previously
*     initialized with D(nblks) )
*
      CALL DGEMM( 'N', 'T', KSK, KSK, RANK( NBLKS-1 ), MONE,
     $            D( 1, 1, NBLKS ), L1D, E( 1, 1, NBLKS-1 ), L1E, ONE,
     $            Z( NPP1, NPP1 ), LDZ )
*
*     restore the original D(nblks) from WORK
*
      CALL DLACPY( 'L', KSK, KSK, WORK( 1 ), KSK, 
     $             D( 1, 1, NBLKS ), L1D )
*
*     eigenanalysis of block NBLKS (using dsyevd)
*
*      WRITE( *,* ) '  dibtdc: calling DSYEV for block NBLKS.'
      CALL DSYEV( 'V', 'L', KSK, Z( NPP1, NPP1 ), LDZ, EV( NPP1 ),
     $             WORK, LWORK, INFO )
*      WRITE( *,* ) '  dibtdc: calling DSYEVD for block NBLKS.'
*      CALL DSYEVD( 'V', 'L', KSK, Z( NPP1, NPP1 ), LDZ, EV( NPP1 ),
*     $             WORK, LWORK, IWORK( 1 ), LIWORK2, INFO )
      IF( INFO.NE.0 ) THEN
         WRITE( *,* ) '  Dibtdc: Error in DSYEVD for block ',
     $                NBLKS, ' INFO =', INFO
         K = NBLKS
         GO TO 800
      END IF
*
****DEBUGGING BEGIN***********************************************
      IF( DEBUG.NE.0 )THEN
         DO 920 I = NPP1, NPP1+KSK-1
            WRITE( *,* ) '  EV(', I, ') =', EV(I)
  920    CONTINUE
         DO 922 J = NPP1, NPP1+KSK-1
            DO 923 I = NPP1, NPP1+KSK-1
               WRITE( *,* ) '  Z(', I, ',', J, ') =', Z( I, J )
  923        CONTINUE
  922    CONTINUE
      END IF
****DEBUGGING END*************************************************
*
*     EV( NPP1: ) contains the eigenvalues in ascending order
*     (they are returned this way by DSYEVD)
*
      DO 45 I = 1, KSK
         IWORK( NP+I ) = I
   45 CONTINUE
*
*     note that from here on the entire workspace is available again
*
*************************************************************************
*
* ...Synthesis step (merge)..............................................
*
*     Successively merge eigensystems of adjacent submatrices
*     into the eigensystem for the corresponding larger matrix.
*
*     If the eigenvectors are not accumulated then - at the moment -
*     assign and initialize extra space MODWRK for the modification
*     vectors (which have to be updated after every merging operation)
*
      IF( JOBZ.EQ.'N' ) THEN
*
*        determine the total number of modification vectors RKTOT
*        note that as a crude upper bound we have:
*         RKTOT.LEQ.( NBLKS-1 )*( KMAX-1 )/2 
*
         RKTOT = 0
         DO 230 K = 1, NBLKS-1
            RKTOT = RKTOT+RANK( K )
  230    CONTINUE

        print *, 'SWITCH:', CHANGE, RKTOT, N
        CHANGE = max(0,RKTOT - (N-CHANGE))
        print *, 'SWITCH:', CHANGE, RKTOT, N

*
*        initialize the workspace MODWRK with zero
*******NOTE THAT FOR NOW we are allocating a SEPARATE workspace MODWRK
*******In a later version this workspace will be integrated into the
******* variable WORK !
*
         CALL DLASET( 'A', N, RKTOT, ZERO, ZERO, MODWRK( 1 ), N )
*
*        copy the singular vectors from the E-matrices into the workspace
*        (in the order in which they will be needed for the merging
*        operations !)
*
         VSTRT = 0
         DO 240 K = 1, NBLKS-1
            KBRK  = IWORK( ICUT+K-1 )
            KSK   = KSIZES( KBRK )
            KSKP1 = KSIZES( KBRK+1 )
            RKK   = RANK( KBRK )

*
*           find the starting point for the nonzero portion VSTRT
*
            NP = 1
            DO 260 I = 1, KBRK-1
               NP = NP+KSIZES( I )
  260       CONTINUE
            VSTRT = VSTRT+NP
*
*           multiply the v-vectors by the transpose of the corresponding
*           eigenvector matrix from the initial solution step and copy them
*           into MODWRK
*
            CALL DGEMM( 'T', 'N', KSK, RKK, KSK, ONE, Z( NP,NP ),
     $                  LDZ, E( 1, RKK+2, KBRK ), L1E, ZERO,
     $                  MODWRK( VSTRT ), N )
*
*           multiply the u-vectors by the transpose of the corresponding
*           eigenvector matrix from the initial solution step and copy them
*           under the v-vectors into MODWRK
*
            CALL DGEMM( 'T', 'N', KSKP1, RKK, KSKP1, ONE,
     $                  Z( NP+KSK,NP+KSK ), LDZ,
     $                  E( 1, 1, KBRK ), L1E, ZERO,
     $                  MODWRK( VSTRT+KSK ), N )
            IMODWRK(2*K) = NP
            IMODWRK(2*K+1) = NP+KSKP1+KSK-1
*
*           update VSTRT
*
            VSTRT = VSTRT-NP+RKK*N
  240    CONTINUE
*
****DEBUGGING BEGIN***********************************************
         IF( DEBUG.NE.0 ) THEN
            DO 960 K = 1, NBLKS-1
               KBRK = IWORK( ICUT+K-1 )
               WRITE( *,* ) '  dibtdc: KBRK=', KBRK
               KSK   = KSIZES( KBRK )
               KSKP1 = KSIZES( KBRK+1 )
               RKK   = RANK( KBRK )
  960       CONTINUE
         END IF
****DEBUGGING END**************************************************
*
                  CALL DLASET( 'A', N, N, ZERO, ONE,
     $                         Z( 1,1 ), LDZ )
      END IF
*
*     Perform all the merging operations.
*
      VSTRT  = 0
      CURMOD = 0

** **    print *, 'DIBTDC', RKTOT, CHANGE, N

      JOBZ2 = 'D'
      K = IWORK( ISIZE )

      DO 220 I = 1, NBLKS-1

*
*        MATSIZ = total size of the current rank RANK modification problem
*
         MATSIZ = IWORK( ISIZE+I-1 )
         NP     = IWORK( ISTRTP+I-1 )
         KBRK   = IWORK( ICUT+I-1 )
         MAT1   = IWORK( ILSUM+I-1 )
         VSTRT  = VSTRT+NP

*
         DO 221 J = 1, RANK( KBRK )
*
*           NOTE: The parameter RHO in DMERG2 is modified in DSRTDF
*                 (multiplied by 2) ! In order not to change the
*                 singular value stored in E( :, RANK( KBRK )+1, KBRK ),
*                 we do not pass on this variable as an argument to DMERG2,
*                 but we assign a separate variable RHO here which is passed
*                 on to DMERG2.
*                 Alternative solution in F90:
*                 pass E( :,RANK( KBRK )+1,KBRK ) to an INTENT( IN ) parameter
*                 in DMERG2.
*
            RHO = E( J,RANK( KBRK )+1,KBRK )
*
            IF( JOBZ.EQ.'N' ) THEN

               CURMOD = CURMOD+1

*        print *,'SSP', CURMOD, MATSIZ, RKTOT-CURMOD,n,
*     $        2*DBLE(MATSIZ)**3-DBLE(MATSIZ)**2 + 
*     $        2*DBLE(MATSIZ)**2 - DBLE(MATSIZ)+ 
*     $        2*DBLE(n)**2*((RKTOT-CURMOD)-1.0) - 
*     $        DBLE(n)*((RKTOT-CURMOD)-1.0) - 
*     $        (2*DBLE(n)**2*(RKTOT-CURMOD) - 
*     $        DBLE(n)*(RKTOT-CURMOD) + 
*     $        2*DBLE(MATSIZ)**2*(RKTOT-CURMOD) - 
*     $        DBLE(MATSIZ)*(RKTOT-CURMOD))

        if(CURMOD<CHANGE .and. 
     $      2*DBLE(MATSIZ)**3-DBLE(MATSIZ)**2 + 
     $      2*DBLE(MATSIZ)**2 - DBLE(MATSIZ)+ 
     $      2*DBLE(n)**2*((RKTOT-CURMOD)-1.0) - 
     $      DBLE(n)*((RKTOT-CURMOD)-1.0) - 
     $      (2*DBLE(n)**2*(RKTOT-CURMOD) - 
     $      DBLE(n)*(RKTOT-CURMOD) + 
     $      2*DBLE(MATSIZ)**2*(RKTOT-CURMOD) - 
     $      DBLE(MATSIZ)*(RKTOT-CURMOD))
     $      .ge.ZERO) then
*                print *, 'SSP', CURMOD
*            print *,'DSP', CURMOD, MATSIZ,K,RKTOT-CURMOD,n, 
*     $               2*DBLE(K)**3-DBLE(K)**2 + 
*     $               2*DBLE(MATSIZ)**2 - DBLE(MATSIZ)+ 
*     $               2*DBLE(n)**2*((RKTOT-CURMOD)-1) - 
*     $               DBLE(n)*((RKTOT-CURMOD)-1) - 
*     $               (2*DBLE(n)**2*(RKTOT-CURMOD) - 
*     $               DBLE(n)*(RKTOT-CURMOD) + 
*     $               2*DBLE(K)**2*(RKTOT-CURMOD) -
*     $                DBLE(K)*(RKTOT-CURMOD))
            K2=K
*            (MATSIZ+K)/2
            if(2*DBLE(K2)**3-DBLE(K2)**2 + 
     $         2*DBLE(MATSIZ)**2 - DBLE(MATSIZ)+ 
     $         2*DBLE(n)**2*((RKTOT-CURMOD)-1) - 
     $         DBLE(n)*((RKTOT-CURMOD)-1) - 
     $         (2*DBLE(n)**2*(RKTOT-CURMOD) - 
     $         DBLE(n)*(RKTOT-CURMOD) + 
     $         2*DBLE(K2)**2*(RKTOT-CURMOD) -
     $          DBLE(K2)*(RKTOT-CURMOD))
     $         .ge.ZERO)then
*                print *, 'DSP', CURMOD
                CHANGE = CURMOD
            end if
        end if

*        print *, N, MATSIZ, (RKTOT-CURMOD)

               if( CURMOD<CHANGE ) then
            K = MATSIZ
                     CALL FINALDMERG2( 'N', J, MATSIZ, EV( NP ),
     $                         Z( NP,NP ),LDZ,IWORK( NP ),
     $                         RHO, MODWRK( VSTRT+MAT1 ),
     $                         MATSIZ-MAT1, MODWRK( VSTRT ),
     $                         MAT1, MAT1, WORK, LWORK,
     $                         IWORK( N+1 ), TOL, INFO,
     $                         MODWRK(VSTRT+N),N,RKTOT-CURMOD, K )
                     VSTRT = VSTRT+N

                  IF( INFO.NE.0 ) THEN
                     WRITE( *,* ) 'Dibtdc: ERROR in DMERG2, INFO  =',
     $                            INFO
                     GO TO 810
                  END IF
               else
            if( CURMOD-1<CHANGE) then

        print *, 'SWITCH:', CURMOD, RKTOT, N

                      CALL DLACPY( 'A',N,RKTOT-CURMOD+1,
     $                            MODWRK(1+(CURMOD-1)*N),
     $                            N,WORK(1),N)
                      CALL DGEMM('T','N',N,RKTOT-CURMOD+1,
     $                            N,ONE, Z( 1,1 ), LDZ,
     $                            WORK(1), N,
     $                            ZERO, MODWRK(1+(CURMOD-1)*N), N )

*            print *, CURMOD, LDZ, N

                      VSTRT = NP + (CURMOD-1)*N
                  end if
*
*                 eigenvectors are not accumulated:
*                 set Z to the identity
*
                  CALL DLASET( 'A', MATSIZ, MATSIZ, ZERO, ONE,
     $                         Z( NP,NP ), LDZ )
*

                  CALL SYSTEM_CLOCK(t_count, t_count_rate, t_count_max)
                  CALL FINALDMERG2( JOBZ, J, MATSIZ, EV( NP ),
     $                         Z( NP,NP ),LDZ,IWORK( NP ),
     $                         RHO, MODWRK( VSTRT+MAT1 ),
     $                         MATSIZ-MAT1, MODWRK( VSTRT ),
     $                         MAT1, MAT1, WORK, LWORK,
     $                         IWORK( N+1 ), TOL, INFO,
     $                         MODWRK(VSTRT+N),N,RKTOT-CURMOD,0 )

                  IF( INFO.NE.0 ) THEN
                     WRITE( *,* ) 'Dibtdc: ERROR in DMERG2, INFO  =',
     $                            INFO
                     GO TO 810
                  END IF
*
                  IF( CURMOD.LT.RKTOT ) THEN
*
*                    not the last merging operation:
*                    update the remaining modification vectors using
*                    the eigenvectors computed by DMERG2. For this
*                    purpose, copy the remaining modification vectors
*                    contained in MODWRK( VSTRT: ) into WORK( 1: )
*                    (requires a work space of MATSIZ x RKTOT-CURMOD),
*                    multiply WORK( 1: ) by Z( NP,NP )^T from the left
*                    and write the result back into MODWRK( VSTRT: ).
*
                     VSTRT = VSTRT+N

*
*                    update VSTRT in case we go to a new block of modification
*                    vectors
*

                  END IF
               end if
                     IF( J.EQ.RANK( KBRK ) ) THEN
                        VSTRT = VSTRT-NP
                     END IF
*
            ELSE
*
*              eigenvectors are accumulated ( JOBZ.EQ.'D' )
*

               CALL FINALDMERG2( JOBZ, J, MATSIZ, EV( NP ),
     $                      Z( NP,NP ),LDZ, 
     $                      IWORK( NP ), RHO, E( 1, J, KBRK),
     $                      KSIZES( KBRK+1 ),
     $                      E( 1, RANK( KBRK )+1+J, KBRK),
     $                      KSIZES( KBRK ), MAT1, WORK, LWORK,
     $                      IWORK( N+1 ), TOL, INFO )
               IF( INFO.NE.0 ) THEN
                  WRITE( *,* ) 'Dibtdc: ERROR in DMERG2, INFO  =',
     $                         INFO
                  GO TO 810
               END IF
            END IF
*
  221    CONTINUE
*
*        at this point all RANK( KBRK ) rank-one modifications corresponding
*        to the current off-diagonal block are finished.
*        Move on to the next off-diagonal block.
*
  220 CONTINUE

*
*     Re-merge the eigenvalues/vectors which were deflated at the final
*     merging step by sorting all eigenvalues and eigenvectors according
*     to the permutation stored in IWORK.
*
*     copy eigenvalues and eigenvectors in ordered form into WORK
*     (eigenvalues into WORK( 1:N ), eigenvectors into WORK( N+1:N+1+N^2 ) )
*
      DO 110 I = 1, N
         J         = IWORK( I )
         WORK( I ) = EV( J )
         IF( JOBZ.EQ.'D' ) THEN
            CALL DCOPY( N, Z( 1, J ), 1, WORK( N*I+1 ), 1 )
         END IF
  110 CONTINUE
*
*     copy ordered eigenvalues back from WORK( 1:N ) into EV
*
      CALL DCOPY( N, WORK( 1 ), 1, EV, 1 )
*
      IF( JOBZ.EQ.'D' ) THEN
*
*        copy ordered eigenvectors back from WORK( N+1:N+1+N^2 ) into Z
*
         CALL DLACPY( 'A', N, N, WORK( N+1 ), N, Z, LDZ )
      END IF
*****************************************************************************************************************
*
      GO TO 830
*
*************************************************************************
*
* ...Error handling...............................................
*
*      determine below the value of info when errors occurred in DSYEV
*
  800 CONTINUE
      INFO = 0
      DO 500 I = 1, K-1
         INFO = INFO + KSIZES(I)
  500 CONTINUE
      INFO = ( INFO + 1 ) * ( N + 1 ) + KSIZES(K)
      GO TO 830
*
*      determine below the value of info when errors occurred in DMERG2
*
  810 CONTINUE
      IF( INFO.GT.-200 ) THEN
         INFO = NP * ( N + 1 ) + ( NP + MATSIZ - 1 )
      END IF
      GO TO 830
*
*      determine below the value of info when errors occurred in preprocessing
*
  820 CONTINUE
      INFO = -99
      GO TO 830

  830 CONTINUE
*
      RETURN
*
*     End of DIBTDC
*
      END
