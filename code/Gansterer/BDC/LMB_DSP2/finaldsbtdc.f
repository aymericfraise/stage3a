      SUBROUTINE DSBTDC( JOBZ, JOBACC, N, NBLKS, KSIZES, D, L1D,
     $                   L2D, E, L1E, L2E, TOL, TAU1, TAU2, EV,
     $                   Z, LDZ, WORK, LWORK, IWORK, LIWORK,
     $                   MODWRK, IMODWRK, MINGAP, MINGAPI, INFO,
     $                   CHANGE)
*
*  -- Routine written in LAPACK Version 3.0 style --
****************************************************
*     started: February 29, 2000
*     April 16, 2003:    cleaned up research code and finalized version 1.0
*     last modification: August 25, 2008
****************************************************
*
*     .. Scalar Arguments ..
      CHARACTER          JOBZ, JOBACC
      INTEGER            N, NBLKS, L1D, L2D, L1E, L2E, LDZ,
     $                   LWORK, LIWORK, INFO, MINGAPI,
     $                   CHANGE, CHANGE2
      DOUBLE PRECISION   TOL, TAU1, TAU2, MINGAP
*     ..
*     .. Array Arguments ..
      INTEGER            KSIZES( * ), IWORK( LIWORK ), IMODWRK( * )
      DOUBLE PRECISION   D( L1D, L2D, * ), E( L1E, L2E, * ),
     $                   EV( N ), Z( LDZ, N ), 
     $                   WORK( LWORK ), MODWRK( * )
*     ..
*f2py intent(in) JOBZ
*f2py intent(in) JOBACC
*f2py intent(in) N
*f2py intent(in) NBLKS
*f2py intent(in) KSIZES
*f2py intent(in) D
*f2py intent(in) L1D
*f2py intent(in) L2D
*f2py intent(in) E
*f2py intent(in) L1E
*f2py intent(in) L2E
*f2py intent(in) TOL
*f2py intent(in) TAU1
*f2py intent(in) TAU2
*f2py intent(in) LDZ
*f2py intent(in) WORK
*f2py intent(in) LIWORK
*f2py intent(in) LWORK
*f2py intent(in) MODWORK
*f2py intent(out) EV
*f2py intent(out) Z
*f2py intent(out) MINGAP
*f2py intent(out) MINGAPI
*f2py intent(out) INFO
*
*  Purpose
*  =======
*
*  DSBTDC computes approximations to all eigenvalues and eigenvectors
*  of a symmetric block tridiagonal matrix using the divide and
*  conquer method with lower rank approximations to the subdiagonal blocks.
*
*  This code makes very mild assumptions about floating point
*  arithmetic. It will work on machines with a guard digit in
*  add/subtract, or on those binary machines without guard digits
*  which subtract like the Cray X-MP, Cray Y-MP, Cray C-90, or Cray-2.
*  It could conceivably fail on hexadecimal or decimal machines
*  without guard digits, but we know of none.  See DLAED3M for details.
*
*  Arguments
*  =========
*
*  JOBZ    (input) CHARACTER*1
*          = 'N':  Compute eigenvalues only;
*          = 'D':  Compute eigenvalues and eigenvectors. Eigenvectors
*                  are accumulated in the divide-and-conquer process.
*
*  JOBACC  (input) CHARACTER*1
*          = 'A' ("automatic"): The accuracy parameters TAU1 and TAU2
*                               are determined automatically from the
*                               parameter TOL according to the analytical
*                               bounds. In that case the input values of
*                               TAU1 and TAU2 are irrelevant (ignored).
*          = 'M' ("manual"): The input values of the accuracy parameters
*                            TAU1 and TAU2 are used. In that case the input
*                            value of the parameter TOL is irrelevant
*                            (ignored).
*
*  N       (input) INTEGER
*          The dimension of the symmetric block tridiagonal matrix.
*          N >= 1.
*
*  NBLKS   (input) INTEGER, 1 <= NBLKS <= N
*          The number of diagonal blocks in the matrix.
*
*  KSIZES  (input) INTEGER array, dimension (NBLKS)
*          The dimensions of the square diagonal blocks from top left
*          to bottom right.  KSIZES(I) >= 1 for all I, and the sum of
*          KSIZES(I) for I = 1 to NBLKS has to be equal to N.
*
*  D       (input) DOUBLE PRECISION array, dimension (L1D,L2D,NBLKS)
*          The lower triangular elements of the symmetric diagonal
*          blocks of the block tridiagonal matrix. The elements of the top
*          left diagonal block, which is of dimension KSIZES(1), have to
*          be placed in D(*,*,1); the elements of the next diagonal
*          block, which is of dimension KSIZES(2), have to be placed in
*          D(*,*,2); etc.
*
*  L1D     (input) INTEGER
*          The leading dimension of the array D.  L1D >= max(3,KMAX),
*          where KMAX is the dimension of the largest diagonal block,
*          i.e.,  KMAX = max_I ( KSIZES(I) ).
*
*  L2D     (input) INTEGER
*          The second dimension of the array D.  L2D >= max(3,KMAX),
*          where KMAX is as stated in L1D INFOabove.
*
*  E       (input) DOUBLE PRECISION array, dimension (L1E,L2E,NBLKS-1)
*          The elements of the subdiagonal blocks of the
*          block tridiagonal matrix. The elements of the top left
*          subdiagonal block, which is KSIZES(2) x KSIZES(1), have to be
*          placed in E(*,*,1); the elements of the next subdiagonal block,
*          which is KSIZES(3) x KSIZES(2), have to be placed in E(*,*,2); etc.
*          During runtime, the original contents of E(*,*,K) is
*          overwritten by the singular vectors and singular values of
*          the lower rank representation.
*
*  L1E     (input) INTEGER
*          The leading dimension of the array E.  L1E >= max(3,2*KMAX+1),
*          where KMAX is as stated in L1D above. The size of L1E enables
*          the storage of ALL singular vectors and singular values for
*          the corresponding off-diagonal block in E(*,*,K) and therefore
*          there are no restrictions on the rank of the approximation
*          (only the "natural" restriction
*          RANK( K ) .LE. MIN( KSIZES( K ),KSIZES( K+1 ) )).
*
*  L2E     (input) INTEGER
*          The second dimension of the array E.  L2E >= max(3,2*KMAX+1),
*          where KMAX is as stated in L1D above. The size of L2E enables
*          the storage of ALL singular vectors and singular values for 
*          the corresponding off-diagonal block in E(*,*,K) and therefore
*          there are no restrictions on the rank of the approximation
*          (only the "natural" restriction 
*          RANK( K ) .LE. MIN( KSIZES( K ),KSIZES( K+1 ) )).
*
*  TOL     (input) DOUBLE PRECISION, TOL.LE.TOLMAX
*          User specified tolerance for the residuals of the computed
*          eigenpairs. If ( JOBACC.EQ.'A' ) then it is used to determine
*          TAU1 and TAU2; ignored otherwise.
*          If ( TOL.LT.40*EPS .AND. JOBACC.EQ.'A' ) then TAU1 is set to machine
*          epsilon and TAU2 is set to the standard deflation tolerance from
*          LAPACK.
*
*  TAU1    (input) DOUBLE PRECISION, TAU1.LE.TOLMAX/2
*          User specified tolerance for determining the rank of the
*          lower rank approximations to the off-diagonal blocks.
*          The rank for each off-diagonal block is determined such that
*          the resulting absolute eigenvalue error is less than or equal
*          to TAU1.
*          If ( JOBACC.EQ.'A' ) then TAU1 is determined automatically from
*             TOL and the input value is ignored.
*          If ( JOBACC.EQ.'M' .AND. TAU1.LT.20*EPS ) then TAU1 is set to
*             machine epsilon.
*
*  TAU2    (input) DOUBLE PRECISION, TAU2.LE.TOLMAX/2
*          User specified deflation tolerance for the routine DIBTDC.
*          If ( 1.0D-1.GT.TAU2.GT.20*EPS ) then TAU2 is used as
*          the deflation tolerance in DSRTDF (EPS is the machine epsilon).
*          If ( TAU2.LE.20*EPS ) then the standard deflation tolerance from
*          LAPACK is used as the deflation tolerance in DSRTDF.
*          If ( JOBACC.EQ.'A' ) then TAU2 is determined automatically from
*             TOL and the input value is ignored.
*          If ( JOBACC.EQ.'M' .AND. TAU2.LT.20*EPS ) then TAU2 is set to
*             the standard deflation tolerance from LAPACK.
*
*  EV      (output) DOUBLE PRECISION array, dimension (N)
*          If INFO = 0, then EV contains the computed eigenvalues of the
*          symmetric block tridiagonal matrix in ascending order.
*
*  Z       (output) DOUBLE PRECISION array, dimension (LDZ,N)
*          If ( JOBZ.EQ.'D' .AND. INFO = 0 )
*          then Z contains the orthonormal eigenvectors of the symmetric
*          block tridiagonal matrix computed by the routine DIBTDC
*          (accumulated in the divide-and-conquer process).
*          If ( -199 < INFO < -99 ) then Z contains the orthonormal
*          eigenvectors of the symmetric block tridiagonal matrix,
*          computed without divide-and-conquer (quick returns).
*          Otherwise not referenced.
*
*  LDZ     (input) INTEGER
*          The leading dimension of the array Z.  LDZ >= max(1,N).
*
*  WORK    (workspace/output) DOUBLE PRECISION array, dimension (LWORK)
*
*  LWORK   (input) INTEGER
*          The dimension of the array WORK.
*          If NBLKS.EQ.1, then LWORK has to be at least 2N^2+6N+1
*          (for the call of DSYEVD).
*          If NBLKS.GE.2 and ( JOBZ.EQ.'D' ) then the absolute minimum
*             required for DIBTDC is ( N**2 + 3*N ). This will not always
*             suffice, though, the routine will return a corresponding
*             error code and report how much work space was missing (see
*             INFO).
*          In order to guarantee correct results in all cases where
*          NBLKS.GE.2, LWORK must be at least ( 2*N**2 + 3*N ).
*
*  IWORK   (workspace/output) INTEGER array, dimension (LIWORK)
*
*  LIWORK  (input) INTEGER
*          The dimension of the array IWORK.
*          LIWORK must be at least ( 5*N + 4*NBLKS - 4 ) (for DIBTDC)
*          Note that this should also suffice for the call of DSYEVD on a
*          diagonal block which requires ( 5*KMAX + 3 ).
*
*  MODWRK  (workspace/input) DOUBLE PRECISION array, dimension ( N*N )
*          If ( JOBZ.EQ.'N' ) workspace used in DIBTDC for
*          storing and updating the modification vectors in case the
*          eigenvector matrices are NOT accumulated in the
*          divide-and-conquer process. Otherwise not referenced.
*FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
****NOTE: MODWRK IS A ONE-DIMENSIONAL DOUBLE PRECISION WORKSPACE HANDED
****      OVER TO FINALDIBTDC ! IN A FUTURE CODE VERSION IT WILL BE INTEGRATED
****      INTO THE PARAMETER WORK !!!!
*FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
*
*  MINGAP  (output) DOUBLE PRECISION
*          The minimum "gap" between the approximate eigenvalues
*          computed, i.e., MIN( ABS( EV( I+1 )-EV( I ) ) for I=1,2,..., N-1
*          IF ( MINGAP.LE.TOL/10 ) THEN a warning flag is returned in INFO,
*          because the computed eigenvectors may be unreliable individually
*          (only the subspaces spanned are approximated reliably).
*
*  MINGAPI (output) INTEGER
*          Index I where the minimum gap in the spectrum occurred.
*
*  INFO    (output) INTEGER
*          = 0:  successful exit, no special cases occurred.
*          < -200: not enough workspace. Space for ABS(INFO + 200)
*                numbers is required in addition to the workspace provided,
*                otherwise some of the computed eigenvectors will be incorrect.
*          < -99, > -199: successful exit, but quick returns.
*                if INFO = -100, successful exit, but the input matrix
*                                was the zero matrix and no
*                                divide-and-conquer was performed
*                if INFO = -101, successful exit, but N was 1 and no
*                                divide-and-conquer was performed
*                if INFO = -102, successful exit, but only a single
*                                dense block. Standard dense solver
*                                was called, no divide-and-conquer was
*                                performed
*                if INFO = -103, successful exit, but warning that
*                                MINGAP.LE.TOL/10 and therefore the
*                                eigenvectors corresponding to close
*                                approximate eigenvalues may individually
*                                be unreliable (although taken together they
*                                do approximate the corresponding subspace to
*                                the desired accuracy)
*          = -99: error in the preprocessing in DIBTDC (when determining
*                 the merging order).
*          < 0, > -99: illegal arguments.
*                if INFO = -i, the i-th argument had an illegal value.
*          > 0:  The algorithm failed to compute an eigenvalue while
*                working on the submatrix lying in rows and columns
*                INFO/(N+1) through mod(INFO,N+1).
*
*  Further Details
*  ===============
*
*  Written by
*     Wilfried Gansterer and Bob Ward,
*     Department of Computer Science, University of Tennessee 
*
*  Based on the design of the LAPACK code sstedc.f written by Jeff
*  Rutter, Computer Science Division, University of California at
*  Berkeley, and modified by Francoise Tisseur, University of Tennessee.
*
*  =====================================================================
*
*     .. Parameters ..
      INTEGER            DEBUG
      DOUBLE PRECISION   ZERO, ONE, TWO, HALF, TOLMAX
*
*        DEBUG .EQ. 0 .... special debugging output is suppressed
*        DEBUG .NE. 0 .... special debugging output appears
*        TOLMAX       .... upper bound for tolerances TOL, TAU1, TAU2
*                          NOTE: in the routine FINALDIBTDC, the value
*                                1.D-1 is hardcoded for TOLMAX !
*
      PARAMETER          ( DEBUG = 0, TOLMAX = 1.0D-1,
     $                     ZERO = 0.0D0, ONE = 1.0D0,
     $                     TWO = 2.D0, HALF = 0.5D0 )
*     ..
*     .. Local Scalars ..
      INTEGER            IEND, I, II, J, K, LIWMIN, LWMIN,
     $                   NRBLKS, START, KSUM, KMAX, KSK, KSKP1,
     $                   NBLKM1, IP, JP, KCHK, NP, NK,
     $                   ISVALS, IU, IVT, IWSPC, LDU, LDVT, RP1, RK,
     $                   SPNEED
      DOUBLE PRECISION   EPS, P, DNRM, EXDNRM, EMAX, DMAX,
     $                   ANORM, DROPSV, ABSDIFF, TINY, D2
*
*     .. Local Arrays..
      INTEGER            RANK( NBLKS-1 )
*
*     ..
*     .. External Functions ..
      INTEGER            ILAENV
      DOUBLE PRECISION   DLAMCH, DLANST, DLANSY
      EXTERNAL           ILAENV, DLAMCH, DLANST, DLANSY
*     ..
*     .. External Subroutines ..
      EXTERNAL           DIBTDC, DLASCL, DLASET,
     $                   DSWAP, XERBLA, DSYEVD, DGESVD
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          ABS, INT, MAX, SQRT
*     ..
*     .. Executable Statements ..
*
*     Determine machine epsilon.
*
      EPS = DLAMCH( 'Epsilon' )
*
*     ...Check the input parameters................................
*
    !   WRITE(*,*) JOBZ, JOBACC
    !   WRITE(*,*) N, NBLKS, L1D, L2D, L1E, L2E, LDZ
    !   WRITE(*,*) LWORK, LIWORK, INFO, MINGAPI
    !   WRITE(*,*) CHANGE, CHANGE2
    !   WRITE(*,*) TOL, TAU1, TAU2, MINGAP

      INFO = 0
*
      IF( JOBZ.NE.'N' .AND. JOBZ.NE.'D' )THEN
         INFO = -1
      ELSE IF( JOBACC.NE.'A' .AND. JOBACC.NE.'M' )THEN
         INFO = -2
      ELSE IF( N.LT.1 )THEN
         INFO = -3
         print *,'TEST'
      ELSE IF( NBLKS.LT.1 .OR. NBLKS.GT.N )THEN
         WRITE(*,*) 'NBLKS', NBLKS
         INFO = -4
      END IF
      IF( INFO.EQ.0 )THEN
         KSUM = 0
         KMAX = 0
         KCHK = 0
         DO 1 K  = 1, NBLKS
            KSK  = KSIZES(K)
            KSUM = KSUM + KSK
            IF( KSK.GT.KMAX ) KMAX = KSK
            IF( KSK.LT.1 )    KCHK = 1
    1    CONTINUE
         IF( NBLKS.EQ.1 )THEN 
            LWMIN  = 2*N**2 + 6*N + 1
         ELSE
            LWMIN  = N**2 + 3*N
         END IF
         LIWMIN = 5*N + 4*NBLKS - 4
         IF ( KSUM.NE.N .OR. KCHK.EQ.1 )THEN
              WRITE(*,*) 'KSUM,KCHK,NBLKS', KSUM, KCHK,NBLKS
              DO K=1, NBLKS
                WRITE(*,*) 'KSIZES', K, KSIZES(K)
              END DO
              INFO = -5
         ELSE IF( L1D.LT.MAX( 3,KMAX ) )THEN
              INFO = -7
         ELSE IF( L2D.LT.MAX( 3,KMAX ) )THEN
              INFO = -8
         ELSE IF( L1E.LT.MAX( 3,2*KMAX+1 ) )THEN
              WRITE(*,*) 'L1E, KMAX', L1E, KMAX
              INFO = -10
         ELSE IF( L2E.LT.MAX( 3,2*KMAX+1 ) )THEN
              INFO = -11
         ELSE IF( JOBACC.EQ.'A' .AND. TOL.GT.TOLMAX )THEN 
              INFO = -12
         ELSE IF( JOBACC.EQ.'M' .AND. TAU1.GT.TOLMAX/2 )THEN 
              INFO = -13
         ELSE IF( JOBACC.EQ.'M' .AND. TAU2.GT.TOLMAX/2 )THEN 
              INFO = -14
         ELSE IF( LDZ.LT.MAX( 1,N ) )THEN
              WRITE(*,*) 'LDZ, MAX(1,N)', LDZ, MAX(1,N)
              INFO = -17
         ELSE IF( LWORK.LT.LWMIN )THEN
              INFO = -19
         ELSE IF( LIWORK.LT.LIWMIN )THEN 
              INFO = -21
         END IF
      END IF
*
      IF( INFO.NE.0 )THEN
	     write(*,*) 'INFO: ', INFO
         CALL XERBLA( 'DSBTDC', -INFO )
         RETURN
      END IF
*
*     Quick return if possible
*
      IF( N.EQ.1 )THEN
         EV( 1 ) = D( 1, 1, 1)
         Z( 1, 1 ) = ONE
         INFO = -101
         RETURN 
      END IF

*
*     If NBLKS is equal to 1, then solve the problem with standard
*     dense solver (in this case KSIZES(1) = N).
*
      IF( NBLKS.EQ.1 ) THEN
         WRITE( *,* ) ' dsbtdc: This branch still needs to be checked !'
         DO 5 I = 1, N
            DO 15 J = 1, I
               Z( I, J ) = D( I, J, 1)
   15       CONTINUE
    5    CONTINUE
         CALL DSYEVD( 'V', 'L', N, Z, LDZ, EV, WORK, LWORK,
     $                IWORK, LIWORK, INFO )
         IF( INFO.NE.0 ) THEN
            WRITE( *,* ) ' dsbtdc: Error in DSYEVD -- INFO =', INFO
            K = 1
            GO TO 800
         END IF
         INFO = -102
         RETURN
      END IF
*
*     determine the accuracy parameters (if requested)
*
      IF( JOBACC.EQ.'A' )THEN
         TAU1 = TOL/2
         IF( TAU1.LT.20*EPS ) TAU1 = EPS
         TAU2 = TOL/2
      END IF
*
*     Initialize Z as the identity matrix
*
      IF( JOBZ.EQ.'D' )THEN
         CALL DLASET( 'Full', N, N, ZERO, ONE, Z, LDZ )
      END IF
*
*     Determine the off-diagonal ranks, form and store the lower rank
*     approximations based on the tolerance parameters, the
*     RANK( K ) largest singular values and the associated singular
*     vectors of each subdiagonal block. Also find the maximum norm of
*     the subdiagonal blocks (in EMAX).
*
*     Compute SVDs of the subdiagonal blocks....
*
      NBLKM1 = NBLKS - 1
*
*     EMAX .... maximum norm of the off-diagonal blocks
*
      EMAX = ZERO
      DO 2 K = 1, NBLKM1
         KSK    = KSIZES( K )
         KSKP1  = KSIZES( K+1 )
         ISVALS = 1
*
*        Note that min(KSKP1,KSK).LE.N/2 (equality possible for
*        NBLKS=2), and therefore storing the singular values requires
*        at most N/2 entries of the *        array WORK.
*
         IU    = ISVALS + N/2
         IVT   = ISVALS + N/2
*
*        Call of DGESVD: The space for U is not referenced, since
*         JOBU='O' and therefore this portion of the array WORK
*         is not referenced for U.
*
         LDU   = KSKP1
         LDVT  = MIN( KSKP1,KSK )
         IWSPC = IVT + N*N/2
*
*         Note that the minimum workspace required for this call
*         of DGESVD is: N/2 for storing the singular values + N**2/2 for
*         storing V^T + 5*N/2 workspace =  N**2/2 + 3*N.
*
         CALL DGESVD( 'O', 'S', KSKP1, KSK, E( 1, 1, K ), L1E,
     $                WORK( ISVALS ), WORK( IU ), LDU,
     $                WORK( IVT ), LDVT,
     $                WORK( IWSPC ), LWORK-IWSPC+1, INFO )
         IF( INFO.NE.0 ) THEN
            WRITE( *,* ) ' dsbtdc: ERROR in DGESVD -- INFO =', INFO
            GO TO 800
         END IF
*
*        Note that after the return from DGESVD U is stored in
*        E(*,*,K), and V^{\top} is stored in WORK( IVT, IVT+1, .... )
*
****DEBUGGING BEGIN*****************************************************
         IF( DEBUG.NE.0 ) THEN
            WRITE( *,* ) ' U (stored in E(*,*,K)):'
            DO 802 J = 1, KSK
               DO 801 I = 1, KSKP1
                  WRITE( *,* ) ' E(', I, ',', J,',', K,')=',
     $                         E( I,J,K )
  801          CONTINUE
  802       CONTINUE
            WRITE( *,* ) ' V^T (stored in WORK):'
            DO 803 I = IVT, IVT+30
               WRITE( *,* ) ' WORK(', I, ')=', WORK( I )
  803       CONTINUE
         END IF
****DEBUGGING END*******************************************************
*
*        determine the ranks RANK() for the approximations
*
****DEBUGGING BEGIN*****************************************************
         IF( DEBUG.NE.0 ) THEN
            WRITE( *,* ) ' dsbtdc: printing the singular values ',
     $                   'of off-diagonal block ', K
            DO 88 I = 1, MIN( KSK,KSKP1 )
               WRITE( *,* ) ' dsbtdc: singval #', I, ' =',
     $                      WORK( ISVALS-1+I )
   88       CONTINUE 
         END IF
****DEBUGGING END*************************************************************
*
         RK     = MIN( KSK,KSKP1 )
    8    CONTINUE
         DROPSV = WORK( ISVALS-1+RK )
*
****DEBUGGING BEGIN*****************************************************
         IF( DEBUG.NE.0 ) THEN
            WRITE( *,* ) ' dsbtdc: DROPSV = ', DROPSV
         END IF
****DEBUGGING END*************************************************************
*
         IF( TWO*DROPSV.LE.TAU1 )THEN
*
*            the error caused by dropping singular value RK is
*            small enough, try to reduce the rank by one more
*
             RK = RK-1
             IF( RK.GT.0 )THEN
                GO TO 8
             ELSE
                RANK( K ) = 0
             END IF
         ELSE
*
*            the error caused by dropping singular value RK is
*            too large already, RK is the rank required to achieve the
*            desired accuracy
*
             RANK( K ) = RK
         END IF
*
****DEBUGGING BEGIN*****************************************************
         IF ( DEBUG.NE.0 ) THEN
            WRITE( *,* ) ' dsbtdc: RANK(', K, ') =', RANK( K )
         END IF
****DEBUGGING END*************************************************************
*
***************************************************************************
*
*        Store the first RANK( K ) terms of the SVD of the current
*        off-diagonal block.
*        NOTE that here it is required that L1E, L2E >= 2*KMAX+1 in order
*        to have enough space for storing singular vectors and values up
*        to the full SVD of an off-diagonal block !!!!
*        
*        u1-u_RANK(K) is already contained in E(:,1:RANK(K),K) (as a
*        result of the call of DGESVD !), the sigma1-sigmaK are to be
*        stored in E(1:RANK(K),RANK(K)+1,K),  and v1-v_RANK(K) are to be
*        stored in E(:,RANK(K)+2:2*RANK(K)+1,K)
*
         RP1 = RANK( K ) + 1
         DO 3 J = 1, RANK( K )
*
*           store sigma_J in E( J,RANK( K )+1,K )
*
            E( J, RP1, K ) = WORK( ISVALS-1 + J )
*
****DEBUGGING BEGIN*****************************************************
            IF( DEBUG.NE.0 ) THEN
               WRITE( *,* ) ' dsbtdc: sigma',J,' =', E( J, RP1, K )
            END IF
****DEBUGGING END*******************************************************
*
*           update maximum norm of subdiagonal blocks
*
            IF( E( J, RP1, K).GT.EMAX ) EMAX = E( J, RP1, K )
*
*           store v_J in E( :,RANK( K )+1+J,K )
*           (note that WORK contains V^{\top} and therefore
*           we need to read rowwise !)
*
****DEBUGGING BEGIN*****************************************************
            IF( DEBUG.NE.0 ) THEN
               WRITE( *,* ) ' dsbtdc: components of v',J,' ='
            END IF
****DEBUGGING END*******************************************************
*
            DO 7 I = 1, KSK
               E( I, RP1+J, K ) = WORK( IVT-1 + J + ( I - 1 )*LDVT ) 
*
****DEBUGGING BEGIN*****************************************************
               IF( DEBUG.NE.0 )THEN
                  WRITE( *,* ) I, E( I, RP1+J, K )
               END IF
****DEBUGGING END*******************************************************
*
    7       CONTINUE
    3    CONTINUE
*
****DEBUGGING BEGIN*****************************************************
         IF( DEBUG.NE.0 )THEN
            IF( RANK( K ).LT.MIN( KSK,KSKP1 ) )THEN
               WRITE( *,* ) ' dsbtdc: first neglected singular value ',
     $                      ' in block ', K, ':'
               WRITE( *,* ) '         ', WORK( ISVALS-1 + RP1 )
            END IF
         END IF
****DEBUGGING END*******************************************************
*
    2 CONTINUE
*
****DEBUGGING BEGIN*****************************************************
      IF( DEBUG.NE.0 )THEN
         WRITE( *,* ) ' dsbtdc: largest singular value EMAX =', EMAX
      END IF
****DEBUGGING END*******************************************************
*
*     Compute the maximum norm of diagonal blocks and store the norm
*     of each diagonal block in E(RP1,RP1,K) (after the singular values);
*     store the norm of the last diagonal block in EXDNRM.
*
*     DMAX .... maximum one-norm of the diagonal blocks
*
      DMAX = ZERO
      DO 4 K = 1, NBLKS
         RP1 = RANK( K ) + 1
*
*        compute the one-norm of diagonal block K
*
         DNRM = DLANSY( '1', 'L', KSIZES( K ), D( 1, 1, K ),
     $      L1D, WORK )
         IF ( K.EQ.NBLKS ) THEN
            EXDNRM = DNRM
         ELSE
            E( RP1, RP1, K ) = DNRM
         END IF
         IF( DNRM.GT.DMAX ) DMAX = DNRM
    4 CONTINUE
*
****DEBUGGING BEGIN*****************************************************
      IF( DEBUG.NE.0 )THEN
         WRITE( *,* ) ' dsbtdc: maximum norm of diagonal blocks ',
     $                'DMAX =', DMAX
      END IF
****DEBUGGING END*******************************************************
*
*     Check for zero matrix.
*
      IF( EMAX.EQ.ZERO .AND. DMAX.EQ.ZERO ) THEN
         DO 6 I = 1, N
            EV( I ) = ZERO
    6    CONTINUE
         INFO = -100
         RETURN
      END IF
*
*****************************************************************
*
*     ....Identify irreducible parts of the block tridiagonal matrix
*     [while ( START <= NBLKS )]....
*
      START = 1
      NP    = 1
   10 CONTINUE
      IF( START.LE.NBLKS )THEN
*
*     Let IEND be the number of the next subdiagonal block such that
*     its RANK is 0 or IEND = NBLKS if no such subdiagonal exists.
*     The matrix identified by the elements between the diagonal block START
*     and the diagonal block IEND constitutes an independent (irreducible)
*     sub-problem.
*
         IEND = START
*
   20    CONTINUE
         IF( IEND.LT.NBLKS )THEN
            RK = RANK( IEND )
*
*           NOTE: if RANK( IEND ).EQ.0 then decoupling happens due to
*                 reduced accuracy requirements ! (because in this case
*                 we would not merge the corresponding two diagonal blocks)
*
*           NOTE: seems like any combination may potentially happen:
*                 (i) RANK = 0 but no decoupling due to small norm of
*                     off-diagonal block (corresponding diagonal blocks
*                     also have small norm) as well as 
*                 (ii) RANK > 0 but decoupling due to small norm of
*                     off-diagonal block (corresponding diagonal blocks
*                     have very large norm)
*                 case (i) is ruled out by checking for RANK = 0 above
*                 (we decide to decouple all the time when the rank
*                 of an off-diagonal block is zero, independently of
*                 the norms of the corresponding diagonal blocks.
*
            IF( RK.GT.0 )THEN
*
*              check for decoupling due to small norm of off-diagonal block
*              (relative to the norms of the corresponding diagonal blocks)
*
               IF ( IEND.EQ.NBLKM1 )THEN
                  D2 = SQRT( EXDNRM )
               ELSE
                  D2 = SQRT( E( RANK( IEND+1 )+1, RANK( IEND+1 )+1,
     $                       IEND+1 ) )
               END IF
*
*              this definition of TINY is analogous to the definition
*              in the tridiagonal divide&conquer (dstedc)
*
               TINY = EPS*SQRT( E( RANK( IEND )+1, RANK( IEND )+1,
     $                       IEND ) )*D2
               IF( E( 1, RANK( IEND )+1, IEND ).GT.TINY )THEN
*
*                 no decoupling due to small norm of off-diagonal block
*
                  IEND = IEND + 1
                  GO TO 20
               END IF
            END IF
         END IF
*
*        ....(Sub) Problem determined: between diagonal blocks
*            START and IEND. Compute its size and solve it....
*
****DEBUGGING BEGIN*****************************************************
         IF( DEBUG.NE.0 )THEN
            WRITE( *,* ) ' dsbtdc: (Sub)problem determined. START =',
     $                  START, ', IEND =', IEND
         END IF
****DEBUGGING END*******************************************************
*
         NRBLKS = IEND - START + 1
         IF( NRBLKS.EQ.1 )THEN
*
*           Isolated problem is a single diagonal block
*
            NK = KSIZES( START )
            WRITE(*,*) NK, START, 1    
            return
*       
*           copy this isolated block into Z
*
            DO 25 I = 1, NK
               IP = NP + I - 1
               DO 35 J = 1, I
                  JP = NP + J - 1
                  Z( IP, JP ) = D( I, J, START)
   35          CONTINUE
   25       CONTINUE
*   
*            WRITE( *,* ) ' dsbtdc: no divide-and-conquer,',
*     $                   ' calling DSYEVD for single diagonal',
*     $                   ' block....'
*
*           check whether there is enough workspace
*
            SPNEED = 2*NK**2+6*NK+1
            IF( SPNEED.GT.LWORK )THEN
               WRITE( *,* ) ' dsbtdc: not enough workspace ',
     $                      'for DSYEVD, INFO =', INFO
               INFO = -200 + LWORK - SPNEED
               GO TO 820
            END IF
*

	        IF(JOBZ.EQ.'N')THEN
                CALL DSYEVD( 'N', 'L', NK, Z( NP,NP ), LDZ, EV( NP ),
     $                    WORK, LWORK, IWORK, LIWORK, INFO )
	        ELSE
                CALL DSYEVD( 'V', 'L', NK, Z( NP,NP ), LDZ, EV( NP ),
     $                    WORK, LWORK, IWORK, LIWORK, INFO )
	        END IF
            IF( INFO.NE.0 ) THEN
               WRITE( *,* ) ' dsbtdc: ERROR in DSYEVD--INFO =', INFO
               K = START
               GO TO 800
            END IF
            START = IEND + 1
            NP    = NP + NK
*
*           go to the next irreducible subproblem
*
            GO TO 10
         END IF
*
*        ....Isolated problem consists of more than one diagonal block.
*            Start the divide and conquer algorithm....
*
*        Scale: Divide by the maximum of all norms of diagonal blocks
*               and singular values of the subdiagonal blocks
*
*        ....determine maximum of the norms of all diagonal and subdiagonal
*            blocks....
*
         IF( IEND.EQ.NBLKS ) THEN
            ANORM = EXDNRM
         ELSE
            ANORM = E( RANK( IEND )+1, RANK( IEND )+1, IEND )
         END IF
         DO 26 K = START, IEND - 1
            RP1 = RANK( K ) + 1
*
*           norm of diagonal block
*
            ANORM = MAX( ANORM, E( RP1, RP1, K ) )
*
*           singular value of subdiagonal block
*
            ANORM = MAX( ANORM, E( 1,   RP1, K ) )
   26    CONTINUE
*
****DEBUGGING BEGIN*****************************************************
         IF( DEBUG.NE.0 )THEN
            WRITE( *,* ) ' dsbtdc: scaling factor ANORM =', ANORM
         END IF
****DEBUGGING BEGIN*****************************************************
*
         NK = 0
         DO 27 K = START, IEND
            KSK = KSIZES( K )
            NK = NK + KSK
*
*           scale the diagonal block
*
            CALL DLASCL( 'L', 0, 0, ANORM, ONE, KSK,
     $                   KSK, D( 1, 1, K ), L1D, INFO )
            IF( INFO.NE.0 ) THEN
               WRITE( *,* ) ' dsbtdc: ERROR in DLASCL--INFO =', INFO 
               GO TO 800
            END IF
*
*           scale the (approximated) off-diagonal block by dividing its
*           singular values
*
            IF( K.NE.IEND ) THEN
*
*           the last subdiagonal block has index IEND-1 !!!!
*
               DO 28 I = 1, RANK( K )
                  E( I, RANK( K )+1, K ) =
     $            E( I, RANK( K )+1, K ) / ANORM
   28          CONTINUE
            END IF
   27    CONTINUE
*
*        call the block-tridiagonal divide-and-conquer on the
*        irreducible subproblem which has been identified
*
        CHANGE2=max(0,NK-(N-CHANGE))
** ** **		print *, NK, N, CHANGE, CHANGE2
         CALL FINALDIBTDC( JOBZ, NK, NRBLKS, KSIZES( START ),
     $                D( 1,1,START ), L1D, L2D, E( 1,1,START ),
     $                RANK( START ), L1E, L2E, TAU2, EV( NP ),
     $                Z( NP,NP ), LDZ, WORK, LWORK,
     $                IWORK, LIWORK, INFO, MODWRK, IMODWRK, CHANGE2 )
         IF( INFO.NE.0 )THEN
            WRITE( *,* ) ' dsbtdc: ERROR in DIBTDC--INFO =', INFO
            GO TO 820
         END IF
*
***************************************************************************
*
*        Scale back the computed eigenvalues.
*
         CALL DLASCL( 'G', 0, 0, ONE, ANORM, NK, 1, EV( NP ), NK,
     $                INFO )
         IF( INFO.NE.0 )THEN
            WRITE( *,* ) ' dsbtdc: ERROR in DLASCL--INFO =', INFO

            GO TO 810 
         END IF
*
         START = IEND + 1
         NP = NP + NK
*
*        Go to the next irreducible subproblem.
*
         GO TO 10
      END IF
*
*     [end while ( START <= NBLKS )]....
*
****DEBUGGING BEGIN*****************************************************
      IF( DEBUG.NE.0 )THEN
         DO 900 I = 1, N
            WRITE( *,* ) ' dsbtdc: Z(',I,',1) =', Z( I,1 ),
     $                   '   Z(',I,',2) =', Z( I,2 )
  900    CONTINUE
      END IF
****DEBUGGING END*******************************************************
*
*     ....If the problem split any number of times, then the eigenvalues
*     will not be properly ordered. Here we permute the eigenvalues
*     (and the associated eigenvectors) across the irreducible parts
*     into ascending order....
*
*      IF( NRBLKS.LT.NBLKS )THEN
*
*        Use Selection Sort to minimize swaps of eigenvectors
*
         DO 40 II = 2, N
            I = II - 1
            K = I
            P = EV( I )
            DO 30 J = II, N
               IF( EV( J ).LT.P )THEN
                  K = J
                  P = EV( J )
               END IF
   30       CONTINUE
            IF( K.NE.I )THEN
               EV( K ) = EV( I )
               EV( I ) = P
		if(JOBZ.eq.'D') then
			CALL DSWAP( N, Z( 1, I ), 1, Z( 1, K ), 1 )
		end if
            END IF
   40    CONTINUE
*      END IF
*
* ...Compute MINGAP (minimum difference between neighboring eigenvalue
*    approximations)..............................................
*
      MINGAP  = EV( 2 )-EV( 1 )
      IF( MINGAP.LT.ZERO )THEN
         WRITE( *,* ) ' dsbtdc: Eigenvalue approximations are',
     $                ' not ordered properly.'
         WRITE( *,* ) '         Approximation ', I,
     $                ' is larger than approximation ', I+1, '.'
         WRITE( *,* ) '         Emergency stop.'
         STOP
      END IF
      MINGAPI = 1
      DO 80 I = 2, N-1
         ABSDIFF = EV( I+1 )-EV( I )
         IF( ABSDIFF.LT.ZERO )THEN
            WRITE( *,* ) ' dsbtdc: Eigenvalue approximations are',
     $                   ' not ordered properly.'
            WRITE( *,* ) '         Approximation ', I,
     $                   ' is larger than approximation ', I+1, '.'
            WRITE( *,* ) '         Emergency stop.'
            STOP
         ELSE IF( ABSDIFF.LT.MINGAP )THEN
            MINGAP  = ABSDIFF
            MINGAPI = I
         END IF
   80 CONTINUE
*
*     check whether the minimum gap between eigenvalue approximations
*     may indicate severe inaccuracies in the eigenvector approximations
*
      IF( MINGAP.LE.TOL/10 )THEN
         INFO = -103
      END IF
*
      RETURN
*
* ...Error handling...............................................
*
*    info value is determined from the block number k
*
  800 CONTINUE
      INFO = 0
      DO 500 I = 1, K-1
         INFO = INFO + KSIZES(I)
  500 CONTINUE
      INFO = ( INFO + 1 ) * ( N + 1 ) + KSIZES(K)
      RETURN
*
*    info value is determined from first and last row
*
  810 CONTINUE
      INFO = NP * ( N + 1 ) + ( NP + NK - 1 )
      RETURN
*
*    when coming from inside DSBTDC or from DIBTDC, 
*    INFO already has the proper value
*
  820 CONTINUE
      RETURN
*
*     End of DSBTDC
*
      END
