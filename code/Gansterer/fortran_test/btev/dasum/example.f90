module example
  implicit none
  contains
  subroutine test(n,dx,incx)
    integer :: n
    double precision, dimension(n) :: dx
    integer :: incx
    double precision :: DASUM
    double precision :: res
    
    integer :: i
    do i = 1,n
        dx(i) = i
    end do
    
    res = DASUM(n,dx,incx)
    ! res = 5
    write(*,*) 'n', n
    write(*,*) 'dx', dx
    write(*,*) 'res', res
  end subroutine
end module

