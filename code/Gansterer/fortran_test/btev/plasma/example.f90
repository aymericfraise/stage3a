program example
  use mpi
  use plasma
  implicit none

  integer :: a = 1
  integer :: info

  call MPI_Init(info)
  write(*,*) info
  call plasma_init(a, info)
  write(*,*) info
end program

