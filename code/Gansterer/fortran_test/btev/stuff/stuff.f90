MODULE STUFF
use mpi
use plasma
IMPLICIT NONE

!!
!! THIS PROGRAM CALCULATES THE EIGENVECTOR FOR A GIVEN SHIFT
!! OF A FOR SYMMETRIC BLOCK-TRIDIAGONAL MATRIX 
!!

CONTAINS
  ! Calculates the eigenvector based on twisted factorizations
    SUBROUTINE F(n)
        INTEGER :: n
        write(*,*) n
    END SUBROUTINE F
    
  SUBROUTINE DSYBTEV (a,b,c,pivot,piv,ev,shift,count,steps,nit,ierr,seleshift) 
    INTEGER,INTENT(IN)                   :: count         ! Number of submatrices in main diagonal 
    INTEGER,INTENT(IN)                   :: steps         ! Number of columns in current submatrix 
    INTEGER,INTENT(IN)                   :: nit           ! Max. Number of Inverse Iterations
    INTEGER                              :: cs            ! Count * steps     
    INTEGER                              :: i,j,k,f,g,tmp     ! For loops
    INTEGER                              :: fac           ! # of the factorization with the min. dia. ele.
    INTEGER,INTENT(OUT) 		      :: ierr		  ! Errorcode
    DOUBLE PRECISION , INTENT(INOUT):: a(:,:,:,:)    ! dim 1:2 = block ,  
    DOUBLE PRECISION , INTENT(INOUT):: b(:,:,:,:)    ! dim   3 = #block
    DOUBLE PRECISION , INTENT(INOUT):: c(:,:,:,:)    ! dim   4 = (1 = fw, 2 = bw, 3 = twisted factorization)
    INTEGER, INTENT(INOUT)          :: pivot(:,:,:)  ! dim   1 = pivoting vector, 2 = #block , 3 = (1 = fw, 2 = bw, 3 = twisted f
    DOUBLE PRECISION , INTENT(INOUT):: ev(:)         ! storage for the eigenvector
    DOUBLE PRECISION , INTENT(IN)        :: shift 
    DOUBLE PRECISION                     :: norm,norm2
    INTEGER                              :: info, seleshift, half 
    DOUBLE PRECISION                     :: dlamch, eps
    DOUBLE PRECISION , INTENT(INOUT) :: piv(:)
    DOUBLE PRECISION ONE,ZERO,VIEL
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

    INTEGER :: DEPL
!   PARAMETER (DEPL=198)

	integer my_rank, minimum(2)
    integer status(MPI_STATUS_SIZE),request

  call MPI_Comm_rank(MPI_COMM_WORLD, my_rank, ierr)

    half = FLOOR(count/2.0)
    DEPL = half - 1
   

    ! A     (input)  DOUBLE PRECISION array, dimension (steps,steps,count-1,3)
    ! On input a(:,:,:,1) contains the subdiagonal blocks 
    
    ! B     (input)  DOUBLE PRECISION array, dimension (steps,steps,count,3)
    ! On input b(:,:,:,1) contains the main diagonal blocks
    
    ! C     (input)  DOUBLE PRECISION array, dimension (steps,steps,count-1,3)
    ! On input c(:,:,:,1) contains the superdiagonal blocks 
    
    ! pivot (input)  INTEGER array, dimension (steps,count,3)

    ! ev    (output) Contains the eigenvector, dimension (steps*count) 

    ! shift (input)  Shift to be used for eigenvector calculation

    ! count (input)  Number of blocks in main diagonal 
    
    ! steps (input)  Number of columns in a block


    eps = dlamch('E')   ! machine epsilon
    viel = dlamch('O')  ! maximal double
    cs = count * steps 

    ! Subtract shift from diagonal elements 

    if(modulo(my_rank,2).eq.0)then
    	DO i = 1 , half
       	DO j = 1,steps
			b(j,j,i,1)= b(j,j,i,1) - shift 
       	END DO
    	END DO
    else
    	DO i = half+1 , count
       	DO j = 1,steps
			b(j,j,i-DEPL,1)= b(j,j,i-DEPL,1) - shift 
       	END DO
    	END DO
    end if

    ! Prepare data for forward (and reverse) factorization
    a(:,:,:,2) = a(:,:,:,1) 

    b(:,:,:,2) = b(:,:,:,1)

    b(:,:,:,3) = b(:,:,:,1)

    c(:,:,:,2) = c(:,:,:,1) 

!		print *, 'BEFORE DSYBTTWF:', my_rank
!		call flush()

    ! Calculate Twisted Factorizations
    CALL DSYBTTWF (a,b,c,pivot,piv,count,steps,ierr,request ) 

!		print *, 'AFTER DSYBTTWF:', my_rank
!		call flush()

    if(ierr .ne. 0)then
	!return
    end if

    ! Determine starting position for backward substitution 
    ! based on scalar strategy

    if(modulo(my_rank,2).eq.0)then

	fac = 3                  ! Factorization for the starting vector
	g = half                    ! Number of the block 
	f = (half-1)*steps + 1                    ! Element (absolute)  
	norm = abs(b(1,1,half,3))   ! Tmp for comparison of the diagonal elements

	! Twisted 
	k = 3 
	DO i = 1, half
		DO j = 1 , steps ! element 
			IF (ABS(b(j,j,i,k) ) .LT. norm ) THEN
				norm = ABS(b(j,j,i,k)) 
				fac = k
				g =  i
				f = (i-1)*steps + j 
			END IF
		END DO
	END DO
	call MPI_SendRecv(norm,1,MPI_DOUBLE,my_rank+1,100, &
               &     norm2,1,MPI_DOUBLE,my_rank+1,100, &
               &     MPI_COMM_WORLD,status,ierr)

	if(norm.le.norm2)then
		minimum(1)=g
		minimum(2)=f
!		print *, 'BEFORE SEND:', my_rank
		call MPI_Send(minimum, 2, MPI_INT, my_rank+1, 101, MPI_COMM_WORLD, ierr)
	else
!		print *, 'BEFORE RECV:', my_rank
		call MPI_Recv(minimum, 2, MPI_INT, my_rank+1, 101, MPI_COMM_WORLD,status, ierr)
		g = minimum(1)
		f = minimum(2)
		norm = norm2
	end if
!	print *, my_rank, 'MINIMUM: ', norm, g, f
!	call flush()
    else

	fac = 3                  ! Factorization for the starting vector
	g = half+1                    ! Number of the block 
	f = half*steps + 1                    ! Element (absolute)  
	norm = abs(b(1,1,half+1-DEPL,3))   ! Tmp for comparison of the diagonal elements

	! Twisted 
	k = 3 
	DO i = half+1, count 
		DO j = 1 , steps ! element 
			IF (ABS(b(j,j,i-DEPL,k) ) .LT. norm ) THEN
				norm = ABS(b(j,j,i-DEPL,k)) 
				fac = k
				g =  i    
				f = (i-1)*steps + j 
			END IF
		END DO
	END DO
	call MPI_SendRecv(norm,1,MPI_DOUBLE,my_rank-1,100, &
               &     norm2,1,MPI_DOUBLE,my_rank-1,100, &
               &     MPI_COMM_WORLD,status,ierr)

	if(norm.lt.norm2)then
		minimum(1)=g
		minimum(2)=f
!		print *, 'BEFORE SEND:', my_rank
		call MPI_Send(minimum, 2, MPI_INT, my_rank-1, 101, MPI_COMM_WORLD, ierr)
	else
!		print *, 'BEFORE RECV:', my_rank
		call MPI_Recv(minimum, 2, MPI_INT, my_rank-1, 101, MPI_COMM_WORLD,status, ierr)
		g = minimum(1)
		f = minimum(2)
		norm = norm2
	end if
!	print *, my_rank, 'MINIMUM: ', norm, g, f
!	call flush()
    end if

!		print *, 'BEFORE SOLUTION:', my_rank
!		call flush()

    do tmp = 1, nit

    	! (B) Forward Substitution

	if(g.gt.half .and. modulo(my_rank,2).eq.1)then
		! (A) Consider Pivoting 
		ev(:) = zero
		ev(f) = ONE
		call ANTIPIVOTINGVEC ( ev((g-1)*steps+1:g*steps), pivot(:,g-DEPL,3), steps ) 

       	CALL DTRSM('l','l','n','u',steps,1,ONE,b(:,:,g-DEPL,3),steps,ev((g-1)*steps+1:g*steps),steps)
       	! First, the block of the twisted factorization 
       	CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,g-DEPL,3),steps,ev((g-1)*steps+1:g*steps),steps)
	else if(g.le.half .and. modulo(my_rank,2).eq.0)then
		! (A) Consider Pivoting 
		ev(:) = zero
		ev(f) = ONE
		call ANTIPIVOTINGVEC ( ev((g-1)*steps+1:g*steps), pivot(:,g,3), steps ) 

		CALL DTRSM('l','l','n','u',steps,1,ONE,b(:,:,g,3),steps,ev((g-1)*steps+1:g*steps),steps)
       	! First, the block of the twisted factorization 
       	CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,g,3),steps,ev((g-1)*steps+1:g*steps),steps)
	end if

	call MPI_Wait(request,status,ierr)

    	! Backward substitution
    	CALL DSYBTBS (a,b,c,pivot,ev,fac,g,f,count,steps ) 

    	! scale ev to a length of 1
    	ev = ev  / SQRT(SUM(ev**2))
  end do

  END SUBROUTINE DSYBTEV 








  ! Calculates the twisted factorizations of block-tridiagonal matrices (in blocked format)
  SUBROUTINE DSYBTTWF (a,b,c,pivot,piv,count,steps,ierr,request) 
    INTEGER,INTENT(IN)                   :: count         ! Number of submatrices in main diagonal 
    INTEGER,INTENT(IN)                   :: steps         ! Number of columns in current submatrix 
    INTEGER                              :: cs            ! Count * steps     
    INTEGER                              :: i,j,k,f,g     ! For loops 
    INTEGER                              :: fac           ! # of the factorization with the min. dia. ele.
    INTEGER,INTENT(OUT)		      :: ierr,request  ! Errorcode
    DOUBLE PRECISION , INTENT(INOUT):: a(:,:,:,:)    ! dim 1:2 = block ,  
    DOUBLE PRECISION , INTENT(INOUT):: b(:,:,:,:)    ! dim   3 = #block
    DOUBLE PRECISION , INTENT(INOUT):: c(:,:,:,:)    ! dim   4 = (1 = fw, 2 = bw, 3 = twisted factorization)
    INTEGER, INTENT(INOUT)          :: pivot(:,:,:)  ! dim   1 = pivoting vector, 2 = #block , 3 = (1 = fw, 2 = bw, 3 = twisted f
    INTEGER                              :: info    
    DOUBLE PRECISION ONE,ZERO,VIEL
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)
    integer my_rank, anz, start, ende, half
    integer status(MPI_STATUS_SIZE,9)
    DOUBLE PRECISION , INTENT(INOUT) :: piv(:)
    INTEGER :: DEPL
    integer :: nb_DGETRF,nb_DGEMM,nb_DTRSM,ib_DGETRF,ib_DGEMM,ib_DTRSM
    integer np
!    PARAMETER (DEPL=198)


    call MPI_Comm_rank(MPI_COMM_WORLD, my_rank, ierr)

    cs = count * steps 
    half = FLOOR(count/2.0)
    DEPL = half - 1

    ierr = 0

    CALL PLASMA_Disable(PLASMA_AUTOTUNING,info);

    np=2

    if(np.eq.1)then
	nb_DGETRF = 16
	ib_DGETRF = 16
	nb_DGEMM  = 160
	ib_DGEMM  = 11
	nb_DTRSM  = 160
	ib_DTRSM  = 40
    else if(np.eq.2)then
	nb_DGETRF = 16
	ib_DGETRF = 8
	nb_DGEMM  = 130
	ib_DGEMM  = 1
	nb_DTRSM  = 100
	ib_DTRSM  = 90
    else if(np.eq.4)then
	nb_DGETRF = 16
	ib_DGETRF = 8
	nb_DGEMM  = 130
	ib_DGEMM  = 18
	nb_DTRSM  = 40
	ib_DTRSM  = 14
    end if



    if(modulo(my_rank,2).eq.0)then

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!    F O R W A R D      R U N S    !!!!!!!!!!!!!!!!!!!
	!!!!!!!!                                  !!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	k = 1 
    
	! LU Factorization of first diagonal block B_1
	CALL DGETRF( steps, steps, b(:,:,k,1), steps, pivot(:,1,1), info )

	DO k = 2, half

		j = k-1

		! We calculate M'_k= P_k M_k        M'_k * U_k-1 = A_k 
		call DTRSM( 'r', 'u', 'n', 'n', steps, steps, one, b(:,:,j,1), steps, a(:,:,j,1) , steps)

		! We calculate N_k-1
		call pivoting ( c(:,:,j,1),a(:,1,1,3), pivot(:,j,1), steps )
		call DTRSM( 'l', 'l', 'n', 'u', steps, steps, one, b(:,:,j,1), steps, c(:,:,j,1), steps)

		! We calculate M_k * N_k-1  
    CALL PLASMA_Set(PLASMA_TILE_SIZE, nb_DGEMM,info);
    CALL PLASMA_Set(PLASMA_INNER_BLOCK_SIZE, ib_DGEMM,info);
       call PLASMA_DGEMM(PlasmaNoTrans,PlasmaNoTrans,steps,steps,steps,ONE,a(:,:,j,1),steps,c(:,:,j,1),steps, &
          		     &     ZERO,a(:,:,j,3),steps,info)

!		call DGEMM('N','N',steps,steps,steps,ONE,a(:,:,j,1),steps,c(:,:,j,1),steps,ZERO,a(:,:,j,3),steps)
		! (This result is saved for later use in the twisted factorizations)

		! Now we subtract the contribution of m*n to b 
		b(:,:,k,1) = b(:,:,k,1) -  a(:,:,j,3)

		! LU factorization
		call DGETRF( steps, steps, b(:,:,k,1), steps, pivot(:,k,1), info )

		! pivot m 
		call PIVOTING (a(:,:,j,1),piv, pivot(:,k,1), steps )


		if (info .ne. 0) then
			!print *, "Error LU factorization in FW direction" , info
			!call flush()
			!stop 
			ierr = 1
			!return
		end if

	END DO

	call MPI_SendRecv(b(:,:,half,1)  ,steps*steps,MPI_DOUBLE,my_rank+1,1, &
               &     b(:,:,half+1,2),steps*steps,MPI_DOUBLE,my_rank+1,1, &
               &     MPI_COMM_WORLD,status(:,1),ierr)
	call MPI_SendRecv(pivot(:,half,1)  ,steps,MPI_INT,my_rank+1,2, &
               &     pivot(:,half+1,2),steps,MPI_INT,my_rank+1,2, &
               &     MPI_COMM_WORLD,status(:,1),ierr)

	DO k = half, 1 , -1  ! loop over all submatrices in reverse direction

		! We calculate N_k
		call pivoting ( a(:,:,k,2), c(:,1,1,3), pivot(:,k+1,2), steps )
		call DTRSM( 'l', 'l', 'n', 'u', steps, steps, one, b(:,:,k+1,2), steps, a(:,:,k,2), steps)

		! We calculate M'= P_k+1 M_k+1        M'_k+1 * U_k+1 = C_k+1  
		call DTRSM( 'r', 'u', 'n', 'n', steps, steps, one, b(:,:,k+1,2) , steps, c(:,:,k,2) , steps)

		! We calculate M_k * N_k-1  
    CALL PLASMA_Set(PLASMA_TILE_SIZE, nb_DGEMM,info);
    CALL PLASMA_Set(PLASMA_INNER_BLOCK_SIZE, ib_DGEMM,info);
       call PLASMA_DGEMM(PlasmaNoTrans,PlasmaNoTrans,steps,steps,steps,ONE,c(:,:,k,2),steps,a(:,:,k,2),steps,ZERO, &
          		     &     c(:,:,k,3),steps,info)

!		call DGEMM('N','N',steps,steps,steps,ONE,c(:,:,k,2),steps,a(:,:,k,2),steps,ZERO,c(:,:,k,3),steps)
		! (This result is saved for later use in the twisted factorizations)

		! Now we subtract the contribution of m*n (saved in c and a) to b  
		b(:,:,k,2) = b(:,:,k,2) -  c(:,:,k,3)

		! LU factorization
		call DGETRF( steps, steps, b(:,:,k,2), steps, pivot(:,k,2), info )

		! pivoting of m 
		call PIVOTING( c(:,:,k,2), piv, pivot(:,k,2), steps )

		if (info .ne. 0) then
			!print *, "Error LU factorization in BW direction" 
			!call flush()
			!stop 
			ierr = 2
			!return
		end if

	END DO


    else


	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!    B A C K W A R D      R U N S  !!!!!!!!!!!!!!!!!!!
	!!!!!!!!                                  !!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	k = count 

	! lu factorization of the last element
	CALL DGETRF( steps, steps,  b(:,:,k-DEPL,2), steps, pivot(:,k-DEPL,2), info )


	DO k = count-1, half+1 , -1  ! loop over all submatrices in reverse direction

		! We calculate N_k
		call pivoting ( a(:,:,k-DEPL,2), c(:,1,1,3), pivot(:,k+1-DEPL,2), steps )
		call DTRSM( 'l', 'l', 'n', 'u', steps, steps, one, b(:,:,k+1-DEPL,2), steps, a(:,:,k-DEPL,2), steps)

		! We calculate M'= P_k+1 M_k+1        M'_k+1 * U_k+1 = C_k+1  
		call DTRSM( 'r', 'u', 'n', 'n', steps, steps, one, b(:,:,k+1-DEPL,2) , steps, c(:,:,k-DEPL,2) , steps)

		! We calculate M_k * N_k-1  
    CALL PLASMA_Set(PLASMA_TILE_SIZE, nb_DGEMM,info);
    CALL PLASMA_Set(PLASMA_INNER_BLOCK_SIZE, ib_DGEMM,info);
       call PLASMA_DGEMM(PlasmaNoTrans,PlasmaNoTrans,steps,steps,steps,ONE,c(:,:,k-DEPL,2),steps,a(:,:,k-DEPL,2),steps,ZERO, &
          		     &     c(:,:,k-DEPL,3),steps,info)
!		call DGEMM('N','N',steps,steps,steps,ONE,c(:,:,k-DEPL,2),steps,a(:,:,k-DEPL,2),steps,ZERO,c(:,:,k-DEPL,3),steps)
		! (This result is saved for later use in the twisted factorizations)

		! Now we subtract the contribution of m*n (saved in c and a) to b  
		b(:,:,k-DEPL,2) = b(:,:,k-DEPL,2) -  c(:,:,k-DEPL,3)

		! LU factorization
		call DGETRF( steps, steps, b(:,:,k-DEPL,2), steps, pivot(:,k-DEPL,2), info )

		! pivoting of m 
		call PIVOTING( c(:,:,k-DEPL,2), piv, pivot(:,k-DEPL,2), steps )


		if (info .ne. 0) then
			!print *, "Error LU factorization in BW direction" 
			!call flush()
			!stop 
			ierr = 2
			!return
		end if

	END DO

	call MPI_SendRecv(b(:,:,half+1-DEPL,2),steps*steps,MPI_DOUBLE,my_rank-1,1, &
               &     b(:,:,half-DEPL,1)  ,steps*steps,MPI_DOUBLE,my_rank-1,1, &
               &     MPI_COMM_WORLD,status(:,1),ierr)

	call MPI_SendRecv(pivot(:,half+1-DEPL,2),steps,MPI_INT,my_rank-1,2, &
               &     pivot(:,half-DEPL,1)  ,steps,MPI_INT,my_rank-1,2, &
               &     MPI_COMM_WORLD,status(:,1),ierr)
	
	DO k = half+1, count

		j = k-1

		! We calculate M'_k= P_k M_k        M'_k * U_k-1 = A_k 
		call DTRSM( 'r', 'u', 'n', 'n', steps, steps, one, b(:,:,j-DEPL,1) , steps, a(:,:,j-DEPL,1) , steps)

		! We calculate N_k-1
		call pivoting ( c(:,:,j-DEPL,1),a(:,1,1,3), pivot(:,j-DEPL,1), steps )
		call DTRSM( 'l', 'l', 'n', 'u', steps, steps, one, b(:,:,j-DEPL,1), steps, c(:,:,j-DEPL,1), steps)

		! We calculate M_k * N_k-1  
    CALL PLASMA_Set(PLASMA_TILE_SIZE, nb_DGEMM,info);
    CALL PLASMA_Set(PLASMA_INNER_BLOCK_SIZE, ib_DGEMM,info);
       call PLASMA_DGEMM(PlasmaNoTrans,PlasmaNoTrans,steps,steps,steps,ONE,a(:,:,j-DEPL,1),steps,c(:,:,j-DEPL,1),steps, &
          		     &     ZERO,a(:,:,j-DEPL,3),steps,info)

!		call DGEMM('N','N',steps,steps,steps,ONE,a(:,:,j-DEPL,1),steps,c(:,:,j-DEPL,1),steps,ZERO,a(:,:,j-DEPL,3),steps)
		! (This result is saved for later use in the twisted factorizations)

		! Now we subtract the contribution of m*n to b 
		b(:,:,k-DEPL,1) = b(:,:,k-DEPL,1) -  a(:,:,j-DEPL,3)

		! LU factorization
		call DGETRF( steps, steps, b(:,:,k-DEPL,1), steps, pivot(:,k-DEPL,1), info )

		! pivot m 
		call PIVOTING (a(:,:,j-DEPL,1),piv, pivot(:,k-DEPL,1), steps )


		if (info .ne. 0) then
			!print *, "Error LU factorization in FW direction" , info
			!call flush()
			!stop 
			ierr = 1
			!return
		end if

	END DO


    end if

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!    T W I S T E D    F A C T O R I Z A T I O N !!!!!!
    !!!!!!!!                                               !!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    if(modulo(my_rank,2).eq.0)then

	DO k = 2, half   ! loop over all possible split elements  
	
		! We subtract the contributions of forward and reverse m*n (which were already calculated
		! for the forward and reverse factorization)    from b 
		b(:,:,k,3) = b(:,:,k,3) - a(:,:,k-1,3) - c(:,:,k,3)  

		! LU factorization
		call DGETRF( steps, steps, b(:,:,k,3), steps, pivot(:,k,3), info )

		if (info .ne. 0) then
			!print *, "Error LU Twisted Factorization" 
			!stop 
			ierr = 3
			!return
		end if

	END DO

	call MPI_ISend(a(:,:,half,2),steps*steps,MPI_DOUBLE,my_rank+1,3, MPI_COMM_WORLD,request,ierr)
	call MPI_IRecv(c(:,:,half,1),steps*steps,MPI_DOUBLE,my_rank+1,3, MPI_COMM_WORLD,request,ierr)
!	call MPI_SendRecv(a(:,:,half,2)   ,steps*steps    ,MPI_DOUBLE,my_rank+1,3, &
!               &     c(:,:,half,1),steps*steps,MPI_DOUBLE,my_rank+1,3, &
!               &     MPI_COMM_WORLD,status(:,1),ierr)

	b(:,:,1,3) = b(:,:,1,2)
	pivot(:,1,3) = pivot(:,1,2)

    else

	DO k = half+1, count-1   ! loop over all possible split elements  
	
		! We subtract the contributions of forward and reverse m*n (which were already calculated
		! for the forward and reverse factorization)    from b 
		b(:,:,k-DEPL,3) = b(:,:,k-DEPL,3) - a(:,:,k-1-DEPL,3) - c(:,:,k-DEPL,3)  

		! LU factorization
		call DGETRF( steps, steps, b(:,:,k-DEPL,3), steps, pivot(:,k-DEPL,3), info )

		if (info .ne. 0) then
			!print *, "Error LU Twisted Factorization" 
			!stop 
			ierr = 3
			!return
		end if

	END DO


	call MPI_ISend(c(:,:,half-DEPL,1),steps*steps,MPI_DOUBLE,my_rank-1,3, MPI_COMM_WORLD,request,ierr)
	call MPI_IRecv(a(:,:,half-DEPL,2),steps*steps,MPI_DOUBLE,my_rank-1,3, MPI_COMM_WORLD,request,ierr)

!	call MPI_SendRecv(c(:,:,half-DEPL,1),steps*steps,MPI_DOUBLE,my_rank-1,3, &
!               &     a(:,:,half-DEPL,2)   ,steps*steps    ,MPI_DOUBLE,my_rank-1,3, &
!               &     MPI_COMM_WORLD,status(:,1),ierr)

	b(:,:,count-DEPL,3) = b(:,:,count-DEPL,1)
	pivot(:,count-DEPL,3) = pivot(:,count-DEPL,1)

    end if

    
  END SUBROUTINE DSYBTTWF 



  ! For backward substitution in twisted factorizations
  SUBROUTINE DSYBTBS (a,b,c,pivot,ev,fac,blocks,ele,count,steps) 
    INTEGER,INTENT(IN)                   :: count         ! Number of submatrices in main diagonal 
    INTEGER,INTENT(IN)                   :: steps         ! Number of columns in current submatrix
    INTEGER                              :: cs            ! Count * steps     
    INTEGER                              :: i,j,k,f,g     ! For loops 
    INTEGER,INTENT(IN)                   :: fac           ! # of the factorization with the min. dia. ele.
    INTEGER,INTENT(IN)                   :: blocks        ! # of the block  with the min. dia. ele.
    INTEGER,INTENT(IN)                   :: ele           ! # of the element of submatrix "blocks" with the min. dia. ele.
    DOUBLE PRECISION , INTENT(IN):: a(:,:,:,:)    ! dim 1:2 = block ,  
    DOUBLE PRECISION , INTENT(IN):: b(:,:,:,:)    ! dim   3 = #block
    DOUBLE PRECISION , INTENT(IN):: c(:,:,:,:)    ! dim   4 = (1 = fw, 2 = bw, 3 = twisted factorization)
    INTEGER, INTENT(IN)          :: pivot(:,:,:)  ! dim   1 = pivoting vector, 2 = #block , 3 = (1 = fw, 2 = bw, 3 = twisted f
    DOUBLE PRECISION , INTENT(INOUT):: ev(:)         ! storage for the eigenvector
    DOUBLE PRECISION                     :: norm
    INTEGER                              :: info    
    DOUBLE PRECISION ONE,ZERO,VIEL
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

    integer my_rank,ierr,half
    integer status(MPI_STATUS_SIZE)
    INTEGER :: DEPL
!    PARAMETER (DEPL=198)

    call MPI_Comm_rank(MPI_COMM_WORLD, my_rank, ierr)

    half = FLOOR(count/2.0)
    DEPL = half - 1

    cs = count * steps 

    ! (C) Backward Subsitution

    if(modulo(my_rank,2).eq.0)then
	if(blocks.le.half)then
		! BW part
		DO i = blocks+1 , half
			! Contribution of n to the next block
			CALL DGEMV('n',steps,steps,-ONE,a(:,:,i-1,2),steps,ev((i-2)*steps+1:(i-1)*steps) &
				&     ,1,ONE,ev((i-1)*steps+1:(i)*steps),1)

			CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,i,2),steps,ev((i-1)*steps+1:i*steps),steps)

		END DO

        call MPI_Send(ev((half-1)*steps+1:half*steps), steps, MPI_DOUBLE, my_rank+1, 201, MPI_COMM_WORLD, ierr)

       	! FW part
		DO i = blocks-1, 1 , -1 
			! Contribution of n to the next block
			CALL DGEMV('n',steps,steps,-ONE,c(:,:,i,1),steps,ev((i)*steps+1:(i+1)*steps) &
				&     ,1,ONE,ev((i-1)*steps+1:(i)*steps),1)

			CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,i,1),steps,ev((i-1)*steps+1:i*steps),steps)

		END DO


	else
		call MPI_Recv(ev(half*steps+1:(half+1)*steps), steps, MPI_DOUBLE, my_rank+1, 201, MPI_COMM_WORLD,status, ierr)
       	! FW part
		DO i = half, 1 , -1 
			! Contribution of n to the next block
			CALL DGEMV('n',steps,steps,-ONE,c(:,:,i,1),steps,ev((i)*steps+1:(i+1)*steps) &
				&     ,1,ONE,ev((i-1)*steps+1:(i)*steps),1)

			CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,i,1),steps,ev((i-1)*steps+1:i*steps),steps)

		END DO
	end if
	call MPI_SendRecv(ev(1:half*steps),half*steps,MPI_DOUBLE,my_rank+1,202, &
               &         ev(half*steps+1:cs),cs-half*steps,MPI_DOUBLE,my_rank+1,202, &
               &         MPI_COMM_WORLD,status,ierr)
    else
	if(blocks.gt.half)then
       	! FW part
		DO i = blocks-1, half+1 , -1 
			! Contribution of n to the next block
			CALL DGEMV('n',steps,steps,-ONE,c(:,:,i-DEPL,1),steps,ev((i)*steps+1:(i+1)*steps) &
				&     ,1,ONE,ev((i-1)*steps+1:(i)*steps),1)

			CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,i-DEPL,1),steps,ev((i-1)*steps+1:i*steps),steps)

		END DO

		call MPI_Send(ev(half*steps+1:(half+1)*steps), steps, MPI_DOUBLE, my_rank-1, 201, MPI_COMM_WORLD, ierr)

		! BW part
		DO i = blocks+1 , count
			! Contribution of n to the next block
			CALL DGEMV('n',steps,steps,-ONE,a(:,:,i-1-DEPL,2),steps,ev((i-2)*steps+1:(i-1)*steps) &
				&     ,1,ONE,ev((i-1)*steps+1:(i)*steps),1)

			CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,i-DEPL,2),steps,ev((i-1)*steps+1:i*steps),steps)

		END DO
	else
		call MPI_Recv(ev((half-1)*steps+1:half*steps), steps, MPI_DOUBLE, my_rank-1, 201, MPI_COMM_WORLD,status, ierr)
		! BW part
		DO i = half+1 , count
			! Contribution of n to the next block
			CALL DGEMV('n',steps,steps,-ONE,a(:,:,i-1-DEPL,2),steps,ev((i-2)*steps+1:(i-1)*steps) &
				&     ,1,ONE,ev((i-1)*steps+1:(i)*steps),1)

			CALL DTRSM('l','u','n','n',steps,1,ONE,b(:,:,i-DEPL,2),steps,ev((i-1)*steps+1:i*steps),steps)

		END DO
	end if

	call MPI_SendRecv(ev(half*steps+1:cs),cs-half*steps,MPI_DOUBLE,my_rank-1,202, &
               &         ev(1:half*steps),half*steps,MPI_DOUBLE,my_rank-1,202, &
               &         MPI_COMM_WORLD,status,ierr)
    end if


  END SUBROUTINE DSYBTBS


  SUBROUTINE PIVOTING ( test,temp, ipiv, schritte )       ! for pivoting vectors from dgetrf
    IMPLICIT NONE
    INTEGER, INTENT(IN)                 :: ipiv(:)        ! pivoting vector (from dgetrf)
    DOUBLE PRECISION, INTENT(INOUT)     :: test(:,:)      ! matrix, whereto the pivoting should be applied
    DOUBLE PRECISION, INTENT(INOUT)     :: temp(:)        ! empty array
    INTEGER,    INTENT(IN)              :: schritte       ! length of the matrix 
    INTEGER                             :: i              ! variable for the loop
    DOUBLE PRECISION ONE,ZERO
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

    DO i = 1 , schritte
       temp =  test(i,:)
       test(i,:)       = test(ipiv(i),:)
       test(ipiv(i),:) = temp
    END DO

  END SUBROUTINE PIVOTING


  SUBROUTINE ANTIPIVOTING ( test,temp, ipiv, schritte )   ! for pivoting vectors from dgetrf
    IMPLICIT NONE
    INTEGER, INTENT(IN)                 :: ipiv(:)        ! pivoting vector (from dgetrf)
    DOUBLE PRECISION, INTENT(INOUT)     :: test(:,:)      ! matrix, whereto the pivoting should be applied
    DOUBLE PRECISION                    :: temp(:)        ! empty array
    INTEGER,    INTENT(IN)              :: schritte       ! length of the matrix 
    INTEGER                             :: i              ! variable for the loop
    DOUBLE PRECISION ONE,ZERO
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

    DO I = schritte, 1 , -1
       IF (ipiv(i) .NE. i) THEN 
          temp =  test(i,:)
          test(i,:)       = test(ipiv(i),:)
          test(ipiv(i),:) = temp
       END IF 
    END DO

  END SUBROUTINE ANTIPIVOTING


  SUBROUTINE ANTIPIVOTINGVEC ( test, ipiv, schritte )   ! for pivoting vectors from dgetrf
    IMPLICIT NONE
    INTEGER, INTENT(IN)                 :: ipiv(:)        ! pivoting vector (from dgetrf)
    DOUBLE PRECISION, INTENT(INOUT)     :: test(:)      ! matrix, whereto the pivoting should be applied
    DOUBLE PRECISION                    :: temp  
    INTEGER,    INTENT(IN)              :: schritte       ! length of the matrix 
    INTEGER                             :: i              ! variable for the loop
    DOUBLE PRECISION ONE,ZERO
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

    DO I = schritte, 1 , -1
       IF (ipiv(i) .NE. i) THEN 
          temp =  test(i)
          test(i)       = test(ipiv(i))
          test(ipiv(i)) = temp
       END IF 
    END DO

  END SUBROUTINE ANTIPIVOTINGVEC


  ! Converts the blocktridiagonal format to a full matrix
  SUBROUTINE UNITESUBS2TOTAL ( totalmatrix,a,b,c, count, steps )      ! creates one matrix from the submatrices
    IMPLICIT NONE
    DOUBLE PRECISION, INTENT(INOUT)     :: totalmatrix(:,:)! matrix, whereto all submatrices will be saved
    DOUBLE PRECISION, INTENT(IN)        :: a(:,:,:,:)       ! subdiagonal 
    DOUBLE PRECISION, INTENT(IN)        :: b(:,:,:,:)       ! diagonal
    DOUBLE PRECISION, INTENT(IN)        :: c(:,:,:,:)       ! superdiagonal
    INTEGER,    INTENT(IN)              :: count, steps   ! number of matrices , length of each matrix 
    INTEGER                             :: i            ! variables for the loops
    DOUBLE PRECISION ONE,ZERO
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

    DO i = 1 , count-1

       totalmatrix ( ((i-1)*steps)+1:i*steps,((i-1)*steps)+1:i*steps) = b(:,:,i,1)

       totalmatrix ( (i*steps)+1:(i+1)*steps,((i-1)*steps)+1:i*steps) = a(:,:,i,1)

       totalmatrix ( ((i-1)*steps)+1:i*steps,(i*steps)+1:(i+1)*steps) = c(:,:,i,1)

    END DO
    i = count

    totalmatrix ( ((i-1)*steps)+1:i*steps,((i-1)*steps)+1:i*steps) = b(:,:,i,1)

  END SUBROUTINE UNITESUBS2TOTAL
END MODULE
