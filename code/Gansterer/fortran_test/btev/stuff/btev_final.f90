
PROGRAM BTEV
    USE STUFF
    USE ACCURACY
    IMPLICIT NONE
    INTEGER                              :: count                ! Number of submatrices in main diagonal 
    INTEGER                              :: steps                ! Number of columns in current submatrix 
    INTEGER                              :: cs                   ! Count * steps     

    INTEGER                              :: i,j,k,g,h,l          ! For loops 
    INTEGER                              :: ierr                 ! Errorcode

    INTEGER                              :: fac                  ! # of the factorization with the min. dia. ele.

!      For time measurement  
    REAL                                 :: tew,tev,tew2,tev2,ttri,timer, timer2, timer3, timer4  
!      For eigenvalue calculation 
    DOUBLE PRECISION , ALLOCATABLE       ::  work(:),work2(:),aband(:,:),eigenvalues(:),r(:),u(:),myev(:)
    INTEGER, ALLOCATABLE                 ::  iwork(:),iwork2(:)

!      For testing
    DOUBLE PRECISION , ALLOCATABLE       :: totalmatrix(:,:),totalmatrix2(:,:) 
!       DOUBLE PRECISION , ALLOCATABLE       :: lapackev(:)

    DOUBLE PRECISION , ALLOCATABLE       :: a(:,:,:,:)           ! 1:2 = block, 3 = #block , 4 = (1 = fw, 2 = bw, 3 = twisted factorization) 
    DOUBLE PRECISION , ALLOCATABLE       :: b(:,:,:,:)           ! 1:2 = block, 3 = #block , 4 = (1 = fw, 2 = bw, 3 = twisted factorization) 
    DOUBLE PRECISION , ALLOCATABLE       :: c(:,:,:,:)           ! 1:2 = block, 3 = #block , 4 = (1 = fw, 2 = bw, 3 = twisted factorization) 

    DOUBLE PRECISION , ALLOCATABLE       :: ev(:),ev2(:)         ! storage for the eigenvector
    DOUBLE PRECISION , ALLOCATABLE       :: tmp_ev(:)            ! storage for the residuum
    INTEGER, ALLOCATABLE                 :: pivot(:,:,:)         ! 1 = pivoting vector, 2 = #block , 3 = (1 = fw, 2 = bw, 3 = twisted factorization)

    DOUBLE PRECISION , ALLOCATABLE       :: a_tmp(:,:,:,:)       ! 1:2 = block, 3 = #block , 4 = (1 = fw, 2 = bw, 3 = twisted factorization) 
    DOUBLE PRECISION , ALLOCATABLE       :: b_tmp(:,:,:,:)       ! 1:2 = block, 3 = #block , 4 = (1 = fw, 2 = bw, 3 = twisted factorization) 
    DOUBLE PRECISION , ALLOCATABLE       :: c_tmp(:,:,:,:)       ! 1:2 = block, 3 = #block , 4 = (1 = fw, 2 = bw, 3 = twisted factorization) 

    DOUBLE PRECISION , ALLOCATABLE       :: ev_tmp(:,:),ev_tmp2(:,:) ! storage for all eigenvectors
    DOUBLE PRECISION                     :: time(7)
    INTEGER, ALLOCATABLE                 :: pivot_tmp(:,:,:,:)   ! 1 = pivoting vector, 2 = #block , 3 = (1 = fw, 2 = bw, 3 = twisted factorization)
    INTEGER                              :: t_count, t_count_rate, t_count_max, t_tmp, t_count2



    DOUBLE PRECISION                     :: shift
    DOUBLE PRECISION                     :: lnorm,norm(3),res(4)
    INTEGER                              :: info , seleshift
    INTEGER                              :: arguments            ! for command line parameters 
    CHARACTER(LEN=16)                    :: withfile = ''        ! command line parameter
    DOUBLE PRECISION                     :: dlamch, eps
!      FOR I/O 
    CHARACTER(LEN=70)                    :: filename  = ""       ! For input of matrix
    INTEGER, ALLOCATABLE                 :: breadth(:)           ! Breadths of the submatrices
    INTEGER                              :: io_error, read_error, alloc_stat
    DOUBLE PRECISION                        ONE,ZERO,VIEL
    PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

    INTEGER                              :: nit                  ! Max. Number of Inverse Iterations
    INTEGER                              :: nt                   ! Number of Tests
    INTEGER                              :: nans                 ! Number of NaNs
    INTEGER                              :: nnorm(3)
    DOUBLE PRECISION                     :: shift_error
    INTEGER                              :: acc, nb, ib

    INTEGER, ALLOCATABLE                 :: new (:)

!      For symmetric blocked tridiagonal matrices. a b c are submatrices
!      (b c    )   (u)   (r)
!      (a b c  ) x (u) = (r)
!      (  a b c)   (u)   (r)
!      (    a b)   (u)   (r)

!      Example (IF the spliting element = 3)
!      (b c    )   (l      )   (u n    )
!      (a b c  ) = (m l    ) x (  u n  )
!      (  a b c)   (  m l m)   (    u  )
!      (    a b)   (      l)   (    n u)


    integer                                 my_rank, half 
    integer                                 p
    integer                                 request
    integer                                 status(MPI_STATUS_SIZE)
    integer                                 start
    integer                                 ende
    integer, ALLOCATABLE                 :: recvcounts(:), displs(:)
    DOUBLE PRECISION , ALLOCATABLE       :: piv(:)
    INTEGER                              :: DEPL
    integer                              :: nb_DGETRF,nb_DGEMM,nb_DTRSM,ib_DGETRF,ib_DGEMM,ib_DTRSM
    INTEGER*4                               HL( 2 ), HPIV( 2 )
    integer                                 np
!      PARAMETER (DEPL=198)

    call MPI_Init(ierr)


    call MPI_Comm_rank(MPI_COMM_WORLD, my_rank, ierr)
    call MPI_Comm_size(MPI_COMM_WORLD, p, ierr)

    IF (modulo(p,2).EQ.1) THEN
        write(*,*) 'Even number of processes necessary (comm_rank: ', my_rank, ', com_size: ', p, ')'
        call MPI_Finalize(ierr)
        STOP
    END IF

    eps = dlamch('E')   ! machine epsilon
    viel = dlamch('O')  ! maximal double

!      Get command line arguments
    arguments = command_argument_count() !ARGC()
    shift_error = 0;

    call GET_COMMAND_ARGUMENT(1, withfile)
    IF (withfile .NE. "-f") THEN
      STOP 'NO FILE'  
    END IF

    call GET_COMMAND_ARGUMENT(2, filename)

    open(unit=24,file=filename,status='old',action='read',iostat=io_error) 
        read(24,*,iostat=read_error) count
        read(24,*,iostat=read_error) steps

    half = FLOOR(count/2.0)
    DEPL = half - 1

!      Set length 
    IF (count < 2 ) THEN        
        PRINT *, "The number of blocks is not adequate" 
        STOP 
    END IF
    
    allocate(b(steps,steps,half+2,3), STAT=alloc_stat)
    IF (alloc_stat .NE. 0) THEN
        STOP "Error in memory allocation"
    END IF
    
    allocate(a(steps,steps,half+2,3), STAT=alloc_stat)
    IF (alloc_stat .NE. 0) THEN
        STOP "Error in memory allocation"
    END IF
    
    allocate(c(steps,steps,half+2,3), STAT=alloc_stat)
    IF (alloc_stat .NE. 0) THEN
        STOP "Error in memory allocation"
    END IF
    
    allocate(a_tmp(steps,steps,count-1,3),b_tmp(steps,steps,count,3),c_tmp(steps,steps,count-1,3), STAT=alloc_stat)
    IF (alloc_stat .NE. 0) THEN
        STOP "Error in memory allocation"
    END IF

    cs = count*steps

    allocate(totalmatrix(cs,cs),totalmatrix2(cs,cs))
    allocate(ev(cs),ev_tmp(cs,cs), STAT=alloc_stat)
    IF (alloc_stat .NE. 0) THEN
        STOP "Error in memory allocation"
    END IF
    
    IF (my_rank.EQ.0) THEN
!         read(24,*,iostat=read_error) totalmatrix2(:,:)
        read(24,*,iostat=read_error) a_tmp(:,:,:,1)
        read(24,*,iostat=read_error) b_tmp(:,:,:,1)
        read(24,*,iostat=read_error) c_tmp(:,:,:,1)
        read(24,*,iostat=read_error) totalmatrix(:,:)
        read(24,*,iostat=read_error) ev(:)
        allocate(pivot_tmp(steps,count,3,cs))
        DO i = 1, cs
            read(24,*,iostat=read_error) pivot_tmp(:,:,:,i) 
!            print *, pivot_tmp(:,:,:,i) 
        END DO
        call UNITESUBS2TOTAL ( totalmatrix2,a_tmp,b_tmp,c_tmp, count, steps )
    END IF
    close(24)

    call GET_COMMAND_ARGUMENT(3, withfile)
    read (withfile,*) nit 
    call GET_COMMAND_ARGUMENT(4, withfile)
    read (withfile,*) nt 
    call GET_COMMAND_ARGUMENT(5, withfile)
    read (withfile,*) acc 
    call GET_COMMAND_ARGUMENT(6, withfile)
    read (withfile,*) np 
    call GET_COMMAND_ARGUMENT(7, withfile)
    read (withfile,*) nb 
    call GET_COMMAND_ARGUMENT(8, withfile)
    read (withfile,*) ib 

    call PLASMA_INIT( np, INFO )
    call PLASMA_Disable(PLASMA_AUTOTUNING,info);

    IF (np.EQ.1) THEN
      nb_DGETRF = 16
      ib_DGETRF = 16
      nb_DGEMM  = 160
      ib_DGEMM  = 11
      nb_DTRSM  = 160
      ib_DTRSM  = 40
    ELSE IF (np.EQ.2) THEN
      nb_DGETRF = 16
      ib_DGETRF = 8
      nb_DGEMM  = 130
      ib_DGEMM  = 1
      nb_DTRSM  = 100
      ib_DTRSM  = 90
    ELSE IF (np.EQ.4) THEN
      nb_DGETRF = 16
      ib_DGETRF = 8
      nb_DGEMM  = 130
      ib_DGEMM  = 18
      nb_DTRSM  = 40
      ib_DTRSM  = 14
    END IF

    call PLASMA_Set(PLASMA_TILE_SIZE, nb_DGETRF,info);
    call PLASMA_Set(PLASMA_INNER_BLOCK_SIZE, ib_DGETRF,info);

    allocate(pivot(steps,count,3),piv(steps))!,pivot_tmp(steps,count,3))

!      IF (my_rank.EQ.0) THEN
!         a_tmp(:,:,:,1) = a(:,:,:,1)
!         b_tmp(:,:,:,1) = b(:,:,:,1)
!         c_tmp(:,:,:,1) = c(:,:,:,1)
!         pivot_tmp(:,:,1) = pivot(:,:,1)
!      END IF

    seleshift = 99 !bad for A3.200, scalar product: 0.95610276313759535

!      ev und ev_tmp kleiner machen, nur soviel platz benoetigt wie ein prozessor braucht
    allocate(ev2(cs),ev_tmp2(cs,cs))
    allocate(recvcounts(p),displs(p))

    DO i = 1, nt

        call MPI_Barrier(MPI_COMM_WORLD,ierr)

        call MPI_Bcast(a_tmp    ,steps*steps*(count-1),MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
        call MPI_Bcast(b_tmp    ,steps*steps*count    ,MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
        call MPI_Bcast(c_tmp    ,steps*steps*(count-1),MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)
        call MPI_Bcast(ev       ,steps*count          ,MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)

        IF (my_rank.EQ.0) THEN
            displs(1)=0
            displs(2)=0
!            print *, 'displs: ', '1', displs(1)
!            print *, 'displs: ', '2', displs(2)
            DO seleshift = 1, p-2, 2
                recvcounts(seleshift)=cs/p*2
!               print *, 'recvcounts: ', seleshift, recvcounts(seleshift)
                recvcounts(seleshift+1)=cs/p*2
!               print *, 'recvcounts: ', seleshift+1, recvcounts(seleshift+1)
                displs(seleshift + 2) = displs(seleshift) + recvcounts(seleshift)
!               print *, 'displs: ', seleshift + 2, displs(seleshift + 2)
                displs(seleshift + 3) = displs(seleshift) + recvcounts(seleshift)
!               print *, 'displs: ', seleshift + 3, displs(seleshift + 3)
            END DO
            recvcounts(p-1)=cs-(cs/p*2)*(p/2-1)
!            print *, 'recvcounts: ', p-1, recvcounts(p-1)
            recvcounts(p)=cs-(cs/p*2)*(p/2-1)
!            print *, 'recvcounts: ', p, recvcounts(p)
            call Flush()
        END IF

        IF (modulo(my_rank,2).EQ.1) THEN
            start = (cs/p)*(my_rank-1) + 1
            ende = start + cs/p*2 - 1
        ELSE 
            start = (cs/p)*my_rank + 1
            ende = start + cs/p*2 - 1
        END IF

        IF (my_rank.ge.p-2) THEN
            ende = cs
        END IF

        time(1) = MPI_WTIME()

        DO seleshift = 1, 1
            IF (my_rank.EQ.0) THEN
                DO j = 1, count
!                  print *, my_rank, b_tmp(:,:,seleshift,1)
                   call flush()
                END DO
            END IF

            IF (modulo(my_rank,2).EQ.0) THEN
                a(:,:,1:half+1,1) = a_tmp(:,:,1:half+1,1) 
                b(:,:,1:half,1) = b_tmp(:,:,1:half,1)
                c(:,:,1:half+1,1) = c_tmp(:,:,1:half+1,1)
            ELSE
                a(:,:,half-depl:count-1-depl,1) = a_tmp(:,:,half:count-1,1) 
                b(:,:,half+1-depl:count-depl,1) = b_tmp(:,:,half+1:count,1)
                c(:,:,half-depl:count-1-depl,1) = c_tmp(:,:,half:count-1,1)
            END IF
!            a(:,:,:,1) = a_tmp(:,:,:,1) 
!            b(:,:,:,1) = b_tmp(:,:,:,1)
!            c(:,:,:,1) = c_tmp(:,:,:,1)


            IF (seleshift.EQ.cs/2) THEN
                shift = ev(seleshift)+shift_error
            ELSE
                shift = ev(seleshift)
            END IF
!            ev = zero
            ev_tmp(:,seleshift)=zero

!            calculate eigenvector based on twisted factorization
            call DSYBTEV(a,b,c,pivot,piv,ev_tmp(:,seleshift),shift,count,steps,nit,ierr,seleshift)

        END DO

        call MPI_Barrier(MPI_COMM_WORLD,ierr)
        time(2) = MPI_WTIME()
        time(5) = time(2)-time(1)

!         Gatherv verwenden damit auch unterschiedliche anzahl an elemente geschickt werden koennen
!         Call MPI_Gatherv(ev(start:ende),ende-start+1,MPI_DOUBLE,ev2,recvcounts,displs,MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)

        Call MPI_Gatherv(ev_tmp(:,start:ende),cs*(ende-start+1),MPI_DOUBLE,ev_tmp2,cs*recvcounts, &
                       &     cs*displs,MPI_DOUBLE,0,MPI_COMM_WORLD,ierr)


!         print *, my_rank, 'TEST1'
!         call flush()
        call MPI_Barrier(MPI_COMM_WORLD,ierr)
!         print *, my_rank, 'TEST2'
!         call flush()

!         print *, my_rank, time(5)
!         call flush()

        IF (i.EQ.1) THEN
            time(6) = time(5)
        elseif(time(5).lt.time(6)) THEN
            time(6) = time(5)
        END IF

    END DO


    IF (my_rank.EQ.0) THEN

        IF (acc.EQ.1) THEN
            allocate(tmp_ev(cs))

            call ORTHOGONALITY(totalmatrix,ev_tmp2,cs,norm,nnorm,lnorm,nans)
            call RESIDUUM(totalmatrix2,ev_tmp2,ev,cs,totalmatrix,tmp_ev,res)

            deallocate(tmp_ev)
        END IF

        IF (shift_error .EQ. 0) THEN
            print *,cs,steps, p,time(6), nans,norm(1)/nnorm(1),nnorm(1),norm(2)/nnorm(2),nnorm(2),norm(3)/nnorm(3),nnorm(3), &
            &  lnorm, res(1), res(2), res(3), res(4)
        ELSE
            print *,cs,steps,shift_error, nans,norm(1)/nnorm(1),nnorm(1),norm(2)/nnorm(2),nnorm(2),norm(3)/nnorm(3),nnorm(3), &
            &  lnorm, res(1), res(2), res(3), res(4), &
            &  "Time:", time(1), time(2), time(3), time(4), time(5), time(6)
        END IF


    END IF

    deallocate(ev2,ev_tmp2)
    deallocate(recvcounts,displs)

    deallocate(a,b,c)
    deallocate(pivot,ev)
    deallocate(a_tmp,b_tmp,c_tmp)
    deallocate(ev_tmp) !pivot_tmp,
    deallocate(piv)
    IF (my_rank.EQ.0) THEN
        deallocate(totalmatrix,totalmatrix2)   
    END IF

    call PLASMA_FINALIZE( INFO )
    call MPI_Finalize(ierr)

   STOP
END PROGRAM BTEV

  