
program test
  use accuracy
  implicit none
!   include 'mpif.h'
  integer                              :: count         ! number of submatrices in main diagonal 
  integer                              :: steps         ! number of columns in current submatrix 
  integer                              :: cs            ! count * steps     

  double precision , allocatable       :: totalmatrix(:,:)
  double precision , allocatable       :: ev_tmp2(:,:)     ! storage for all eigenvectors

  double precision                     :: lnorm, norm(3)
  integer                :: nans         ! number of nans
  integer                :: nnorm(3)

  count = 10
  steps = 10

  cs = count*steps

  allocate(totalmatrix(cs,cs),ev_tmp2(cs,cs))
  
  call orthogonality(totalmatrix,ev_tmp2,cs,norm,nnorm,lnorm,nans)

end program test

