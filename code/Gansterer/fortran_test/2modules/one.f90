module one
    use two
    
    contains
    
    function onefunc(x) result(y)
        real(8) :: x,y
        y = twofunc(x)
        y = twofunc(y)
    end function

    function onefuncb(x) result(y)
        real(8) :: x,y
        y = twofunc(x)
    end function    
end module