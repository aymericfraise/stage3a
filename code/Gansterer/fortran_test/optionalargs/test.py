from opt import opt

import numpy as np
import scipy.sparse

from pathlib import Path
import pickle

def load_object_from_file(filepath):
    filepath = Path(filepath)
    with filepath.open(mode='rb') as f:
        return pickle.load(f)

def to_fortran_indexing(diag,subdiag):
    n_blocks = len(diag)
    block_size = diag[0].shape[0]
    D = np.empty((block_size,block_size,n_blocks), dtype=np.float)
    E = np.empty((block_size,block_size,n_blocks-1), dtype=np.float)
    for i,block in enumerate(diag):
        b = scipy.sparse.tril(block)
        D_ = D[:,:,i]
        D[:,:,i] = b.todense()
    for i,block in enumerate(subdiag):
        b = scipy.sparse.diags(block) # subdiagonal blocks are diagonal
        E[:,:,i] = b.todense()
    print('done')
    return D, E, block_size, n_blocks

print(opt.__doc__)

laplacian_path = '/home/fraise/Documents/stage3a/code/Gansterer/BDC/LMB_DSP2/L'
diag, subdiag = load_object_from_file(laplacian_path)

jobz = 'D' # compute eigenvalues and eigenvectors
jobacc = 'A' # choose tau1 and tau2 automatically according to tol
# d: diag blocks , e: subdiag blocks
d,e,block_size,nblks = to_fortran_indexing(diag,subdiag)
# d, e, block_size, nblks = load_laplacian_fortran(laplacian_path)
n = nblks*block_size # size of the symmetric block tridiag matrix
ksizes = np.array([block_size for _ in range(nblks)]) # sizes of square diag blocks
tol = 1e-7 # tolerance for the residuals of the eigpairs, 1e-7 ~= float32's epsilon
tau1 = 1  # determined automatically according to tol (if jobacc=='A')
tau2 = 1  # determined automatically according to tol (if jobacc=='A')
ldz = 10 # leading dimension of the eigvecs array of dim (ldz,n) (=number of eigvecs to compute ?)
# workspace sizes are set according to dsbtdc documentation
lwork = 2*n**2+6*n if nblks == 1 else 2*n**2+3*n
liwork = 5*n + 4*nblks - 4
work = np.empty((lwork),dtype=np.float)
iwork = np.empty((liwork),dtype=np.int)
modwrk = np.empty((n,n), dtype=np.float, order='c')
# imodwrk isn't documented, but it is iterated on from 2*1 to 2*(nblks-1) in dibtdc and integer values are put inside so i infered the size and type
imodwrk = np.empty((2*(nblks-1),2*(nblks-1)),dtype=np.int)
change = 0 # not documented so i guessed a value from its usage

args = [jobz,jobacc,n,nblks,ldz,change,tol,tau1,tau2,ksizes,iwork,imodwrk,d,e,work,modwrk]
opt(*args)
