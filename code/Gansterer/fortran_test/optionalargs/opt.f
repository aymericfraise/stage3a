      subroutine opt( jobz,
     $  jobacc,
     $  n,
     $  nblks,
     $  l1d,
     $  l2d,
     $  l1e,
     $  l2e,
     $  ldz,
     $  lwork,
     $  liwork,
     $  mingapi,
     $  change,
     $  tol,
     $  tau1,
     $  tau2,
     $  mingap,
     $  ksizes,
     $  iwork,
     $  imodwrk,
     $  d,
     $  e,
     $  ev,
     $  z,
     $  work,
     $  modwrk)

      character jobz
      character jobacc
      integer n
      integer nblks
      integer l1d
      integer l2d
      integer l1e
      integer l2e
      integer ldz
      integer lwork
      integer liwork
      integer info
      integer mingapi
      integer change
      integer change2
      double precision tol
      double precision tau1
      double precision tau2
      double precision mingap

      integer ksizes( * )
      integer iwork( liwork )
      integer imodwrk( * )
      double precision d( l1d, l2d, * )
      double precision e( l1e, l2e, * )
      double precision ev( n )
      double precision z( ldz, n )
      double precision work( lwork )
      double precision modwrk( * )

      
      double precision k
      k = d(1000,10000,100000)
      write(*,*) 'k', k

*f2py intent(in) jobz
*f2py intent(in) jobacc
*f2py intent(in) n
*f2py intent(in) nblks
*f2py intent(in) ksizes
*f2py intent(in) d
*f2py intent(in) l1d
*f2py intent(in) l2d
*f2py intent(in) e
*f2py intent(in) l1e
*f2py intent(in) l2e
*f2py intent(in) tol
*f2py intent(in) tau1
*f2py intent(in) tau2
*f2py intent(in) ldz
*f2py intent(in) work
*f2py intent(in) liwork
*f2py intent(in) lwork
*f2py intent(in) modwork
*f2py intent(out) ev
*f2py intent(out) z
*f2py intent(out) mingap
*f2py intent(out) mingapi
*f2py intent(out) info
      
      end