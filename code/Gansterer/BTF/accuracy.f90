MODULE ACCURACY
IMPLICIT NONE

!!
!! THIS PROGRAM CALCULATES THE ACCURACY OF THE CALCULATED EIGENVECTORS
!!

CONTAINS

! Calculates the eigenvector based on twisted factorizations
  SUBROUTINE ORTHOGONALITY(totalmatrix,ev,n,norm,nnorm,matrix_norm,nans)
  INTEGER, INTENT(INOUT)                                :: nnorm(3)        ! norm(1): best, norm(2): middle, norm(3): worst
  INTEGER, INTENT(INOUT)                        :: nans        ! Number of NaNs
  INTEGER                            :: n
  DOUBLE PRECISION , INTENT(INOUT)           :: totalmatrix(:,:)
  DOUBLE PRECISION , INTENT(INOUT)            :: ev(:,:)
  DOUBLE PRECISION, INTENT(INOUT)                :: norm(3)
  INTEGER                                          :: i,j,k, anz
  DOUBLE PRECISION                        :: tmp, matrix_norm
  DOUBLE PRECISION                         :: DNRM2, DASUM,IDAMAX
  DOUBLE PRECISION ONE,ZERO
  PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

  nnorm(:)=0
  nans=0
  norm(:)=0

  do i = 1, n
    tmp = 0
    do k = 1, n 
             tmp = tmp + ev(k,i)*totalmatrix(k,i)
      end do

    if(abs(tmp) .ne. abs(tmp))then
        nans = nans + 1
             ev(:,i) = 0
        !print *, "Error at:", seleshift
    else
        !norm_sum = norm_sum + abs(tmp)
        if(abs(tmp)>0.999)then
            norm(1) = norm(1) + abs(tmp)
            nnorm(1) = nnorm(1) + 1
        else     
            if(abs(tmp)<0.9)then
            norm(3) = norm(3) + abs(tmp)
            nnorm(3) = nnorm(3) + 1
            else
            norm(2) = norm(2) + abs(tmp)
            nnorm(2) = nnorm(2) + 1
            end if
        end if
        !print *, norm_sum
    end if
  end do

  call dgemm('T','N',n,n,n,ONE,ev,n,ev,n,ZERO,totalmatrix,n)

  do i = 1, n
    !print *, totalmatrix(i,i), totalmatrix(i,i)-ONE
    totalmatrix(i,i)=ZERO!totalmatrix(i,i)-ONE
  end do

    nans=1
  matrix_norm = IDAMAX(n,totalmatrix(:,1),1)

  do i = 2, n
    tmp = IDAMAX(n,totalmatrix(:,i),1)
    !print *,tmp
    if(tmp>matrix_norm)then
        matrix_norm = tmp
        nans = i
    end if
  end do

!print *, nans,norm(1)/nnorm(1),nnorm(1),norm(2)/nnorm(2),nnorm(2),norm(3)/nnorm(3),nnorm(3), &
!    &  lnorm(1)/lnnorm(1), lnnorm(1), lnorm(2)/lnnorm(2), lnnorm(2), lnorm(3)/lnnorm(3), lnnorm(3)

  END SUBROUTINE ORTHOGONALITY 

! Calculates the eigenvector based on twisted factorizations
  SUBROUTINE RESIDUUM(A,ev,ew,n,tmp_A,tmp_v,norm)
!(totalmatrix2,totalmatrix,ev,cs,ev_tmp,tmp_ev)
IMPLICIT NONE
  INTEGER                                       :: n
  DOUBLE PRECISION , INTENT(INOUT) :: A(:,:)
  DOUBLE PRECISION , INTENT(INOUT) :: ev(:,:)
  DOUBLE PRECISION , INTENT(INOUT) :: ew(:)
  DOUBLE PRECISION , INTENT(INOUT) :: tmp_A(:,:)
  DOUBLE PRECISION , INTENT(INOUT) :: tmp_v(:)
  DOUBLE PRECISION                              :: norm(4)
  INTEGER                                       :: i,j,k
  DOUBLE PRECISION                              :: tmp, tmp2
  DOUBLE PRECISION                              :: vektor_norm, matrix_norm
  DOUBLE PRECISION                              :: DASUM
  DOUBLE PRECISION                              :: min,max,average,deviation
  DOUBLE PRECISION , ALLOCATABLE                :: errors(:)
  DOUBLE PRECISION ONE,ZERO
  PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)

  allocate(errors(n))

  norm(:)=0

  tmp_A = A

  matrix_norm = DASUM(n,A(:,1),1)

  do i = 2, n
    tmp = DASUM(n,A(:,i),1)
    if(tmp>matrix_norm)then
        matrix_norm = tmp
    end if
  end do

  !print *, matrix_norm

  do j = 1, n
      do i = 1, n
        tmp_A(i,i)=A(i,i)-ew(j)    
      end do
    tmp2 = DASUM(n,ev(:,j),1)
    if(tmp2 .ne. 0)then
        tmp_v(:) = 0

          CALL DGEMV('N',n,n,ONE,tmp_A,n,ev(:,j),1,ZERO,tmp_v,1)

        tmp = DASUM(n,tmp_v,1)/matrix_norm/tmp2

        if(j.eq.1)then
            min = tmp
            max = tmp
            average = tmp
            errors(1) = tmp
        else
            if(tmp<min)then
                min = tmp
            endif
            if(tmp>max)then
                max = tmp
            endif
            average = average + tmp
            errors(j) = tmp
        endif
    else
        errors(j) = ZERO
    end if
  end do
  average = average/n

  deviation = ZERO
  do j = 1, n
    deviation = deviation + (errors(j)-average)*(errors(j)-average) 
  end do

  deviation = sqrt(deviation/(n-1))

  norm(1) = min
  norm(2) = max
  norm(3) = average
  norm(4) = deviation

  deallocate(errors)

  !print *, norm(1)/nnorm(1),nnorm(1),norm(2)/nnorm(2),nnorm(2),norm(3)/nnorm(3),nnorm(3)

  END SUBROUTINE RESIDUUM

END MODULE ACCURACY