for b in 16 # 8 32 64 # 32 64 # 32  # 64 # 16 32 64 # 10 50 # 5 20 25 40 80 100 # 10 # 8 16 # 10 # 5  20 30 40 50
do
#	echo $b >> debug
    for n in 1024 2048 3072 4096 5120 # # 6144 # 8192 10240 # 1000 1500 2000 2500 3000 5000 # 1024 2048 3072 4096 5120 # 8000 10000 #  1024 2048 3072 4096 5120 # 2048 # 4096 # 1024 # 2048 3072 4096 5120
    do
	for t in 1 2 3 4 5 6 7 # 1 # 1b
	do
		for s in 1 # 2 3 4 5 # 6 7 8 9 10 # 11 # 
		do
			#echo $t >> ../Output_dsbevd/tmp
			#$MATGEN/dynamicmatgen $n $b $t $s > $MATRIX/M$t-$n-$b-$s
			i=$[n+1]
			while [ $i -le $[n+1] ]
			do
				if [ $[5*$b] -le $n ]
				then
					if [ $[$n%$b] -eq 0 ]
					then
						echo $MATRIX/M$t-$n-$b-$s
						../runable/test_hrf_esp D $MATRIX/M$t-$n-$b-$s $[n-4*b] >> ../Output/hrf_esp/M$t-B$b-D-S$s
						../runable/test_hrf_esp N $MATRIX/M$t-$n-$b-$s $[n-4*b] >> ../Output/hrf_esp/M$t-B$b-S$s

						../runable/test_lmb_ssp D $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/lmb_ssp/M$t-B$b-D-S$s
						../runable/test_lmb_ssp N $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/lmb_ssp/M$t-B$b-S$s
						../runable/test_lmb_dsp D $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/lmb_dsp/M$t-B$b-D-S$s
						../runable/test_lmb_dsp N $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/lmb_dsp/M$t-B$b-S$s

						../runable/test_rbi_ssp D $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/rbi_ssp/M$t-B$b-D-S$s
						../runable/test_rbi_ssp N $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/rbi_ssp/M$t-B$b-S$s
						../runable/test_rbi_dsp D $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/rbi_dsp/M$t-B$b-D-S$s
						../runable/test_rbi_dsp N $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/rbi_dsp/M$t-B$b-S$s
						
						../runable/test_dsbevd2 V $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/dsbevd/M$[$t]-B$[$b]-V-S$s
						#../runable/test_dsyevd2 V $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/dsyevd/M$[$t]-B$[$b]-V 
						
						../runable/test_dsbevd2 N $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/dsbevd/M$[$t]-B$[$b]-S$s
						#../runable/test_dsyevd2 N $MATRIX/M$t-$n-$b-$s $[$i] >> ../Output/dsyevd/M$[$t]-B$[$b]
					fi
				fi
				i=$[$i+10000]
			done
		done
	done
    done
done
