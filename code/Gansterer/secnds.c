/*  secnds.c - fuer Linux:                                        */
/*  Aufruf:   t1=secnds(t0)                                       */
/*            liefert ( aktuelle Zeit - t0 )                      */
/*            mit t0=0.0  also aktuelle Zeit                      */
/*  secnds  - einfache Genauigkeit  (float, bzw. real)            */
/*  dsecnds - doppelte Genauigkeit  (double bzw. doubleprecision) */
/*  M. Pester, 01/94  */

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>

#ifdef DOUBLEPRECISION
#define SECNDS dsecnds_
#define REAL   double
#else
#define SECNDS secnds_
#define REAL   float
#endif

REAL SECNDS ( REAL *time0 )
{
  int tag = 24*60*60;
  struct timeval tv;
  struct timezone tz;
  gettimeofday(&tv,&tz);
  tv.tv_sec %=  tag ;
  return (REAL)((double)tv.tv_sec+(double)tv.tv_usec*1e-6 -(double)*time0);
}