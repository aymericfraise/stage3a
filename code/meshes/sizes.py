import os
from os.path import join
script_folder = os.path.dirname(os.path.abspath(__file__))
for root, _ , files in os.walk(script_folder):
    if not files:
        continue
    filename = files[0]
    if not filename.endswith('.off'):
        continue
    with open(join(root,filename)) as f:
        if f.readline().rstrip() != 'OFF':
            continue
        vertex_count,_,_ = f.readline().rstrip().split()
    frame_count = len(files)
    print(root, vertex_count, frame_count)

